Require Import Nat.
Require Import PeanoNat.
Require Import List.

Import ListNotations.

Require Import Sorting.Permutation.
Require Import Arith.Factorial.
Require Import Lists.ListSet.

Require Import subsequence.
Require Import exists_set.



Set Implicit Arguments.

Section Count_dec.

Variable X: Type.
Hypothesis eq_dec : forall x y: X, {x=y} + {x <> y}.


(* Precondition:
     - no duplicates in 'a'
     - ('x' should be a member of 'a' but result is correct anyway)
*)
Fixpoint Count (a: list X) (x: X) (l: list X) : nat :=
  match l with
  | nil    => 0
  | hd::tl => if eq_dec x hd then
      let r := remove eq_dec x a in
           (match r with
            | nil => 1
            | _ => list_sum (map (fun y => Count r y tl) r)
            end)
    else Count a x tl
  end.


Fixpoint Collect (a: list X) (x: X) (l: list X) : list (list X) :=
  match l with
  | nil    => nil
  | hd::tl => if eq_dec x hd then
      let r := remove eq_dec x a in
           (match r with
            | nil => [[x]]
            | _ => map (cons x) (flat_map (fun y => Collect r y tl) r)
            end)
    else Collect a x tl
  end.







(*
  main section

 *)


Lemma Collect_defective :
  forall (a: list X) (x: X) (l: list X), Collect a x l = Collect (x::a) x l.
Proof.
  intros. induction l. reflexivity. simpl.
  destruct (eq_dec x a0). destruct (eq_dec x x). reflexivity.
  contradiction. assumption.
Qed.


Lemma Count_defective :
  forall (a: list X) (x: X) (l: list X), Count a x l = Count (x::a) x l.
Proof.
  intros. induction l. reflexivity. simpl.
  destruct (eq_dec x a0). destruct (eq_dec x x). reflexivity.
  contradiction. assumption.
Qed.


Lemma Collect_defective_2 :
  forall (a: list X) (x: X) (l: list X),
    Collect a x l = Collect (remove eq_dec x a) x l.
Proof.
  intros a x l. induction l; intros. reflexivity.
  simpl. rewrite remove_remove_eq. rewrite <- IHl. reflexivity.
Qed.


Lemma Count_defective_2 :
  forall (a: list X) (x: X) (l: list X),
    Count a x l = Count (remove eq_dec x a) x l.
Proof.
  intros a x l. induction l; intros. reflexivity.
  simpl. rewrite remove_remove_eq. rewrite <- IHl. reflexivity.
Qed.


Theorem Count_base_case :
  forall (x: X) (l: list X), In x l <-> Count [x] x l = 1.
Proof.
  intros. split; intro; induction l.
  apply in_nil in H. contradiction.
  simpl. destruct (eq_dec x x). destruct (eq_dec x a). reflexivity.
  apply IHl. apply in_inv in H. destruct H. rewrite H in n. contradiction.
  assumption. contradiction.
  apply O_S in H. contradiction.
  simpl in H. destruct (eq_dec x x) in H. destruct (eq_dec x a) in H.
  rewrite e0. apply in_eq. apply in_cons. apply IHl. assumption.
  contradiction.
Qed.


Theorem Count_alphabet_perm :
  forall (a1 a2: list X) (x: X) (l: list X),
    Permutation a1 a2 -> Count a1 x l = Count a2 x l.
Proof.
  intros a1 a2 x l. generalize a1 a2 x. induction l; intros. reflexivity.
  simpl. destruct (eq_dec x0 a).
  assert (Permutation (remove eq_dec x0 a0) (remove eq_dec x0 a3)).

  (* tiny lemma *)
  assert (L: forall l1 l2 (z: X), Permutation l1 l2
    -> Permutation (remove eq_dec z l1) (remove eq_dec z l2)).
  intro l1. induction l1; intros. apply Permutation_nil in H0.
  rewrite H0. apply perm_nil.
  assert (H1 := H0). apply Permutation_sym in H1.
  apply Permutation_vs_cons_inv in H1. destruct H1. destruct H1.
  rewrite H1. rewrite remove_app.
  assert ({z = a4} + {z <> a4}). apply eq_dec. destruct H2. rewrite e0.
  repeat rewrite remove_cons.
  rewrite <- remove_app. apply IHl1. rewrite H1 in H0.
  apply Permutation_cons_app_inv with (a := a4). assumption.
  simpl. destruct (eq_dec z a4). rewrite e0 in n. contradiction.
  apply Permutation_cons_app. rewrite <- remove_app. apply IHl1.
  apply Permutation_cons_app_inv with (a := a4). rewrite <- H1. assumption.

  apply L. assumption.
  destruct (remove eq_dec x0 a0). apply Permutation_nil in H0. rewrite H0.
  reflexivity. destruct (remove eq_dec x0 a3). apply Permutation_sym in H0.
  apply Permutation_nil in H0. inversion H0.

  assert (forall z1 z2, Permutation z1 z2 -> list_sum z1 = list_sum z2).
  intro z1. induction z1; intros. apply Permutation_nil in H1. rewrite H1.
  reflexivity.
  assert (H2 := H1). apply Permutation_sym in H2.
  apply Permutation_vs_cons_inv in H2. destruct H2. destruct H2.
  rewrite H2. rewrite list_sum_app.
  replace (list_sum x3 + list_sum (a4::x4)) with (list_sum (a4::(x3++x4))).
  simpl. apply Nat.add_cancel_l. apply IHz1.

  rewrite H2 in H1. apply Permutation_cons_app_inv with (a := a4).
  assumption. simpl. rewrite Nat.add_shuffle3.
  rewrite Nat.add_cancel_l. rewrite list_sum_app. reflexivity.


  assert (forall z, map (fun y => Count (x1::l0) y l) z
    = map (fun y => Count (x2::l1) y l) z).
  intro z. induction z. reflexivity. simpl.
  rewrite IHl with (a2 := x2::l1). rewrite IHz. reflexivity. assumption.
  rewrite H2.

  apply Permutation_list_sum. apply Permutation_map. assumption.

  apply IHl. assumption.
Qed.


Theorem Collect_alphabet_perm :
  forall (a1 a2: list X) (x: X) (l: list X),
    Permutation a1 a2 -> Permutation (Collect a1 x l) (Collect a2 x l).
Proof.
  intros a1 a2 x l. generalize a1 a2 x. induction l; intros. reflexivity.
  simpl. destruct (eq_dec x0 a).
  assert (Permutation (remove eq_dec x0 a0) (remove eq_dec x0 a3)).

  (* tiny lemma *)
  assert (L: forall l1 l2 (z: X), Permutation l1 l2
    -> Permutation (remove eq_dec z l1) (remove eq_dec z l2)).
  intro l1. induction l1; intros. apply Permutation_nil in H0.
  rewrite H0. apply perm_nil.
  assert (H1 := H0). apply Permutation_sym in H1.
  apply Permutation_vs_cons_inv in H1. destruct H1. destruct H1.
  rewrite H1. rewrite remove_app.
  assert ({z = a4} + {z <> a4}). apply eq_dec. destruct H2. rewrite e0.
  repeat rewrite remove_cons.
  rewrite <- remove_app. apply IHl1. rewrite H1 in H0.
  apply Permutation_cons_app_inv with (a := a4). assumption.
  simpl. destruct (eq_dec z a4). rewrite e0 in n. contradiction.
  apply Permutation_cons_app. rewrite <- remove_app. apply IHl1.
  apply Permutation_cons_app_inv with (a := a4). rewrite <- H1. assumption.

  apply L. assumption.
  destruct (remove eq_dec x0 a0). apply Permutation_nil in H0. rewrite H0.
  reflexivity. destruct (remove eq_dec x0 a3). apply Permutation_sym in H0.
  apply Permutation_nil in H0. inversion H0.

  apply Permutation_map.

  assert (forall (l1 l2: list X) (f g: X -> list (list X)),
    Permutation l1 l2 -> (forall e, In e l1 -> Permutation (f e) (g e))
      -> Permutation (flat_map f l1) (flat_map g l2)).
  intro l2. induction l2; intros. apply Permutation_nil in H1. rewrite H1.
  apply Permutation_refl.

  apply Permutation_sym in H1. assert (J := H1).
  apply Permutation_vs_cons_inv in H1. destruct H1. destruct H1.
  rewrite H1. rewrite flat_map_app.

  simpl. rewrite H2.
  apply Permutation_trans with (l' := (g a4 ++ flat_map g x3 ++ flat_map g x4)).
  apply Permutation_app_head. rewrite <- flat_map_app. apply IHl2.
  apply Permutation_cons_app_inv with (a := a4). apply Permutation_sym.
  rewrite <- H1. assumption.

  intro. intro. apply H2. apply in_cons. assumption.
  apply Permutation_sym. apply Permutation_app_swap_app. apply in_eq.

  apply H1. assumption. intro. intro. apply IHl. assumption.
  apply IHl. assumption.
Qed.


Definition Count_no_change (x: X) (l : list X) :=
  forall a, Count a x (x::l) = Count a x l.


Definition Collect_no_change (x: X) (l : list X) :=
  forall a, Collect a x (x::l) = Collect a x l.


Theorem Count_no_change_generalize :
  forall (a: list X) (x y: X) (z: list X),
    Count a x (x::z) = Count a x z -> Count a y (x::z) = Count a y z.
Proof.
  intros. assert ({x=y}+{~ x = y}). apply eq_dec.
  destruct H0. rewrite <- e. assumption. simpl.
  destruct (eq_dec y x). rewrite e in n. contradiction. reflexivity.
Qed.


Theorem Collect_no_change_generalize :
  forall (a: list X) (x y: X) (z: list X),
    Collect a x (x::z) = Collect a x z -> Collect a y (x::z) = Collect a y z.
Proof.
  intros. assert ({x=y}+{~ x = y}). apply eq_dec.
  destruct H0. rewrite <- e. assumption. simpl.
  destruct (eq_dec y x). rewrite e in n. contradiction. reflexivity.
Qed.


Theorem Count_no_change_extend :
  forall (v: list X) x l,
    Count_no_change x l
      -> forall y a, Count a y (v ++ x::l) = Count a y (v++l).
Proof.
  intro v. induction v; intros. apply Count_no_change_generalize. apply H.
  repeat rewrite <- app_comm_cons. simpl. destruct (eq_dec y a).
  destruct (remove eq_dec y a0). reflexivity.
  assert (forall z, map (fun y => Count (x0::l0) y (v++x::l)) z
    = map (fun y => Count (x0::l0) y (v++l)) z).
  intro z. induction z. reflexivity. simpl. rewrite IHz. f_equal.
  apply IHv. intro a2. apply H. rewrite H0. reflexivity.
  apply IHv. apply H.
Qed.


Theorem Collect_no_change_extend :
  forall (v: list X) x l,
    Collect_no_change x l
      -> forall y a, Collect a y (v ++ x::l) = Collect a y (v++l).
Proof.
  intro v. induction v; intros. apply Collect_no_change_generalize. apply H.
  repeat rewrite <- app_comm_cons. simpl. destruct (eq_dec y a).
  destruct (remove eq_dec y a0). reflexivity.
  assert (forall z, flat_map (fun y => Collect (x0::l0) y (v++x::l)) z
    = flat_map (fun y => Collect (x0::l0) y (v++l)) z).
  intro z. induction z. reflexivity. simpl. rewrite IHz. f_equal.
  apply IHv. intro a2. apply H. rewrite H0. reflexivity.
  apply IHv. apply H.
Qed.


Theorem Count_no_change_2 :
  forall (a: list X) (x: X) (u v: list X),
  ~ In x u -> Count a x (u++v) = Count a x v.
Proof.
  intros a x u. induction u; intros. reflexivity.
  rewrite <- app_comm_cons. simpl. apply not_in_cons in H. destruct H.
  destruct (eq_dec x a0). apply H in e. contradiction.
  apply IHu. assumption.
Qed.


Theorem Count_Collect_length :
  forall (a: list X) (x: X) (l: list X),
    Count a x l = length (Collect a x l).
Proof. 
  intros a x l. generalize a x. induction l; intros.
  reflexivity.

  simpl. destruct (eq_dec x0 a0).
  destruct (remove eq_dec x0 a1). reflexivity. rewrite length_map.
  rewrite length_flat_map.

  assert (forall z, map (fun y : X => Count (x1 :: z) y l) (x1 :: z)
       = map (fun x2 : X => length (Collect (x1 :: z) x2 l)) (x1 :: z)).
  intros. apply map_ext_in.

  intros. apply in_split in H. destruct H. destruct H. rewrite H.
  assert (Permutation (a2::x2++x3) (x2++a2::x3)). apply Permutation_middle.
  replace (Count (x2++a2::x3) a2 l) with (Count (a2::x2++x3) a2 l).
  replace (length (Collect (x2++a2::x3) a2 l))
    with (length (Collect (a2::x2++x3) a2 l)).
  apply IHl. 
  apply Permutation_length. apply Collect_alphabet_perm. assumption.
  apply Count_alphabet_perm. assumption.
  rewrite H. reflexivity. apply IHl.
Qed.


Theorem Collect_head :
  forall (a: list X) (x y: X) (l v: list X), In (y::v) (Collect a x l) -> x = y.
Proof.
  intros a x y l. generalize a x y. induction l; intros. apply in_nil in H.
  contradiction. simpl in H. destruct (eq_dec x0 a0).
  destruct (remove eq_dec x0 a1). apply in_inv in H. destruct H. inversion H.
  reflexivity. apply in_nil in H. contradiction.
  apply in_map_iff in H. destruct H. destruct H. inversion H. reflexivity.
  apply IHl with (a := a1) (x := x0) (v := v). assumption.
Qed.


Theorem Collect_nil :
  forall (a: list X) (x: X) (l: list X), ~ In nil (Collect a x l).
Proof.
  intros a x l. induction l; intros. apply in_nil.
  simpl. destruct (eq_dec x a0). destruct (remove eq_dec x a).
  intro. apply in_inv in H. destruct H. inversion H. apply in_nil in H.
  assumption. intro. apply in_map_iff in H. destruct H. destruct H.
  inversion H. apply IHl.
Qed.


Theorem Collect_different_head :
  forall (a1 a2: list X) (x y : X) (v l1 l2 : list X),
    x <> y -> In v (Collect a1 x l1) -> ~ In v (Collect a2 y l2).
Proof.
  intros. destruct v. apply Collect_nil.
  intro. apply Collect_head in H0. apply Collect_head in H1.
  rewrite H0 in H. rewrite H1 in H. contradiction.
Qed.


Theorem Collect_cons_not_eq :
  forall (a: list X) (x y: X) (l: list X),
    x <> y -> Collect a x (y::l) = Collect a x l.
Proof.
  intros. simpl. destruct (eq_dec x y). apply H in e. contradiction.
  reflexivity.
Qed.


Theorem Count_cons_not_eq :
  forall (a: list X) (x y: X) (l: list X),
    x <> y -> Count a x (y::l) = Count a x l.
Proof.
  intros. apply Collect_cons_not_eq with (a := a) (l := l) in H.
  repeat rewrite Count_Collect_length. rewrite H. reflexivity.
Qed.


Theorem Collect_head_not_eq :
  forall (a: list X) (x: X) (l v: list X),
    ~ In x v -> Collect a x (v ++ l) = Collect a x l.
Proof.
  intros a x l v. generalize a x l. induction v; intros. reflexivity.
  rewrite <- app_comm_cons. simpl. apply not_in_cons in H. destruct H.
  destruct (eq_dec x0 a0). rewrite e in H. contradiction.
  apply IHv. assumption.
Qed.


Theorem Count_head_not_eq :
  forall (a: list X) (x: X) (l v: list X),
    ~ In x v -> Count a x (v ++ l) = Count a x l.
Proof.
  intros. apply Collect_head_not_eq with (a := a) (l := l) in H.
  repeat rewrite Count_Collect_length. rewrite H. reflexivity.
Qed.


Theorem Collect_permutation :
  forall (a: list X) (x: X) (l v: list X),
    NoDup a -> In x a -> In v (Collect a x l) -> Permutation v a.
Proof.
  intros a x l. generalize a x. induction l; intros.
  apply in_nil in H1. contradiction. assert (L := H0).
  apply in_split in H0. destruct H0. destruct H0. assert (K := H).
  rewrite H0 in H.
  apply NoDup_remove in H. destruct H.
  apply notin_remove with (eq_dec := eq_dec) in H2.
  rewrite remove_app in H2.
  replace (remove eq_dec x0 x2) with (remove eq_dec x0 (x0::x2)) in H2.
  rewrite <- remove_app in H2. rewrite <- H0 in H2. simpl in H1.
  destruct (eq_dec x0 a0). destruct (remove eq_dec x0 a1).
  symmetry in H2. apply app_eq_nil in H2. destruct H2.
  rewrite H2 in H0. rewrite H3 in H0. rewrite H0. apply in_inv in H1.
  destruct H1. rewrite <- H1. apply Permutation_refl. apply in_nil in H1.
  contradiction. apply in_map_iff in H1. destruct H1. destruct H1.
  apply in_flat_map in H3. destruct H3. destruct H3.
  assert (J := H4). apply IHl in H4.
  
  rewrite <- H1. rewrite H0. apply Permutation_cons_app.
  apply IHl with (x := x5). assumption. rewrite <- H2. assumption.
  rewrite <- H2. assumption. rewrite H2. assumption. assumption.
  apply IHl with (x := x0). assumption. assumption. assumption.
  simpl. destruct (eq_dec x0 x0). reflexivity. contradiction.
Qed.


Theorem Collect_subsequence :
  forall (a: list X) (x: X) (l v: list X),
    In v (Collect a x l) -> Subsequence l v.
Proof.
  intros a x l. generalize a x. induction l; intros.
  apply in_nil in H. contradiction.

  destruct v. apply Subsequence_nil_r.
  assert (J := H). apply Collect_head in H. simpl in J.
  destruct (eq_dec x0 a0).

  destruct (remove eq_dec x0 a1). rewrite <- e. rewrite <- H.
  apply in_inv in J. destruct J. inversion H0.
  apply Subsequence_cons_eq. apply Subsequence_nil_r.
  apply in_nil in H0. contradiction.

  apply in_map_iff in J. destruct J. destruct H0. rewrite <- H0. rewrite e.
  apply Subsequence_cons_eq.

  destruct v. inversion H0. apply Subsequence_nil_r.
  apply IHl with (a := x2::l0) (x := x4).
  apply in_flat_map in H1. destruct H1. destruct H1.

  inversion H0. rewrite H5 in H2. assert (J := H2). apply Collect_head in H2.
  rewrite H2 in J. assumption.

  apply Subsequence_cons_l. apply IHl with (a := a1) (x := x0).
  assumption.
Qed.


Theorem Collect_prefix :
  forall (a: list X) (x: X) (l u: list X),
    ~ In x u -> Collect a x l = Collect a x (u++l).
Proof.
  intros a x l u. induction u; intro.
  reflexivity.
  rewrite <- app_comm_cons. simpl. destruct (eq_dec x a0).
  rewrite e in H. assert False. apply H. apply in_eq. contradiction.
  apply IHu. intro. apply H. apply in_cons. assumption.
Qed.


Theorem Count_prefix :
  forall (a: list X) (x: X) (l u: list X),
    ~ In x u -> Count a x l = Count a x (u++l).
Proof.
  intros. apply Collect_prefix with (a := a) (l := l) in H.
  repeat rewrite Count_Collect_length. f_equal. assumption.
Qed.


Theorem Collect_subsequence_2 :
  forall (a: list X) (x: X) (l u: list X),
    NoDup a -> Permutation a (x::u) -> Subsequence l (x::u)
      -> In (x::u) (Collect a x l).
Proof.
  intros a x l. generalize a x. induction l; intros. 
  apply Subsequence_nil_cons_r in H1. contradiction.

  assert (K := H).
  assert (J := H0). assert (J2 := J).
  assert (L: NoDup (x0::u)). apply Permutation_NoDup with (l := a1).
  assumption. assumption.
  apply Permutation_vs_cons_inv in H0. destruct H0. destruct H0.
  rewrite H0 in J. apply Permutation_sym in J.
  apply Permutation_cons_app_inv in J.

  rewrite H0 in H. apply NoDup_remove_2 in H.
  apply notin_remove with (eq_dec := eq_dec) in H.

  rewrite remove_app in H.
  replace (remove eq_dec x0 x2) with (remove eq_dec x0 (x0::x2)) in H
         by apply remove_cons.
  rewrite <- remove_app in H. rewrite <- H0 in H.

  simpl. destruct (eq_dec x0 a0). rewrite e in H1.
  apply Subsequence_cons_eq in H1. rewrite H.
  destruct (x1++x2).
  apply Permutation_sym in J. apply Permutation_nil in J. rewrite J.
  apply in_eq.

  apply in_map_iff. exists u. split. reflexivity.
  destruct u. apply Permutation_nil in J. inversion J.
  apply IHl with (a := x3::l0) in H1.

  apply in_flat_map. exists x4. split.
  apply Permutation_in with (l := x4::u). assumption. apply in_eq.
  assumption.

  apply NoDup_cons_iff in L. destruct L.
  apply Permutation_NoDup with (l' := x3::l0) in H3. assumption. assumption.
  apply Permutation_sym. assumption.

  apply Subsequence_cons_diff in H1.
  apply IHl with (a := a1) in H1. assumption. assumption. assumption.
  intro. rewrite H2 in n. contradiction.
Qed.


(* TODO: la condition de permutation est trop forte ; il suffit
   de remplacer NoDUp a   par NoDup (x::u)
   de "incl a (x::u)" et de "incl (x::u) a"
  TODO: incl u a  suffit puisque x n'est pas obligatoire dans a
   (ce qui lève la condition NoDup implicite sur a
   On autorise ici :
     - doublons dans a
     - absence de x dans a
 *)
Theorem Collect_subsequence_3 :
  forall (a: list X) (x: X) (l u: list X),
    NoDup (x::u) -> incl a (x::u) -> incl u a
      -> Subsequence l (x::u) -> In (x::u) (Collect a x l).
Proof.
  intros a x l. generalize a x. induction l; intros. 
  apply Subsequence_nil_cons_r in H2. contradiction.

  assert (I: incl u (remove eq_dec x0 a1)). repeat intro.
  apply NoDup_cons_iff in H. destruct H.
  assert (a2 <> x0). intro. rewrite H5 in H3. apply H in H3. assumption.
  apply H1 in H3. apply in_in_remove. assumption. assumption.

  assert (J: incl (remove eq_dec x0 a1) u). repeat intro.
  assert (remove eq_dec x0 (x0::u) = u). simpl.
  destruct (eq_dec x0 x0). apply notin_remove.
  apply NoDup_cons_iff in H. destruct H. assumption. contradiction.
  apply in_remove in H3. destruct H3. apply H0 in H3.
  destruct H3. rewrite H3 in H5. contradiction. assumption.

  simpl. destruct (eq_dec x0 a0). rewrite e in H2.
  apply Subsequence_cons_eq in H2.

  destruct (remove eq_dec x0 a1). apply incl_l_nil in I. rewrite I.
  apply in_eq.

  apply in_map_iff. exists u. split. reflexivity. apply in_flat_map.
  destruct u. apply incl_l_nil in J. inversion J.

  exists x2. split. apply I. apply in_eq. apply IHl.
  apply NoDup_cons_iff in H. destruct H. assumption. assumption.
  apply incl_cons_inv in I. destruct I. assumption. assumption.

  apply IHl; try assumption.
  apply Subsequence_cons_diff in H2. assumption. intro.
  rewrite H3 in n. contradiction.
Qed.


Theorem Collect_empty :
  forall (l a: list X) (x: X), ~ incl a l -> Collect a x l = nil.
Proof.
  intro l. induction l; intros. reflexivity.
  simpl. destruct (eq_dec x a). rewrite <- e in H.

  assert ({remove eq_dec x a0 = nil}+{~ remove eq_dec x a0 = nil}).
  apply (list_eq_dec eq_dec). destruct H0.

  assert False. apply H. repeat intro.
  destruct (eq_dec x a1). rewrite e1. apply in_eq.
  apply in_split in H0. destruct H0. destruct H0.
  rewrite H0 in e0. rewrite remove_app in e0.
  simpl in e0. destruct (eq_dec x a1). apply n in e1. contradiction.
  apply app_eq_nil in e0. destruct e0. inversion H2. contradiction.

  assert (~ incl (remove eq_dec x a0) l). intro. apply H. repeat intro.
  simpl. destruct (eq_dec x a1). left. assumption. right. apply H0.
  apply in_in_remove. intro. rewrite H2 in n0. contradiction. assumption.
  
  destruct (remove eq_dec x a0). contradiction.

  assert (forall v, flat_map (fun y : X => Collect (x0 :: l0) y l) v = nil).
  intro v. induction v. reflexivity. simpl. rewrite IHv. rewrite app_nil_r.
  apply IHl. assumption. rewrite H1. reflexivity.

  apply IHl. intro. apply incl_tl with (a := a) in H0.
  apply H in H0. assumption.
Qed.


Theorem Collect_empty_2 :
  forall (l a: list X) (x: X), ~ In x l -> Collect a x l = nil.
Proof.
  intro l; induction l; intros. reflexivity.
  apply not_in_cons in H. destruct H.
  simpl. destruct (eq_dec x a). rewrite e in H.
  contradiction. apply IHl. assumption.
Qed.


Theorem Count_zero :
  forall (l a: list X) (x: X), ~ incl a l -> Count a x l = 0.
Proof.
  intros. apply Collect_empty with (x := x) in H.
  rewrite Count_Collect_length. rewrite H. reflexivity.
Qed.


Theorem Count_zero_2 :
  forall (l a: list X) (x: X), ~ In x l -> Count a x l = 0.
Proof.
  intros. apply Collect_empty_2 with (a := a) in H.
  rewrite Count_Collect_length. rewrite H. reflexivity.
Qed.


Theorem Collect_nodup :
  forall (a: list X) (x: X) (l: list X),
    NoDup a -> NoDup (Collect a x l).
Proof.
  intros a x l. generalize a x. induction l; intros. apply NoDup_nil.
  assert (forall a x, NoDup a -> NoDup (remove eq_dec x a)).
  intro b. induction b; intros. apply NoDup_nil.
  apply NoDup_cons_iff in H0. destruct H0.
  simpl. destruct (eq_dec x1 a2). apply IHb.
  assumption. apply NoDup_cons. intro. apply in_remove in H2. destruct H2.
  apply H0 in H2. contradiction. apply IHb. assumption.

  simpl. destruct (eq_dec x0 a0).
  apply H0 with (x := x0) in H. destruct (remove eq_dec x0 a1).
  apply NoDup_cons. apply in_nil. apply NoDup_nil.
  assert (forall v, NoDup v -> NoDup (map (cons x0) v)).
  intro v. induction v; intro. apply NoDup_nil.
  simpl. apply NoDup_cons. intro. apply in_map_iff in H2.
  destruct H2. destruct H2. inversion H2. rewrite <- H5 in H1.
  apply NoDup_cons_iff in H1. destruct H1. apply H1 in H3. assumption.
  apply IHv. apply NoDup_cons_iff in H1. destruct H1. assumption. apply H1.

  assert (forall (v1 v2: list (list X)), NoDup v1 -> NoDup v2
    -> (forall e, In e v1 -> ~ In e v2) -> NoDup (v1++v2)).
  intro v1; induction v1; intros. apply H3. rewrite <- app_comm_cons.
  apply NoDup_cons. intro. apply in_app_or in H5. destruct H5.
  apply NoDup_cons_iff in H2. destruct H2. apply H2 in H5. assumption.
  apply H4 in H5. assumption. apply in_eq. apply IHv1.
  apply NoDup_cons_iff in H2. destruct H2. assumption. assumption.
  intros. apply H4. simpl. right. assumption.

  assert (forall u,
     NoDup u -> NoDup (flat_map (fun y : X => Collect (x1 :: l0) y l) u)).
  intro u; induction u; intro. apply NoDup_nil.
  simpl. apply H2. apply IHl. assumption. apply NoDup_cons_iff in H3.
  destruct H3. apply IHu in H4. assumption.
  intros. destruct e0. apply Collect_nil in H4. contradiction.
  apply Collect_head in H4. rewrite H4 in H3.
  apply NoDup_cons_iff in H3. destruct H3.
  intro. apply in_flat_map in H6. destruct H6. destruct H6.
  apply Collect_head in H7. rewrite H7 in H6. apply H3 in H6. assumption.
    
  apply H3. assumption. apply IHl. assumption.
Qed.


Theorem Collect_useless_element:
  forall (l : list X) (x y : X) (a : list X),
    ~ In x a -> x <> y
      -> Collect a y l = Collect a y (remove eq_dec x l).
Proof.
  intro l. induction l; intros. reflexivity.
  destruct (eq_dec x a). simpl. destruct (eq_dec y a).
  rewrite <- e0 in e. apply H0 in e. contradiction.
  destruct (eq_dec x a). apply IHl; assumption.
  apply n0 in e. contradiction.

  simpl. destruct (eq_dec y a).
  destruct (eq_dec x a). apply n in e0. contradiction. rewrite <- e.

  assert ({remove eq_dec y a0 = nil}+{~ remove eq_dec y a0 = nil}).
  apply (list_eq_dec eq_dec). destruct H1.
  simpl. rewrite e0. destruct (eq_dec y y). reflexivity. contradiction.

  simpl. destruct (eq_dec y y).
  assert ({In x (remove eq_dec y a0)} + {~ In x (remove eq_dec y a0)}).
  apply ListDec.In_dec. apply eq_dec. destruct H1. apply in_remove in i.
  destruct i. apply H in H1. contradiction.
  destruct (remove eq_dec y a0). contradiction.

  assert (forall u, ~ In x u ->
    flat_map (fun y => Collect (x0::l0) y l) u
      = flat_map (fun y => Collect (x0::l0) y (remove eq_dec x l)) u).
  intro u. induction u; intro. reflexivity. simpl. rewrite IHl with (x := x).
  rewrite IHu. reflexivity. intro. apply H1. apply in_cons. assumption.
  assumption. intro. apply H1. rewrite H2. apply in_eq.
  rewrite H1. reflexivity. assumption. contradiction.
  
  destruct (eq_dec x a). apply n in e. contradiction.
  simpl. destruct (eq_dec y a). apply n0 in e. contradiction.
  apply IHl; assumption.
Qed.


Theorem Collect_only_first :
  forall (l: list X) (x: X) (a: list X),
  Collect a x (x::l) = Collect a x (x::(remove eq_dec x l)).
Proof.
  assert (F :
    forall (f g : X -> list (list X)) l b,
      (forall a, In a l -> a <> b) -> (forall a, a <> b -> f a = g a)
      -> flat_map f l = flat_map g (remove eq_dec b l)).
  intros f g l. induction l; intros. reflexivity.
  simpl. destruct (eq_dec b a). assert (a <> b). apply H.
  apply in_eq. rewrite e in H1. contradiction. simpl. rewrite H0.
  rewrite IHl with (b := b). reflexivity.
  intros. apply H. apply in_cons. assumption. assumption. intro.
  rewrite H1 in n. contradiction.

  intros. simpl. destruct (eq_dec x x).
  assert (~ In x (remove eq_dec x a)). apply remove_In.
  destruct (remove eq_dec x a). reflexivity. f_equal.

  replace (x0::l0) with (remove eq_dec x (x0::l0)) at 2. apply F.
  intros. intro. rewrite H1 in H0. apply H in H0. assumption.
  intros.
  (* sans doute facile : on n'a x dans aucun des deux premiers
     arguments de Collect *)
  apply Collect_useless_element. assumption.
  intro. rewrite H1 in H0. contradiction.
  simpl. destruct (eq_dec x x0). rewrite e0 in H.
  apply notin_remove with (eq_dec := eq_dec) in H.
  simpl in H. destruct (eq_dec x0 x0). rewrite e0. rewrite H.
  reflexivity. contradiction.

  assert (~ In x l0). intro. apply H. apply in_cons. assumption.
  apply notin_remove with (eq_dec := eq_dec) in H0. rewrite H0.
  reflexivity.

  contradiction.
Qed.


Theorem Count_useless_element:
  forall (l : list X) (x y : X) (a : list X),
    ~ In x a -> x <> y
      -> Count a y l = Count a y (remove eq_dec x l).
Proof.
  intros. apply Collect_useless_element with (a := a) (l := l) in H0.
  repeat rewrite Count_Collect_length. rewrite H0. reflexivity.
  assumption.
Qed.


Theorem Count_only_first :
  forall (l: list X) (x: X) (a: list X),
  Count a x (x::l) = Count a x (x::(remove eq_dec x l)).
Proof.
  intros. repeat rewrite Count_Collect_length.
  rewrite Collect_only_first. reflexivity.
Qed.


Theorem Collect_cons_dup :
  forall (l: list X) (x: X),
    Collect_no_change x (x::l).
Proof.
  intros. intro. rewrite Collect_only_first. rewrite remove_cons.
  symmetry. apply Collect_only_first.
Qed.


Theorem Count_cons_dup :
  forall (l: list X) (x: X),
    Count_no_change x (x::l).
Proof.
  intros. intro. repeat rewrite Count_Collect_length. f_equal.
  apply Collect_cons_dup.
Qed.


Theorem Collect_nodup_2 :
  forall (a: list X) (x: X) (l v: list X),
    In v (Collect a x l) -> NoDup v.
Proof.
  intros a x l v. generalize a x l. induction v; intros. apply NoDup_nil.
  assert (I := H). apply Collect_head in H.
  rewrite <- H in I. rewrite <- H. assert (K := I). assert (J := I).
  apply Collect_subsequence in I. rewrite Subsequence_strong in I.
  destruct I. destruct H0. destruct H0. destruct H1. rewrite H0 in J.
  rewrite Collect_head_not_eq in J. apply NoDup_cons.

  intro. apply in_split in H3. destruct H3. destruct H3. rewrite H3 in J.

  rewrite Collect_only_first in J.
  apply Collect_subsequence in J. rewrite Subsequence_cons_eq in J.
  apply Subsequence_split_3 in J. destruct J. destruct H4. destruct H4.
  assert (In x0 (x5++x0::x6)). apply in_elt. rewrite <- H4 in H6.
  apply remove_In in H6. assumption.

  simpl in J. destruct (eq_dec x0 x0). destruct (remove eq_dec x0 a1).
  apply in_inv in J. destruct J. inversion H3. apply NoDup_nil.
  inversion H3. apply in_map_iff in J. destruct J. destruct H3.
  inversion H3. rewrite H6 in H4. apply in_flat_map in H4. destruct H4.
  destruct H4. apply IHv in H5. assumption. contradiction. assumption.
  apply eq_dec.
Qed.


Theorem Collect_single :
  forall (a: list X) (x : X) (l: list X), In x l
    -> remove eq_dec x a = nil <-> Collect a x l = [[x]].
Proof.
  intros a x l. induction l; intros; split; intro.
  apply in_nil in H. contradiction.
  apply in_nil in H. contradiction.
  simpl. destruct (eq_dec x a0). rewrite H0. reflexivity.
  apply IHl. destruct H. rewrite H in n. contradiction.
  assumption. assumption.

  apply Subsequence_In in H. rewrite Subsequence_strong in H.
  destruct H. destruct H. destruct H. destruct H1. rewrite H in H0.
  rewrite <- Collect_prefix in H0.
  simpl in H0. destruct (eq_dec x x). destruct (remove eq_dec x a).
  reflexivity.
  assert (In [x] (map (cons x)
        (flat_map (fun y => Collect (x2::l0) y x1) (x2::l0)))).
  rewrite H0. apply in_eq. apply in_map_iff in H3. destruct H3. destruct H3.
  inversion H3. rewrite H6 in H4. apply in_flat_map in H4.
  destruct H4. destruct H4. apply Collect_nil in H5. contradiction.
  contradiction. assumption. apply eq_dec.
Qed.


Theorem Collect_alphabet_incl :
  forall (a: list X) (x : X) (l v: list X),
    In v (Collect a x l) -> incl v (x::a).
Proof.
  intros a x l. generalize a x. induction l; intros.
  apply in_nil in H. contradiction.
  simpl in H. destruct (eq_dec x0 a0).
  assert ({remove eq_dec x0 a1 = nil}+{~ remove eq_dec x0 a1 = nil}).
  apply (list_eq_dec eq_dec). destruct H0. rewrite e0 in H.
  apply in_inv in H. destruct H. rewrite <- H. repeat intro.
  apply in_inv in H0. destruct H0. rewrite H0. apply in_eq.
  apply in_nil in H0. contradiction. apply in_nil in H. contradiction.

  assert (forall a' v' (x': X),
    incl v' (x'::(remove eq_dec x' a')) -> incl v' (x'::a')).
  intros. repeat intro. apply H0 in H1.
  destruct (eq_dec a2 x'). rewrite e0. apply in_eq. apply in_cons.
  destruct H1. rewrite H1 in n0. contradiction. apply in_remove in H1.
  destruct H1. assumption.
  apply H0.

  destruct (remove eq_dec x0 a1). repeat intro.
  inversion H. rewrite <- H2 in H1. assumption.
  apply in_nil in H2. contradiction.

  apply in_map_iff in H. destruct H. destruct H. rewrite <- H.
  apply in_flat_map in H1. destruct H1. destruct H1.
  apply incl_cons. apply in_eq. repeat intro.
  apply IHl in H2. apply H2 in H3.

  destruct H3. rewrite H3 in H1. destruct H1. rewrite H1.
  apply in_cons. apply in_eq.
  apply in_cons. apply in_cons. assumption.
  apply in_cons. assumption.

  apply IHl. assumption.
Qed.


Theorem Collect_alphabet_incl_2 :
  forall (a: list X) (x : X) (l v: list X),
    In v (Collect a x l) -> incl a v.
Proof.
  intros a x l. generalize a x. induction l; intros; repeat intro.
  apply in_nil in H. contradiction.
  simpl in H. destruct (eq_dec x0 a0).

  assert (In a2 (x0::(remove eq_dec x0 a1))).
  destruct (eq_dec a2 x0). rewrite e0. apply in_eq.
  apply in_cons. apply in_in_remove. assumption. assumption.
  destruct (remove eq_dec x0 a1). destruct H. rewrite <- H. assumption.
  apply in_nil in H. contradiction.
  apply in_map_iff in H. destruct H. destruct H. rewrite <- H.
  apply in_flat_map in H2. destruct H2. destruct H2.
  apply IHl in H3. destruct H1. rewrite H1. apply in_eq.
  apply H3 in H1. apply in_cons. assumption.
  apply IHl in H. apply H in H0. assumption.
Qed.


Theorem Collect_alphabet_incl_3 :
  forall (a: list X) (x : X) (l v: list X),
    In v (Collect a x l) -> incl (x::a) l.
Proof.
  intros. assert (I := H). assert (J := H).
  apply Collect_alphabet_incl_2 in H.
  apply Collect_subsequence in I. apply Subsequence_incl in I.
  apply incl_cons.
  assert ({In x l} + {~ In x l}). apply ListDec.In_dec. apply eq_dec.
  destruct H0. assumption.
  apply Collect_empty_2 with (a := a) in n. rewrite n in J.
  apply in_nil in J. contradiction.
  apply incl_tran with (m := v); assumption.
Qed. 


Theorem Collect_permutation_all :
  forall (a: list X) (x : X) (l u v: list X),
    In u (Collect a x l) -> In v (Collect a x l) -> Permutation u v.
Proof.
  intros.
  assert (I := H). assert (J := H0). assert (K := H). assert (L := H0).
  apply Collect_alphabet_incl in H. apply Collect_alphabet_incl in H0.
  apply Collect_nodup_2 in I. apply Collect_nodup_2 in J.
  rewrite Collect_defective in K. rewrite Collect_defective in L.
  apply Collect_alphabet_incl_2 in K. apply Collect_alphabet_incl_2 in L.

  assert (incl u v). apply incl_tran with (m := x::a); assumption.
  assert (incl v u). apply incl_tran with (m := x::a); assumption.

  assert (length u <= length v \/ length v <= length u).

  apply Nat.le_ge_cases. destruct H3.
  apply Permutation_sym. apply NoDup_Permutation_bis; assumption.
  apply NoDup_Permutation_bis; assumption.
Qed.


Theorem Collect_length_equal :
  forall (a: list X) (x : X) (l u v: list X),
    In u (Collect a x l) -> In v (Collect a x l) -> length u = length v.
Proof.
  intros.
  apply Collect_permutation_all with (u := u) in H0.
  apply Permutation_length. assumption. assumption.
Qed.


Theorem Collect_append :
  forall (a: list X) (x y : X) (l: list X),
  incl (Collect a x l) (Collect a x (y::l)).
Proof.
  intros. repeat intro.

  destruct a0. apply Collect_nil in H. contradiction.

  assert (I := H). apply Collect_subsequence in H.
  apply Subsequence_cons_l with (a := y) in H.
  assert (J := I). apply Collect_head in J. rewrite J.
  assert (K := I). apply Collect_alphabet_incl_2 in K.
  assert (L := I). apply Collect_alphabet_incl in L.
  apply incl_cons_inv in L. destruct L.
  apply Collect_nodup_2 in I.
  assert (~ In x a0). rewrite J. apply NoDup_cons_iff in I.
  destruct I. assumption.

  apply Collect_subsequence_3; try assumption. repeat intro. 
  assert (a1 <> x). intro. rewrite H4 in H3. apply H2 in H3. assumption.

  apply H1 in H3. destruct H3. rewrite H3 in H4. contradiction. assumption.
Qed.


Theorem Collect_extend :
  forall (a: list X) (x : X) (l u: list X),
  incl (Collect a x l) (Collect a x (u ++ l)).
Proof.
  intros a x l u. generalize a x l. induction u; intros; repeat intro.
  assumption. rewrite <- app_comm_cons. apply Collect_append.
  apply IHu. assumption.
Qed.


Theorem Collect_shorten :
  forall (a: list X) (x y : X) (l v: list X),
    In (x::y::v) (Collect a x l)
      -> In (y::v) (Collect (remove eq_dec x a) y l).
Proof.
  intros a x y l. generalize a x y. induction l; intros.
  apply in_nil in H. contradiction.
  assert (I := H). apply Collect_nodup_2 in I. simpl in H.
  destruct (eq_dec x0 a0). destruct (remove eq_dec x0 a1).
  inversion H. inversion H0. apply in_nil in H0. contradiction.
  apply in_map_iff in H. destruct H. destruct H. inversion H.
  rewrite H2 in H0. apply in_flat_map in H0. destruct H0. destruct H0.
  simpl. destruct (eq_dec y0 a0). rewrite e0 in I. rewrite e in I.
  inversion I. apply not_in_cons in H5. destruct H5. contradiction.
  assert (J := H1). apply Collect_head in H1. rewrite H1 in J. assumption.
  apply IHl in H. apply Collect_append. assumption.
Qed. 


Theorem Collect_remove_arbitrary :
  forall (a: list X) (x y : X) (l: list X),
    incl (Collect a x (remove eq_dec y l)) (Collect a x l).
Proof.
  intros a x y l. generalize a x y. induction l; intros. apply incl_refl.
  destruct (eq_dec a0 y0). rewrite e. rewrite remove_cons.
  destruct (eq_dec x0 y0). rewrite e0. rewrite Collect_only_first.
  apply Collect_append. rewrite Collect_cons_not_eq.
  apply IHl. assumption.
  replace (remove eq_dec y0 (a0::l)) with (a0::(remove eq_dec y0 l)).
  destruct (eq_dec x0 a0). rewrite e.
  simpl. destruct (eq_dec a0 a0). destruct (remove eq_dec a0 a1).
  apply incl_refl. repeat intro.
  apply in_map_iff in H. destruct H. destruct H.
  apply in_map_iff. exists x2. split. assumption.
  apply in_flat_map in H0. destruct H0. destruct H0.
  apply in_flat_map. exists x3. split. assumption. apply IHl in H1.
  assumption. contradiction.
  repeat rewrite Collect_cons_not_eq. apply IHl. assumption. assumption.
  simpl. destruct (eq_dec y0 a0). rewrite e in n. contradiction.
  reflexivity.
Qed.


(*
Theorem Collect_incl_reduce :
  forall (x a : X) (l: list X),
    (forall a0, incl (Collect a0 x (x :: a :: l)) (Collect a0 x (a :: l)))
    -> (forall a0, incl (Collect a0 x (x :: l)) (Collect a0 x (a::l))).
Proof.
  intros.
  destruct (eq_dec x a). 
  rewrite <- Collect_cons_dup. rewrite <- e in H. rewrite <- e. apply H.
  rewrite Collect_cons_not_eq with (y := a) by assumption.

  intro. repeat intro. assert (I := H0). assert (H1 := H0).
  apply Collect_nodup_2 in H1. destruct a1. apply Collect_nil in H0.
  contradiction.

  assert (H2 := H0). apply Collect_head in H2.
  rewrite <- H2. rewrite <- H2 in H0.
  assert (K : incl a1 a0).
  apply Collect_alphabet_incl in H0. apply incl_cons_inv in H0.
  destruct H0. repeat intro.
  assert (H5 := H4). apply H3 in H4. destruct H4. rewrite <- H4 in H5.
  apply in_split in H5. destruct H5. destruct H5. rewrite H5 in H1.
  rewrite H2 in H1. rewrite app_comm_cons in H1. apply NoDup_remove_2 in H1.
  rewrite <- app_comm_cons in H1. apply not_in_cons in H1. destruct H1.
  contradiction. assumption.

  apply Collect_subsequence_3. rewrite H2. assumption.
  apply Collect_alphabet_incl_2 with (x := x) (l := x::l).
  assumption. assumption.

  apply Collect_subsequence in H0. apply Subsequence_cons_eq in H0.
  apply Subsequence_cons_l with (a := a) in H0.
  rewrite <- Subsequence_cons_eq with (a := x) in H0.
  apply Collect_subsequence_3 with (a := a0) in H0. apply H in H0.
  rewrite Collect_cons_not_eq in H0. apply Collect_subsequence in H0.
  assumption. assumption. rewrite H2. assumption.

  apply Collect_alphabet_incl_2 with (x := x) (l := x::l).
  rewrite <- H2 in I. assumption. assumption.
Qed.
 *)


Theorem Collect_length_1 :
  forall (a: list X) (x y: X) (l: list X),
    In [y] (Collect a x l) -> Collect a x l = [[y]].
Proof.
  intros.
  assert (I := H). apply Collect_head in I. rewrite <- I.
  assert (J := H). apply Collect_alphabet_incl_2 in J.
  apply remove_incl with (eq_dec := eq_dec) (x := y) in J.
  simpl in J. destruct (eq_dec y y). apply incl_l_nil in J.

  apply Collect_subsequence in H. apply Subsequence_strong in H.
  destruct H. destruct H. destruct H. destruct H0.
  rewrite H. rewrite <- Collect_prefix. rewrite <- I.

  simpl. destruct (eq_dec x x). rewrite <- I in J. rewrite J.
  reflexivity. contradiction. rewrite I. assumption. assumption.
  contradiction.
Qed.


Theorem Collect_length_2 :
  forall (a: list X) (x y1 y2: X) (l: list X),
    In [y1; y2] (Collect a x l)
      -> incl (Collect a x l) [[y1; y2]].
Proof.
  intros. repeat intro.
  assert (I := H). apply Collect_head in I. rewrite <- I. rewrite <- I in H.
  assert (J : forall u, In u (Collect a x l) -> Permutation u [x;y2]). intros.
  apply Collect_permutation_all with (a := a) (x := x) (l := l); assumption.
  assert (H1 := H0). apply J in H0. apply Permutation_sym in H0.
  apply Permutation_length_2_inv in H0. destruct H0. rewrite H0. apply in_eq.
  rewrite H0 in H1. assert (H2 := H1). apply Collect_head in H2.
  rewrite H2 in H1. apply Collect_nodup_2 in H1. apply NoDup_cons_iff in H1.
  destruct H1. apply not_in_cons in H1. destruct H1. contradiction.
Qed.


Theorem Collect_length_2_nodup :
  forall (a: list X) (x y1 y2: X) (l: list X),
  NoDup a -> In [y1; y2] (Collect a x l) -> Collect a x l = [[y1; y2]].
Proof.
  intros.
  assert (NoDup (Collect a x l)). apply Collect_nodup. assumption.
  assert (H2 := H0). apply Collect_length_2 in H2.
  destruct (Collect a x l). apply in_nil in H0. contradiction.
  destruct l1. apply in_inv in H0. destruct H0. rewrite H0.
  reflexivity. apply in_nil in H0. contradiction.
  apply incl_cons_inv in H2. destruct H2. inversion H2.
  apply incl_cons_inv in H3. destruct H3. inversion H3.
  assert (l0 = [y1; y2]). symmetry. assumption.
  assert (l1 = [y1; y2]). symmetry. assumption.
  rewrite H7 in H1. rewrite H8 in H1.
  apply NoDup_cons_iff in H1.
  destruct H1. apply not_in_cons in H1. destruct H1. contradiction.
  apply in_nil in H6. contradiction.
  apply in_nil in H4. contradiction.
Qed.


Theorem Collect_incl_subsequence :
  forall (a: list X) (x : X) (l1 l2 : list X),
    incl (Collect a x l1) (Collect a x l2)
      -> Subsequence (Collect a x l2) (Collect a x l1).
Proof.
  intros a x l1. generalize a x. induction l1; intros. apply Subsequence_nil_r.

  assert ({In x0 l2} + {~ In x0 l2}).
  apply ListDec.In_dec. apply eq_dec. destruct H0.
  assert (Subsequence l2 [x0]). apply Subsequence_In. assumption.

  destruct (eq_dec x0 a0).
  apply Subsequence_strong in H0. destruct H0. destruct H0. destruct H0.
  destruct H1. rewrite H0. rewrite H0 in H.
  rewrite <- Collect_prefix by assumption.
  rewrite <- Collect_prefix in H by assumption.
  simpl. simpl in H. destruct (eq_dec x0 a0).
  destruct (remove eq_dec x0 a1).
  apply incl_cons_inv in H. destruct H.
  destruct (eq_dec x0 x0). apply Subsequence_id.
  apply Collect_length_1 in H. rewrite H. apply Subsequence_id.
  destruct (eq_dec x0 x0). apply Subsequence_map.

  assert (incl
         (flat_map (fun y : X => Collect (x3 :: l) y l1) (x3 :: l))
         (flat_map (fun y : X => Collect (x3 :: l) y x2) (x3 :: l))).
  repeat intro.
  assert (In (x0::a2)
      (map (cons x0)
         (flat_map (fun y : X => Collect (x3 :: l) y l1) (x3 :: l)))).
  apply in_map. assumption. apply H in H4. apply in_map_iff in H4.
  destruct H4. destruct H4. inversion H4. rewrite H7 in H5. assumption.

  assert (forall (u v: list X),
   incl (flat_map (fun y : X => Collect v y l1) u)
        (flat_map (fun y : X => Collect v y x2) u)
     -> Subsequence (flat_map (fun y : X => Collect v y x2) u)
                    (flat_map (fun y : X => Collect v y l1) u)).
  intro u. induction u; intros. easy.

  simpl. repeat rewrite <- Collect_defective.
  apply Subsequence_app. apply IHl1. repeat intro.
  replace (flat_map (fun y : X => Collect v y l1) (a2 :: u))
    with ((Collect v a2 l1) ++
        flat_map (fun y : X => Collect v y l1) u) in H4.
  apply incl_app_inv in H4. destruct H4.
  destruct a3. apply Collect_nil in H5. contradiction.
  assert (H7 := H5). apply Collect_head in H7. rewrite <- H7 in H5.
  rewrite <- H7.
  apply H4 in H5. apply in_flat_map in H5. destruct H5. destruct H5.
  assert (H9 := H8). apply Collect_head in H9. rewrite H9 in H8.
  assumption. reflexivity. apply IHu.

  assert ({In a2 u} + {~ In a2 u}).
  apply ListDec.In_dec. apply eq_dec. destruct H5.
  assert (forall w (f g: X -> list (list X)), In a2 w
     -> incl (flat_map f (a2 :: w)) (flat_map g (a2 :: w))
     -> incl (flat_map f w) (flat_map g w)).
  intro w. induction w; intros. apply incl_nil_l.
  repeat intro. assert (In a4 (flat_map f (a2::a3::w))).
  apply in_or_app. right. assumption. apply H6 in H8.
  apply in_flat_map in H8. destruct H8. destruct H8.
  destruct H8. rewrite <- H8 in H9.
  apply in_flat_map. exists a2. split; assumption.
  apply in_flat_map. exists x4. split; assumption.
  apply H5; assumption.
  assert (forall w (f g: X -> list (list X)), ~ In a2 w
     -> (forall x y z, In z (f x) -> In z (g y) -> x = y)
     -> incl (flat_map f (a2 :: w)) (flat_map g (a2 :: w))
     -> incl (flat_map f w) (flat_map g w)).
  intros. simpl in H6. apply incl_app_inv in H7. destruct H7.
  repeat intro. assert (K := H9). apply H8 in H9. simpl in H9.
  apply in_app_or in H9. destruct H9. apply in_flat_map in K.
  destruct K. destruct H10. assert (x4 = a2). 
  apply H6 with (z := a3); assumption. rewrite H12 in H10. apply H5 in H10.
  contradiction. assumption. apply H5. assumption.
  intros. destruct (eq_dec x4 y). assumption.
  apply Collect_different_head
       with (a1 := v) (a2 := v) (v := z) (l1 := l1) (l2 := x2) in n0.
  apply n0 in H7. contradiction. assumption. assumption. apply H4.
  assumption. contradiction.

  apply n in e. contradiction. apply eq_dec.
  rewrite Collect_cons_not_eq. rewrite Collect_cons_not_eq in H.
  apply IHl1. assumption. assumption. assumption.

  rewrite Collect_empty_2 with (l := l2) in H.
  apply incl_l_nil in H. rewrite H. apply Subsequence_nil_r. assumption.
Qed.


Theorem Collect_incl_eq :
  forall (a: list X) (x : X) (l1 l2 : list X),
    incl (Collect a x l1) (Collect a x l2)
     -> incl (Collect a x l2) (Collect a x l1)
     -> Collect a x l1 = Collect a x l2.
Proof.
  intros. apply Subsequence_eq; apply Collect_incl_subsequence; assumption.
Qed.


Theorem Count_incl_eq :
  forall (a: list X) (x : X) (l1 l2 : list X),
    incl (Collect a x l1) (Collect a x l2)
     -> incl (Collect a x l2) (Collect a x l1)
     -> Count a x l1 = Count a x l2.
Proof.
  intros. repeat rewrite Count_Collect_length. f_equal.
  apply Collect_incl_eq; assumption.
Qed.


Theorem Collect_perm_eq :
  forall (a l1 l2: list X) (x: X),
    Permutation (Collect a x l1) (Collect a x l2)
      -> Collect a x l1 = Collect a x l2.
Proof.
  intros. apply Collect_incl_eq; repeat intro.
  apply Permutation_in with (l := Collect a x l1). assumption.
  assumption.
  apply Permutation_in with (l := Collect a x l2).
  apply Permutation_sym. assumption. assumption.
Qed.


Theorem Collect_no_change_set :
  forall (x : X) (l: list X),
    (forall a, incl (Collect a x (x::l)) (Collect a x l))
    ->  Collect_no_change x l.
Proof.
  intros. intro. apply Collect_incl_eq. apply H. apply Collect_append.
Qed.


Theorem Count_no_change_set :
  forall (x : X) (l: list X),
    (forall a, incl (Collect a x (x::l)) (Collect a x l))
    ->  Count_no_change x l.
Proof.
  intros. intro. repeat rewrite Count_Collect_length. f_equal.
  apply Collect_no_change_set. assumption.
Qed.


Theorem Count_monotonic :
  forall (a: list X) (x y : X) (l: list X),
    Count a x l <= Count a x (y::l).
Proof.
  intros. repeat rewrite Count_Collect_length. apply Subsequence_length.
  apply Collect_incl_subsequence. apply Collect_append.
Qed.


Theorem Count_monotonic_extend :
  forall (a: list X) (x : X) (l u: list X),
    Count a x l <= Count a x (u++l).
Proof.
  intros a x l u. generalize a x l. induction u; intros. apply le_n.
  rewrite <- app_comm_cons.
  apply Nat.le_trans with (m := Count a1 x0 (u++l0)). apply IHu.
  apply Count_monotonic.
Qed.


Theorem Collect_permut_parallel :
  forall (a1 a2 : list X) (x : X) (l1 l2: list X),
    Permutation a1 a2
      -> Collect a1 x l1 = Collect a1 x l2
      -> Collect a2 x l1 = Collect a2 x l2.
Proof.
  intros. assert (I := H).
  apply Collect_alphabet_perm with (x := x) (l := l1) in H.
  apply Collect_alphabet_perm with (x := x) (l := l2) in I.

  rewrite H0 in H.
  assert (Permutation (Collect a2 x l1) (Collect a2 x l2)).
  apply Permutation_trans with (l' := Collect a1 x l2).
  apply Permutation_sym. assumption. assumption.

  apply Collect_incl_eq. repeat intro.
  apply Permutation_in with (l := Collect a2 x l1); assumption.
  apply Permutation_sym in H1. repeat intro.
  apply Permutation_in with (l := Collect a2 x l2); assumption.
Qed.


Theorem Collect_no_change_exists :
  forall (x: X) (l: list X), ~ Collect_no_change x l
    -> exists a, ~ incl (Collect a x (x::l)) (Collect a x l).
Proof.
  intros.

  assert (exists q, forall v, In v q <-> (NoDup v /\ Subsequence l v)).
  apply Subsequence_nodup_exists_set. assumption. destruct H0 as [q1 J1].
  assert (exists q, forall v, In v q <-> (NoDup v /\ Subsequence (x::l) v)).
  apply Subsequence_nodup_exists_set. assumption. destruct H0 as [q2 J2].

  assert ({ q2 = nil } + { ~ q2 = nil }).
  apply (list_eq_dec (list_eq_dec eq_dec)).
  destruct H0. assert (In [x] nil). rewrite <- e. apply J2.
  split. apply NoDup_cons. apply in_nil. apply NoDup_nil.
  apply Subsequence_In. apply in_eq. apply in_nil in H0. contradiction.

  assert (~ incl q2 q1). intro. apply H. apply Collect_no_change_set.
  intro. intro w. intro. assert (In w q2). apply J2.
  split. apply Collect_nodup_2 in H1. assumption.

  assert (K := H1). apply Collect_nodup_2 in K.
  destruct w. apply Collect_nil in H1. contradiction.
  assert (H2 := H1).
  apply Collect_head in H1. rewrite H1. rewrite H1 in H2.
  apply Collect_subsequence with (a := a) (x := x0). assumption.

  apply H0 in H2. apply J1 in H2. destruct H2.
  destruct w. apply Collect_nil in H1. contradiction.
  assert (H4 := H1). apply Collect_head in H1. rewrite H1. rewrite H1 in H4.
  apply Collect_subsequence_3. assumption.

  apply Collect_alphabet_incl_2 in H4. assumption.
  apply Collect_alphabet_incl in H4.
  repeat intro. destruct (eq_dec x0 a0). apply in_split in H5.
  destruct H5. destruct H5. rewrite <- e in H5. rewrite H5 in H2.
  apply NoDup_cons_iff in H2. destruct H2. assert False. apply H2.
  apply in_elt. contradiction. apply incl_cons_inv in H4. destruct H4.
  apply H6 in H5. destruct H5.
  apply n0 in H5. contradiction. assumption. assumption.

  apply not_incl_exists in H0. destruct H0 as [w]. destruct H0.

  destruct w. assert False. apply H1. apply J1.
  split. apply NoDup_nil. apply Subsequence_nil_r.
  contradiction. destruct (eq_dec x0 x).

  exists (x::w). intro. rewrite e in H0. rewrite e in H1.
  assert (In (x::w) (Collect (x::w) x (x::l))).
  apply Collect_subsequence_3. apply J2 in H0. destruct H0.
  assumption. apply incl_refl. apply incl_tl. apply incl_refl.
  apply J2 in H0. destruct H0. assumption.

  assert (H4 := H3). apply Collect_nodup_2 in H4.
  apply H1. apply H2 in H3. apply Collect_subsequence in H3. apply J1.
  split; assumption.

  apply J2 in H0. destruct H0.
  apply Subsequence_cons_diff in H2.
  assert False. apply H1. apply J1. split; assumption. contradiction.
  intro. rewrite H3 in n0. contradiction. assumption.
Qed.


Theorem Count_no_change_exists :
  forall (x: X) (l: list X), ~ Count_no_change x l
    -> exists a, ~ incl (Collect a x (x::l)) (Collect a x l).
Proof.
  intros.
  apply Collect_no_change_exists. intro. apply H. intro.
  repeat rewrite Count_Collect_length. f_equal. apply H0.
Qed.


Theorem Collect_no_change_exists_full :
  forall (x: X) (l: list X), ~ Collect_no_change x l
    -> exists a v, ~ incl (Collect a x (x::l)) (Collect a x l)
        /\ incl a (x::l)
        /\ In v (Collect a x (x::l)) /\ ~ In v (Collect a x l).
Proof.
  intros. apply Collect_no_change_exists in H.
  destruct H. assert (I := H). apply not_incl_exists in I. destruct I.
  destruct H0. exists x0. exists x1. split. assumption. split.
  apply Collect_alphabet_incl_3 in H0. apply incl_cons_inv in H0.
  destruct H0. assumption. split; assumption. assumption.
Qed.


Theorem Count_no_change_exists_full :
  forall (x: X) (l: list X), ~ Count_no_change x l
    -> exists a v, ~ incl (Collect a x (x::l)) (Collect a x l)
        /\ incl a (x::l)
        /\ In v (Collect a x (x::l)) /\ ~ In v (Collect a x l).
Proof.
  intros. apply Collect_no_change_exists_full. intro. apply H. intro.
  repeat rewrite Count_Collect_length. f_equal. apply H0.
Qed.


Theorem Collect_not_incl_no_change :
  forall (a: list X) (x: X) (l: list X),
    ~ incl (Collect a x (x::l)) (Collect a x l)
      -> ~ Collect_no_change x l.
Proof.
  intros. intro. apply H. rewrite H0. apply incl_refl.
Qed.


Theorem Collect_no_change_nil :
  forall (x : X), ~ Collect_no_change x nil.
Proof.
  repeat intro. specialize (H nil).
  simpl in H. destruct (eq_dec x x). inversion H. contradiction.
Qed.


Theorem Count_no_change_nil :
  forall (x : X), ~ Count_no_change x nil.
Proof.
  repeat intro. specialize (H nil).
  simpl in H. destruct (eq_dec x x). inversion H. contradiction.
Qed.


Theorem Count_Collect_no_change :
  forall (x : X) (l: list X),
    Count_no_change x l <-> Collect_no_change x l.
Proof.
  intros. split; repeat intro. apply Collect_no_change_set. intro.
  assert ({incl (Collect a0 x (x::l)) (Collect a0 x l)}
    + {~ incl (Collect a0 x (x::l)) (Collect a0 x l)}).
  apply ListDec.incl_dec. apply (list_eq_dec eq_dec). destruct H0. assumption.

  apply Collect_not_incl_no_change in n.
  apply Collect_no_change_exists_full in n.
  destruct n as [a']. destruct H0 as [v]. destruct H0. destruct H1. destruct H2.
  assert (Subsequence (Collect a' x (x::l)) (Collect a' x l)).
  apply Collect_incl_subsequence. apply Collect_append.
  apply Subsequence_length_2 in H4. rewrite H4 in H0.
  assert False. apply H0. apply incl_refl. contradiction.
  repeat rewrite <- Count_Collect_length. rewrite H. reflexivity.
  repeat rewrite Count_Collect_length. f_equal. apply H.
Qed.


Theorem Collect_no_change_cases :
  forall (x : X) (l: list X),
    Collect_no_change x l \/ ~ Collect_no_change x l.
Proof.
  intros.
  assert (exists q, forall v, In v q <-> (NoDup v /\ Subsequence (x::l) v)).
  apply Subsequence_nodup_exists_set. assumption. destruct H.
  assert (exists q, forall v, In v q <-> (NoDup v /\ Subsequence l v)).
  apply Subsequence_nodup_exists_set. assumption. destruct H0.
  
  assert (forall (q1 q2: list (list X)),
    incl q2 q1 -> set_diff (list_eq_dec eq_dec) q1 q2 = nil
      -> incl q1 q2).
  intros. repeat intro.
  assert ({In a q2} + {~ In a q2}). apply ListDec.In_dec.
  apply (list_eq_dec eq_dec). destruct H4. assumption.
  apply set_diff_intro with (Aeq_dec := list_eq_dec eq_dec) (x := q1) in n.
  rewrite H2 in n. apply in_nil in n. contradiction. assumption.

  assert ({set_diff (list_eq_dec eq_dec) x0 x1 = nil}
     + {set_diff (list_eq_dec eq_dec) x0 x1 <> nil}).
  apply (list_eq_dec (list_eq_dec eq_dec)). destruct H2.

  left. apply Collect_no_change_set. intro.
  repeat intro. assert (H3 := H2). assert (H4 := H2). assert (K := H2).
  apply Collect_nodup_2 in H2. apply Collect_subsequence in H3.
  assert (In a0 x0). apply H. split; assumption.
  assert ({In a0 x1} + {~ In a0 x1}). apply ListDec.In_dec.
  apply (list_eq_dec eq_dec). destruct H6.
  apply H0 in i. destruct i.
  destruct a0. apply Collect_nil in H4. contradiction.
  apply Collect_head in H4. rewrite H4.
  apply Collect_subsequence_3. assumption. repeat intro.
  apply Collect_alphabet_incl_2 in K. apply K in H8. assumption.
  apply Collect_alphabet_incl in K. rewrite <- H4 in H2.
  apply incl_cons_inv in K. destruct K. apply NoDup_cons_iff in H2.
  destruct H2. repeat intro. assert (H12 := H11). apply H9 in H11.
  destruct H11. rewrite <- H11 in H12. apply H2 in H12. contradiction.
  assumption. assumption.
  assert (In a0 (set_diff (list_eq_dec eq_dec) x0 x1)).
  apply set_diff_intro; assumption. rewrite e in H6. apply in_nil in H6.
  contradiction.

  right.

  assert (forall a, In a (set_diff (list_eq_dec eq_dec) x0 x1)
                         <-> In a x0 /\ ~ In a x1).
  intros. apply set_diff_iff.

  destruct (set_diff (list_eq_dec eq_dec) x0 x1). contradiction.
  apply Collect_not_incl_no_change with (a := l0).
  intro.
  assert (In l0 x0 /\ ~ In l0 x1). apply H2. apply in_eq. destruct H4.
  apply H in H4. destruct H4. apply H5. apply H0. split. assumption.
  apply Collect_subsequence with (a := l0) (x := x).
  destruct l0. assert False. apply H5. apply H0. split. apply NoDup_nil.
  apply Subsequence_nil_r. contradiction.
  destruct (eq_dec x2 x). rewrite e in H6. rewrite e in H3.
  apply Collect_subsequence_3 with (a := x::l0) in H6.
  apply H3 in H6. rewrite e. assumption. rewrite <- e. assumption.
  apply incl_refl. apply incl_tl. apply incl_refl.
  assert False. apply H5. apply H0. split. assumption.
  apply Subsequence_cons_diff in H6. assumption.
  intro. rewrite H7 in n0. contradiction. contradiction.
Qed.


Theorem Count_no_change_cases :
  forall (x : X) (l: list X),
    Count_no_change x l \/ ~ Count_no_change x l.
Proof.
  intros.
  assert (Collect_no_change x l \/ ~ Collect_no_change x l).
  apply Collect_no_change_cases.
  destruct H; [left | right]; rewrite Count_Collect_no_change; assumption.
Qed.


Theorem Count_Collect_eq_swap :
  forall (a: list X) (x : X) (l: list X),
    Count a x l = Count a x (x::l) <-> Collect a x l = Collect a x (x::l).
Proof.
  intros. split; intro. symmetry.
  apply Subsequence_length_2. apply Collect_incl_subsequence.
  apply Collect_append. repeat rewrite <- Count_Collect_length.
  rewrite H. reflexivity. repeat rewrite Count_Collect_length.
  f_equal. assumption.
Qed.


Theorem Count_change_less_than :
  forall (x : X) (l: list X),
    ~ Count_no_change x l ->
      exists a, In x a /\ incl a (x::l) /\ Count a x l < Count a x (x::l).
Proof.
  intros. assert (I := H). apply Count_no_change_exists_full in H.
  destruct H as [a]. destruct H as [u]. destruct H. destruct H0. destruct H1.

  exists (x::a). split. apply in_eq. split. apply incl_cons. apply in_eq.
  assumption. repeat rewrite <- Count_defective.
  assert (Count a x l <= Count a x (x :: l)). apply Count_monotonic.
  apply Nat.lt_eq_cases in H3. destruct H3. assumption.

  apply Count_Collect_eq_swap in H3. rewrite H3 in H2. apply H2 in H1.
  contradiction.
Qed.


Theorem Collect_alphabet_subsequence :
  forall (a1 a2: list X) (x: X) (l: list X),
    Subsequence a1 a2 -> incl a1 a2
      -> Subsequence (Collect a1 x l) (Collect a2 x l).
Proof.
  intros a1 a2 x l. generalize a1 a2 x. induction l; intros.
  apply Subsequence_id.
  simpl. destruct (eq_dec x0 a).
  assert (Subsequence (remove eq_dec x0 a0) (remove eq_dec x0 a3)).
  apply Subsequence_remove_2. assumption.
  assert (incl (remove eq_dec x0 a0) (remove eq_dec x0 a3)).
  apply remove_incl. assumption.
  destruct (remove eq_dec x0 a3).
  destruct (remove eq_dec x0 a0). apply Subsequence_id.
  apply incl_l_nil in H2. inversion H2.
  destruct (remove eq_dec x0 a0). apply Subsequence_nil_cons_r in H1.
  contradiction. apply Subsequence_map.
  apply Subsequence_trans with (l2 :=
    flat_map (fun y : X => Collect (x1 :: l0) y l) (x2 :: l1)).
  apply Subsequence_flat_map_2. intros. apply IHl. assumption.
  assumption. apply Subsequence_flat_map. assumption.
  apply IHl; assumption.
Qed.


Theorem Collect_alphabet_nodup :
  forall (a: list X) (x: X) (l: list X),
    Subsequence (Collect a x l) (Collect (nodup eq_dec a) x l).
Proof.
  intros. apply Collect_alphabet_subsequence.
  apply Subsequence_nodup. apply Subsequence_id.
  apply nodup_incl. apply incl_refl.
Qed.


Theorem Count_alphabet_nodup :
  forall (a: list X) (x: X) (l: list X),
    Count (nodup eq_dec a) x l <= Count a x l.
Proof.
  intros. repeat rewrite Count_Collect_length. apply Subsequence_length.
  apply Collect_alphabet_nodup.
Qed.


Theorem Collect_alphabet_remove :
  forall (a: list X) (x: X) (l: list X),
    Subsequence (Collect a x l) (Collect (remove eq_dec x a) x l).
Proof.
  intros. rewrite Collect_defective.
  replace (Collect (remove eq_dec x a) x l)
     with (Collect (x::(remove eq_dec x a)) x l).
  apply Collect_alphabet_subsequence. apply Subsequence_cons_eq.
  apply Subsequence_remove with (eq_dec := eq_dec) (x := x).
  apply Subsequence_id. repeat intro.
  destruct (eq_dec a0 x). rewrite e. apply in_eq.
  simpl. right. apply in_in_remove. assumption.
  destruct H. rewrite H in n. contradiction. assumption.
  symmetry. apply Collect_defective.
Qed.


Theorem Count_alphabet_remove :
  forall (a: list X) (x: X) (l: list X),
    Count (remove eq_dec x a) x l <= Count a x l.
Proof.
  intros. repeat rewrite Count_Collect_length. apply Subsequence_length.
  apply Collect_alphabet_remove.
Qed.


Theorem Count_new_subsequence :
  forall (l u: list X) (x: X), NoDup (x::u) ->
    Subsequence (x::l) (x::u) -> ~ Subsequence l (x::u)
      -> Count (x::u) x l < Count (x::u) x (x::l).
Proof.
  intros. assert (Subsequence (Collect (x::u) x (x::l))
                                  (Collect (x::u) x l)).
  apply Collect_incl_subsequence. apply Collect_append.
  repeat rewrite Count_Collect_length.
  apply Subsequence_length_3 with (a := x::u). assumption.
  apply Collect_subsequence_3. assumption. apply incl_refl. apply incl_tl.
  apply incl_refl. assumption. intro. apply H1.
  apply Collect_subsequence with (a := x::u) (x := x).
  assumption.
Qed.


Theorem Count_increase_exists_subsequence :
  forall (l a: list X) (x: X), In x a -> NoDup a ->
    Count a x l < Count a x (x::l)
    -> exists u, NoDup (x::u) /\ Subsequence l u /\ ~ Subsequence l (x::u).
Proof.
  intros. assert (I := H0).
  assert (Subsequence (Collect a x (x::l)) (Collect a x l)).
  apply Collect_incl_subsequence. apply Collect_append. assert (L := H).
  apply Collect_nodup with (x := x) (l := x::l) in H0.
  apply Subsequence_nodup_in_notin in H2. destruct H2. destruct H2.
  destruct x0. apply Collect_nil in H2. contradiction.
  assert (H4 := H2). apply Collect_head in H4.
  exists x1. split. apply Collect_nodup_2 in H2. rewrite H4. assumption.
  split. apply Collect_subsequence in H2. apply Subsequence_double_cons in H2.
  assumption. intro. apply H3. rewrite <- H4. apply Collect_subsequence_2.
  assumption. apply Collect_permutation in H2. rewrite H4.
  apply Permutation_sym. assumption. assumption. assumption. assumption.
  assumption. intro. repeat rewrite Count_Collect_length in H1.
  rewrite H3 in H1. apply Nat.lt_irrefl in H1. contradiction.
Qed.


Theorem Collect_no_change_subsequence :
  forall (l: list X) (x: X),
    Collect_no_change x l ->
    forall (u: list X), NoDup u -> Subsequence (x::l) u -> Subsequence l u.
Proof.
  intros. destruct u. apply Subsequence_nil_r.
  destruct (eq_dec x x0). rewrite <- e. rewrite <- e in H1.
  apply Collect_subsequence_3 with (a := u) in H1.
  rewrite H in H1.
  apply Collect_subsequence with (a := u) (x := x).
  assumption. rewrite e. assumption. apply incl_tl. apply incl_refl.
  apply incl_refl. apply Subsequence_cons_diff with (a := x); assumption.
Qed.


Theorem Count_no_change_subsequence :
  forall (l: list X) (x: X),
    Count_no_change x l ->
    forall (u: list X), NoDup u -> Subsequence (x::l) u -> Subsequence l u.
Proof.
  intros. apply Collect_no_change_subsequence with (x := x).
  apply Count_Collect_no_change. assumption. assumption. assumption.
Qed.


Theorem Count_recursion :
  forall (a: list X) (x y: X) (l: list X),
  In y a -> Count (remove eq_dec x a) y l <= Count a x (x::l).
Proof.
  intros.
  destruct (eq_dec x y). rewrite <- e.
  apply Nat.le_trans with (m := Count (remove eq_dec x a) x (x::l)).
  repeat rewrite Count_Collect_length. apply Subsequence_length.
  apply Collect_incl_subsequence. apply Collect_append.
  apply Count_alphabet_remove.
  assert (In y (remove eq_dec x a)). apply in_in_remove.
  intro. rewrite H0 in n. contradiction. assumption.
  repeat rewrite Count_Collect_length.
  apply in_split in H0. destruct H0. destruct H0.

  simpl. destruct (eq_dec x x). destruct(remove eq_dec x a).
  symmetry in H0. apply app_eq_nil in H0. destruct H0. inversion H1.
  rewrite H0. rewrite flat_map_app. rewrite map_app. rewrite length_app.
  rewrite <- Nat.add_0_l at 1. apply Nat.add_le_mono. apply le_0_n.
  simpl. rewrite map_app. rewrite length_app. rewrite length_map.
  apply Nat.le_add_r.

  contradiction.
Qed.


Theorem Count_factorial :
  forall (a: list X) (x: X) (u: list X),
    In x a -> NoDup a -> Count a x u <= fact (pred (length a)).
Proof.
  assert (J: forall w k, (forall e, In e w -> e <= k)
         -> list_sum w <= (length w) *k).
  intro w. induction w; intros. apply le_0_n. simpl.
  apply Nat.add_le_mono. apply H. apply in_eq. apply IHw.
  intros. apply H. apply in_cons. assumption.

  intros a x u. generalize a x. induction u; intros. apply le_0_n.
  destruct (eq_dec x0 a0). rewrite <- e. simpl. destruct (eq_dec x0 x0).
  destruct a1. simpl. easy. destruct (eq_dec x0 x1).

  (* first part of the proof *)
  assert (remove eq_dec x0 (x1::a1) = a1). rewrite e1. simpl.
  apply NoDup_cons_iff in H0. destruct H0.
  destruct (eq_dec x1 x1). apply notin_remove.
  assumption. contradiction.
  rewrite H1. destruct a1. replace 1 with (fact 0). apply fact_le.
  apply le_0_n. easy.

  assert (length (map (fun y : X => Count (x2 :: a1) y u) (x2 :: a1))
    = length (x2::a1)). rewrite length_map. reflexivity.
  assert (forall e, In e (x2::a1)
    -> (fun y => Count (x2::a1) y u) e <= fact (pred (length (x2::a1)))).
  intros. apply IHu. assumption.
  apply NoDup_cons_iff in H0. destruct H0. assumption.

  rewrite J with (k := fact (pred (length (x2::a1)))).
  replace (fact (pred (length (x1::x2::a1))))
    with ((length (x2::a1)) * (fact (pred (length (x2::a1))))).
  apply Nat.mul_le_mono_r. rewrite length_map. apply le_n.

  replace (pred (length (x2::a1))) with (length a1) by apply pred_Sn.
  replace (pred (length (x1::x2::a1))) with (length (x2::a1)) by apply pred_Sn.
  easy.

 intros. apply in_map_iff in H4. destruct H4. destruct H4.
 rewrite <- H4. apply H3. assumption.

 (* second part of the proof *)
 replace (remove eq_dec x0 (x1::a1)) with (x1 :: (remove eq_dec x0 a1)).

  assert (length (map
              (fun y : X => Count (x1 :: remove eq_dec x0 a1) y u)
              (x1 :: remove eq_dec x0 a1)) = length a1).
  rewrite length_map.
  destruct H. rewrite H in n. contradiction. apply in_split in H.
  destruct H. destruct H.
  assert (remove eq_dec x0 a1 = x2++x3). rewrite H. rewrite remove_app.
  simpl. destruct (eq_dec x0 x0). rewrite <- remove_app. apply notin_remove.
  apply NoDup_remove_2. rewrite H in H0. apply NoDup_cons_iff in H0.
  destruct H0. assumption. contradiction. rewrite H1. rewrite H.
  rewrite app_comm_cons. repeat rewrite length_app. apply Nat.add_succ_comm.

  assert (forall e, In e (x1 :: remove eq_dec x0 a1)
    -> (fun y => Count (x1 :: remove eq_dec x0 a1) y u) e
               <= fact (pred (length (x1 :: remove eq_dec x0 a1)))).
  intros. apply Nat.le_trans with (m := Count (x1 :: remove eq_dec x0 a1) e1 u).
  apply le_n. apply IHu. assumption. apply NoDup_cons. intro.
  apply in_remove in H3. destruct H3. apply NoDup_cons_iff in H0.
  destruct H0. apply H0 in H3. assumption.

  assert (forall w z, NoDup w -> NoDup (remove eq_dec z w)).
  intro w. induction w; intros. easy. simpl.
  apply NoDup_cons_iff in H3. destruct H3.
  destruct (eq_dec z a2). apply IHw.
  assumption. apply NoDup_cons.
  intro. apply in_remove in H5. destruct H5.
  apply H3 in H5. assumption. apply IHw. assumption. apply H3.
  apply NoDup_cons_iff in H0. destruct H0. assumption.

  replace (fact (pred (length (x1::a1))))
    with ((length (map (fun y : X => Count (x1 :: remove eq_dec x0 a1) y u)
                       (x1 :: remove eq_dec x0 a1)))
               * (fact (pred (length (x1 :: remove eq_dec x0 a1))))).
  apply J. intros. apply in_map_iff in H3. destruct H3. destruct H3.
  apply H2 in H4. rewrite <- H3. assumption. rewrite H1.
  replace (pred (length (x1 :: remove eq_dec x0 a1)))
    with (length (remove eq_dec x0 a1)) by apply pred_Sn.
  replace (pred (length (x1::a1))) with (length a1) by apply pred_Sn.
  replace (length a1) with (S (length (remove eq_dec x0 a1))). easy.

  destruct H. rewrite H in n. contradiction. apply in_split in H.
  destruct H. destruct H. rewrite H in H0.
  replace (remove eq_dec x0 a1) with (x2 ++ x3). rewrite H.
  repeat rewrite length_app. apply Nat.add_succ_comm.
  rewrite H. rewrite remove_app.
  replace (remove eq_dec x0 x2) with x2.
  replace (remove eq_dec x0 (x0::x3)) with x3. reflexivity.
  simpl. destruct (eq_dec x0 x0). rewrite app_comm_cons in H0.
  symmetry. apply notin_remove. apply NoDup_app_remove_l in H0.
  apply NoDup_cons_iff in H0. destruct H0. assumption. contradiction.
  apply NoDup_cons_iff in H0. destruct H0. apply NoDup_remove_2 in H3.
  symmetry. apply notin_remove. intro. apply H3. apply in_or_app. left.
  assumption.

  simpl. destruct (eq_dec x0 x1). apply n in e1. contradiction.
  reflexivity. contradiction. rewrite Count_cons_not_eq.
  apply IHu; assumption. assumption.
Qed.


Theorem Count_missing_subsequence_fact :
  forall (a: list X) (x: X) (l v: list X),
    NoDup a -> In x a -> In v (Collect a x (x::l))
      -> ~ In v (Collect a x l) -> Count a x l < fact (pred (length a)).
Proof.
  intros. apply Nat.lt_le_trans with (m := Count a x (x::l)).
  assert (Count a x l <= Count a x (x :: l)).
  apply Count_monotonic. apply Nat.lt_eq_cases in H3. destruct H3.
  assumption. apply Count_Collect_eq_swap in H3. rewrite H3 in H2.
  apply H2 in H1. contradiction. apply Count_factorial; assumption.
Qed.


Theorem Count_no_change_explode :
  forall (x: X) (l v: list X),
    ~ Count_no_change x l
      -> exists a, NoDup a /\ In x a /\ Count a x l < Count a x (x::l)
            /\ Count a x l < fact (pred (length a)).
Proof.
  intros. apply Count_no_change_exists_full in H.
  destruct H as [a]. destruct H as [w]. destruct H. destruct H0. destruct H1.
  exists (nodup eq_dec (x::a)).

  assert (I := H1). assert (J := I). assert (K := J). assert (L := K).
  apply Collect_subsequence in H1. apply Collect_alphabet_incl in J.
  apply Collect_nodup_2 in K. apply Collect_alphabet_incl_2 in L.

  split. apply NoDup_nodup. split. apply nodup_In. apply in_eq. split.

  assert (In w (Collect (nodup eq_dec (x::a)) x (x::l))).
  destruct w. apply Collect_nil in I. contradiction.
  assert (M := I). apply Collect_head in M. rewrite <- M.
  apply Collect_subsequence_3. rewrite M. assumption.
  repeat intro. apply nodup_In in H3. destruct H3.
  rewrite H3. apply in_eq. apply L in H3. rewrite M. assumption.
  repeat intro. apply nodup_In. apply J. apply in_cons. assumption.
  rewrite <- M in H1. assumption.

  assert (~ In w (Collect (nodup eq_dec (x::a)) x l)).
  intro. assert (Subsequence (Collect (x::a) x l)
     (Collect (nodup eq_dec (x::a)) x l)). apply Collect_alphabet_nodup.
  rewrite <- Collect_defective in H5. apply Subsequence_incl in H5.
  apply H5 in H4. apply H2 in H4. assumption.

  assert (~ Collect (nodup eq_dec (x :: a)) x l
    = Collect (nodup eq_dec (x :: a)) x (x::l)). intro. rewrite <- H5 in H3.
  apply H4 in H3. assumption.
  assert (~ Count (nodup eq_dec (x :: a)) x l
      = Count (nodup eq_dec (x :: a)) x (x::l)).
  intro. apply H5. apply Count_Collect_eq_swap in H6. apply H5 in H6.
  contradiction.

  assert (Count (nodup eq_dec (x :: a)) x l
              <= Count (nodup eq_dec (x :: a)) x (x :: l)).
  apply Count_monotonic. apply Nat.lt_eq_cases in H7. destruct H7.
  assumption. rewrite H7 in H6. contradiction.

  apply Count_missing_subsequence_fact with (v := w). apply NoDup_nodup.
  apply nodup_In. apply in_eq. destruct w. apply Collect_nil in I. contradiction.
  apply Collect_head in I. rewrite I. apply Collect_subsequence_3.
  assumption.

  repeat intro. apply nodup_In in H3.
  destruct H3. rewrite H3. apply in_eq. apply L in H3. assumption.

  apply incl_cons_inv in J. destruct J.
  repeat intro. apply nodup_In. apply H4 in H5. rewrite <- I. assumption.
  rewrite I in H1. assumption.

  intro.
  assert (Subsequence (Collect (x::a) x l) (Collect (nodup eq_dec (x::a)) x l)).
  apply Collect_alphabet_nodup. apply Subsequence_incl in H4. apply H4 in H3.
  rewrite <- Collect_defective in H3. apply H2 in H3. assumption.
Qed.


Theorem Count_eq_fact :
  forall (a: list X) (x: X) (l: list X),
    NoDup a -> In x a -> Count a x l = fact (pred (length a))
    -> Count a x l = Count a x (x::l).
Proof.
  intros. apply Nat.le_antisymm. apply Count_monotonic. rewrite H1.
  apply Count_factorial; assumption.
Qed.


Theorem Count_incl_less_than:
  forall (a: list X) (x: X) (l v w: list X),
    NoDup (x::v) -> ~ Subsequence l (x::v) -> incl v a -> incl a (x::v)
      -> Subsequence (w ++ l) (x::v) -> Count a x l < Count a x (w ++ l).
Proof.
  intros.
  assert (Subsequence (Collect a x (w++l)) (Collect a x l)).
  apply Collect_incl_subsequence. apply Collect_extend.
  apply Subsequence_length in H4.
  apply Nat.lt_eq_cases in H4. destruct H4.
  repeat rewrite Count_Collect_length. assumption.
  symmetry in H4. apply Subsequence_length_2 in H4.
  
  apply Collect_subsequence_3 with (a := a) in H3.
  rewrite H4 in H3. apply Collect_subsequence in H3. apply H0 in H3.
  contradiction. assumption. assumption. assumption.
  apply Collect_incl_subsequence. apply Collect_extend.
Qed.


Theorem Collect_eq_fact :
  forall (a: list X) (x: X) (l: list X),
    NoDup a -> In x a -> Count a x l = fact (pred (length a))
    -> forall p, Permutation a (x::p) -> Subsequence l (x::p).
Proof.
  intros. assert (Count a x l = Count a x (x::l)).
  apply Count_eq_fact; assumption.
  assert ({Subsequence l (x::p)} + {~ Subsequence l (x::p)}).
  apply Subsequence_dec. assumption. destruct H4. assumption.
  assert (Subsequence ((x::p) ++ l) (x::p)). apply Subsequence_app2_r.
  apply Subsequence_id. assert (In (x::p) (Collect a x ((x::p)++l))).
  apply Collect_subsequence_2; assumption.
  assert (~ In (x::p) (Collect a x l)). intro. apply Collect_subsequence in H6.
  apply n in H6. contradiction.
  assert (incl (Collect a x l) (Collect a x ((x::p)++l))). apply Collect_extend.
  apply NoDup_incl_length in H7. rewrite <- Count_Collect_length in H7.
  rewrite H1 in H7. apply Nat.lt_eq_cases in H7. destruct H7.
  assert (length (Collect a x ((x::p) ++ l)) <= fact (pred (length a))).
  rewrite <- Count_Collect_length. apply Count_factorial; assumption.
  apply Nat.le_ngt in H8. apply H8 in H7. contradiction.
  assert (Subsequence (Collect a x ((x::p)++l)) (Collect a x l)).
  apply Collect_incl_subsequence. apply Collect_extend.
  rewrite <- H1 in H7. rewrite Count_Collect_length in H7. symmetry in H7.
  apply Subsequence_length_2 in H7. rewrite H7 in H5. apply H6 in H5.
  contradiction. assumption. apply Collect_nodup. assumption.
Qed.


Theorem Collect_eq_fact_2 :
  forall (a: list X) (x: X) (l: list X),
    NoDup a -> In x a 
      -> (forall p, Permutation a (x::p) -> Subsequence l (x::p))
      -> Count a x l = fact (pred (length a)).
Proof.
  intros. assert (J := H0).
  apply in_split in H0. destruct H0. destruct H0.
  assert (NoDup (x0 ++ x1)). rewrite H0 in H. apply NoDup_remove in H.
  destruct H. assumption.
  apply Permutation_exists_set_length in H2. destruct H2 as [q].
  destruct H2. destruct H3. rewrite Count_Collect_length.
  assert (incl (map (cons x) q) (Collect a x l)).
  repeat intro.
  apply in_map_iff in H5. destruct a0. destruct H5. destruct H5. inversion H5.
  destruct H5. destruct H5. inversion H5. apply Collect_subsequence_2.
  assumption. rewrite <- H5. apply H2 in H6.
  apply Permutation_trans with (l' := x::x0++x1). rewrite H0.
  apply Permutation_sym. apply Permutation_middle. apply perm_skip.
  assumption. rewrite <- H8. apply H1. rewrite H8. rewrite H0.
  rewrite <- H5. apply Permutation_trans with (l' := x::x0++x1).
  apply Permutation_sym. apply Permutation_middle. apply perm_skip.
  apply H2. assumption. apply NoDup_incl_length in H5.
  rewrite length_map in H5. rewrite H3 in H5.
  replace (length (x0 ++ x1)) with (pred (length a)) in H5.
  apply Nat.le_antisymm. rewrite <- Count_Collect_length.
  apply Count_factorial; assumption. assumption.
  rewrite H0. rewrite length_app. rewrite length_app.
  rewrite <- Nat.add_pred_r. simpl. reflexivity.
  easy. apply FinFun.Injective_map_NoDup. repeat intro.
  inversion H6. reflexivity. assumption. assumption.
Qed.


End Count_dec.
