(** * Notebook 3
 *)


Require Import Nat.
Require Import PeanoNat.
Require Import List.

Import ListNotations.

Require Import Sorting.Permutation.
Require Import Arith.Factorial.

Require Import subsequence.
Require Import count.
Require Import scs.
Require Import swap.
Require Import notebook.
Require Import notebook2.




Set Implicit Arguments.

Section Notebook_dec.

Variable X: Type.
Hypothesis eq_dec : forall x y: X, {x = y} + {x <> y}.


Definition Quasicomplete (a: list X) (x: X) (l: list X) :=
  Complete_sequence (x::l) a.


Lemma Quasicomplete_trivial_extend :
  forall (a: list X) (x: X) (l u: list X),
    NoDup a -> Quasicomplete a x l -> Quasicomplete a x (u++l).
Proof.
  intros. unfold Quasicomplete. replace (x::u++l) with ([x] ++ u ++ l).
  apply Complete_sequence_extend_middle. assumption. easy.
Qed.


Theorem Quasicomplete_dec (a: list X) (x: X) (l: list X) :
  { Quasicomplete a x l } + {~ Quasicomplete a x l}.
Proof.
  destruct (Complete_sequence_dec eq_dec (x::l) a); [left | right]; assumption.
Qed.




Theorem Count_eq_swappable_quasicomplete :
  forall (l: list X) (x y : X),
    (forall (a : list X) (z : X), NoDup a ->
     Count eq_dec a z l = Count eq_dec (Swap eq_dec x y a)
       (if eq_dec x z then y else if eq_dec y z then x else z) l
       \/ Quasicomplete a x l) -> Swappable eq_dec x y l.
Proof.
  intros. intro. intros.


End Notebook_dec.
