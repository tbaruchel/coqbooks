(** * Notebook 1

This notebook contains several useful but still "intuitive" theorems.

Proofs here are not very interesting and have been written very quickly in a poor way.

 *)

Require Import Nat.
Require Import PeanoNat.
Require Import List.

Import ListNotations.

Require Import Sorting.Permutation.
Require Import Arith.Factorial.

Require Import subsequence.
Require Import count.
Require Import scs.
Require Import swap.
Require Import utilities.
Require Import exists_set.



Set Implicit Arguments.

Section Notebook_dec.

Variable X: Type.
Hypothesis eq_dec : forall x y: X, {x = y} + {x <> y}.




Theorem Count_no_change_complete :
  forall (x: X) (l u: list X),
    Count_no_change eq_dec x l
    -> forall (base: list X),
        NoDup base -> Complete_sequence (u++x::l) base
        -> Complete_sequence (u++l) base.
Proof.
  intros. intro. repeat intro. assert (K := H2). apply H1 in H2.
  apply Subsequence_split in H2. destruct H2. destruct H2. destruct H2.
  destruct H3. apply Count_no_change_subsequence with (eq_dec := eq_dec) in H4.
  rewrite H2. apply Subsequence_app; assumption. assumption.
  assert (NoDup v). apply Permutation_NoDup with (l := base); assumption.
  rewrite H2 in H5. apply NoDup_app_remove_l in H5. assumption.
Qed.


Theorem Count_no_change_not_shortest :
  forall (x: X) (l u: list X),
    Count_no_change eq_dec x l
    -> forall (base: list X),
        NoDup base -> ~ Shortest_complete_sequence (u++x::l) base.
Proof.
  intros. intro. destruct H1.
  assert (Complete_sequence (u++l) base).
  apply Count_no_change_complete with (x := x); assumption.
  apply H2 in H3. assumption. repeat rewrite length_app.
  apply Nat.add_lt_mono_l. apply Nat.lt_succ_diag_r.
Qed.


Theorem Collect_full_swap :
  forall (l: list X) (x y z: X) (a: list X),
  Collect eq_dec (Swap eq_dec x y a)
                 (if eq_dec x z then y else if eq_dec y z then x else z)
                 (Swap eq_dec x y l)
     = map (Swap eq_dec x y) (Collect eq_dec a z l).
Proof.
  assert (L: forall (u: list (list X)) (e: X), map (Swap eq_dec e e) u = u).
  intro u. induction u; intros. reflexivity.
  simpl. rewrite Swap_trivial_id. rewrite IHu. reflexivity.

  assert (M: forall u (z y: X), map (Swap eq_dec z y) (map (cons z) u)
    = map (cons y) (map (Swap eq_dec z y) u)).
  intro u. induction u; intros. reflexivity. simpl. destruct (eq_dec z z).
  rewrite IHu. reflexivity. contradiction.

  assert (M': forall u (z y: X), map (Swap eq_dec y z) (map (cons z) u)
    = map (cons y) (map (Swap eq_dec z y) u)).
  intro u. induction u; intros. reflexivity. simpl. destruct (eq_dec z z).
  destruct (eq_dec y z).
  rewrite Swap_trivial_swap. rewrite IHu. rewrite e0. reflexivity.
  rewrite Swap_trivial_swap. rewrite IHu. reflexivity. contradiction.

  assert (M'': forall u (z y x: X), x <> z -> y <> z
       -> map (Swap eq_dec x y) (map (cons z) u)
            = map (cons z) (map (Swap eq_dec x y) u)).
  intro u. induction u; intros. reflexivity. simpl. destruct (eq_dec x z).
  apply H in e. contradiction. destruct (eq_dec y z). apply H0 in e.
  contradiction. rewrite IHu. reflexivity. assumption. assumption.

  assert (P: forall u (z y: X), remove eq_dec y (Swap eq_dec z y u)
      = (Swap eq_dec z y (remove eq_dec z u))). intros.
  rewrite Swap_trivial_swap. symmetry.
  rewrite Swap_trivial_swap. symmetry.
  apply Swap_remove.

  intro l. induction l; intros. reflexivity.
  destruct (eq_dec a z). rewrite e.
  simpl. destruct (eq_dec x z). rewrite e0.
  destruct (eq_dec y y). destruct (eq_dec z z).
  destruct (eq_dec z y). rewrite e3.
  rewrite Swap_trivial_id.
  destruct (remove eq_dec y a0). simpl.
  destruct (eq_dec y y); reflexivity. rewrite L. f_equal.
  rewrite Swap_trivial_id. reflexivity.

  assert(remove eq_dec z a0 = [] <-> remove eq_dec y (Swap eq_dec z y a0) = []).
  split; intro.
  rewrite Swap_trivial_swap. rewrite Swap_remove. rewrite H. easy.
  rewrite Swap_trivial_swap in H. rewrite Swap_remove in H.
  destruct (remove eq_dec z a0). reflexivity.
  apply length_zero_iff_nil in H. rewrite Swap_length in H. inversion H.

  specialize (P a0 z y).

  destruct (remove eq_dec z a0).
  assert ((nil: list X) = nil). reflexivity. apply H in H0. rewrite H0.
  simpl. destruct (eq_dec z z). reflexivity. contradiction.
  assert (~ x0::l0 = nil). easy.
  assert (~ remove eq_dec y (Swap eq_dec z y a0) = nil). intro.
  apply H in H1. apply H0 in H1. assumption.
  destruct (remove eq_dec y (Swap eq_dec z y a0)). contradiction.

  rewrite M. f_equal.

  rewrite P. assert (N: forall (u w: list X),
  flat_map
    (fun y0 : X =>
     Collect eq_dec (Swap eq_dec z y w) y0 (Swap eq_dec z y l))
      (Swap eq_dec z y u) = map (Swap eq_dec z y)
      (flat_map (fun y0 : X => Collect eq_dec w y0 l) u)).
  intro u. induction u; intros. reflexivity.
  simpl. destruct (eq_dec z a1). rewrite <- e3. rewrite map_app.
  replace (map (Swap eq_dec z y) (Collect eq_dec w z l))
    with (Collect eq_dec (Swap eq_dec z y w) y (Swap eq_dec z y l)).
  apply app_inv_head_iff. apply IHu.

  specialize (IHl z y x w).
  replace (map (Swap eq_dec z y) (Collect eq_dec w z l))
    with (map (Swap eq_dec z y) (Collect eq_dec w x l)). rewrite <- IHl.
  destruct (eq_dec z x). reflexivity. rewrite e0 in n0. contradiction.
  rewrite e0. reflexivity.

  rewrite map_app.
  replace (Collect eq_dec (Swap eq_dec z y w) (if eq_dec y a1 then z else a1)
    (Swap eq_dec z y l)) with (map (Swap eq_dec z y) (Collect eq_dec w a1 l)).
  apply app_inv_head_iff. apply IHu.

  specialize (IHl z y a1 w). rewrite <- IHl.
  destruct (eq_dec z a1). apply n0 in e3. contradiction. reflexivity.
  apply N. contradiction. contradiction.

  destruct (eq_dec z z). destruct (eq_dec y z). destruct (eq_dec x x).

  assert(remove eq_dec z a0 = [] <-> remove eq_dec x (Swap eq_dec x y a0) = []).
  split; intro. rewrite e1. rewrite Swap_remove. rewrite H. easy.
  rewrite Swap_remove in H. rewrite <- e1.
  destruct (remove eq_dec y a0). reflexivity.
  apply length_zero_iff_nil in H. rewrite Swap_length in H. inversion H.

  specialize (P a0 z x). rewrite Swap_trivial_swap in P.

  rewrite e1. rewrite e1 in H. destruct (remove eq_dec z a0).
  assert ((nil: list X) = nil). reflexivity. apply H in H0. rewrite H0.
  simpl. destruct (eq_dec x z). apply n in e3. contradiction.
  destruct (eq_dec z z). reflexivity. contradiction.
  assert (~ x0::l0 = nil). easy.
  assert (~ remove eq_dec x (Swap eq_dec x y a0) = nil). intro.
  rewrite e1 in H1. apply H in H1. apply H0 in H1. assumption.
  rewrite e1 in H1.
  destruct (remove eq_dec x (Swap eq_dec x z a0)). contradiction.

  rewrite M'. f_equal.
  
  rewrite P. assert (N: forall (u w: list X),
  flat_map
    (fun y0 : X =>
     Collect eq_dec (Swap eq_dec z x w) y0 (Swap eq_dec x z l))
      (Swap eq_dec z x u) = map (Swap eq_dec z x)
      (flat_map (fun y0 : X => Collect eq_dec w y0 l) u)).
  intro u. induction u; intros. reflexivity.
  simpl. destruct (eq_dec z a1). rewrite <- e3. rewrite map_app.
  replace (map (Swap eq_dec z x) (Collect eq_dec w z l))
    with (Collect eq_dec (Swap eq_dec z x w) x (Swap eq_dec x z l)).
  apply app_inv_head_iff. apply IHu.

  specialize (IHl z x y w).
  replace (map (Swap eq_dec z x) (Collect eq_dec w z l))
    with (map (Swap eq_dec z x) (Collect eq_dec w y l)). rewrite <- IHl.
  destruct (eq_dec z y). replace (Swap eq_dec x z l) with (Swap eq_dec z x l).
  reflexivity. apply Swap_trivial_swap. rewrite e1 in n0. contradiction.
  rewrite e1. reflexivity.

  rewrite map_app.
  replace (Collect eq_dec (Swap eq_dec z x w) (if eq_dec x a1 then z else a1)
    (Swap eq_dec x z l)) with (map (Swap eq_dec z x) (Collect eq_dec w a1 l)).
  apply app_inv_head_iff. apply IHu.

  specialize (IHl z x a1 w). rewrite <- IHl.
  destruct (eq_dec z a1). apply n0 in e3. contradiction.
  replace (Swap eq_dec x z l) with (Swap eq_dec z x l). reflexivity.
  apply Swap_trivial_swap.
  apply N. contradiction.

  destruct (eq_dec z z).

  assert (remove eq_dec z (Swap eq_dec x y a0)
                    = Swap eq_dec x y (remove eq_dec z a0)).
  apply Swap_remove_2. intro. rewrite H in n. contradiction.
  intro. rewrite H in n0. contradiction.

  assert (I := H).
  rewrite <- remove_remove_eq in I. rewrite Swap_remove_2 in I.

  assert (J : ~ In z (remove eq_dec z a0)). apply remove_In.

  destruct (remove eq_dec z a0). rewrite H.
  simpl. destruct (eq_dec x z). apply n in e2. contradiction.
  destruct (eq_dec y z). apply n0 in e2. contradiction. reflexivity.
  rewrite H. rewrite M''.

  assert (Swap eq_dec x y (x0::l0) <> nil). intro.
  apply length_zero_iff_nil in H0. rewrite Swap_length in H0. inversion H0.

  rewrite Swap_remove_2 in I.
  destruct (Swap eq_dec x y (x0::l0)). contradiction. f_equal.

  rewrite <- I. rewrite notin_remove.

  assert (N: forall u,
    flat_map
      (fun y0 : X =>
       Collect eq_dec (Swap eq_dec x y (x0 :: l0)) y0 (Swap eq_dec x y l))
      (Swap eq_dec x y u) =
    map (Swap eq_dec x y)
      (flat_map (fun y0 : X => Collect eq_dec (x0 :: l0) y0 l) u)).
  intro u. induction u. reflexivity.
  simpl. destruct (eq_dec x x0). rewrite map_app.
  destruct (eq_dec x a1).
  replace (Collect eq_dec (y :: Swap eq_dec x y l0) y (Swap eq_dec x y l))
     with (map (Swap eq_dec x y) (Collect eq_dec (x0 :: l0) a1 l)).
  apply app_inv_head_iff.
  replace (y :: Swap eq_dec x y l0) with (Swap eq_dec x y (x0::l0)).
  apply IHu. simpl. destruct (eq_dec x x0). reflexivity.
  apply n1 in e2. contradiction.
  replace (y :: Swap eq_dec x y l0) with (Swap eq_dec x y (x0::l0)).
  rewrite <- IHl. destruct (eq_dec x a1). reflexivity.
  apply n1 in e3. contradiction. simpl. destruct (eq_dec x x0).
  reflexivity. apply n1 in e2. contradiction.

  destruct (eq_dec y a1).
  replace (Collect eq_dec (y :: Swap eq_dec x y l0) x (Swap eq_dec x y l))
    with (map (Swap eq_dec x y) (Collect eq_dec (x0 :: l0) a1 l)).
  apply app_inv_head_iff.
  replace (y :: Swap eq_dec x y l0) with (Swap eq_dec x y (x0::l0)).
  apply IHu. simpl. destruct (eq_dec x x0). reflexivity.
  apply n2 in e2. contradiction.
  replace (y :: Swap eq_dec x y l0) with (Swap eq_dec x y (x0::l0)).
  rewrite <- IHl. destruct (eq_dec x a1).
  apply n1 in e4. contradiction. destruct (eq_dec y a1). reflexivity.
  apply n3 in e3. contradiction. simpl. destruct (eq_dec x x0). reflexivity.
  apply n2 in e2. contradiction.

  replace (Collect eq_dec (y :: Swap eq_dec x y l0) a1 (Swap eq_dec x y l))
    with (map (Swap eq_dec x y) (Collect eq_dec (x0 :: l0) a1 l)).
  apply app_inv_head_iff.
  replace (y :: Swap eq_dec x y l0) with (Swap eq_dec x y (x0::l0)).
  apply IHu. simpl. destruct (eq_dec x x0). reflexivity.
  apply n3 in e2. contradiction.
  rewrite <- IHl. destruct (eq_dec x a1). apply n1 in e3. contradiction.
  destruct (eq_dec y a1). apply n2 in e3. contradiction.
  replace (y :: Swap eq_dec x y l0) with (Swap eq_dec x y (x0::l0)).
  reflexivity. simpl. destruct (eq_dec x x0). reflexivity.
  apply n5 in e2. contradiction.

  rewrite map_app. destruct (eq_dec y x0).
  replace (Collect eq_dec (x :: Swap eq_dec x y l0)
      (if eq_dec x a1 then y else if eq_dec y a1 then x else a1)
                    (Swap eq_dec x y l))
    with (map (Swap eq_dec x y) (Collect eq_dec (x0 :: l0) a1 l)).
  apply app_inv_head_iff.
  replace (x :: Swap eq_dec x y l0) with (Swap eq_dec x y (x0::l0)).
  apply IHu. simpl. destruct (eq_dec y x0). destruct (eq_dec x x0).
  apply n1 in e4. contradiction. reflexivity.
  apply n2 in e2. contradiction. rewrite <- IHl.
  destruct (eq_dec x a1).
  replace (x :: Swap eq_dec x y l0) with (Swap eq_dec x y (x0::l0)).
  reflexivity. simpl. destruct (eq_dec y x0). destruct (eq_dec x x0).
  apply n1 in e5. contradiction. reflexivity.
  apply n2 in e2. contradiction.
  replace (x :: Swap eq_dec x y l0) with (Swap eq_dec x y (x0::l0)).
  reflexivity. simpl. destruct (eq_dec y x0). destruct (eq_dec x x0).
  apply n1 in e4. contradiction. reflexivity.
  apply n3 in e2. contradiction.

  replace (Collect eq_dec (x0 :: Swap eq_dec x y l0)
    (if eq_dec x a1 then y else if eq_dec y a1 then x else a1)
           (Swap eq_dec x y l))
    with (map (Swap eq_dec x y) (Collect eq_dec (x0 :: l0) a1 l)).
  apply app_inv_head_iff.
  
  replace (x0 :: Swap eq_dec x y l0) with (Swap eq_dec x y (x0::l0)).
  apply IHu. simpl. destruct (eq_dec x x0). apply n1 in e2.
  contradiction. destruct (eq_dec y x0). apply n2 in e2. contradiction.
  reflexivity. rewrite <- IHl.
  replace (x0 :: Swap eq_dec x y l0) with (Swap eq_dec x y (x0::l0)).
  reflexivity. simpl. destruct (eq_dec y x0). destruct (eq_dec x x0).
  apply n1 in e3. contradiction. apply n2 in e2. contradiction.
  destruct (eq_dec x x0). apply n1 in e2. contradiction. reflexivity.

  apply N. assumption.
  intro. rewrite H1 in n. contradiction.
  intro. rewrite H1 in n0. contradiction.
  assumption. assumption.
  intro. rewrite H0 in n. contradiction.
  intro. rewrite H0 in n0. contradiction.
  contradiction. contradiction.

  rewrite Collect_cons_not_eq. rewrite <- IHl.
  destruct (eq_dec x z). simpl. destruct (eq_dec x a).
  rewrite e0 in e. apply n in e. contradiction.
  destruct (eq_dec y a). destruct (eq_dec y x).
  rewrite e0 in e1. rewrite e1 in n0. contradiction. reflexivity.
  destruct (eq_dec y a). apply n1 in e0. contradiction. reflexivity.
  simpl. destruct (eq_dec y z). destruct (eq_dec x a).
  destruct (eq_dec x y). rewrite e in e1. apply n0 in e1. contradiction.
  reflexivity. destruct (eq_dec y a).
  rewrite e in e0. rewrite e0 in n. contradiction.
  destruct (eq_dec x a). apply n1 in e0. contradiction. reflexivity.
  destruct (eq_dec x a). destruct (eq_dec z y). rewrite e0 in n1. contradiction.
  reflexivity.
  destruct (eq_dec y a). destruct (eq_dec z x). rewrite e0 in n0.
  contradiction. reflexivity.
  destruct (eq_dec z a). rewrite e in n. contradiction. reflexivity.
  intro. rewrite H in n. contradiction.
Qed.


Theorem Count_full_swap :
  forall (l: list X) (x y z: X) (a: list X),
  Count eq_dec a z l = Count eq_dec (Swap eq_dec x y a)
                (if eq_dec x z then y else if eq_dec y z then x else z)
                   (Swap eq_dec x y l).
Proof.
  intros. repeat rewrite Count_Collect_length. rewrite Collect_full_swap.
  rewrite length_map. reflexivity.
Qed.


Theorem Count_not_eq_swap_swap :
  forall (l: list X) (x y z: X) (a: list X),
    x <> z -> y <> z -> Count eq_dec a z (Swap eq_dec x y l)
                           = Count eq_dec (Swap eq_dec x y a) z l.
Proof.
  intros.
  rewrite Count_full_swap with (x := x) (y := y).
  destruct (eq_dec x z). apply H in e. contradiction.
  destruct (eq_dec y z). apply H0 in e. contradiction.
  rewrite Swap_trivial_swap_swap. reflexivity.
Qed.


Theorem Count_forall_swap :
  forall (l: list X) (x y: X),
    (forall a, Count eq_dec a x l
                 = Count eq_dec (Swap eq_dec x y a) y l)
     -> (forall a, Count eq_dec a y l
                 = Count eq_dec (Swap eq_dec x y a) x l).
Proof.
  intro l. induction l; intros. reflexivity.
  simpl in H. simpl.
  destruct (eq_dec y a); destruct (eq_dec x a).
  rewrite e in H. rewrite <- e0 in H.
  rewrite e. rewrite <- e0. apply H. rewrite H.
  repeat rewrite Swap_trivial_swap_swap. reflexivity.
  rewrite H. rewrite Swap_trivial_swap_swap. reflexivity.
  rewrite H. rewrite Swap_trivial_swap_swap. reflexivity.
Qed.


Theorem Count_swap_extend :
  forall (u l base: list X) (x y: X),
    NoDup base ->
    (forall a z, NoDup a ->
           Count eq_dec a z l
                 = Count eq_dec (Swap eq_dec x y a) 
             (if eq_dec x z then y else if eq_dec y z then x else z) l)
     -> (forall a z, NoDup a ->
       Count eq_dec a z (u ++ l)
         = Count eq_dec (Swap eq_dec x y a) 
             (if eq_dec x z then y else if eq_dec y z then x else z)
         ((Swap eq_dec x y u)++l)).
Proof.
  intro u. induction u; intros.
  destruct (eq_dec x z). rewrite <- e. rewrite H0.
  destruct (eq_dec x x). reflexivity. contradiction. assumption.
  destruct (eq_dec x y). rewrite e. rewrite Swap_trivial_id.
  destruct (eq_dec y z). rewrite <- e0. reflexivity. reflexivity.
  destruct (eq_dec y z). rewrite H0.
  destruct (eq_dec x z). apply n in e0. contradiction.
  destruct (eq_dec y z). reflexivity. apply n2 in e. contradiction.
  assumption. rewrite H0.
  destruct (eq_dec x z). apply n in e. contradiction.
  destruct (eq_dec y z). apply n1 in e. contradiction.
  reflexivity. assumption.

  (* lemme *)
  assert (H4: forall w w', NoDup w' ->
    list_sum (map (fun y0 : X => Count eq_dec w' y0 (u ++ l)) w) =
    list_sum
      (map
         (fun y0 : X =>
          Count eq_dec (Swap eq_dec x y w') y0 (Swap eq_dec x y u ++ l))
     (Swap eq_dec x y w))).
  intro w. induction w; intros. reflexivity.
  simpl. rewrite IHw.
  rewrite IHu with (base := base) (x := x) (y := y). reflexivity.
  assumption. assumption. assumption. assumption.
  (* fin du lemme *)

  assert (~ In z (remove eq_dec z a0)). apply remove_In.
  assert (NoDup (remove eq_dec z a0)). apply NoDup_remove. assumption.

  destruct (in_dec eq_dec x a0); destruct (in_dec eq_dec y a0).

  (* premier cas *)
  destruct (eq_dec x z). simpl.
  destruct (eq_dec z a). rewrite <- e0. rewrite <- e.
  rewrite <- e in H2. rewrite <- e in H3.
  replace (remove eq_dec y (Swap eq_dec x y a0))
    with (Swap eq_dec x y (remove eq_dec x a0)).
  destruct (eq_dec x x). destruct (remove eq_dec x a0).
  destruct (eq_dec y y). reflexivity. contradiction.
  destruct (eq_dec y y).
  replace (match Swap eq_dec x y (x0 :: l0) with
    | [] => 1
    | _ :: _ =>
        list_sum
          (map
             (fun y0 : X =>
              Count eq_dec (Swap eq_dec x y (x0 :: l0)) y0
                (Swap eq_dec x y u ++ l)) (Swap eq_dec x y (x0 :: l0)))
    end) with (list_sum
          (map
             (fun y0 : X =>
              Count eq_dec (Swap eq_dec x y (x0 :: l0)) y0
                (Swap eq_dec x y u ++ l)) (Swap eq_dec x y (x0 :: l0)))).

  
  apply H4. assumption.

  assert (length (Swap eq_dec x y (x0 :: l0)) = length (x0::l0)).
  apply Swap_length.
  destruct (Swap eq_dec x y (x0::l0)). inversion H5. reflexivity.
  contradiction. contradiction.
  rewrite Swap_trivial_swap. rewrite <- Swap_remove.
  rewrite Swap_trivial_swap. reflexivity.

  replace (remove eq_dec y (Swap eq_dec x y a0))
    with (Swap eq_dec x y (remove eq_dec x a0)).
  rewrite <- e in H2. rewrite <- e in H3. rewrite <- e.
  destruct (eq_dec x a). rewrite e0 in e. rewrite e in n. contradiction.
  destruct (eq_dec y a).
  destruct (eq_dec y x). rewrite e0 in e1. rewrite e1 in n0. contradiction.

  rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x x). reflexivity. contradiction. assumption.
  assumption. assumption.

  destruct (eq_dec y a). apply n1 in e0. contradiction.
  rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x x). reflexivity. contradiction. assumption.
  assumption. assumption.
  rewrite Swap_trivial_swap. rewrite <- Swap_remove.
  rewrite Swap_trivial_swap. reflexivity.

  destruct (eq_dec y z). rewrite <- e. rewrite <- e in H2. rewrite <- e in H3.
  simpl.
  destruct (eq_dec y a).
  destruct (eq_dec x a).
  rewrite <- e1 in e0. rewrite e0 in e. apply n in e. contradiction.
  destruct (eq_dec x x).

  replace (remove eq_dec x (Swap eq_dec x y a0))
    with (Swap eq_dec x y (remove eq_dec y a0)).
  destruct (remove eq_dec y a0). reflexivity.

  replace (match Swap eq_dec x y (x0 :: l0) with
    | [] => 1
    | _ :: _ =>
        list_sum
          (map
             (fun y0 : X =>
              Count eq_dec (Swap eq_dec x y (x0 :: l0)) y0
                (Swap eq_dec x y u ++ l)) (Swap eq_dec x y (x0 :: l0)))
    end) with (list_sum
          (map
             (fun y0 : X =>
              Count eq_dec (Swap eq_dec x y (x0 :: l0)) y0
                (Swap eq_dec x y u ++ l)) (Swap eq_dec x y (x0 :: l0)))).
  apply H4. assumption.

  assert (length (Swap eq_dec x y (x0 :: l0)) = length (x0::l0)).
  apply Swap_length.
  destruct (Swap eq_dec x y (x0::l0)). inversion H5. reflexivity.
  rewrite <- Swap_remove. reflexivity. contradiction.

  destruct (eq_dec x a).
  destruct (eq_dec x y). rewrite e1 in e0. apply n0 in e0. contradiction.

  rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x y). apply n1 in e1. contradiction.
  destruct (eq_dec y y). reflexivity. contradiction. assumption.
  assumption. assumption.

  destruct (eq_dec x a). apply n1 in e0. contradiction.
  rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x y). rewrite e0. reflexivity.
  destruct (eq_dec y y). reflexivity. contradiction.
  assumption. assumption. assumption.

  simpl.
  destruct (eq_dec z a).
  destruct (eq_dec x a).
  rewrite <- e0 in e. rewrite e in n. contradiction.
  destruct (eq_dec y a).
  rewrite <- e0 in e. rewrite e in n0. contradiction.

  rewrite Swap_remove_2. destruct (remove eq_dec z a0).
  destruct (eq_dec z a). reflexivity.
  apply n3 in e. contradiction.
  destruct (eq_dec z a).

  replace (match Swap eq_dec x y (x0 :: l0) with
    | [] => 1
    | _ :: _ =>
        list_sum
          (map
             (fun y0 : X =>
              Count eq_dec (Swap eq_dec x y (x0 :: l0)) y0
                (Swap eq_dec x y u ++ l)) (Swap eq_dec x y (x0 :: l0)))
    end) with (list_sum
          (map
             (fun y0 : X =>
              Count eq_dec (Swap eq_dec x y (x0 :: l0)) y0
                (Swap eq_dec x y u ++ l)) (Swap eq_dec x y (x0 :: l0)))).
  apply H4. assumption.

  assert (length (Swap eq_dec x y (x0 :: l0)) = length (x0::l0)).
  apply Swap_length.
  destruct (Swap eq_dec x y (x0::l0)). inversion H5. reflexivity.
  apply n3 in e. contradiction.
  rewrite e. intro. rewrite H5 in n1. contradiction.
  rewrite e. intro. rewrite H5 in n2. contradiction.

  destruct (eq_dec x a).
  destruct (eq_dec z y). rewrite e0 in n0. contradiction.

  rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x z). apply n in e0. contradiction.
  destruct (eq_dec y z). apply n0 in e0. contradiction.
  reflexivity. assumption. assumption. assumption.

  destruct (eq_dec y a).
  destruct (eq_dec z x). rewrite e0 in n. contradiction.

  rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x z). apply n in e0. contradiction.
  destruct (eq_dec y z). apply n0 in e0. contradiction.
  reflexivity. assumption. assumption. assumption.

  destruct (eq_dec z a). apply n1 in e. contradiction.

  rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x z). apply n in e. contradiction.
  destruct (eq_dec y z). apply n0 in e. contradiction.
  reflexivity. assumption. assumption. assumption.

  (* second cas *)
  simpl. destruct (eq_dec z a). destruct (eq_dec x z).
  rewrite <- e0. rewrite <- e0 in H2. rewrite <- e0 in H3.
  destruct (eq_dec x a). destruct (eq_dec y y).
  replace (remove eq_dec y (Swap eq_dec x y a0))
    with (Swap eq_dec x y (remove eq_dec x a0)).
  destruct (remove eq_dec x a0). reflexivity.

  replace (match Swap eq_dec x y (x0 :: l0) with
    | [] => 1
    | _ :: _ =>
        list_sum
          (map
             (fun y0 : X =>
              Count eq_dec (Swap eq_dec x y (x0 :: l0)) y0
                (Swap eq_dec x y u ++ l)) (Swap eq_dec x y (x0 :: l0)))
    end) with (list_sum
          (map
             (fun y0 : X =>
              Count eq_dec (Swap eq_dec x y (x0 :: l0)) y0
                (Swap eq_dec x y u ++ l)) (Swap eq_dec x y (x0 :: l0)))).
  apply H4. assumption.

  assert (length (Swap eq_dec x y (x0 :: l0)) = length (x0::l0)).
  apply Swap_length.
  destruct (Swap eq_dec x y (x0::l0)). inversion H5. reflexivity.

  rewrite Swap_trivial_swap. rewrite <- Swap_remove.
  rewrite Swap_trivial_swap. reflexivity. contradiction.
  rewrite e in e0. apply n0 in e0. contradiction.

  destruct (eq_dec y z). rewrite <- e0. destruct (eq_dec x a).
  rewrite e1 in n0. rewrite e in n0. contradiction.
  destruct (eq_dec y a). destruct (eq_dec x x).
  replace (remove eq_dec x (Swap eq_dec x y a0))
    with (Swap eq_dec x y (remove eq_dec y a0)).
  rewrite <- e0 in H2. rewrite <- e0 in H3.
  destruct (remove eq_dec y a0). reflexivity.

  replace (match Swap eq_dec x y (x0 :: l0) with
    | [] => 1
    | _ :: _ =>
        list_sum
          (map
             (fun y0 : X =>
              Count eq_dec (Swap eq_dec x y (x0 :: l0)) y0
                (Swap eq_dec x y u ++ l)) (Swap eq_dec x y (x0 :: l0)))
    end) with (list_sum
          (map
             (fun y0 : X =>
              Count eq_dec (Swap eq_dec x y (x0 :: l0)) y0
                (Swap eq_dec x y u ++ l)) (Swap eq_dec x y (x0 :: l0)))).
  apply H4. assumption.

  assert (length (Swap eq_dec x y (x0 :: l0)) = length (x0::l0)).
  apply Swap_length.
  destruct (Swap eq_dec x y (x0::l0)). inversion H5. reflexivity.
  rewrite Swap_remove. reflexivity. contradiction.
  rewrite e0 in n2. apply n2 in e. contradiction.

  destruct (eq_dec x a). rewrite <- e in e0. apply n0 in e0. contradiction.
  destruct (eq_dec y a). rewrite <- e in e0. apply n1 in e0. contradiction.
  destruct (eq_dec z a).
  replace (remove eq_dec z (Swap eq_dec x y a0))
    with (Swap eq_dec x y (remove eq_dec z a0)).
  destruct (remove eq_dec z a0). reflexivity.

  replace (match Swap eq_dec x y (x0 :: l0) with
    | [] => 1
    | _ :: _ =>
        list_sum
          (map
             (fun y0 : X =>
              Count eq_dec (Swap eq_dec x y (x0 :: l0)) y0
                (Swap eq_dec x y u ++ l)) (Swap eq_dec x y (x0 :: l0)))
    end) with (list_sum
          (map
             (fun y0 : X =>
              Count eq_dec (Swap eq_dec x y (x0 :: l0)) y0
                (Swap eq_dec x y u ++ l)) (Swap eq_dec x y (x0 :: l0)))).
  apply H4. assumption.

  assert (length (Swap eq_dec x y (x0 :: l0)) = length (x0::l0)).
  apply Swap_length.
  destruct (Swap eq_dec x y (x0::l0)). inversion H5. reflexivity.
  rewrite Swap_remove_2. reflexivity.
  intro. rewrite H5 in n0. contradiction.
  intro. rewrite H5 in n1. contradiction.
  apply n4 in e. contradiction.

  destruct (eq_dec x z). destruct (eq_dec x a).
  rewrite e in e0. apply n0 in e0. contradiction.
  destruct (eq_dec y a).
  destruct (eq_dec y x). rewrite e1 in n. apply n in i. contradiction.
  rewrite <- e. rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x x). reflexivity. contradiction. assumption.
  assumption. assumption.

  destruct (eq_dec y a). apply n2 in e0. contradiction.
  rewrite <- e. rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x x). reflexivity. contradiction. assumption.
  assumption. assumption.

  destruct (eq_dec y z).
  destruct (eq_dec x a).
  destruct (eq_dec x y). rewrite e in e1. apply n1 in e1. contradiction.
  rewrite <- e. rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x y). rewrite e1 in i. apply n in i. contradiction.
  destruct (eq_dec y y). reflexivity. contradiction.
  assumption. assumption. assumption.

  destruct (eq_dec y a). rewrite e in e0. apply n0 in e0. contradiction.
  destruct (eq_dec x a). apply n2 in e0. contradiction.
  rewrite <- e. rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x y). rewrite e0 in i. apply n in i. contradiction.
  destruct (eq_dec y y). reflexivity. contradiction.
  assumption. assumption. assumption.

  destruct (eq_dec x a). destruct (eq_dec z y).
  rewrite e0 in n2. contradiction.
  rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x z). apply n1 in e0. contradiction.
  destruct (eq_dec y z). apply n2 in e0. contradiction.
  reflexivity. assumption. assumption. assumption.

  destruct (eq_dec y a).
  destruct (eq_dec z x). rewrite e0 in n1. contradiction.
  rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x z). apply n1 in e0. contradiction.
  destruct (eq_dec y z). apply n2 in e0. contradiction.
  reflexivity. assumption. assumption. assumption.

  destruct (eq_dec z a). apply n0 in e. contradiction.
  rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x z). apply n1 in e. contradiction.
  destruct (eq_dec y z). apply n2 in e. contradiction.
  reflexivity. assumption. assumption. assumption.

  (* troisième cas *)
  simpl. destruct (eq_dec z a). destruct (eq_dec x z).
  rewrite <- e0. rewrite <- e0 in H2. rewrite <- e0 in H3.
  destruct (eq_dec x a). destruct (eq_dec y y).
  replace (remove eq_dec y (Swap eq_dec x y a0))
    with (Swap eq_dec x y (remove eq_dec x a0)).
  destruct (remove eq_dec x a0). reflexivity.

  replace (match Swap eq_dec x y (x0 :: l0) with
    | [] => 1
    | _ :: _ =>
        list_sum
          (map
             (fun y0 : X =>
              Count eq_dec (Swap eq_dec x y (x0 :: l0)) y0
                (Swap eq_dec x y u ++ l)) (Swap eq_dec x y (x0 :: l0)))
    end) with (list_sum
          (map
             (fun y0 : X =>
              Count eq_dec (Swap eq_dec x y (x0 :: l0)) y0
                (Swap eq_dec x y u ++ l)) (Swap eq_dec x y (x0 :: l0)))).
  apply H4. assumption.

  assert (length (Swap eq_dec x y (x0 :: l0)) = length (x0::l0)).
  apply Swap_length.
  destruct (Swap eq_dec x y (x0::l0)). inversion H5. reflexivity.

  rewrite Swap_trivial_swap. rewrite <- Swap_remove.
  rewrite Swap_trivial_swap. reflexivity. contradiction.
  rewrite e in e0. apply n0 in e0. contradiction.

  destruct (eq_dec y z). rewrite <- e0. destruct (eq_dec x a).
  rewrite e1 in n0. rewrite e in n0. contradiction.
  destruct (eq_dec y a). destruct (eq_dec x x).
  replace (remove eq_dec x (Swap eq_dec x y a0))
    with (Swap eq_dec x y (remove eq_dec y a0)).
  rewrite <- e0 in H2. rewrite <- e0 in H3.
  destruct (remove eq_dec y a0). reflexivity.

  replace (match Swap eq_dec x y (x0 :: l0) with
    | [] => 1
    | _ :: _ =>
        list_sum
          (map
             (fun y0 : X =>
              Count eq_dec (Swap eq_dec x y (x0 :: l0)) y0
                (Swap eq_dec x y u ++ l)) (Swap eq_dec x y (x0 :: l0)))
    end) with (list_sum
          (map
             (fun y0 : X =>
              Count eq_dec (Swap eq_dec x y (x0 :: l0)) y0
                (Swap eq_dec x y u ++ l)) (Swap eq_dec x y (x0 :: l0)))).
  apply H4. assumption.

  assert (length (Swap eq_dec x y (x0 :: l0)) = length (x0::l0)).
  apply Swap_length.
  destruct (Swap eq_dec x y (x0::l0)). inversion H5. reflexivity.
  rewrite Swap_remove. reflexivity. contradiction.
  rewrite e0 in n2. apply n2 in e. contradiction.

  destruct (eq_dec x a). rewrite <- e in e0. apply n0 in e0. contradiction.
  destruct (eq_dec y a). rewrite <- e in e0. apply n1 in e0. contradiction.
  destruct (eq_dec z a).
  replace (remove eq_dec z (Swap eq_dec x y a0))
    with (Swap eq_dec x y (remove eq_dec z a0)).
  destruct (remove eq_dec z a0). reflexivity.

  replace (match Swap eq_dec x y (x0 :: l0) with
    | [] => 1
    | _ :: _ =>
        list_sum
          (map
             (fun y0 : X =>
              Count eq_dec (Swap eq_dec x y (x0 :: l0)) y0
                (Swap eq_dec x y u ++ l)) (Swap eq_dec x y (x0 :: l0)))
    end) with (list_sum
          (map
             (fun y0 : X =>
              Count eq_dec (Swap eq_dec x y (x0 :: l0)) y0
                (Swap eq_dec x y u ++ l)) (Swap eq_dec x y (x0 :: l0)))).
  apply H4. assumption.

  assert (length (Swap eq_dec x y (x0 :: l0)) = length (x0::l0)).
  apply Swap_length.
  destruct (Swap eq_dec x y (x0::l0)). inversion H5. reflexivity.
  rewrite Swap_remove_2. reflexivity.
  intro. rewrite H5 in n0. contradiction.
  intro. rewrite H5 in n1. contradiction.
  apply n4 in e. contradiction.

  destruct (eq_dec x z). destruct (eq_dec x a).
  rewrite e in e0. apply n0 in e0. contradiction.
  destruct (eq_dec y a).
  destruct (eq_dec y x). rewrite e1 in i. apply n in i. contradiction.
  rewrite <- e. rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x x). reflexivity. contradiction. assumption.
  assumption. assumption.

  destruct (eq_dec y a). apply n2 in e0. contradiction.
  rewrite <- e. rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x x). reflexivity. contradiction. assumption.
  assumption. assumption.

  destruct (eq_dec y z).
  destruct (eq_dec x a).
  destruct (eq_dec x y). rewrite e in e1. apply n1 in e1. contradiction.
  rewrite <- e. rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x y). rewrite e1 in n. apply n in i. contradiction.
  destruct (eq_dec y y). reflexivity. contradiction.
  assumption. assumption. assumption.

  destruct (eq_dec y a). rewrite e in e0. apply n0 in e0. contradiction.
  destruct (eq_dec x a). apply n2 in e0. contradiction.
  rewrite <- e. rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x y). rewrite e0 in n. apply n in i. contradiction.
  destruct (eq_dec y y). reflexivity. contradiction.
  assumption. assumption. assumption.

  destruct (eq_dec x a). destruct (eq_dec z y).
  rewrite e0 in n2. contradiction.
  rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x z). apply n1 in e0. contradiction.
  destruct (eq_dec y z). apply n2 in e0. contradiction.
  reflexivity. assumption. assumption. assumption.

  destruct (eq_dec y a).
  destruct (eq_dec z x). rewrite e0 in n1. contradiction.
  rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x z). apply n1 in e0. contradiction.
  destruct (eq_dec y z). apply n2 in e0. contradiction.
  reflexivity. assumption. assumption. assumption.

  destruct (eq_dec z a). apply n0 in e. contradiction.
  rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x z). apply n1 in e. contradiction.
  destruct (eq_dec y z). apply n2 in e. contradiction.
  reflexivity. assumption. assumption. assumption.

  (* quatrième cas *)
  simpl. destruct (eq_dec z a). destruct (eq_dec x z).
  rewrite <- e0. rewrite <- e0 in H2. rewrite <- e0 in H3.
  destruct (eq_dec x a). destruct (eq_dec y y).
  replace (remove eq_dec y (Swap eq_dec x y a0))
    with (Swap eq_dec x y (remove eq_dec x a0)).
  destruct (remove eq_dec x a0). reflexivity.

  replace (match Swap eq_dec x y (x0 :: l0) with
    | [] => 1
    | _ :: _ =>
        list_sum
          (map
             (fun y0 : X =>
              Count eq_dec (Swap eq_dec x y (x0 :: l0)) y0
                (Swap eq_dec x y u ++ l)) (Swap eq_dec x y (x0 :: l0)))
    end) with (list_sum
          (map
             (fun y0 : X =>
              Count eq_dec (Swap eq_dec x y (x0 :: l0)) y0
                (Swap eq_dec x y u ++ l)) (Swap eq_dec x y (x0 :: l0)))).
  apply H4. assumption.

  assert (length (Swap eq_dec x y (x0 :: l0)) = length (x0::l0)).
  apply Swap_length.
  destruct (Swap eq_dec x y (x0::l0)). inversion H5. reflexivity.

  rewrite Swap_trivial_swap. rewrite <- Swap_remove.
  rewrite Swap_trivial_swap. reflexivity. contradiction.
  rewrite e in e0. apply n1 in e0. contradiction.

  destruct (eq_dec y z). rewrite <- e0. destruct (eq_dec x a).
  rewrite e1 in n1. rewrite e in n1. contradiction.
  destruct (eq_dec y a). destruct (eq_dec x x).
  replace (remove eq_dec x (Swap eq_dec x y a0))
    with (Swap eq_dec x y (remove eq_dec y a0)).
  rewrite <- e0 in H2. rewrite <- e0 in H3.
  destruct (remove eq_dec y a0). reflexivity.

  replace (match Swap eq_dec x y (x0 :: l0) with
    | [] => 1
    | _ :: _ =>
        list_sum
          (map
             (fun y0 : X =>
              Count eq_dec (Swap eq_dec x y (x0 :: l0)) y0
                (Swap eq_dec x y u ++ l)) (Swap eq_dec x y (x0 :: l0)))
    end) with (list_sum
          (map
             (fun y0 : X =>
              Count eq_dec (Swap eq_dec x y (x0 :: l0)) y0
                (Swap eq_dec x y u ++ l)) (Swap eq_dec x y (x0 :: l0)))).
  apply H4. assumption.

  assert (length (Swap eq_dec x y (x0 :: l0)) = length (x0::l0)).
  apply Swap_length.
  destruct (Swap eq_dec x y (x0::l0)). inversion H5. reflexivity.
  rewrite Swap_remove. reflexivity. contradiction.
  rewrite e in e0. apply n3 in e0. contradiction.

  destruct (eq_dec x a). rewrite <- e in e0. apply n1 in e0. contradiction.
  destruct (eq_dec y a). rewrite <- e in e0. apply n2 in e0. contradiction.
  destruct (eq_dec z a).
  replace (remove eq_dec z (Swap eq_dec x y a0))
    with (Swap eq_dec x y (remove eq_dec z a0)).
  destruct (remove eq_dec z a0). reflexivity.

  replace (match Swap eq_dec x y (x0 :: l0) with
    | [] => 1
    | _ :: _ =>
        list_sum
          (map
             (fun y0 : X =>
              Count eq_dec (Swap eq_dec x y (x0 :: l0)) y0
                (Swap eq_dec x y u ++ l)) (Swap eq_dec x y (x0 :: l0)))
    end) with (list_sum
          (map
             (fun y0 : X =>
              Count eq_dec (Swap eq_dec x y (x0 :: l0)) y0
                (Swap eq_dec x y u ++ l)) (Swap eq_dec x y (x0 :: l0)))).
  apply H4. assumption.

  assert (length (Swap eq_dec x y (x0 :: l0)) = length (x0::l0)).
  apply Swap_length.
  destruct (Swap eq_dec x y (x0::l0)). inversion H5. reflexivity.
  rewrite Swap_remove_2. reflexivity.
  intro. rewrite H5 in n1. contradiction.
  intro. rewrite H5 in n2. contradiction.
  apply n5 in e. contradiction.

  destruct (eq_dec x z). destruct (eq_dec x a).
  rewrite e in e0. apply n1 in e0. contradiction.
  destruct (eq_dec y a).
  destruct (eq_dec y x). rewrite e1 in e0. apply n2 in e0. contradiction.
  rewrite <- e. rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x x). reflexivity. contradiction. assumption.
  assumption. assumption.

  destruct (eq_dec y a). apply n3 in e0. contradiction.
  rewrite <- e. rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x x). reflexivity. contradiction. assumption.
  assumption. assumption.

  destruct (eq_dec y z).
  destruct (eq_dec x a).
  destruct (eq_dec x y). rewrite e in e1. apply n2 in e1. contradiction.
  rewrite <- e. rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x y). rewrite e1 in n. apply n3 in e1. contradiction.
  destruct (eq_dec y y). reflexivity. contradiction.
  assumption. assumption. assumption.

  destruct (eq_dec y a). rewrite e in e0. apply n1 in e0. contradiction.
  destruct (eq_dec x a). apply n3 in e0. contradiction.
  rewrite <- e. rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x y). rewrite e0. reflexivity.
  destruct (eq_dec y y). reflexivity. contradiction.
  assumption. assumption. assumption.

  destruct (eq_dec x a). destruct (eq_dec z y).
  rewrite e0 in n3. contradiction.
  rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x z). apply n2 in e0. contradiction.
  destruct (eq_dec y z). apply n3 in e0. contradiction.
  reflexivity. assumption. assumption. assumption.

  destruct (eq_dec y a).
  destruct (eq_dec z x). rewrite e0 in n2. contradiction.
  rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x z). apply n2 in e0. contradiction.
  destruct (eq_dec y z). apply n3 in e0. contradiction.
  reflexivity. assumption. assumption. assumption.

  destruct (eq_dec z a). apply n1 in e. contradiction.
  rewrite IHu with (base := base) (x := x) (y := y).
  destruct (eq_dec x z). apply n2 in e. contradiction.
  destruct (eq_dec y z). apply n3 in e. contradiction.
  reflexivity. assumption. assumption. assumption.
Qed.



(* TODO: is this really useful? *)
(*
Theorem Subsequence_rev_nodup_rev :
  forall (l: list X),
    Subsequence l (rev (nodup eq_dec (rev l))).
Proof.
  intros. apply Subsequence_rev. rewrite rev_involutive.
  apply Subsequence_nodup. apply Subsequence_id.
Qed.
 *)


Theorem Collect_subsets_nodup :
  forall (u v: list X) (x: X),
    In (x::v) (subsets_nodup eq_dec u)
         -> In (x::v) (Collect eq_dec (x::v) x u).
Proof.
  intro u. induction u; intros. simpl in H. destruct H. inversion H.
  contradiction.
  assert (I := H). apply subsets_nodup_NoDup in I.
  apply subsets_nodup_subsets in H.
  apply subsets_subsequence in H.
  apply Collect_subsequence_2. assumption.
  apply Permutation_refl. assumption.
Qed.


Theorem Collect_incl_subsets_nodup :
  forall (u v: list X) (x: X),
    incl (Collect eq_dec v x u) (subsets_nodup eq_dec u).
Proof.
  repeat intro. destruct a. apply Collect_nil in H. contradiction.
  assert (I := H). apply Collect_nodup_2 in I.
  apply Collect_subsequence in H. apply subsets_subsequence in H.
  apply nodup_fixed_point with (decA := eq_dec) in I.
  apply filter_In. split. assumption. rewrite I.
  destruct (list_eq_dec eq_dec (x0::a) (x0::a)). reflexivity.
  contradiction.
Qed.


Theorem Collect_no_change_subsets_nodup :
  forall (l: list X) (x: X),
    incl (subsets_nodup eq_dec (x::l)) (subsets_nodup eq_dec l)
      <-> Collect_no_change eq_dec x l.
Proof.
  intros. split; intro.
  assert ( Collect_no_change eq_dec x l
      \/ ~ Collect_no_change eq_dec x l). apply Collect_no_change_cases.
  destruct H0. assumption.
  apply Collect_no_change_exists in H0. destruct H0.
  assert False. apply H0.
  repeat intro. assert (I := H1). assert (J := I).
  assert (H2 := H1). apply Collect_nodup_2 in H1.
  assert (K := H1). apply nodup_fixed_point with (decA := eq_dec) in H1.
  apply Collect_subsequence in H2.
  apply subsets_subsequence in H2.
  assert (In a (subsets_nodup eq_dec (x::l))).
  apply filter_In. split. assumption. rewrite H1.
  destruct (list_eq_dec eq_dec a a). reflexivity. contradiction.
  destruct a. apply Collect_nil in I. contradiction.
  apply Collect_head in I. apply H in H3. rewrite <- I in H3.
  apply Collect_subsets_nodup in H3.

  assert (Permutation (x1::a) (nodup eq_dec (x1::x0))).
  assert (In (x1::a) (Collect eq_dec (nodup eq_dec (x1::x0)) x (x::l))).
  rewrite <- I. apply Collect_subsequence_3. rewrite I. assumption.
  rewrite Collect_defective in J. apply Collect_alphabet_incl_2 in J.
  repeat intro. apply nodup_In in H4. rewrite I. apply J. assumption.
  apply Collect_alphabet_incl in J. apply nodup_incl.
  apply incl_cons_inv in J. destruct J. assumption.
  apply Collect_subsequence in J. rewrite <- I in J. assumption.
  apply Collect_permutation in H4. assumption.
  apply NoDup_nodup. rewrite I. apply nodup_In. apply in_eq.
  apply Collect_alphabet_perm
           with (eq_dec := eq_dec) (l := l) (x := x) in H4.
  rewrite <- I in H4.
  apply Subsequence_in_2
         with (v := Collect eq_dec (nodup eq_dec (x::x0)) x l).
  rewrite Collect_defective. apply Collect_alphabet_nodup.
  apply Permutation_in with (l := Collect eq_dec (x::a) x l).
  assumption. rewrite <- I. assumption. contradiction.

  repeat intro. specialize (H a). destruct a. apply subsets_nodup_nil_in.

  assert (H1 := H0). apply subsets_nodup_NoDup in H0.
  apply subsets_nodup_subsets in H1.
  apply subsets_subsequence in H1.
  apply filter_In. split. apply subsets_subsequence.
  destruct (eq_dec x x0). rewrite <- e in H.
  apply Collect_subsequence with (eq_dec := eq_dec) (a := x::a) (x := x).
  rewrite <- H. rewrite e. apply Collect_subsequence_2. assumption.
  apply Permutation_refl. rewrite e in H1. assumption.
  apply Subsequence_cons_diff in H1. assumption. assumption.
  apply nodup_fixed_point with (decA := eq_dec) in H0. rewrite H0.
  destruct (list_eq_dec eq_dec (x0::a) (x0::a)). reflexivity.
  contradiction.
Qed.


Theorem Count_no_change_subsets_nodup :
  forall (l: list X) (x: X),
    incl (subsets_nodup eq_dec (x::l)) (subsets_nodup eq_dec l)
      <-> Count_no_change eq_dec x l.
Proof.
  intros. split; intro. apply Count_Collect_no_change.
  apply Collect_no_change_subsets_nodup. assumption.
  apply Count_Collect_no_change in H.
  apply Collect_no_change_subsets_nodup. assumption.
Qed.


(*
Theorem Collect_no_change_subsets_nodup_eq :
  forall (l: list X) (x: X),
    subsets_nodup eq_dec (x::l) = subsets_nodup eq_dec l
      -> Collect_no_change eq_dec x l.
Proof.
  intros.
  assert ( Collect_no_change eq_dec x l
      \/ ~ Collect_no_change eq_dec x l). apply Collect_no_change_cases.
  destruct H0. assumption.
  apply Collect_no_change_exists in H0. destruct H0.
  assert False. apply H0.
  repeat intro. assert (I := H1). assert (J := I).
  assert (H2 := H1). apply Collect_nodup_2 in H1.
  assert (K := H1). apply nodup_fixed_point with (decA := eq_dec) in H1.
  apply Collect_subsequence in H2.
  apply subsets_subsequence in H2.
  assert (In a (subsets_nodup eq_dec (x::l))).
  apply filter_In. split. assumption. rewrite H1.
  destruct (list_eq_dec eq_dec a a). reflexivity. contradiction.
  destruct a. apply Collect_nil in I. contradiction.
  apply Collect_head in I. rewrite H in H3. rewrite <- I in H3.
  apply Collect_subsets_nodup in H3.

  assert (Permutation (x1::a) (nodup eq_dec (x1::x0))).
  assert (In (x1::a) (Collect eq_dec (nodup eq_dec (x1::x0)) x (x::l))).
  rewrite <- I. apply Collect_subsequence_3. rewrite I. assumption.
  rewrite Collect_defective in J. apply Collect_alphabet_incl_2 in J.
  repeat intro. apply nodup_In in H4. rewrite I. apply J. assumption.
  apply Collect_alphabet_incl in J. apply nodup_incl.
  apply incl_cons_inv in J. destruct J. assumption.
  apply Collect_subsequence in J. rewrite <- I in J. assumption.
  apply Collect_permutation in H4. assumption.
  apply NoDup_nodup. rewrite I. apply nodup_In. apply in_eq.
  apply Collect_alphabet_perm
           with (eq_dec := eq_dec) (l := l) (x := x) in H4.
  rewrite <- I in H4.
  apply Subsequence_in_2
         with (v := Collect eq_dec (nodup eq_dec (x::x0)) x l).
  rewrite Collect_defective. apply Collect_alphabet_nodup.
  apply Permutation_in with (l := Collect eq_dec (x::a) x l).
  assumption. rewrite <- I. assumption. assumption. contradiction.
Qed.


Theorem Count_no_change_subsets_nodup_eq :
  forall (l: list X) (x: X),
    subsets_nodup eq_dec (x::l) = subsets_nodup eq_dec l
      -> Count_no_change eq_dec x l.
Proof.
  intros. apply Count_Collect_no_change.
  apply Collect_no_change_subsets_nodup_eq. assumption.
Qed.
 *)


Theorem Collect_no_change_dec :
  forall (l: list X) (x: X),
    { Collect_no_change eq_dec x l } + { ~ Collect_no_change eq_dec x l }.
Proof.
  intros.
  assert ({incl (subsets_nodup eq_dec (x::l)) (subsets_nodup eq_dec l)}
      + { ~ incl (subsets_nodup eq_dec (x::l)) (subsets_nodup eq_dec l)}).
  apply ListDec.incl_dec. apply (list_eq_dec eq_dec). destruct H.
  apply Collect_no_change_subsets_nodup in i. left. assumption.
  right. intro. apply n. apply Collect_no_change_subsets_nodup. assumption.
Qed.


Theorem Count_no_change_dec :
  forall (l: list X) (x: X),
    { Count_no_change eq_dec x l } + { ~ Count_no_change eq_dec x l }.
Proof.
  intros.
  assert ({Collect_no_change eq_dec x l} + {~ Collect_no_change eq_dec x l}).
  apply Collect_no_change_dec. destruct H.
  apply Count_Collect_no_change in c. left. assumption.
  right. intro. apply n. apply Count_Collect_no_change. assumption.
Qed.


Theorem Permutation_as_subsequence :
  forall (u base: list X),
    Permutation base u ->
      Subsequence (fold_right (app (A:=X)) nil (repeat base (length base))) u.
Proof.
  intro u. induction u; intros.
  apply Subsequence_nil_r. simpl.
  assert (In a base). apply Permutation_in with (l := a::u).
  apply Permutation_sym. assumption. apply in_eq.
  apply in_split in H0. destruct H0. destruct H0. exists x.
  exists (x0++(fold_right (app (A:=X)) [] (repeat base (pred (length base))))).
  rewrite app_comm_cons. rewrite app_assoc. rewrite <- H0. split.
  replace (length base) with (S (pred (length base))) at 1. reflexivity.
  apply Nat.succ_pred_pos. destruct base. apply app_cons_not_nil in H0.
  contradiction. apply Nat.lt_0_succ.
  specialize (IHu (x++x0)). rewrite H0 in H.
  apply Permutation_sym in H. apply Permutation_cons_app_inv in H.
  apply Permutation_sym in H. apply IHu in H.
  replace (pred (length base)) with (length (x++x0)).
  apply Subsequence_trans
    with (l2 :=
        fold_right (app (A:=X)) [] (repeat (x ++ x0) (length (x ++ x0)))).
  apply Subsequence_app2_l.

  assert (forall n (u v: list X), Subsequence u v ->
    Subsequence (fold_right (app (A:=X)) nil (repeat u n))
                (fold_right (app (A:=X)) nil (repeat v n))).
  intro n. induction n; intros. apply Subsequence_id.
  simpl. apply Subsequence_app. assumption. apply IHn. assumption.
  apply H1. rewrite H0. apply Subsequence_app. apply Subsequence_id.
  apply Subsequence_cons_l. apply Subsequence_id. assumption.
  rewrite H0. rewrite length_app. rewrite length_app.
  simpl. rewrite Nat.add_succ_r. rewrite Nat.pred_succ. reflexivity.
Qed.


Theorem subsets_Complete :
  forall (base l: list X),
    incl (subsets
               (fold_right (app (A:=X)) nil (repeat base (length base))))
         (subsets l) -> Complete_sequence l base.
Proof.
  repeat intro. apply Permutation_as_subsequence in H0.
  apply subsets_subsequence in H0. apply H in H0.
  apply subsets_subsequence in H0; assumption.
Qed.


Theorem subsets_nodup_Complete :
  forall (base l: list X), NoDup base ->
    incl (subsets_nodup eq_dec
               (fold_right (app (A:=X)) nil (repeat base (length base))))
         (subsets_nodup eq_dec l) -> Complete_sequence l base.
Proof.
  repeat intro.
  assert (H2 := H1). apply Permutation_as_subsequence in H1.
  apply subsets_subsequence in H1. apply Permutation_NoDup in H2.
  apply subsets_NoDup_nodup with (eq_dec := eq_dec) in H1. apply H0 in H1.
  apply subsets_nodup_subsets in H1. apply subsets_subsequence.
  assumption. assumption. assumption.
Qed.


Theorem Complete_sequence_example :
  forall (base: list X),
    Complete_sequence
        (fold_right (app (A:=X)) nil (repeat base (length base))) base.
Proof.
  intro. apply subsets_Complete. apply incl_refl.
Qed.


Theorem Complete_sequence_example_2 :
  forall (n: nat) (base: list X),
    incl (fold_right (app (A:=X)) [] (repeat base n)) base.
Proof.
  intro n. induction n; repeat intro.
  apply in_nil in H. contradiction.
  simpl in H. apply in_app_or in H. destruct H. assumption.
  apply IHn. assumption.
Qed.


Theorem subsets_nodup_Complete_equiv :
  forall (base l: list X), NoDup base ->
    (incl (subsets_nodup eq_dec
               (fold_right (app (A:=X)) nil (repeat base (length base))))
         (subsets_nodup eq_dec l) <-> Complete_sequence l base).
Proof.
  intros. split; repeat intro.
  assert (H2 := H1). apply Permutation_as_subsequence in H1.
  apply subsets_subsequence in H1. apply Permutation_NoDup in H2.
  apply subsets_NoDup_nodup with (eq_dec := eq_dec) in H1. apply H0 in H1.
  apply subsets_nodup_subsets in H1. apply subsets_subsequence.
  assumption. assumption. assumption.
  assert (H2 := H1). apply subsets_nodup_NoDup in H2.
  apply subsets_nodup_subsets in H1. apply subsets_subsequence in H1.
  apply subsets_NoDup_nodup. assumption. apply subsets_subsequence.
  assert (exists w, Permutation base (w++a)).
  assert (forall (a' b': list X),
     Subsequence (fold_right (app (A:=X)) [] (repeat b' (length b'))) a'
     -> NoDup a' -> exists w', Permutation b' (w'++a')).
  intro a'. induction a'; intros. exists b'. rewrite app_nil_r.
  apply Permutation_refl. simpl in H3. destruct H3. destruct H3. destruct H3.
  assert (forall n (w: list X) z,
    In z (fold_right (app (A:=X)) nil (repeat w n)) -> In z w).
  intro n. induction n; intros. apply in_nil in H6. contradiction.
  simpl in H6. apply in_app_or in H6. destruct H6. assumption.
  apply IHn. assumption.
  assert (In a0 b'). apply H6 with (n := length b'). rewrite H3. apply in_elt.
  assert (incl a' b'). repeat intro. apply Subsequence_incl in H5.
  apply H5 in H8. apply H6 with (n := length b').
  rewrite H3. apply in_split in H8. destruct H8. destruct H8. rewrite H8.
  apply in_or_app. right. apply in_cons. apply in_or_app. right. apply in_eq.
  apply incl_cons with (a := a0) in H8.
  assert (forall (t s: list X), NoDup t -> incl t s ->
                       exists r, Permutation s (r++t)).
  intro t. induction t; intros. exists s. rewrite app_nil_r.
  apply Permutation_refl. apply NoDup_cons_iff in H9. destruct H9.
  apply IHt with (s := s) in H11. destruct H11.
  assert (In a1 s). apply H10. apply in_eq. apply in_split in H12.
  destruct H12. destruct H12. assert (Permutation (a1::x2++x3) (x1++t)).
  apply Permutation_trans with (l' := s). rewrite H12.
  apply Permutation_middle. assumption.
  apply Permutation_in with (x := a1) in H13. apply in_app_or in H13.
  destruct H13. apply in_split in H13. destruct H13. destruct H13.
  rewrite H13 in H11. exists (x4++x5).
  apply Permutation_trans with (l' := (x4++a1::x5)++t). assumption.
  rewrite <- app_assoc. rewrite <- app_comm_cons. apply Permutation_elt.
  rewrite app_assoc. apply Permutation_refl. apply H9 in H13. contradiction.
  apply in_eq. apply incl_cons_inv in H10. destruct H10. assumption.
  apply H9 with (s := b') in H4. destruct H4. exists x1. assumption.
  assumption. assumption. apply H3; assumption. destruct H3. apply H0 in H3.
  apply Subsequence_split_3 in H3. assumption.
Qed.


Theorem Shortest_complete_sequence_length :
  forall (u b: list X),
    Shortest_complete_sequence u b -> length u <= (length b) * (length b).
Proof.
  intros.
  assert (Complete_sequence
              (fold_right (app (A:=X)) nil (repeat b (length b))) b).
  apply Complete_sequence_example.
  assert (length u <= (length b) * (length b)
      \/ (length b) * (length b) < length u).
  apply Nat.le_gt_cases. destruct H1. assumption.
  replace ((length b) * (length b))
    with (length (fold_right (app (A:=X)) [] (repeat b (length b)))) in H1.
  destruct H. apply H2 in H1. apply H1 in H0. contradiction.
  assert (forall n w,
    length (fold_right (app (A:=X)) [] (repeat w n)) =
       n * length w).
  intro n. induction n; intro. reflexivity. simpl.
  rewrite length_app. f_equal. apply IHn. apply H2.
Qed.


Theorem Complete_sequence_incl_base :
  forall (u base: list X),
    Complete_sequence u base
     -> exists w, Complete_sequence w base
                     /\ length w <= length u /\ incl w base.
Proof.
  assert (forall w b, incl w b
    -> filter (fun e => if (in_dec eq_dec e b)
                                      then true else false) w = w).
  intro w. induction w; intros. reflexivity.
  apply incl_cons_inv in H. destruct H.
  simpl. destruct (in_dec eq_dec a b). f_equal. apply IHw. assumption.
  apply n in H. contradiction.

  intros. exists (filter (fun e => if (in_dec eq_dec e base)
                                      then true else false) u).
  split. repeat intro. assert (I := H1). apply H0 in H1.
  rewrite <- H with (w := v) (b := base).
  apply Subsequence_filter_2. assumption.
  repeat intro. apply Permutation_in with (l := v).
  apply Permutation_sym. assumption. assumption.
  split. apply filter_length_le.

  assert (forall w b,
  incl (filter (fun e => if (in_dec eq_dec e b)
                                      then true else false) w) b).
  intro w. induction w; intro. apply incl_nil_l.
  repeat intro. simpl in H1. destruct (in_dec eq_dec a b).
  destruct H1. rewrite <- H1. assumption. apply IHw in H1.
  assumption. apply IHw in H1. assumption. apply H1.
Qed.


Theorem Complete_sequence_filter_base :
  forall (u base: list X),
    Complete_sequence u base
      <-> Complete_sequence
         (filter (fun e => if (in_dec eq_dec e base)
                            then true else false) u) base.
Proof.
  assert (forall w b, incl w b
    -> filter (fun e => if (in_dec eq_dec e b)
                                      then true else false) w = w).
  intro w. induction w; intros. reflexivity.
  apply incl_cons_inv in H. destruct H.
  simpl. destruct (in_dec eq_dec a b). f_equal. apply IHw. assumption.
  apply n in H. contradiction.
  intros. split; repeat intro.
  replace v with (filter (fun e => if (in_dec eq_dec e base)
                                      then true else false) v).
  apply Subsequence_filter_2. apply H0. assumption. apply H.
  repeat intro. apply Permutation_in with (l := v).
  apply Permutation_sym. assumption. assumption.
  apply H0 in H1. apply Subsequence_filter in H1. assumption.
Qed.


Theorem Complete_sequence_dec :
  forall (u base: list X), { Complete_sequence u base }
                         + {~ Complete_sequence u base}.
Proof.
  assert (forall base u,
      { find (fun e => if in_dec (list_eq_dec eq_dec) e (subsets u)
          then false else true) (all_permutations base) = None }
      + { ~ find (fun e => if in_dec (list_eq_dec eq_dec) e (subsets u)
          then false else true) (all_permutations base) = None }).
  intros.
  destruct (find (fun e => if in_dec (list_eq_dec eq_dec) e (subsets u)
          then false else true) (all_permutations base));
  [ right | left]; easy.
  intros. specialize (X0 base u). destruct X0.
  assert (forall x, In x (all_permutations base)
    -> (fun e => if in_dec (list_eq_dec eq_dec) e (subsets u)
          then false else true) x = false).
  apply find_none. assumption.
  left. repeat intro. apply all_permutations_Permutation in H0.
  apply H in H0. apply subsets_subsequence.
  destruct (in_dec (list_eq_dec eq_dec) v (subsets u)). assumption.
  inversion H0.
  right. intro.
  assert (forall x, In x (all_permutations base)
    -> (fun e => if in_dec (list_eq_dec eq_dec) e (subsets u)
          then false else true) x = false).
  intros. apply all_permutations_Permutation in H0.
  apply H in H0. apply subsets_subsequence in H0.
  destruct (in_dec (list_eq_dec eq_dec) x (subsets u)). reflexivity.
  apply n0 in H0. contradiction.
  assert (forall (w: list (list X)) f,
    (forall x, In x w -> f x = false) -> find f w = None).
  intro w. induction w; intros. easy. simpl. rewrite H1. apply IHw.
  intros. apply in_cons with (a := a) in H2. apply H1. assumption.
  apply in_eq. specialize (H1 (all_permutations base)). apply H1 in H0.
  apply n in H0. assumption.
Qed.


Theorem Shortest_complete_sequence_in_words :
  forall (u base: list X),
    Shortest_complete_sequence u base
      -> In u (words_below_length base (S (length base * length base))).
Proof.
  intros. apply words_below_length_in.
  apply Shortest_complete_sequence_incl_base. assumption.
  apply Nat.lt_succ_r. apply Shortest_complete_sequence_length. assumption.
Qed.


Theorem Shortest_complete_sequence_length_eq :
  forall (u base: list X),
    Shortest_complete_sequence u base
      -> length u = length (hd nil (filter
                       (fun e => if (Complete_sequence_dec e base)
                                    then true else false)
              (words_below_length base (S (length base * length base))))).
Proof.
  intros. assert (I := H).
  apply Shortest_complete_sequence_in_words in H.
  assert (In u
        (filter (fun e => if (Complete_sequence_dec e base)
                            then true else false)
          (words_below_length base (S (length base * length base))))).
  apply filter_In. split. assumption.
  destruct I. destruct (Complete_sequence_dec u base). reflexivity.
  apply n in H0. contradiction.
  assert ({ length u = length (hd [] (filter
              (fun e : list X =>
               if Complete_sequence_dec e base then true else false)
              (words_below_length base (S (length base * length base)))))}
  + { length u <> length (hd [] (filter (fun e : list X =>
               if Complete_sequence_dec e base then true else false)
          (words_below_length base (S (length base * length base)))))}).
  apply Nat.eq_dec. destruct H1. assumption.
  apply Nat.lt_gt_cases in n. destruct n.

  apply in_split in H0. destruct H0. destruct H0.
  destruct x. rewrite H0. reflexivity.
  assert (Subsequence
             (words_below_length base (S (length base * length base)))
             (words_below_length base (S (length base * length base)))).
  apply Subsequence_id.
  apply Subsequence_filter_3
     with (f := fun e : list X => if Complete_sequence_dec e base
                      then true else false) in H2.
  rewrite H0 in H2. apply Subsequence_split_2 in H2.
  destruct H2. destruct H2. destruct H2. destruct H3.
  destruct H3. destruct H3. destruct H3. rewrite H3 in H2.
  destruct H4. destruct H4. destruct H4. rewrite H4 in H2.
  replace ((x3 ++ l :: x4) ++ x5 ++ u :: x6)
     with (x3 ++ l::(x4++x5) ++ u::x6) in H2.
  symmetry in H2. apply words_below_length_ordering in H2.
  apply Nat.le_lt_trans with (n := length l) in H1.
  rewrite H0 in H1. apply Nat.lt_irrefl in H1. contradiction.
  assumption. rewrite app_comm_cons. rewrite app_assoc.
  symmetry. rewrite app_assoc. apply app_inv_tail_iff.
  rewrite app_comm_cons. rewrite app_assoc. reflexivity.

  destruct I. apply H3 in H1.
  assert (forall x, In x (filter (fun e : list X =>
             if Complete_sequence_dec e base then true else false)
       (words_below_length base (S (length base * length base))))
       -> Complete_sequence x base).
  intros. apply filter_In in H4. destruct H4.
  destruct (Complete_sequence_dec x base). assumption. inversion H5.
  destruct (filter (fun e : list X =>
           if Complete_sequence_dec e base then true else false)
          (words_below_length base (S (length base * length base)))).
  apply in_nil in H0. contradiction.
  simpl in H1. specialize (H4 l).
  assert (In l (l::l0)). apply in_eq. apply H4 in H5. apply H1 in H5.
  contradiction.
Qed.


Theorem Shortest_complete_sequence_instance :
  forall (base: list X),
    Shortest_complete_sequence
      (hd nil (filter (fun e => if (Complete_sequence_dec e base)
                                    then true else false)
          (words_below_length base (S (length base * length base))))) base.
Proof.
  intros.
  assert (Complete_sequence
        (fold_right (app (A:=X)) nil (repeat base (length base))) base).
  apply Complete_sequence_example.
  assert (In
        (fold_right (app (A:=X)) nil (repeat base (length base)))
        (words_below_length base (S (length base * length base)))).
  apply words_below_length_in.
  apply Complete_sequence_example_2.

  assert (forall n w,
    length (fold_right (app (A:=X)) [] (repeat w n)) =
       n * length w).
  intro n. induction n; intro. reflexivity. simpl.
  rewrite length_app. f_equal. apply IHn. rewrite H0.
  apply Nat.lt_succ_diag_r.

  assert (In
        (fold_right (app (A:=X)) nil (repeat base (length base)))
     (filter
        (fun e : list X =>
         if Complete_sequence_dec e base then true else false)
        (words_below_length base (S (length base * length base))))).
  apply filter_In. split. assumption.

  destruct (Complete_sequence_dec
    (fold_right (app (A:=X)) [] (repeat base (length base))) base).
  reflexivity. apply n in H. contradiction.

  assert (forall x, In x (filter (fun e : list X =>
             if Complete_sequence_dec e base then true else false)
       (words_below_length base (S (length base * length base))))
       -> Complete_sequence x base).
  intros. apply filter_In in H2. destruct H2.
  destruct (Complete_sequence_dec x base). assumption. inversion H3.

  split.
  destruct (
     (filter
        (fun e : list X =>
         if Complete_sequence_dec e base then true else false)
        (words_below_length base (S (length base * length base))))).
  apply in_nil in H1. contradiction. simpl.
  assert (In l (l::l0)). apply in_eq. apply H2 in H3. assumption.
  intros. intro.

  apply Complete_sequence_incl_base in H4. destruct H4.
  destruct H4. destruct H5.

  assert (In x (words_below_length base (S (length base * length base)))).
  apply words_below_length_in. assumption.
  apply Nat.le_lt_trans with (m := length v). assumption.
  apply Nat.lt_trans with (m := length 
    (hd []
       (filter
          (fun e : list X =>
           if Complete_sequence_dec e base then true else false)
          (words_below_length base (S (length base * length base)))))).
  assumption.
  apply words_below_length_length with (w := base).
  assert (forall (w: list (list X)) f,
    (filter f w) <> nil -> In (hd nil (filter f w)) w).
  intros.
  assert (forall (w: list (list X)) f x, In x (filter f w) -> In x w).
  intros. apply filter_In in H8. destruct H8. assumption.
  apply H8 with (f := f).
  destruct (filter f w). contradiction. simpl. left. reflexivity.
  apply H7.
  specialize (H7
    (words_below_length base (S (length base * length base)))
    (fun e : list X => if Complete_sequence_dec e base then true else false)).
  destruct (filter
    (fun e : list X => if Complete_sequence_dec e base then true else false)
    (words_below_length base (S (length base * length base)))).
  apply in_nil in H1. contradiction. easy.

  assert (In x (filter (fun e : list X =>
           if Complete_sequence_dec e base then true else false)
          (words_below_length base (S (length base * length base))))).
  apply filter_In. split. assumption.
  destruct (Complete_sequence_dec x base). reflexivity.
  apply n in H4. contradiction.
  apply in_split in H8. destruct H8. destruct H8.
  rewrite H8 in H3.
  destruct x0. simpl in H3.
  assert (length v < length v).
  apply Nat.lt_le_trans with (m := length x); assumption.
  apply Nat.lt_irrefl in H9. assumption. simpl in H3.
  apply Nat.le_lt_trans with (n := length x) in H3.

  assert (Subsequence
    (words_below_length base (S (length base * length base)))
    (words_below_length base (S (length base * length base)))).
  apply Subsequence_id.
  apply Subsequence_filter_3
     with (f := fun e : list X => if Complete_sequence_dec e base
                      then true else false) in H9.
  rewrite H8 in H9. apply Subsequence_split_2 in H9.
  destruct H9. destruct H9. destruct H9. destruct H10.
  destruct H10. destruct H10. destruct H10.
  destruct H11. destruct H11. destruct H11.
  rewrite H10 in H9. rewrite H11 in H9.
  replace ((x4 ++ l :: x5) ++ x6 ++ x :: x7)
    with (x4 ++ l::(x5++x6) ++ x::x7) in H9. symmetry in H9.
  apply words_below_length_ordering in H9.
  assert (length l < length l).
  apply Nat.le_lt_trans with (m := length x); assumption.
  apply Nat.lt_irrefl in H14. assumption.
  rewrite app_comm_cons. rewrite app_assoc.
  symmetry. rewrite app_assoc. apply app_inv_tail_iff.
  rewrite app_comm_cons. rewrite app_assoc. reflexivity.
  assumption.
Qed.


Theorem Shortest_complete_sequence_dec :
  forall (u base: list X), { Shortest_complete_sequence u base }
                         + {~ Shortest_complete_sequence u base}.
Proof.
  intros.
  assert (K : { Complete_sequence u base } + {~ Complete_sequence u base}).
  apply Complete_sequence_dec. destruct K.

  assert (L: Shortest_complete_sequence
      (hd nil (filter (fun e => if (Complete_sequence_dec e base)
                                    then true else false)
          (words_below_length base (S (length base * length base))))) base).
  apply Shortest_complete_sequence_instance.

  assert (M: { length u = length (hd nil (filter
                       (fun e => if (Complete_sequence_dec e base)
                                    then true else false)
              (words_below_length base (S (length base * length base)))))}
     + { length u <> length (hd nil (filter
                       (fun e => if (Complete_sequence_dec e base)
                                    then true else false)
              (words_below_length base (S (length base * length base)))))}).
  apply Nat.eq_dec. destruct M. left.
  split. assumption.

  intros. destruct L. rewrite e in H. apply H1 in H. assumption.
  right. intro. apply Shortest_complete_sequence_length_eq in H.
  rewrite H in n. contradiction.

  right. intro. destruct H. apply n in H. contradiction.
Qed.


Theorem not_Complete_sequence_exists_perm :
  forall (u base: list X), ~ Complete_sequence u base
    -> exists p, Permutation base p /\ ~ Subsequence u p.
Proof.
  intros.
  assert (exists q, forall v,
             In v q <-> (Permutation base v /\ ~ Subsequence u v)).
  apply Permutation_absent_exists_set. assumption.
  destruct H0. destruct x. assert False. apply H.
  repeat intro. assert (~ In v nil). apply in_nil.
  destruct (Subsequence_dec eq_dec u v). assumption.
  assert False. apply H2. apply H0. split. assumption.
  assumption. contradiction. contradiction.
  exists l. apply H0. apply in_eq.
Qed.


Theorem not_Complete_sequence_exists_char :
  forall (u base: list X), NoDup base -> ~ Complete_sequence u base
    -> exists x, In x base /\ ~ Count_no_change eq_dec x u.
Proof.
  intros. apply not_Complete_sequence_exists_perm in H0.
  destruct H0. destruct H0.
  assert (
    exists v w : list (list X),
      suffixes x = v ++ w /\
      (forall y : list X, In y v -> Subsequence u y) /\
      (forall y : list X, In y w -> ~ Subsequence u y)).
  apply suffixes_split. assumption. destruct H2. destruct H2.
  destruct H2. destruct H3.
  destruct x1. assert (In x (suffixes x)). apply suffixes_original.
  rewrite app_nil_r in H2. rewrite H2 in H5. apply H3 in H5. apply H1 in H5.
  contradiction.
  destruct l. assert (~ Subsequence u nil). apply H4. apply in_eq.
  assert False. apply H5. apply Subsequence_nil_r. contradiction.
  exists x2. split. apply Permutation_in with (l := x).
  apply Permutation_sym. assumption.
  assert (Subsequence x (x2::l)). apply suffixes_subsequence.
  rewrite H2. apply in_elt. apply Subsequence_In2 with (v := l).
  assumption. 

  assert (Subsequence u l). apply suffixes_middle in H2. apply H3 in H2.
  assumption. intro. apply Count_Collect_no_change in H6.
  specialize (H6 (x2::l)).

  assert (In (x2::l) (Collect eq_dec (x2::l) x2 (x2::u))).
  assert (Subsequence x (x2::l)). apply suffixes_subsequence. rewrite H2.
  apply in_elt. apply Collect_subsequence_2.
  apply Subsequence_nodup_nodup with (u := x).
  apply Permutation_NoDup with (l := base); assumption. assumption.
  apply Permutation_refl. apply Subsequence_cons_eq. assumption.

  assert (~ In (x2::l) (Collect eq_dec (x2::l) x2 u)). intro.
  apply Collect_subsequence in H8. apply H4 in H8. assumption.
  apply in_eq. rewrite H6 in H7. apply H8 in H7. assumption.
Qed.


Theorem Count_fact_Complete :
  forall (u base: list X), NoDup base
    -> (forall x, In x base
      -> Count eq_dec base x u = fact (pred (length base)))
    -> Complete_sequence u base.
Proof.
  repeat intro. destruct v. apply Subsequence_nil_r.
  assert (In x base).
  apply Permutation_in with (l := x::v).
  apply Permutation_sym. assumption. apply in_eq.
  apply Collect_eq_fact with (eq_dec := eq_dec) (a := base).
  assumption. assumption. apply H0. assumption. assumption.
Qed.


Theorem Count_fact_Complete_2 :
  forall (u a: list X) (x: X),
    NoDup (x::a) -> Count eq_dec (x::a) x u = fact (length a)
      -> Complete_sequence u a.
Proof.
  intros.
  assert (forall p, Permutation (x::a) (x::p) -> Subsequence u (x::p)).
  apply Collect_eq_fact with (eq_dec := eq_dec). assumption.
  apply in_eq. assumption. repeat intro. apply perm_skip with (x := x) in H2.
  apply H1 in H2. apply Subsequence_cons_r with (a := x). assumption.
Qed.


Theorem Count_fact_previous_fact :
  forall (u base: list X) (x: X), NoDup (x::base)
    -> Count eq_dec (x::base) x u = fact (length base)
    -> forall y, In y base
        -> Count eq_dec base y u = fact (pred (length base)).
Proof.
  intros.
  replace (length base) with (pred (length (x::base))) in H0.
  assert (forall (p : list X),
       Permutation (x::base) (x :: p) -> Subsequence u (x :: p)).
  apply Collect_eq_fact with (eq_dec := eq_dec). assumption.
  apply in_eq. assumption. apply Collect_eq_fact_2.
  apply NoDup_cons_iff in H. destruct H. assumption. assumption.
  intros. apply perm_skip with (x := x) in H3. apply H2 in H3.
  apply Subsequence_cons_r in H3. assumption. easy.
Qed.


Theorem Collect_both_complete_incl :
  forall (u v base: list X), NoDup base -> Complete_sequence u base
    -> (forall (a: list X) (x: X), incl (Collect eq_dec a x u)
                                        (Collect eq_dec a x v))
    -> Complete_sequence v base.
Proof.
  repeat intro. destruct v0. apply Subsequence_nil_r.
  apply Collect_subsequence with (eq_dec := eq_dec) (a := base) (x := x).
  apply H1. apply Collect_subsequence_2. assumption. assumption.
  apply H0. assumption.
Qed.


Theorem Collect_both_complete_eq :
  forall (u v base: list X), NoDup base -> Complete_sequence u base
    -> (forall (a: list X) (x: X), Collect eq_dec a x u
                                 = Collect eq_dec a x v)
    -> Complete_sequence v base.
Proof.
  repeat intro. destruct v0. apply Subsequence_nil_r.
  apply Collect_subsequence with (eq_dec := eq_dec) (a := base) (x := x).
  rewrite <- H1. apply Collect_subsequence_2. assumption. assumption.
  apply H0. assumption.
Qed.


Lemma NoDup_NoDup_incl :
  forall (u v: list X),
    NoDup v -> incl v u -> exists w, Permutation u (v ++ w).
Proof.
  intro u. induction u; intros. apply incl_l_nil in H0.
  exists nil. rewrite H0.
  apply Permutation_refl.
  destruct (in_dec eq_dec a v). apply in_split in i.
  destruct i. destruct H1.
  assert (incl (x++x0) u). repeat intro.
  destruct (eq_dec a a0). rewrite <- e in H2.
  rewrite H1 in H. apply NoDup_remove_2 in H. apply H in H2. contradiction.
  assert (In a0 v). rewrite H1. apply in_or_app. apply in_app_or in H2.
  destruct H2. left. assumption. right. apply in_cons. assumption.
  apply H0 in H3. apply in_inv in H3. destruct H3. apply n in H3.
  contradiction. assumption. apply IHu in H2.
  destruct H2. exists x1. rewrite H1.
  rewrite <- app_assoc. rewrite <- app_comm_cons.
  apply Permutation_cons_app. rewrite app_assoc. assumption.
  rewrite H1 in H. apply NoDup_remove_1 in H. assumption.
  assert (incl v u). repeat intro. assert (H2 := H1). apply H0 in H1.
  destruct H1. rewrite H1 in n. apply n in H2. contradiction. assumption.
  apply IHu in H1. destruct H1. exists (a::x).
  apply Permutation_cons_app. assumption. assumption.
Qed.


Theorem Complete_sequence_included_base :
  forall (b1 b2 u: list X),
    NoDup b2 -> incl b2 b1
       -> Complete_sequence u b1 -> Complete_sequence u b2.
Proof.
  repeat intro.
  assert (incl v b1). apply incl_tran with (m := b2).
  repeat intro. apply Permutation_in with (l := v).
  apply Permutation_sym. assumption. assumption. assumption.
  apply NoDup_NoDup_incl in H3. destruct H3. apply H1 in H3.
  apply Subsequence_split_4 in H3. assumption.
  apply Permutation_NoDup with (l := b2); assumption.
Qed.


Theorem Complete_Count_fact :
  forall (u base: list X),
    Complete_sequence u base
      -> forall b x, NoDup b -> In x b -> incl b base
          -> Count eq_dec b x u = fact (pred (length b)).
Proof.
  intros.
  assert (Count eq_dec b x u <= fact (pred (length b))).
  apply Count_factorial; assumption. apply Nat.lt_eq_cases in H3.
  destruct H3.
  apply in_split in H1. destruct H1. destruct H1.
  replace (pred (length b)) with (length (x0++x1)) in H3.
  rewrite <- all_permutations_length in H3.
  rewrite Count_Collect_length in H3.

  assert (exists k, 0 < k /\
    length (all_permutations (x0 ++ x1)) = k + length (Collect eq_dec b x u)).
  assert (forall a b, a < b -> exists k, 0 < k /\ b = k + a).
  intro a. induction a; intros. exists b0. split. assumption. apply plus_n_O.
  destruct b0. apply Nat.nlt_0_r in H4. contradiction.
  apply Nat.succ_lt_mono in H4. apply IHa in H4. destruct H4. destruct H4.
  exists x2. split. assumption. rewrite H5. apply plus_n_Sm.
  apply H4 in H3. assumption.

  destruct H4. destruct H4.
  replace (length (all_permutations (x0++x1)))
    with (length (map (cons x) (all_permutations (x0++x1)))) in H5.
  apply nodup_missing_elements in H5.
  destruct H5. destruct H5. destruct H6.
  destruct x3. rewrite <- H5 in H4.
  apply Nat.lt_irrefl in H4. contradiction.
  assert (In l (l::x3)). apply in_eq. apply H7 in H8. destruct H8.

  apply in_map_iff in H8. destruct H8. destruct H8.
  apply all_permutations_Permutation in H10.
  apply Permutation_sym in H10.
  apply Permutation_cons_app with (a := x) in H10. rewrite H8 in H10.
  rewrite <- H1 in H10.

  assert (In l (Collect eq_dec b x u)). rewrite <- H8.
  apply Collect_subsequence_3. rewrite H8.
  apply Permutation_NoDup with (l := b). apply Permutation_sym. assumption.
  assumption. rewrite H8. repeat intro.
  apply Permutation_in with (l := b). apply Permutation_sym. assumption.
  assumption. repeat intro. apply in_cons with (a := x) in H11.
  apply Permutation_in with (l := l). assumption.
  rewrite <- H8. assumption. 

  apply Complete_sequence_included_base with (b2 := b) in H.
  apply Permutation_sym in H10. apply H in H10. rewrite H8. assumption.
  assumption. assumption. apply H9 in H11. contradiction.
  apply (list_eq_dec eq_dec).

  apply NoDup_map_NoDup_ForallPairs. repeat intro. inversion H8. reflexivity.

  apply all_permutations_NoDup. assumption. rewrite H1 in H0.
  apply NoDup_remove_1 in H0. assumption. rewrite length_map. reflexivity.
  rewrite H1. repeat rewrite length_app. simpl.
  rewrite Nat.add_succ_r. apply pred_Sn. assumption.
Qed.


Theorem Complete_sequence_nodup :
  forall (u base: list X),
    Complete_sequence u base -> Complete_sequence u (nodup eq_dec base).
Proof.
  repeat intro. pose (b := nodup eq_dec base).
  assert (forall x, In x b -> Count eq_dec b x u = fact (pred (length b))).
  intros. apply Complete_Count_fact with (base := base). assumption.
  apply NoDup_nodup. assumption. repeat intro. apply nodup_In in H2. assumption.
  apply Count_fact_Complete in H1. apply H1. assumption. apply NoDup_nodup.
Qed.


Theorem Shortest_complete_sequence_greedy_2 :
  forall (v w base: list X) (x: X),
    NoDup base -> Shortest_complete_sequence (v++x::w) base
      -> ~ Count_no_change eq_dec x w.
Proof.
  intros. destruct (Count_no_change_dec w x).
  apply Count_no_change_not_shortest with (u := v) (base := base) in c.
  apply c in H0. contradiction. assumption. assumption.
Qed.


Theorem Shortest_complete_sequence_first_occurrence_2 :
  forall (u v b: list X) (a: X),
    NoDup (a::b) -> Shortest_complete_sequence (u ++ a :: v) (a :: b)
      -> Count eq_dec (a::b) a (a::v) = fact (length b) <-> ~ In a u.
Proof.
  intros. split; intro.
  assert (I := H). apply NoDup_cons_iff in H. destruct H.
  apply Shortest_complete_sequence_first_occurrence with (v := v) (b := b).
  assumption.
  apply Complete_sequence_cons_not_in_base with (a := a).

  apply Count_fact_Complete. assumption.
  apply Count_fact_previous_fact with (x := a); assumption. assumption.
  destruct H0. apply Complete_Count_fact with (b := a::b) (x := a) in H0.
  apply Count_prefix
       with (eq_dec := eq_dec) (a := a::b) (x := a) (l := a::v) in H1.
  rewrite H0 in H1. rewrite H1. reflexivity. assumption. apply in_eq.
  apply incl_refl.
Qed.


Theorem Count_fact_Collect_stable :
  forall (u v a: list X) (x: X),
    In x a -> NoDup a -> Count eq_dec a x v = fact (pred (length a))
      -> Collect eq_dec a x v = Collect eq_dec a x (u++v).
Proof.
  intros.
  assert (incl (Collect eq_dec a x v) (Collect eq_dec a x (u++v))).
  apply Collect_extend. apply Collect_incl_subsequence in H2.
  symmetry. apply Subsequence_length_2. assumption.
  repeat rewrite <- Count_Collect_length.
  rewrite H1. apply Nat.le_antisymm. apply Count_factorial; assumption.
  rewrite <- H1. apply Count_monotonic_extend.
Qed.


Theorem Count_eq_Collect_stable :
  forall (u v a: list X) (x: X),
  Count eq_dec a x v = Count eq_dec a x (u++v)
      <-> Collect eq_dec a x v = Collect eq_dec a x (u++v).
Proof.
  intro u. induction u; intros; split; intro. reflexivity. reflexivity.
  assert (Count eq_dec a0 x (u++v) <= Count eq_dec a0 x ((a::u)++v)).
  apply Count_monotonic. apply Nat.lt_eq_cases in H0. destruct H0.
  rewrite <- H in H0. assert (Count eq_dec a0 x v < Count eq_dec a0 x v).
  apply Nat.le_lt_trans with (m := Count eq_dec a0 x (u++v)).
  apply Count_monotonic_extend. assumption. apply Nat.lt_irrefl in H1.
  contradiction. rewrite <- H0 in H. apply IHu in H. rewrite H.
  rewrite <- app_comm_cons. rewrite <- app_comm_cons in H0.
  destruct (eq_dec x a). rewrite e. apply Count_Collect_eq_swap.
  rewrite e in H0. assumption. rewrite Collect_cons_not_eq. reflexivity.
  assumption. repeat rewrite Count_Collect_length. rewrite H. reflexivity.
Qed.


Theorem Shortest_complete_sequence_two_occurrences :
  forall (l1 l2 l3 a : list X) (x: X),
    NoDup (x::a) -> Shortest_complete_sequence (l1++x::l2++x::l3) (x::a)
    -> Count eq_dec (x::a) x (x::l3) < fact (length a).
Proof.
  intros.
  assert (Count eq_dec (x::a) x (x::l3) <= fact (pred (length (x::a)))).
  apply Count_factorial. apply in_eq. assumption.
  apply Nat.lt_eq_cases in H1. destruct H1. assumption.
  replace (fact (pred (length (x::a)))) with (fact (length a)) in H1.
  rewrite Shortest_complete_sequence_first_occurrence_2
    with (u := l1++x::l2) in H1. assert False. apply H1. apply in_elt.
  contradiction. assumption. rewrite <- app_assoc.
  rewrite <- app_comm_cons. assumption. reflexivity.
Qed.


Theorem Shortest_complete_sequence_two_occurrences_2 :
  forall (l1 l2 l3 a : list X) (x: X),
    NoDup (x::a) -> Shortest_complete_sequence (l1++x::l2++x::l3) (x::a)
    -> exists (p: list X), Permutation a p /\ ~ Subsequence l3 p.
Proof.
  intros. apply not_Complete_sequence_exists_perm.
  assert (H1 := H0). apply Shortest_complete_sequence_two_occurrences in H0.
  replace (fact (length a))
      with (length (map (cons x) (all_permutations a))) in H0.
  rewrite Count_Collect_length in H0.
  assert (forall b a, a < b -> exists k, b = S k + a). 
  intro b. induction b; intros. apply Nat.nlt_0_r in H2. contradiction.
  rewrite Nat.lt_succ_r in H2. apply Nat.lt_eq_cases in H2. destruct H2.
  apply IHb in H2. destruct H2. exists (S x0). rewrite Nat.add_succ_l.
  rewrite H2. reflexivity. exists 0. rewrite H2. reflexivity.
  apply H2 in H0. destruct H0. apply nodup_missing_elements in H0.
  destruct H0. destruct H0. destruct H3.
  destruct x1. inversion H0. assert (In l (l::x1)). apply in_eq.
  apply H4 in H5. destruct H5. repeat intro.
  apply in_map_iff in H5. destruct H5. destruct H5.
  assert (Permutation a x2). apply all_permutations_Permutation. assumption.
  assert (H10 := H9). apply H7 in H9.
  apply H6. rewrite <- H5. apply Collect_subsequence_2. assumption.
  apply perm_skip. assumption. apply Subsequence_cons_eq. assumption.
  apply (list_eq_dec eq_dec). apply NoDup_map_NoDup_ForallPairs.
  repeat intro. inversion H5. reflexivity. apply all_permutations_NoDup.
  assumption. apply NoDup_cons_iff in H. destruct H. assumption.
  rewrite length_map. apply all_permutations_length. assumption.
Qed.


Theorem Count_fact_previous_fact_2:
  forall (u : list X) (base base' : list X) (x : X),
    NoDup (x :: base) ->
      Count eq_dec (x :: base) x u = fact (length base) ->
      NoDup base' -> incl base' base -> forall y : X,
         In y base' -> Count eq_dec base' y u = fact (pred (length base')).
Proof.
  intros. apply Count_fact_Complete_2 in H0.
  apply Complete_Count_fact with (b := base') (x := y) in H0.
  assumption. assumption. assumption. assumption. assumption.
Qed.

End Notebook_dec.
