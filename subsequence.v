Require Import Nat.
Require Import PeanoNat.
Require Import List.
Import ListNotations.


(**************************************************************************)
(** * Basics: definition of a subsequence lists and elementary properties *)
(**************************************************************************)

(** Definition of a subsequence **)

Fixpoint Subsequence {X: Type} (l s : list X) : Prop :=
  match s with
  | nil    => True
  | hd::tl => exists l1 l2, l = l1 ++ (hd::l2) /\ Subsequence l2 tl
  end.


(** Elementary properties **)

Theorem Subsequence_nil_r {X: Type} : forall (l : list X), Subsequence l nil.
Proof.
  intro l. reflexivity.
Qed.


Theorem Subsequence_nil_cons_r {X: Type}: forall (l: list X) (a:X),
  ~ Subsequence nil (a::l).
Proof.
  intros l a. intro H. destruct H. destruct H. destruct H.
  destruct x; apply nil_cons in H; contradiction H.
Qed.


Theorem Subsequence_cons_l {X: Type} : forall (l s: list X) (a: X),
  Subsequence l s -> Subsequence (a::l) s.
Proof.
  intros l s a. intro H. destruct s. apply Subsequence_nil_r.
  destruct H. destruct H. destruct H. exists (a::x0). exists x1. rewrite H.
  split; easy.
Qed.


Theorem Subsequence_cons_r {X: Type} : forall (l s: list X) (a: X),
  Subsequence l (a::s) -> Subsequence l s.
Proof.
  intros l s a. intro H. simpl in H. destruct H. destruct H.
  destruct s. apply Subsequence_nil_r. destruct H. simpl in H0.
  destruct H0. destruct H0. destruct H0.
  exists (x ++ a::x2). exists x3.
  rewrite H. rewrite H0. split. rewrite <- app_assoc.
  rewrite app_comm_cons. reflexivity. assumption.
Qed.


Theorem Subsequence_cons_eq {X: Type} : forall (l1 l2: list X) (a: X),
  Subsequence (a::l1) (a::l2) <-> Subsequence l1 l2.
Proof.
  intros l s a. split; intro H.
  destruct H. destruct H. destruct H.
  destruct x. inversion H. assumption.
  destruct s. apply Subsequence_nil_r.
  destruct H0. destruct H0. destruct H0.
  exists (x1 ++ (a::x3)). exists x4.
  inversion H. rewrite H0. rewrite <- app_assoc.
  rewrite app_comm_cons. split; easy.
  exists nil. exists l. split; easy.
Qed.


(************************************************************************)
(** * Conversion to two other internal representations of a subsequence *)
(************************************************************************)

(** Two Lemmas involved in the main theorems of this subsection **)

Lemma Subsequence_alt_defs {X: Type} :
  forall l s : list X,
  (exists (l1: list X) (l2 : list (list X)),
    length s = length l2
    /\ l = l1 ++ flat_map (fun e => (fst e) :: (snd e)) (combine s l2))
    ->
  (exists (t: list bool),
    length t = length l /\ s = map snd (filter fst (combine t l))).
Proof.
  intros l s. intro H. destruct H. destruct H. destruct H.

  exists (
    (repeat false (length x)) ++
    (flat_map (fun e => true :: (repeat false (length e))) x0)).
  split.
  rewrite H0. rewrite length_app. rewrite length_app. rewrite repeat_length.
  rewrite Nat.add_cancel_l. rewrite length_flat_map. rewrite length_flat_map.

  assert (forall v (u: list X),
    length u = length v
    -> map (fun e => length (fst e :: snd e)) (combine u v)
       = map (fun e => length (true :: repeat false (length e))) v).
  intro v. induction v; intro u; intro I.
  apply length_zero_iff_nil in I. rewrite I. reflexivity.
  destruct u. apply O_S in I. contradiction I.
  simpl. rewrite IHv. rewrite repeat_length. reflexivity.
  inversion I. reflexivity.

  rewrite H1; inversion H; reflexivity. rewrite H0.
  assert (forall (u: list X) v w,
    filter fst (combine (repeat false (length u) ++ v) (u ++ w))
      = filter fst (combine v w)).
  intro u. induction u; intros v w. reflexivity. simpl. apply IHu.

  assert (forall (v: list (list X)) (u : list X),
    length u = length v
    -> u = map snd (filter fst (combine
         (flat_map (fun e => true:: repeat false (length e)) v)
         (flat_map (fun e => fst e :: snd e) (combine u v))))).
  intro v. induction v; intro u; intro I.
  apply length_zero_iff_nil in I. rewrite I. reflexivity.
  destruct u. apply O_S in I. contradiction I.
  simpl. rewrite H1. rewrite <- IHv; inversion I; reflexivity.
  rewrite H1. rewrite <- H2; inversion H; reflexivity.
Qed.


Lemma Subsequence_alt_defs2 {X: Type} :
  forall l s : list X,
  (exists (t: list bool),
    length t = length l /\ s = map snd (filter fst (combine t l)))
  -> Subsequence l s.
Proof.
  intros l s. intro H. destruct H. destruct H.
  assert (I: forall u (v w: list X),
    length u = length w
    -> v = map snd (filter fst (combine u w))
    -> Subsequence w v).
  intro u. induction u; intros v w; intros I J.
  rewrite J. apply Subsequence_nil_r.
  destruct v. apply Subsequence_nil_r.
  destruct w. apply Nat.neq_succ_0 in I. contradiction I.
  destruct a. exists nil. exists w. inversion J.
  apply IHu in H3. split; inversion J; try rewrite <- H5; easy.
  inversion I. reflexivity.
  assert (Subsequence w (x0::v)). apply IHu; inversion I; easy.
  apply Subsequence_cons_l; assumption.
  apply I with (u := x); assumption.
Qed.


(** Unfolding the recursion by using [flat_map] **)

Theorem Subsequence_as_flat_map {X: Type} :
  forall l s : list X, Subsequence l s
  <-> exists (l1: list X) (l2 : list (list X)),
    length s = length l2
    /\ l = l1 ++ flat_map (fun e => (fst e) :: (snd e)) (combine s l2).
Proof.
  intros l s. split. generalize l. induction s; intro l0; intro H.
  exists l0. exists nil. split. reflexivity. rewrite app_nil_r.
  reflexivity. destruct H. destruct H. destruct H. apply IHs in H0.
  destruct H0. destruct H0. destruct H0. exists x. exists (x1::x2).
  split; simpl; [rewrite H0 | rewrite <- H1]; easy.
  intro. apply Subsequence_alt_defs in H. apply Subsequence_alt_defs2.
  assumption.
Qed.


(** Using a characteristic vector **)

Theorem Subsequence_bools {X: Type} :
  forall l s : list X, Subsequence l s
  <-> (exists (t: list bool),
    length t = length l /\ s = map snd (filter fst (combine t l))).
Proof.
  intros l s. split; intro. apply Subsequence_alt_defs.
  apply Subsequence_as_flat_map. assumption.
  apply Subsequence_alt_defs2. assumption.
Qed.


(*************************************************)
(** * Several general properties of subsequences *)
(*************************************************)

Theorem Subsequence_id {X: Type} :
  forall u : list X, Subsequence u u.
Proof.
  intro u. induction u. easy. exists nil. exists u. easy.
Qed.


Theorem Subsequence_cons_diff {X: Type} :
  forall (u v: list X) (a b: X),
    a <> b -> Subsequence (a::u) (b::v) -> Subsequence u (b::v).
Proof.
  intros. apply Subsequence_bools in H0. destruct H0. destruct H0.
  destruct x. inversion H0. destruct b0. inversion H1. rewrite H3 in H.
  contradiction. simpl in H1. apply Subsequence_bools. exists x. split.
  inversion H0. reflexivity. assumption.
Qed.


Theorem Subsequence_app {X: Type} :
  forall l1 s1 l2 s2 : list X,
  Subsequence l1 s1 -> Subsequence l2 s2 -> Subsequence (l1++l2) (s1++s2).
Proof.
  intros l1 s1 l2 s2. intros H I. apply Subsequence_bools.
  apply Subsequence_bools in H. apply Subsequence_bools in I.
  destruct H. destruct I. exists (x++x0). destruct H. destruct H0.
  split. rewrite length_app. rewrite length_app.
  rewrite H. rewrite H0. reflexivity.
  rewrite H1. rewrite H2.

  assert (J: forall (t : list bool) (u : list X) (v: list bool) (w : list X),
    length t = length u
    -> combine (t++v) (u++w) = (combine t u) ++ (combine v w)).
  intros t u v w. generalize u. induction t; intro u0; intro K.
  replace u0 with (nil : list X). reflexivity.
  destruct u0. reflexivity. apply O_S in K. contradiction K.
  destruct u0. apply PeanoNat.Nat.neq_succ_0 in K. contradiction K.
  simpl. rewrite IHt. reflexivity. inversion K. reflexivity.

  rewrite J. rewrite filter_app. rewrite map_app. reflexivity.
  assumption.
Qed.


Theorem Subsequence_app2_l {X: Type} :
  forall (l1 l2 l3: list X),
    Subsequence l2 l3 -> Subsequence (l1++l2) l3.
Proof.
  intro l1. induction l1; intros. assumption. rewrite <- app_comm_cons.
  apply Subsequence_cons_l. apply IHl1. assumption.
Qed.


Theorem Subsequence_trans {X: Type} :
  forall (l1 l2 l3: list X),
  Subsequence l1 l2 -> Subsequence l2 l3 -> Subsequence l1 l3.
Proof.
  intros l1 l2 l3. intros H I. apply Subsequence_bools.
  apply Subsequence_as_flat_map in H. apply Subsequence_bools in I.
  destruct H. destruct H. destruct H. destruct I. destruct H1.
  exists (
    (repeat false (length x)) ++
    (flat_map (fun e => (fst e) :: (repeat false (length (snd e))))
              (combine x1 x0))).
  split.

  rewrite length_app. rewrite repeat_length.
  rewrite H0. rewrite length_app. rewrite Nat.add_cancel_l.
  rewrite length_flat_map. rewrite length_flat_map.
  assert (forall (u: list X) (v: list (list X)),
    length u = length v
    -> list_sum (map (fun z => length (fst z :: snd z))
                     (combine u v))
       = list_sum (map (fun z => S (length z)) v)).
  intros u v. generalize u. induction v; intro u0; intro I.
  apply length_zero_iff_nil in I. rewrite I. reflexivity.
  destruct u0. apply O_S in I. contradiction I.
  simpl. rewrite IHv. reflexivity. inversion I. reflexivity.
  rewrite H3.
  assert (forall (u: list bool) (v: list (list X)),
    length u = length v
    -> list_sum (map (fun z => length (fst z :: repeat false (length (snd z))))
                     (combine u v))
       = list_sum (map (fun z => S (length z)) v)).
  intros u v. generalize u.
  induction v; intro u0; intro I; destruct u0. reflexivity.
  apply PeanoNat.Nat.neq_succ_0 in I. contradiction I.
  apply O_S in I. contradiction I.
  simpl. rewrite IHv. rewrite repeat_length. reflexivity.
  inversion I. reflexivity. rewrite H4. reflexivity.
  rewrite H1. rewrite H. reflexivity. assumption.

  assert (K: forall (w v: list X) u,
    filter fst (combine ((repeat false (length w)) ++ u)
                        (w ++ v)) = filter fst (combine u v)).
  intro w0. induction w0. reflexivity. apply IHw0.
  rewrite H2. rewrite H0.
  assert (forall (u v: list X) (w: list bool),
    map snd (filter fst (combine ((repeat false (length u)) ++ w) (u ++ v)))
      = map snd (filter fst (combine w v))).
  intro u. induction u; intros v w. reflexivity.
  apply IHu. rewrite H3.
  assert (forall (u: list bool) (v: list X) (w: list (list X)),
    length u = length w
    -> length u = length v
    -> filter fst (combine
                      (flat_map
                         (fun e => fst e:: (repeat false (length (snd e))))
                         (combine u w))
                      (flat_map (fun e => fst e :: snd e) (combine v w)))
         = filter fst (combine u v)).
  intros u v w. generalize u. generalize v.
  induction w; intros v0 u0; intros I J.
  apply length_zero_iff_nil in I. rewrite I. reflexivity.
  destruct u0. reflexivity. destruct v0.
  apply PeanoNat.Nat.neq_succ_0 in J; contradiction J.
  destruct b; simpl; rewrite K; rewrite IHw;
  inversion I; inversion J; reflexivity.
  rewrite H4; try rewrite H1; try rewrite H; reflexivity.
Qed.


Theorem Subsequence_length {X: Type} :
  forall (u v: list X), Subsequence u v -> length v <= length u.
Proof.
  intros u v. generalize u. induction v; intro u0; intro H. apply Nat.le_0_l.
  destruct H. destruct H. destruct H.
  apply IHv in H0. simpl. apply Nat.le_succ_l. rewrite H.
  rewrite length_app. apply Nat.lt_lt_add_l.
  rewrite <- Nat.le_succ_l. simpl. rewrite <- Nat.succ_le_mono.
  assumption.
Qed.


Theorem Subsequence_length_2 {X: Type} :
  forall (u v: list X),
    Subsequence u v -> length u = length v -> u = v.
Proof.
  intro u. induction u; intros. symmetry. apply length_zero_iff_nil.
  rewrite <- H0. reflexivity. assert (I := H).
  apply Subsequence_bools in H. destruct H.
  destruct H. destruct x. apply O_S in H. contradiction. destruct b.
  destruct v. inversion H1. inversion H1. rewrite <- H4. f_equal.
  apply IHu. rewrite H3 in I. apply Subsequence_cons_eq in I. assumption.
  inversion H. inversion H0. assumption. simpl in H1.
  rewrite H1 in H0. rewrite length_map in H0.
  assert (length (filter fst (combine x u)) <= length (combine x u)).
  apply filter_length_le. rewrite length_combine in H2.
  inversion H. rewrite H4 in H2. rewrite Nat.min_id in H2.
  rewrite <- H0 in H2. apply Nat.nle_succ_diag_l in H2. contradiction.
Qed.


Theorem Subsequence_length_3 {X: Type} :
  forall (u v: list X) a,
    Subsequence u v -> In a u -> ~ In a v -> length v < length u.
Proof.
  intros. assert (I := H). apply Subsequence_length in H.
  apply Nat.lt_eq_cases in H. destruct H. assumption.
  symmetry in H. apply Subsequence_length_2 in H.
  rewrite H in H0. apply H1 in H0. contradiction. assumption.
Qed.


Theorem Subsequence_eq {X: Type} :
  forall (u v: list X),
  Subsequence u v -> Subsequence v u -> u = v.
Proof.
  intro u. induction u; intro v; intros H I.

  destruct v. reflexivity.
  apply Subsequence_nil_cons_r in H. contradiction H.
  destruct v. apply Subsequence_nil_cons_r in I. contradiction I.

  apply Subsequence_bools in H. apply Subsequence_bools in I.

  destruct H. destruct H. destruct I. destruct H1.
  destruct x0. apply O_S in H. contradiction H.
  destruct x1. apply O_S in H1. contradiction H1.
  destruct b; destruct b0.

  assert (Subsequence u v). apply Subsequence_bools. exists x0.
  split; inversion H; inversion H0; reflexivity.
  assert (Subsequence v u). apply Subsequence_bools. exists x1.
  split; inversion H1; inversion H2; reflexivity.
  apply IHu in H4. rewrite H4. inversion H0. reflexivity. assumption.

  assert (Subsequence u v). apply Subsequence_bools. exists x0.
  split; inversion H; inversion H0; reflexivity.
  assert (Subsequence v (a::u)). apply Subsequence_bools. exists x1.
  split; inversion H1; inversion H2; reflexivity.

  apply Subsequence_trans with (l1 := u) in H4. apply Subsequence_length in H4.
  apply Nat.nle_succ_diag_l in H4. contradiction. assumption.
  
  assert (Subsequence u (x::v)). apply Subsequence_bools. exists x0.
  split; inversion H; inversion H0; reflexivity.
  assert (Subsequence v u). apply Subsequence_bools. exists x1.
  split; inversion H1; inversion H2; reflexivity.
  assert (Subsequence v (x::v)). apply Subsequence_trans with (l2 := u);
  assumption.
  apply Subsequence_length in H5. apply Nat.nle_succ_diag_l in H5.
  contradiction H5.

  assert (Subsequence u (x::v)). apply Subsequence_bools. exists x0.
  split; inversion H; inversion H0; reflexivity.
  assert (Subsequence v (a::u)). apply Subsequence_bools. exists x1.
  split; inversion H1; inversion H2; reflexivity.
  apply Subsequence_cons_r in H3. apply Subsequence_cons_r in H4.
  apply IHu in H4. rewrite H4 in H0. rewrite H4 in H.
  simpl in H0.
  assert (Subsequence v (x::v)). apply Subsequence_bools.
  exists x0. split; inversion H; easy.
  apply Subsequence_length in H5. apply Nat.nle_succ_diag_l in H5.
  contradiction H5. assumption.
Qed.


Theorem Subsequence_In {X: Type} :
   forall (u: list X) a, In a u <-> Subsequence u [a].
 Proof.
   intros u a. split. induction u; intro H.
   apply in_nil in H. contradiction.
   destruct H. rewrite H. exists nil. exists u.
   split. reflexivity. apply Subsequence_nil_r.
   apply Subsequence_cons_l. apply IHu. assumption.
   intro H. destruct H. destruct H. destruct H. rewrite H.
   apply in_or_app. right. apply in_eq.
Qed.


Theorem Subsequence_In2 {X: Type} :
  forall (u v:list X) a, Subsequence u (a::v) -> In a u.
Proof.
  intros. simpl in H. destruct H. destruct H. destruct H.
  rewrite H. apply in_elt.
Qed.


Theorem Subsequence_rev {X: Type} :
  forall (u v: list X), Subsequence u v <-> Subsequence (rev u) (rev v).
Proof.
  assert (MAIN: forall (u v: list X),
      Subsequence u v -> Subsequence (rev u) (rev v)).
  intros u v. intro H.
  apply Subsequence_bools. apply Subsequence_bools in H.
  destruct H. destruct H.
  exists (rev x). split. rewrite length_rev. rewrite length_rev.
  assumption. rewrite H0.

  assert (LEMMA : forall (l1: list bool) (l2: list X),
    length l1 = length l2
    -> combine (rev l1) (rev l2) = rev (combine l1 l2)).
  intro l1. induction l1; intro l2; intro I. reflexivity.
  destruct l2. inversion I. inversion I. apply IHl1 in H2.
  simpl.
  assert (LEMMA2 : forall (u w : list bool) (v x : list X),
    length u = length v
    -> combine (u ++ w) (v ++ x) = (combine u v) ++ (combine w x)).
  intro u0. induction u0; intros w v0 x1; intro J.
  symmetry in J. apply length_zero_iff_nil in J. rewrite J.
  reflexivity. destruct v0. inversion J. inversion J.
  apply IHu0 with (w := w) (x := x1) in H3. simpl. rewrite H3.
  reflexivity. rewrite LEMMA2. rewrite IHl1. reflexivity.
  inversion I. reflexivity. rewrite length_rev. rewrite length_rev.
  inversion I. reflexivity. rewrite LEMMA. rewrite <- map_rev.
  assert (LEMMA3: forall v (w: list (bool * X)),
    rev (filter v w) = filter v (rev w)).
  intros v0 w. induction w. reflexivity. simpl. rewrite filter_app.
  simpl. destruct (v0 a). simpl. rewrite IHw. reflexivity.
  rewrite IHw. symmetry. apply app_nil_r. rewrite LEMMA3.
  reflexivity. assumption.
  split. apply MAIN. intro H. apply MAIN in H.
  rewrite rev_involutive in H. rewrite rev_involutive in H.
  assumption.
Qed.


Theorem Subsequence_map {X Y: Type} :
  forall (u v: list X) (f: X -> Y),
    Subsequence u v -> Subsequence (map f u) (map f v).
Proof.
  intros u v. generalize u. induction v; intros u0 f; intro H.
  apply Subsequence_nil_r. destruct H. destruct H. destruct H.
  exists (map f x). exists (map f x0). split.
  rewrite <- map_cons. rewrite <- map_app. rewrite H. reflexivity.
  apply IHv. assumption.
Qed.


Theorem Subsequence_flat_map {X Y: Type} :
  forall (u v: list X) (f: X -> (list Y)),
    Subsequence u v -> Subsequence (flat_map f u) (flat_map f v).
Proof.
  intros u v. generalize u. induction v; intros u0 f; intro H.
  apply Subsequence_nil_r. destruct H. destruct H. destruct H.
  rewrite H. rewrite flat_map_app. apply Subsequence_app2_l.
  simpl. apply Subsequence_app. apply Subsequence_id. apply IHv.
  assumption.
Qed.


Theorem Subsequence_flat_map_2 {X Y: Type} :
  forall u (f g: X -> list (list Y)), (forall x,
    In x u -> Subsequence (f x) (g x))
      -> Subsequence (flat_map f u) (flat_map g u).
Proof.
  intro u. induction u; intros. apply Subsequence_id.
  simpl. apply Subsequence_app. apply H. apply in_eq.
  apply IHu. intros. apply H. apply in_cons. assumption.
Qed.


Theorem Subsequence_double_cons {X: Type} :
  forall (u v: list X) a b, Subsequence (a::u) (b::v) -> Subsequence u v.
Proof.
  intros u v a b. intro H. apply Subsequence_bools in H.
  destruct H as [t]. destruct H. destruct t. apply O_S in H. contradiction.
  destruct b0. inversion H0. apply Subsequence_bools. exists t.
  split; inversion H; reflexivity.
  simpl in H0. apply Subsequence_cons_r with (a := b).
  apply Subsequence_bools. exists t.
  split; inversion H. reflexivity. assumption.
Qed.


Theorem Subsequence_self_filter {X: Type} :
  forall (u: list X) f, Subsequence u (filter f u).
Proof.
  intros. apply Subsequence_bools. exists (map f u). split.
  apply length_map. induction u. reflexivity.
  simpl. destruct (f a). rewrite IHu. reflexivity. apply IHu.
Qed.


Theorem Subsequence_filter {X: Type} :
  forall (u v: list X) f, Subsequence (filter f u) v -> Subsequence u v.
Proof.
  intros u v f. apply Subsequence_trans. apply Subsequence_self_filter.
Qed.


Theorem Subsequence_incl {X: Type} :
  forall (u v: list X), Subsequence u v -> incl v u.
Proof.
  intros u v. intro H. intro a. intro I.
  generalize I. generalize H. generalize u.
  induction v; intros u0; intros J K. inversion K.
  destruct K. rewrite H0 in J.
  destruct J. destruct H1. destruct H1. rewrite H1.

  assert (J: forall (u v : list X) w, In w (u++w::v)).
  intro  u1. induction u1; intros v0 w.
  left. reflexivity. right. apply IHu1. apply J. apply IHv;
  apply Subsequence_cons_r in H ; apply Subsequence_cons_r in J; assumption.
Qed.


Theorem Subsequence_split {X: Type} :
  forall (u v w: list X),
    Subsequence (u++v) w 
    -> (exists a b, w = a++b /\ (Subsequence u a) /\ Subsequence v b).
Proof.
  intros u v w. intro H.
  apply Subsequence_bools in H. destruct H. destruct H.

  assert (forall (b: list bool) (u v: list X),
    length b = length (u++v)
    -> exists x y, b = x++y /\ length x = length u /\ length y = length v).
  intros b u0 v0. intro I. rewrite length_app in I.
  exists (firstn (length u0) b). exists (skipn (length u0) b).
  split. symmetry. apply firstn_skipn.
  split. rewrite firstn_length_le. reflexivity. rewrite I.
  apply PeanoNat.Nat.le_add_r. rewrite length_skipn.
  rewrite I. rewrite Nat.add_sub_swap. rewrite PeanoNat.Nat.sub_diag.
  reflexivity. apply le_n.
  apply H1 in H. destruct H. destruct H. destruct H. destruct H2.

  exists (map snd (filter fst (combine x0 u))).
  exists (map snd (filter fst (combine x1 v))).
  rewrite <- map_app. rewrite <- filter_app.

  assert (L: forall (g i: list bool) (h j: list X), length g = length h
    -> (combine g h) ++ (combine i j) = combine (g++i) (h++j)).
  intro g. induction g; intros i h j; intro I.
  symmetry in I. apply length_zero_iff_nil in I. rewrite I. reflexivity.
  destruct h. apply PeanoNat.Nat.neq_succ_0 in I. contradiction.
  simpl. rewrite IHg. reflexivity. inversion I. reflexivity.
  rewrite L. rewrite <- H.
  split. assumption. split; apply Subsequence_bools;
  [ exists x0 | exists x1]; split; try assumption; reflexivity.

  assumption.
Qed.


Theorem Subsequence_split_2 {X: Type} :
  forall (u v w: list X),
    Subsequence u (v++w) 
    -> (exists a b, u = a++b /\ (Subsequence a v) /\ Subsequence b w).
Proof.
  intros u v w. intro H.
  apply Subsequence_as_flat_map in H.
  destruct H. destruct H. destruct H.
  exists (x ++
     flat_map (fun e : X * list X => fst e :: snd e) (combine v x0)).
  exists (
     flat_map (fun e : X * list X => fst e :: snd e)
     (combine w (skipn (length v) x0))).
   split. rewrite H0. rewrite <- app_assoc. apply app_inv_head_iff.
   rewrite <- flat_map_app.

   assert (L: forall (w y: list X) (z : list (list X)),
     length z = length w + length y ->
     combine (w ++ y) z = combine w z ++ combine y (skipn (length w) z)).
   intros w0. induction w0; intros y z; intro I. reflexivity.
   destruct z. simpl. rewrite combine_nil. reflexivity.
   simpl. rewrite IHw0. reflexivity. inversion I. reflexivity.
   rewrite L. reflexivity. rewrite length_app in H. rewrite H.
   reflexivity. split.

   apply Subsequence_as_flat_map. exists x. exists (firstn (length v) x0).
   split. rewrite length_firstn. rewrite <- H. rewrite length_app.
   rewrite min_l. reflexivity. apply PeanoNat.Nat.le_add_r.
   rewrite combine_firstn_l. reflexivity.

   apply Subsequence_as_flat_map. exists nil. exists (skipn (length v) x0).
   split. rewrite length_app in H. rewrite length_skipn. rewrite <- H.
   rewrite PeanoNat.Nat.add_sub_swap. rewrite PeanoNat.Nat.sub_diag.
   reflexivity. reflexivity. reflexivity.
 Qed.


Theorem Subsequence_split_3 {X: Type} :
  forall (u v w: list X), Subsequence u (v++w) -> Subsequence u w.
Proof.
  intros u v. generalize u. induction v; intros. assumption.
  rewrite <- app_comm_cons in H. apply Subsequence_cons_r in H.
  apply IHv. assumption.
Qed.


Theorem Subsequence_remove_middle {X: Type} :
  forall (u x y: list X) a, Subsequence u (x++a::y) -> Subsequence u (x++y).
Proof.
  intros u x. generalize u. induction x; intros.
  apply Subsequence_cons_r in H. assumption.
  rewrite <- app_comm_cons in H. simpl in H. destruct H. destruct H.
  destruct H. rewrite <- app_comm_cons. apply IHx in H0.
  exists x0. exists x1. split; assumption.
Qed.


Theorem Subsequence_split_4 {X: Type} :
  forall (u v w: list X), Subsequence u (v++w) -> Subsequence u v.
Proof.
  intros. rewrite Subsequence_rev in H.
  rewrite rev_app_distr in H. apply Subsequence_split_3 in H.
  apply Subsequence_rev. assumption.
Qed.


Theorem Subsequence_app2_r {X: Type} :
  forall (l1 l2 l3: list X),
    Subsequence l1 l3 -> Subsequence (l1++l2) l3.
Proof.
  intros. apply Subsequence_rev. rewrite rev_app_distr.
  apply Subsequence_app2_l. rewrite <- Subsequence_rev.
  assumption.
Qed.


Theorem Subsequence_filter_2 {X: Type} :
  forall (u v: list X) f,
    Subsequence u v -> Subsequence (filter f u) (filter f v).
Proof.
  intros u v. generalize u. induction v; intros u0 f; intro H.
  apply Subsequence_nil_r.

  assert (f a = true \/ f a = false).
  destruct (f a); [ left | right ]; reflexivity. destruct H0.
  simpl. rewrite H0.
  replace (a::v) with ([a] ++ v) in H. apply Subsequence_split_2 in H.
  destruct H. destruct H. destruct H. destruct H1.

  assert (Subsequence ((filter f x) ++ (filter f x0))
                      ((filter f [a]) ++ (filter f v))).
  apply Subsequence_app. simpl. rewrite H0.
  apply Subsequence_incl in H1. apply incl_cons_inv in H1. destruct H1.
  assert (In a (filter f x)). apply filter_In. split; assumption.
  apply Subsequence_In. assumption. apply IHv. assumption.
  rewrite <- filter_app in H3. rewrite <- H in H3. simpl in H3.
  rewrite H0 in H3. assumption. reflexivity.
  simpl. rewrite H0. apply IHv. apply Subsequence_cons_r in H.
  assumption.
Qed.


Theorem Subsequence_filter_3 {X: Type} :
  forall (u v: list X) f,
    Subsequence u v -> Subsequence u (filter f v).
Proof.
  intros u v f. intro H.
  apply Subsequence_trans with (l2 := filter f u).
  apply Subsequence_self_filter. apply Subsequence_filter_2.
  assumption.
Qed.


Theorem Subsequence_add {X: Type} :
  forall (u v w: list X) x, Subsequence u v -> Add x u w -> Subsequence w v.
Proof.
  intros u v w. generalize u v.
  induction w; intros u0 v0 x; intros H I; inversion I.
  rewrite <- H2. apply Subsequence_cons_l. rewrite <- H3. assumption.

  assert (J := H). apply Subsequence_bools in H. rewrite <- H1 in H.
  rewrite H0 in H. destruct H. destruct H.
  destruct x1. apply O_S in H. contradiction. destruct b.
  rewrite H4. simpl. apply Subsequence_cons_eq.
  apply IHw with (u := l) (x := x).
  apply Subsequence_bools. exists x1.
  inversion H. split; reflexivity. assumption.
  apply Subsequence_cons_r with (a:=a).
  rewrite H4. simpl. apply Subsequence_cons_eq.
  apply IHw with (u := l) (x := x).
  apply Subsequence_bools. exists x1.
  inversion H. split; reflexivity. assumption.
Qed.


Theorem Subsequence_NoDup {X: Type} :
  forall (u v: list X), Subsequence u v -> NoDup u -> NoDup v.
Proof.
  intros u v. intros H I. apply Subsequence_bools in H.
  destruct H. destruct H. rewrite H0.

  assert (J: forall z (q: list X),
    NoDup q -> NoDup (map snd (filter fst (combine z q)))).
  intro z. induction z; intro q; intro J. apply NoDup_nil.
  destruct q. rewrite combine_nil. apply NoDup_nil.
  destruct a. simpl. rewrite NoDup_cons_iff in J. destruct J.
  apply IHz in H2. apply NoDup_cons.

  assert (K: forall (q: list X) g h,
    ~ In g q -> ~ In g (map snd (filter fst (combine h q)))).
  intro q0. induction q0; intros g h; intro J.
  rewrite combine_nil. easy. destruct h. easy. destruct b.
  replace (map snd (filter fst (combine (true :: h) (a :: q0))))
    with (a::(map snd (filter fst (combine h q0)))).
  rewrite not_in_cons. split;
  rewrite not_in_cons in J; destruct J. assumption. apply IHq0.
  assumption. reflexivity. apply IHq0.
  rewrite not_in_cons in J; destruct J. assumption. apply K. assumption.
  assumption. simpl. apply IHz.
  rewrite NoDup_cons_iff in J. destruct J. assumption. apply J.
  assumption.
Qed.


Theorem Subsequence_in_2 {X: Type} :
  forall (u v: list X) a, Subsequence u v -> In a v -> In a u.
Proof.
  intros u v. generalize u. induction v; intros u0 a0; intros H I.
  contradiction I. destruct I.
  destruct H. destruct H. destruct H. rewrite H. rewrite H0.
  apply in_elt. apply IHv. apply Subsequence_cons_r in H.
  assumption. assumption.
Qed.


Theorem Subsequence_not_in {X: Type} :
  forall (u v: list X) a, Subsequence u v -> ~ In a u -> ~ In a v.
Proof.
  intros u v. generalize u. induction v; intros u0 a0; intros H I.
  easy. apply not_in_cons. split. apply Subsequence_incl in H.
  apply incl_cons_inv in H. destruct H.

  assert (forall z (x y: X), In x z -> ~ In y z -> x <> y).
  intro z. induction z; intros x y; intros J K. inversion J.
  apply in_inv in J. destruct J. rewrite <- H1.
  apply not_in_cons in K. destruct K. apply not_eq_sym. assumption.
  apply IHz. assumption.
  apply not_in_cons in K. destruct K. assumption. apply not_eq_sym.
  generalize I. generalize H. apply H1. generalize I. apply IHv.
  apply Subsequence_cons_r in H. assumption.
Qed.


Theorem Subsequence_as_filter {X: Type} :
  forall (u: list X) f, Subsequence u (filter f u).
Proof.
  intros u f. apply Subsequence_bools.
  exists (map f u). split. apply length_map.
  induction u. reflexivity. simpl. destruct (f a).
  simpl. rewrite IHu. reflexivity. assumption.
Qed.


Theorem Subsequence_as_partition {X: Type} :
  forall (u v w: list X) f,
    (v, w) = partition f u -> (Subsequence u v) /\ (Subsequence u w).
Proof.

  intros u v w f. intro H. rewrite partition_as_filter in H.
  split; [ replace v with (fst (v, w)) | replace w with (snd (v, w))].
  rewrite H. simpl. apply Subsequence_as_filter. reflexivity.
  rewrite H. simpl. apply Subsequence_as_filter. reflexivity.
Qed.


Theorem Subsequence_nodup_in_notin {X: Type} :
  forall (v u: list X), NoDup u ->
    Subsequence u v -> u <> v -> exists x, In x u /\ ~ In x v.
Proof.
  intro v. induction v; intros.
  destruct u. contradiction. exists x. split. apply in_eq. apply in_nil.
  destruct H0. destruct H0. destruct H0.
  destruct x. rewrite H0 in H1. simpl in H1.
  rewrite H0 in H. apply NoDup_cons_iff in H. destruct H.
  apply IHv in H2. destruct H2. destruct H2.
  exists x. split. rewrite H0. apply in_cons. assumption.
  apply not_in_cons. split. intro.
  rewrite H5 in H2. apply H in H2. contradiction. assumption. assumption.
  intro. rewrite H4 in H1. contradiction.
  exists x. split. rewrite H0. apply in_eq.
  intro. rewrite <- Subsequence_cons_eq with (a := a) in H2.
  apply Subsequence_incl in H2. apply H2 in H3. rewrite H0 in H.
  rewrite <- app_comm_cons in H.
  apply NoDup_cons_iff in H. destruct H. apply H.
  apply in_or_app. right. assumption.
Qed.


Theorem Subsequence_nodup_nodup {X: Type} :
  forall (u v: list X), NoDup u -> Subsequence u v -> NoDup v.
Proof.
  intros u v. generalize u. induction v; intros.
  apply NoDup_nil. destruct H0. destruct H0. destruct H0.
  apply NoDup_cons. intro. apply Subsequence_in_2 with (u := x0) in H2.
  apply in_split in H2. destruct H2. destruct H2. rewrite H2 in H0.
  rewrite H0 in H. apply NoDup_app_remove_l in H.
  apply NoDup_cons_iff in H. destruct H. apply H. apply in_elt. assumption. 
  apply IHv with (u := x0). rewrite H0 in H. apply NoDup_app_remove_l in H.
  apply NoDup_cons_iff in H. destruct H. assumption. assumption.
Qed.


Theorem Subsequence_cardinality {X: Type} :
  forall (l : list X) (q : list (list X)),
    NoDup q -> (forall s, In s q -> Subsequence l s)
    -> length q <= 2^length l.
Proof.
  assert (L1: forall (l : list X) (q1 : list (list X)) q2,
    NoDup q1 -> (forall s, In s q1 -> Subsequence l s)
    -> (forall (u: list bool), length u = length l <-> In u q2)
    -> length q1 <= length q2).
  intros l q1 q2. intros H I K.
  assert (incl q1 (map (fun t => map snd (filter fst (combine t l))) q2)).
  intro a0. intro J1. apply I in J1. apply Subsequence_bools in J1.
  destruct J1. destruct H0. rewrite H1. apply K in H0.
  apply in_map with (f := fun t => map snd (filter fst (combine t l))) in H0.
  assumption. apply NoDup_incl_length in H0. rewrite length_map in H0.
  assumption. assumption.

  assert (L2: forall (l: list X), exists (q: list (list bool)),
    (forall u, length u = length l <-> In u q) /\ length q = 2^length l).
  intro l. induction l. exists [nil]. split. intro u. split; intro H.
  apply length_zero_iff_nil in H. rewrite H. apply in_eq.
  destruct H. rewrite <- H. reflexivity. destruct H. reflexivity.
  destruct IHl. destruct H.
  exists ((map (fun e => false::e) x) ++ (map (fun e => true::e) x)).
  split. intro u. split. intro I.
  destruct u. apply O_S in I. contradiction. inversion I. apply H in H2.
  apply in_or_app. destruct b.
  right; apply in_map with (f := fun t => true :: t) in H2; assumption.
  left; apply in_map with (f := fun t => false :: t) in H2; assumption.
  intro I. apply in_app_or in I. destruct I;
  apply in_map_iff in H1; destruct H1; destruct H1; rewrite <- H1;
  apply H in H2; simpl; rewrite H2; reflexivity.
  rewrite length_app. rewrite length_map. rewrite length_map.
  rewrite H0. simpl. rewrite Nat.add_0_r. reflexivity.

  intros l q. intros H I.
  assert (exists (q2 : list (list bool)),
    (forall u, length u = length l <-> In u q2) /\ length q2 = 2^(length l)).
  apply L2. repeat destruct H0. rewrite <- H1.
  generalize H0. apply L1. assumption. assumption.
Qed.


Theorem Subsequence_cardinality2 {X: Type} :
  forall (l : list X) (q : list (list X)),
    NoDup l -> NoDup q -> (forall s, In s q <-> Subsequence l s)
    -> length q = 2^length l.
Proof.
  assert (L: forall (l: list X), exists (q: list (list bool)),
    (forall u, length u = length l <-> In u q)
    /\ length q = 2^length l /\ NoDup q).
  intro l. induction l. exists [nil]. split. intro u. split; intro H.
  apply length_zero_iff_nil in H. rewrite H. apply in_eq. destruct H.
  rewrite <- H. reflexivity. destruct H. split. reflexivity. apply NoDup_cons.
  easy. apply NoDup_nil. destruct IHl. destruct H. destruct H0.
  exists ((map (fun e => false::e) x) ++ (map (fun e => true::e) x)).
  split. intro u. split. intro I.
  destruct u. apply O_S in I. contradiction. inversion I. apply H in H3.
  apply in_or_app. destruct b.
  right; apply in_map with (f := fun t => true :: t) in H3; assumption.
  left; apply in_map with (f := fun t => false :: t) in H3; assumption.
  intro I. apply in_app_or in I. destruct I;
  apply in_map_iff in H2; destruct H2; destruct H2; rewrite <- H2;
  apply H in H3; simpl; rewrite H3; reflexivity.
  rewrite length_app. rewrite length_map. rewrite length_map. split.
  rewrite H0. simpl. rewrite Nat.add_0_r. reflexivity.

  assert (forall x' (b:bool),
      NoDup x' -> NoDup (map (@tail _) (map (fun e => b::e) x'))).
  intros x' b. intro I. rewrite map_map. simpl.
  assert (forall (x'' : list (list bool)), map (fun e => e) x'' = x'').
  intro x''. induction x''. reflexivity. simpl. rewrite IHx''. reflexivity.
  rewrite H2. assumption.

  assert (forall (u v : list (list bool)),
    NoDup u -> NoDup v -> (forall z, In z u -> ~ In z v) -> NoDup (u++v)).
  intro u; induction u; intro v; intros K1 K2 K3. assumption.
  apply NoDup_cons. rewrite in_app_iff. intro K. destruct K.
  apply NoDup_cons_iff in K1. destruct K1. apply H4 in H3. contradiction.
  assert (In a0 (a0::u)). apply in_eq. apply K3 in H4. apply H4 in H3.
  contradiction. apply IHu. apply NoDup_cons_iff in K1. destruct K1.
  assumption. assumption. intro z. intro K. apply in_cons with (a := a0) in K.
  apply K3 in K. assumption. apply H3.

  apply H2 with (b := false) in H1. apply NoDup_map_inv in H1. assumption.
  apply H2 with (b := true) in H1. apply NoDup_map_inv in H1. assumption.
  intro z. intro K. intro K1.
  apply in_map_iff in K. destruct K. destruct H4. destruct z.
  symmetry in H4. apply nil_cons in H4. contradiction. inversion b.
  apply in_map_iff in K1. destruct K1. destruct H6.
  inversion H4. rewrite <- H9 in H6. inversion H6.
  apply in_map_iff in K1. destruct K1. destruct H6.
  rewrite <- H4 in H6. inversion H6.

  intros l q; intros H I J.
  assert (exists (q2 : list (list bool)),
    (forall u, length u = length l <-> In u q2)
    /\ length q2 = 2^(length l) /\ NoDup q2).
  apply L. repeat destruct H0. destruct H1. rewrite <- H1.

  assert (forall s,
    In s q <-> In s (map (fun e => map snd (filter fst (combine e l))) x)).
  intro s. split; intro K. apply J in K. apply Subsequence_bools in K.
  destruct K. destruct H3. rewrite H4. assert (In x0 x). apply H0.
  assumption.
  apply in_map with (f := fun e => map snd (filter fst (combine e l))).
  assumption.
  apply J. apply Subsequence_bools. apply In_nth with (d := nil) in K.
  destruct K. destruct H3. exists (nth x0 x nil). split. apply H0.
  apply nth_In. rewrite length_map in H3. assumption. rewrite <- H4.
  pose (f := fun e => map snd (filter fst (combine e l))). fold f.
  replace (nth x0 (map f x) []) with (f (nth x0 x nil)).
  unfold f. reflexivity. symmetry. replace nil with (f nil) at 1.
  apply map_nth. reflexivity.

  assert (length (map (fun e => map snd (filter fst (combine e l))) x)
                       <= length q). apply NoDup_incl_length.

  assert (forall (l': list X) (x': list (list bool)),
    NoDup l' -> NoDup x'
    -> (forall u : list bool, length u = length l' <-> In u x')
    -> NoDup (map (fun e => map snd (filter fst (combine e l'))) x')).

  (* lemme intermédiaire *)
  assert (M: forall (l': list X) u v,
    length u = length l' -> length v = length l'
    -> NoDup l'
    -> map snd (filter fst (combine u l')) = map snd (filter fst (combine v l'))
    -> u = v).
  intro l'. induction l'; intros u v; intros K1 K2 K3 K4.
  apply length_zero_iff_nil in K1. apply length_zero_iff_nil in K2.
  rewrite K1. rewrite K2. reflexivity.

  destruct u; destruct v. reflexivity.
  apply O_S in K1. contradiction. apply O_S in K2. contradiction.

  destruct b; destruct b0. inversion K4. apply IHl' in H5. rewrite H5.
  reflexivity. inversion K1. reflexivity. inversion K2. reflexivity.
  apply NoDup_cons_iff in K3. destruct K3. assumption.

  assert (In a (map snd (filter fst (combine v l')))).
  simpl in K4. rewrite <- K4. apply in_eq. apply in_map_iff in H4.
  destruct H4 as [x']. destruct H4. apply filter_In in H5. destruct H5.
  destruct x'. apply in_combine_r in H5. simpl in H4. rewrite H4 in H5.
  apply NoDup_cons_iff in K3. destruct K3. apply H7 in H5. contradiction.

  assert (In a (map snd (filter fst (combine u l')))).
  simpl in K4. rewrite K4. apply in_eq. apply in_map_iff in H4.
  destruct H4 as [x']. destruct H4. apply filter_In in H5. destruct H5.
  destruct x'. apply in_combine_r in H5. simpl in H4. rewrite H4 in H5.
  apply NoDup_cons_iff in K3. destruct K3. apply H7 in H5. contradiction.

  inversion K4. apply IHl' in H5. rewrite H5.
  reflexivity. inversion K1. reflexivity. inversion K2. reflexivity.
  apply NoDup_cons_iff in K3. destruct K3. assumption.
  (* fin du lemme M *)

  intros l' x'. intros K1 K2 K3.
  pose (f := fun e => map snd (filter fst (combine e l'))).
  rewrite NoDup_nth with (d := f nil).
  intros i j. intros K4 K5 K6. fold f in K6.
  rewrite map_nth in K6. rewrite map_nth in K6.
  rewrite length_map in K4. rewrite length_map in K5. apply M in K6.
  generalize K6. apply NoDup_nth. assumption. assumption. assumption.
  rewrite K3. apply nth_In. assumption.
  rewrite K3. apply nth_In. assumption.
  assumption. apply H4; assumption.

  intro. intro. apply H3. assumption. rewrite length_map in H4.
  assert (length q <= length x). rewrite H1. apply Subsequence_cardinality.
  assumption. intro s. intro K. apply J in K. assumption.

  apply Nat.le_antisymm; assumption.
Qed.


Theorem Subsequence_new {X: Type} :
  forall (u v: list X) a,
    Subsequence (a::u) v -> ~ Subsequence u v -> head v = Some a.
Proof.
  intros. apply Subsequence_bools in H. destruct H. destruct H.
  destruct x. apply O_S in H. contradiction. destruct v.
  assert False. apply H0. apply Subsequence_nil_r. contradiction. simpl.
  destruct b. inversion H1. reflexivity.
  simpl in H1. assert False. apply H0. apply Subsequence_bools.
  exists x. split. inversion H. reflexivity. assumption.
  contradiction.
Qed.



Set Implicit Arguments.

Section Subsequence_dec.

Variable X: Type.
Hypothesis eq_dec : forall x y: X, {x=y} + {x <> y}.


(**********************************************************)
(** * Decidability of the property of being a subsequence *)
(**********************************************************)

Theorem Subsequence_dec:
  forall l s : list X, { Subsequence l s } + { ~ Subsequence l s }.
Proof.
  intro l. induction l; intro s. destruct s. left.
  apply Subsequence_nil_r. right. apply Subsequence_nil_cons_r.

  assert ({ Subsequence l s} + { ~ Subsequence l s }).
  apply IHl. destruct H.

  rewrite <- Subsequence_cons_eq with (a := a) in s0.
  apply Subsequence_cons_r in s0. apply Subsequence_bools in s0.
  left. apply Subsequence_bools. assumption.

  destruct s. left. apply Subsequence_nil_r.
  assert ({x=a}+{x<>a}). apply eq_dec. destruct H. rewrite e.
  destruct IHl with (s := s); [ left | right ];
  rewrite Subsequence_cons_eq. assumption. assumption.

  right. intro I. apply Subsequence_bools in I.
  destruct I. destruct H. destruct x0.
  symmetry in H0. apply nil_cons in H0. contradiction H0.
  destruct b. inversion H0. rewrite H2 in n0. easy.
  apply n. apply Subsequence_bools. exists x0; split; inversion H; easy.
Qed.


Theorem Subsequence_self_remove :
  forall u x, Subsequence u (remove eq_dec x u).
Proof.
  intros u. induction u; intros x. apply Subsequence_nil_r.
  assert ({a=x}+{a<>x}). apply eq_dec.
  destruct H.
  rewrite e. rewrite remove_cons. apply Subsequence_cons_l. apply IHu.
  simpl. destruct (eq_dec x a). rewrite e in n. contradiction.
  apply Subsequence_cons_eq. apply IHu.
Qed.


Theorem Subsequence_remove :
  forall u v x,
    Subsequence (remove eq_dec x u) v -> Subsequence u v.
Proof.
  intros u v x. apply Subsequence_trans. apply Subsequence_self_remove.
Qed.


Theorem Subsequence_remove_2 :
  forall u v x,
    Subsequence u v -> Subsequence (remove eq_dec x u) (remove eq_dec x v).
Proof.
  intros u v. generalize u. induction v; intros u0 x; intro H.
  apply Subsequence_nil_r.
  
  assert ({a=x}+{a<>x}). apply eq_dec.
  destruct H0. rewrite e. rewrite remove_cons. apply IHv.
  apply Subsequence_cons_r with (a := a). assumption.
  simpl. destruct (eq_dec x a). rewrite e in n. contradiction.

  destruct H. destruct H. destruct H.
  exists (remove eq_dec x x0). exists (remove eq_dec x x1).
  split. rewrite H. rewrite remove_app. rewrite app_inv_head_iff.
  simpl. destruct (eq_dec x a). rewrite e in n. contradiction.
  reflexivity. apply IHv. assumption.
Qed.


Theorem Subsequence_remove_3 :
  forall u v x,
    Subsequence u v -> Subsequence u (remove eq_dec x v).
Proof.
  intros u v x. intro H.
  apply Subsequence_trans with (l2 := remove eq_dec x u).
  apply Subsequence_self_remove. apply Subsequence_remove_2.
  assumption.
Qed.


Theorem Subsequence_strong : forall (u v: list X) (a: X),
  Subsequence u (a::v)
    <-> exists l1 l2, u = l1 ++ a::l2 /\ ~ In a l1 /\ Subsequence l2 v.
Proof.
  intros. split. generalize a v. induction u; intros.
  apply Subsequence_nil_cons_r in H. contradiction.
  destruct (eq_dec a0 a1). rewrite e in H. rewrite e.
  apply Subsequence_cons_eq in H. exists nil. exists u.
  split. reflexivity. split. apply in_nil. assumption.
  apply Subsequence_cons_diff in H. apply IHu in H.
  destruct H. destruct H.
  exists (a0::x). exists x0. destruct H. destruct H0.
  split. rewrite H. reflexivity.
  split. intro. apply in_inv in H2. destruct H2. apply n in H2.
  assumption. apply H0 in H2. assumption. assumption. assumption.
  intro. destruct H. destruct H. destruct H. destruct H0.
  rewrite H. exists x. exists x0. split. reflexivity. assumption.
Qed.


Theorem Subsequence_nodup : forall (u v: list X),
  Subsequence u v -> Subsequence u (nodup eq_dec v).
Proof.
  intros u v. generalize u. induction v; intros. apply Subsequence_nil_r.
  simpl. destruct (in_dec eq_dec a v). apply IHv.
  apply Subsequence_cons_r with (a := a). assumption.
  destruct H. destruct H. destruct H.
  exists x. exists x0. split. assumption. apply IHv. assumption.
Qed.


End Subsequence_dec.
