Require Import subsequence.
Require Import exists_set.
Require Import utilities.
Require Import Sorting.Permutation.

Require Import Nat.
Require Import PeanoNat.
Require Import List.


Import ListNotations.



Definition Complete_sequence {X: Type} (u base : list X) :=
  forall v, Permutation base v -> Subsequence u v.

Definition Shortest_complete_sequence {X: Type} (u base : list X) :=
  Complete_sequence u base
  /\ forall v, length v < length u -> ~ Complete_sequence v base.


Theorem Complete_sequence_permut_base {X: Type} :
  forall (u base p : list X),
    Complete_sequence u base -> Permutation base p -> Complete_sequence u p.
Proof.
  intros. repeat intro. apply H.
  apply Permutation_trans with (l' := p); assumption.
Qed.


Lemma Complete_sequence_nil {X: Type} :
  forall (u : list X), Complete_sequence u nil.
Proof.
  repeat intro. destruct v. apply Subsequence_nil_r.
  apply Permutation_nil_cons in H. contradiction.
Qed.


Theorem Shortest_complete_sequence_nil {X: Type} :
  forall (u: list X), Shortest_complete_sequence u nil -> u = nil.
Proof.
  intros. destruct u. reflexivity. destruct H.
  assert (~ Complete_sequence (nil: list X) nil).
  apply H0. apply Nat.lt_0_succ.
  assert False. apply H1. apply Complete_sequence_nil. contradiction.
Qed.


Lemma Complete_sequence_nil_l {X: Type} :
  forall (base : list X), Complete_sequence nil base -> base = nil.
Proof.
  intros. destruct base. reflexivity.
  assert (Permutation (x::base) (x::base)). apply Permutation_refl.
  apply H in H0. apply Subsequence_nil_cons_r in H0. contradiction.
Qed.


Theorem Complete_sequence_rev {X: Type} :
  forall (u base : list X),
  Complete_sequence u base <-> Complete_sequence (rev u) base.
Proof.
  intros u base. split; intro H;
  intro p; intro I; apply Subsequence_rev; try rewrite rev_involutive;
  apply H; apply Permutation_trans with (l' := p).
  assumption. apply Permutation_rev. assumption. apply Permutation_rev.
Qed.


Theorem Complete_sequence_cons {X: Type} :
  forall (u base : list X) a,
  Complete_sequence u base -> Complete_sequence (a::u) base.
Proof.
  intros. intro v. intro I.
  apply Subsequence_cons_l. apply H. assumption.
Qed.


Lemma Complete_sequence_in {X: Type} :
  forall (u base: list X), Complete_sequence u base -> incl base u.
Proof.
  intros. repeat intro. apply in_split in H0. destruct H0. destruct H0.
  assert (Permutation base (x++a::x0)). rewrite H0. apply Permutation_refl.
  apply H in H1. apply Subsequence_split_2 in H1.
  destruct H1. destruct H1. destruct H1. destruct H2.
  apply Subsequence_app with (l1 := x1) (s1 := nil) in H3.
  rewrite <- H1 in H3. destruct H3. destruct H3. destruct H3. rewrite H3.
  apply in_elt. apply Subsequence_nil_r.
Qed.


Lemma Complete_sequence_not_in {X: Type} :
  forall (u base: list X) a, Complete_sequence u base
    -> ~ In a u -> ~ In a base.
Proof.
  intros. intro. apply Complete_sequence_in with (u := u) in H1.
  apply H0 in H1. assumption. assumption.
Qed.


Theorem Complete_sequence_extend_left {X: Type } :
  forall (u v base : list X),
  Complete_sequence u base -> Complete_sequence (v++u) base.
Proof.
  intros u v. generalize u. induction v; intros. assumption.
  rewrite <- app_comm_cons. apply Complete_sequence_cons. apply IHv.
  assumption.
Qed.


Theorem Complete_sequence_extend_right {X: Type } :
  forall (u v base : list X),
  Complete_sequence u base -> Complete_sequence (u++v) base.
Proof.
  intros. apply Complete_sequence_rev in H.
  replace (u++v) with (rev ((rev v) ++ (rev u))).
  rewrite <- Complete_sequence_rev. apply Complete_sequence_extend_left.
  assumption. rewrite <- rev_app_distr. apply rev_involutive.
Qed.


Theorem Complete_sequence_middle_element {X: Type} :
  forall (x y base : list X) a,
    Complete_sequence (x++y) base -> Complete_sequence (x++a::y) base.
Proof.
  intros. intro. intro. apply H in H0. apply Subsequence_split in H0.
  destruct H0. destruct H0. destruct H0. destruct H1. rewrite H0.
  apply Subsequence_app. assumption. apply Subsequence_cons_l.
  assumption.
Qed.


Theorem Complete_sequence_extend_middle {X: Type} :
  forall (x y z base : list X),
  Complete_sequence (x++y) base -> Complete_sequence (x++z++y) base.
Proof.
  intros x y z. generalize x. generalize y. induction z; intros.
  assumption. rewrite <- app_comm_cons. apply Complete_sequence_middle_element.
  apply IHz. assumption.
Qed.


Theorem Complete_sequence_cons_not_in_base {X: Type} :
  forall (u base: list X) a,
  Complete_sequence (a::u) base -> ~ In a base -> Complete_sequence u base.
Proof.
  intros. intro. intro.
  assert (~ In a v). intro. apply Permutation_sym in H1.
  apply Permutation_in with (x := a) in H1.
  apply H0 in H1. assumption. assumption. apply H in H1.
  apply Subsequence_bools in H1. destruct H1. destruct H1.
  apply Subsequence_bools. destruct x. apply O_S in H1. contradiction.
  destruct b. simpl in H3. rewrite H3 in H2.
  assert False. apply H2. apply in_eq. contradiction. simpl in H3.
  exists x. split. inversion H1. reflexivity. assumption.
Qed.


Lemma Complete_sequence_neg_not_scs {X: Type} :
  forall (u base: list X),
    ~ Complete_sequence u base -> ~ Shortest_complete_sequence u base.
Proof.
  intros u base. intro H. intro I. destruct I. apply H in H0. assumption.
Qed.


Lemma Complete_sequence_shorten {X: Type} :
  forall (u base: list X) a,
  ~ Complete_sequence (a::u) base -> ~ Complete_sequence u base.
Proof.
  intros. intro. apply H. apply Complete_sequence_cons. assumption.
Qed.


Lemma Complete_sequence_extend_base {X: Type} :
  forall (u base: list X) a,
    ~ Complete_sequence u base -> ~ Complete_sequence u (a::base).
Proof.
  repeat intro. apply H. repeat intro.
  assert (Permutation (a::base) (a::v)). apply perm_skip. assumption.
  apply H0 in H2. apply Subsequence_cons_r in H2. assumption.
Qed.


Lemma Complete_sequence_reduce_base {X: Type} :
  forall (u base: list X) a,
    Complete_sequence u (a::base) -> Complete_sequence u base.
Proof.
  repeat intro.
  assert (Permutation (a::base) (a::v)). apply perm_skip. assumption.
  apply H in H1. apply Subsequence_cons_r in H1. assumption.
Qed.


Lemma Complete_sequence_no_counterexample {X: Type} :
  forall (u base: list X),
    Complete_sequence u base
      -> ~ (exists p, Permutation base p /\ ~ Subsequence u p).
Proof.
  intros. intro. destruct H0. destruct H0.
  apply H in H0. apply H1 in H0. assumption.
Qed.


Theorem Complete_sequence_as_subsequence {X: Type}:
  forall (u v base: list X),
  Subsequence u v -> Complete_sequence v base -> Complete_sequence u base.
Proof.
  repeat intro. apply H0 in H1. generalize H1. apply Subsequence_trans.
  assumption.
Qed.


Lemma Complete_sequence_exists {X: Type} :
  forall (base : list X), exists u, Complete_sequence u base /\ incl u base.
Proof.
  intro base. induction base. exists nil. split. intro. intro.
  apply Permutation_nil in H. rewrite H. apply Subsequence_nil_r. easy.
  destruct IHbase. exists (x ++ a::x). split. intro. intro.
  assert (In a v). apply Permutation_in with (l := a::base). assumption.
  apply in_eq.
  apply in_split in H1. destruct H1. destruct H1. rewrite H1.
  rewrite H1 in H0.
  assert (Permutation (a::x0++x1) (x0++a::x1)). apply Permutation_middle.
  apply Permutation_sym in H2.
  assert (Permutation (a::base) (a::x0++x1)).
  apply perm_trans with (l' := x0++a::x1); assumption.
  apply Permutation_cons_inv in H3.
  assert (Subsequence x (x0++x1)). apply H. assumption.
  apply Subsequence_app.

  assert (Subsequence (x0++x1) (x0++nil)). apply Subsequence_app.
  apply Subsequence_id. apply Subsequence_nil_r. simpl in H5.
  rewrite app_nil_r in H5.
  apply Subsequence_trans with (l2 := (x0++x1)); assumption.

  apply Subsequence_cons_eq.
  assert (Subsequence (x0++x1) (nil++x1)). apply Subsequence_app.
  apply Subsequence_nil_r. apply Subsequence_id.
  apply Subsequence_trans with (l2 := (x0++x1)); assumption.
  destruct H. apply incl_app. apply incl_tl. assumption.
  apply incl_cons. apply in_eq. apply incl_tl. assumption.
Qed.


Lemma Complete_sequence_tail {X: Type} :
  forall (u base: list X) a,
  Complete_sequence (a::u) base -> Complete_sequence u (tail base).
Proof.
  intros. destruct base. apply Complete_sequence_nil.
  simpl. intro. intro. apply perm_skip with (x := x) in H0.
  apply H in H0. apply Subsequence_double_cons in H0.
  assumption.
Qed.


Theorem Complete_sequence_missing_permutation {X: Type} :
  forall (u v base: list X) a,
    Complete_sequence (a::u) base -> ~ Complete_sequence u base
    -> Permutation base v -> ~ Subsequence u v -> head v = Some a.
Proof.
  intros. destruct v.
  assert False. apply H2. apply Subsequence_nil_r. contradiction.
  apply H in H1. apply Subsequence_bools in H1. destruct H1.
  destruct H1. destruct x0. apply O_S in H1. contradiction.
  destruct b. inversion H3. easy.
  assert False. apply H2. apply Subsequence_bools. exists x0. split.
  inversion H1. reflexivity. rewrite H3. reflexivity. contradiction.
Qed.


Theorem Complete_sequence_middle_in_base {X: Type} :
  forall (u w base: list X) a,
    Complete_sequence (u++a::w) base
       -> ~ Complete_sequence (u++w) base -> In a base.
Proof.
  intros.
  assert (exists q, forall v, Permutation base v <-> In v q).
  apply Permutation_exists_set. destruct H1 as [q].
  assert (
    exists q',
    (forall t, In t q' -> length t = length (u++a::w))
    /\ (forall t, In t q'
              -> In (map snd (filter fst (combine t (u++a::w)))) q)
    /\ forall v, In v q -> exists t, In t q'
    /\ v = map snd (filter fst (combine t (u++a::w)))).
  apply Set_of_subsequences_as_set_of_masks.
  intros. apply H1 in H2. apply H in H2. assumption.
  destruct H2 as [q']. destruct H2 as [HK H2]. destruct H2.

  assert (~ forall (tu tw: list bool) ta,
         In (tu ++ ta::tw) q' -> length tu = length u -> ta = false).
  intros. intro. apply H0. intro. intro. apply H1 in H5. assert (I := H5).
  apply H3 in H5. destruct H5. destruct H5.

  assert (forall (p q: list X) (r s: list bool),
    length r = length p
    -> combine (r++s) (p++q) = (combine r p) ++ (combine s q)).
  intros p0 q0 r. generalize p0. generalize q0. induction r; intros.
  symmetry in H7. apply length_zero_iff_nil in H7. rewrite H7.
  reflexivity. destruct p1. inversion H7. simpl. rewrite IHr.
  reflexivity. inversion H7. reflexivity.

  assert (exists tu tw0, tu ++ tw0 = x /\ length tu = length u).
  exists (firstn (length u) x). exists (skipn (length u) x). split.
  apply firstn_skipn. apply firstn_length_le. rewrite HK. rewrite length_app.
  apply Nat.le_add_r. assumption.
  destruct H8 as [tu]. destruct H8 as [tw0].
  destruct H8. destruct tw0. apply HK in H5. rewrite length_app in H5.
  rewrite <- H8 in H5. rewrite <- H9 in H5.
  repeat rewrite length_app in H5. rewrite Nat.add_cancel_l in H5.
  apply O_S in H5. contradiction. destruct b.

  rewrite <- H8 in H5. apply H4 in H5. inversion H5. assumption.
  apply Subsequence_bools.

  apply HK in H5.

  exists ((firstn (length u) x) ++ (skipn (S (length u)) x)).
  split. repeat rewrite length_app. rewrite firstn_length_le.
  rewrite Nat.add_cancel_l. rewrite length_skipn. rewrite H5.
  rewrite length_app. simpl. rewrite Nat.add_succ_r. rewrite Nat.sub_succ.
  rewrite Nat.add_sub_swap. rewrite Nat.sub_diag. reflexivity.
  apply Nat.le_refl. rewrite H5. rewrite length_app. apply Nat.le_add_r.

  rewrite H6. rewrite <- H8. repeat rewrite H7.
  repeat rewrite filter_app. rewrite <- H9.
  replace (length tu) with (length tu + 0). rewrite firstn_app_2.
  rewrite app_nil_r. rewrite Nat.add_0_r. repeat rewrite map_app.
  apply app_inv_head_iff. rewrite skipn_app. rewrite skipn_all2.
  rewrite Nat.sub_succ_l. rewrite Nat.sub_diag. rewrite skipn_cons.
  reflexivity. apply Nat.le_refl. apply Nat.le_succ_diag_r.
  apply Nat.add_0_r. rewrite firstn_length_le. reflexivity.
  rewrite H8. rewrite H5. rewrite length_app. apply Nat.le_add_r. assumption.

  assert (forall q, ~ (forall (tu tw : list bool) (ta : bool),
      In (tu ++ ta :: tw) q -> length tu = length u -> ta = false)
    -> exists tu tw ta, In (tu++ta::tw) q /\ ta = true /\ length tu = length u).
  intro s. induction s; intro.
  assert False. apply H5. intros. apply in_nil in H6. contradiction.
  contradiction.

  assert (length a0 <= length u \/ length u < length a0).
  apply Nat.le_gt_cases. destruct H6.

  assert ( ~ (forall (tu tw : list bool) (ta : bool),
       In (tu ++ ta :: tw) s -> length tu = length u -> ta = false)).
  intro. apply H5. intros. destruct H8.
  rewrite H8 in H6. rewrite <- H9 in H6. rewrite length_app in H6.
  rewrite plus_n_O in H6. apply Nat.add_le_mono_l in H6.
  apply Nat.nle_succ_0 in H6. contradiction.
  apply H7 with (tu := tu) (tw := tw). assumption. assumption.
  apply IHs in H7. destruct H7. destruct H7. destruct H7. destruct H7.
  exists x. exists x0. exists x1. split. apply in_cons. assumption. assumption.

  replace a0 with ((firstn (length u) a0) ++ (skipn (length u) a0)).
  replace a0 with ((firstn (length u) a0) ++ (skipn (length u) a0)) in H5.
  destruct (skipn (length u) a0).

  assert (~ (forall tu tw ta, In (tu++ta::tw) s
       -> length tu = length u -> ta = false)).
  intro. apply H5. intros. apply in_inv in H8. destruct H8.
  assert (length (tu ++ ta::tw) = length (tu ++ ta::tw)). reflexivity.
  rewrite <- H8 in H10 at 2. rewrite <- H9 in H10. rewrite length_app in H10.
  rewrite app_nil_r in H10. rewrite firstn_length_le in H10.
  rewrite <- Nat.add_0_r in H10. rewrite Nat.add_cancel_l in H10. inversion H10.
  replace a0 with ((firstn (length u) a0)++(skipn (length u) a0)).
  rewrite <- Nat.add_0_r at 1. rewrite length_app.
  apply Nat.add_le_mono. rewrite app_nil_r in H8. rewrite H8.
  rewrite length_app. apply Nat.le_add_r. apply le_0_n. apply firstn_skipn.
  apply H7 with (tu := tu) (tw := tw); assumption.
  apply IHs in H7. destruct H7. destruct H7. destruct H7. destruct H7.
  exists x. exists x0. exists x1. split. apply in_cons. assumption. assumption.

  destruct b. exists (firstn (length u) a0). exists l. exists true.
  split. apply in_eq. split. reflexivity.
  apply firstn_length_le. apply Nat.lt_le_incl. assumption.

  assert (~ (forall tu tw ta, In (tu++ta::tw) s
       -> length tu = length u -> ta = false)).
  intro. apply H5. intros. apply in_inv in H8. destruct H8.

  destruct ta.
  assert (forall (w x y z: list bool), w ++ x = y ++ z ->
    length w = length y -> x = z). intro w0. induction w0; intros.
  symmetry in H11. apply length_zero_iff_nil in H11. rewrite H11 in H10.
  assumption. destruct y. apply Nat.neq_succ_0 in H11. contradiction.
  inversion H10. apply IHw0 in H14. assumption. inversion H11. reflexivity.
  apply H10 in H8. inversion H8. rewrite H9. apply firstn_length_le.
  apply Nat.lt_le_incl. assumption. reflexivity.
  apply H7 with (tu := tu) (tw := tw); assumption.

  apply IHs in H7. destruct H7. destruct H7. destruct H7.
  destruct H7. destruct H8. exists x. exists x0. exists true.
  split. apply in_cons. rewrite <- H8. assumption. rewrite H9.
  split; reflexivity. apply firstn_skipn. apply firstn_skipn.

  apply H5 in H4. destruct H4. destruct H4. destruct H4. destruct H4.
  apply H2 in H4. apply H1 in H4. destruct H6. rewrite H6 in H4.

  assert (forall (w x: list bool) (y z: list X),
    length w = length y
       -> combine (w ++ x) (y ++ z) = (combine w y) ++ (combine x z)).
  intro w0. induction w0; intros. symmetry in H8.
  apply length_zero_iff_nil in H8. rewrite H8. reflexivity.
  destruct y. apply Nat.neq_succ_0 in H8. contradiction. simpl.
  f_equal. apply IHw0. inversion H8. reflexivity. rewrite H8 in H4.
  simpl in H4. rewrite filter_app in H4. rewrite map_app in H4. simpl in H4.
  apply Permutation_vs_elt_inv in H4. destruct H4. destruct H4.
  rewrite H4. apply in_elt. assumption.
Qed.


Theorem Complete_sequence_cons_in_base {X: Type} :
  forall (u base: list X) a,
    Complete_sequence (a::u) base -> ~ Complete_sequence u base -> In a base.
Proof.
  intros.
  apply Complete_sequence_middle_in_base with (u := nil) (w := u); assumption.
Qed.


Theorem Shortest_complete_sequence_cons_in_base {X: Type} :
  forall (u base: list X) a,
    Shortest_complete_sequence (a::u) base -> In a base.
Proof.
  intros. destruct H. apply Complete_sequence_cons_in_base in H.
  assumption. apply H0. apply Nat.lt_succ_diag_r.
Qed.


Theorem Complete_sequence_norep {X: Type} :
  forall (u w base : list X) x,
    NoDup base -> Complete_sequence (u ++ x::x::w) base
    -> Complete_sequence (u ++ x::w) base.
Proof.
  intros u w base x. intro K. intro H. intro p. intro J. assert (M := J).
  apply H in J. apply Subsequence_bools in J. destruct J. destruct H0.

  assert (G1: length u <= length x0).
  rewrite H0. rewrite length_app. apply Nat.le_add_r.

  apply Subsequence_bools.
  assert (x0 = (firstn (length u) x0) ++ (skipn (length u) x0)).
  rewrite firstn_skipn. reflexivity.
  destruct (skipn (length u) x0). assert (H3 := H0).
  rewrite H2 in H3. rewrite length_app in H3. rewrite firstn_length_le in H3.
  rewrite length_app in H3. rewrite Nat.add_cancel_l in H3.
  apply O_S in H3. contradiction. assumption.
  destruct l. assert (H3 := H0). rewrite H2 in H3.
  rewrite length_app in H3. rewrite firstn_length_le in H3.
  rewrite length_app in H3. rewrite Nat.add_cancel_l in H3.
  simpl in H3. apply Nat.succ_inj in H3. apply O_S in H3. contradiction.
  assumption.

  destruct b. destruct b0.
  rewrite H2 in H1. rewrite combine_app in H1. rewrite filter_app in H1.
  simpl in H1. rewrite H1 in M. apply Permutation_NoDup in M.
  rewrite map_app in M. apply NoDup_app_remove_l in M.
  apply NoDup_cons_iff in M. destruct M. simpl in H3. assert False.
  apply H3. left. reflexivity. contradiction. assumption.
  symmetry. apply firstn_length_le. assumption.

  exists ((firstn (S (length u)) x0) ++ (skipn (S (S (length u))) x0)).
  split. rewrite length_app. rewrite firstn_length_le. rewrite length_skipn.
  rewrite H0. rewrite length_app. simpl. rewrite <- Nat.add_succ_comm.
  rewrite <- Nat.add_succ_comm.
  rewrite Nat.add_sub_swap. rewrite Nat.sub_diag. rewrite length_app.
  apply plus_n_Sm. apply le_n. rewrite H0.
  rewrite length_app. simpl. rewrite <- Nat.add_succ_comm.
  apply Nat.le_add_r. rewrite H1. rewrite H2.
  rewrite firstn_app. rewrite firstn_firstn. rewrite Nat.min_r.
  rewrite firstn_length_le. rewrite Nat.sub_succ_l.
  rewrite Nat.sub_diag.
  replace (firstn (length u) x0 ++ true::false::l)
    with ((firstn (length u) x0 ++ [true]) ++ (false::l)) at 1.
  replace (u++x::x::w) with ((u++[x]) ++ (x::w)). rewrite combine_app.
  rewrite filter_app. symmetry. replace (u++x::w) with ((u++[x])++w).
  rewrite combine_app. rewrite filter_app. rewrite map_app. rewrite map_app.
  replace (firstn 1 (true::false::l)) with [true].
  rewrite app_inv_head_iff. rewrite skipn_app. rewrite skipn_all2.
  rewrite firstn_length_le. rewrite Nat.sub_succ_l. rewrite Nat.sub_succ_l.
  rewrite Nat.sub_diag. reflexivity.
  apply le_n. apply Nat.le_succ_diag_r. assumption.
  rewrite firstn_length_le. apply le_S. apply Nat.le_succ_diag_r.
  assumption. reflexivity. rewrite length_app. rewrite length_app.
  rewrite firstn_length_le. reflexivity. assumption.
  rewrite <- app_assoc. reflexivity.
  rewrite length_app. rewrite length_app.
  rewrite firstn_length_le. reflexivity. assumption.
  rewrite <- app_assoc. rewrite app_inv_head_iff. reflexivity.
  rewrite <- app_assoc. rewrite app_inv_head_iff. reflexivity.
  apply le_n. assumption. apply Nat.le_succ_diag_r.

  exists ((firstn (length u) x0) ++ (skipn (S (length u)) x0)).
  split. rewrite length_app. rewrite firstn_length_le. rewrite length_skipn.
  rewrite H0. rewrite length_app. simpl. rewrite <- Nat.add_succ_comm.
  rewrite Nat.add_sub_swap. rewrite Nat.sub_diag. rewrite length_app.
  reflexivity. apply le_n. assumption.

  rewrite H1. rewrite H2.
  rewrite firstn_app. rewrite firstn_length_le. rewrite Nat.sub_diag.
  rewrite firstn_firstn. rewrite Nat.min_r. rewrite firstn_O.
  rewrite app_nil_r. repeat rewrite combine_app.
  rewrite filter_app. rewrite filter_app. rewrite map_app. rewrite map_app.
  apply app_inv_head_iff.
  replace (firstn (length u) x0 ++ false::b0::l)
    with ((firstn (length u) x0 ++ [false]) ++ b0::l). rewrite skipn_app.
  rewrite skipn_all2. rewrite length_app. rewrite firstn_length_le.
  rewrite Nat.add_1_r. rewrite Nat.sub_diag. rewrite skipn_O.
  reflexivity. assumption.
  rewrite length_app. rewrite Nat.add_1_r. rewrite firstn_length_le.
  apply le_n. assumption.
  rewrite <- app_assoc. rewrite app_inv_head_iff. reflexivity.
  rewrite firstn_length_le. reflexivity. assumption.
  rewrite firstn_length_le. reflexivity. assumption.
  apply le_n. assumption.
Qed.


Theorem Shortest_complete_sequence_norep {X: Type} :
  forall (u w base : list X) x,
    NoDup base -> ~ Shortest_complete_sequence (u ++ x::x::w) base.
Proof.
  intros u w base x. intro K. intro H. destruct H.
  apply Complete_sequence_norep in H. apply H0 in H. assumption.
  rewrite length_app. rewrite length_app. apply Nat.add_lt_mono_l.
  apply Nat.lt_succ_diag_r. assumption.
Qed.


Theorem Complete_sequence_remove_element {X: Type} :
  forall (u1 u2 base : list X) a,
    Complete_sequence (u1++a::u2) base -> ~ In a base
      -> Complete_sequence (u1++u2) base.
Proof.
  intros.
  assert (forall p, Permutation base p -> Subsequence (u1++u2) p).
  intros. assert (I := H1).
  apply H in H1. apply Subsequence_bools in H1. destruct H1.
  destruct H1. apply Subsequence_bools.

  assert (exists x1 x2, x = x1++x2 /\ length x1 = length u1
      /\ length x2 = S (length u2)).
  exists (firstn (length u1) x).
  exists (skipn (length u1) x).
  split. symmetry. apply firstn_skipn.
  split. apply firstn_length_le.
  rewrite H1. rewrite length_app. apply Nat.le_add_r.
  rewrite length_skipn. rewrite H1. rewrite length_app.
  rewrite Nat.add_sub_swap. rewrite Nat.sub_diag. reflexivity.
  apply Nat.le_refl. destruct H3. destruct H3. destruct H3. destruct H4.

  exists (x0 ++ (tail x1)). split.
  repeat rewrite length_app. rewrite H4. rewrite Nat.add_cancel_l.
  destruct x1. inversion H5. inversion H5. reflexivity.

  symmetry in H4.
  replace x with ((firstn (length u1) x) ++ (skipn (length u1) x)) at 1.
  rewrite combine_app. destruct x1. inversion H5. destruct b.
  rewrite H3 in H2. rewrite combine_app in H2. rewrite filter_app in H2.
  rewrite map_app in H2. simpl in H2. rewrite H2 in I.
  assert False. apply H0. apply Permutation_in with (l :=
      (map snd (filter fst (combine x0 u1)) ++
       a :: map snd (filter fst (combine x1 u2)))).
  apply Permutation_sym. assumption. apply in_elt. contradiction.
  assumption. simpl. rewrite H3 in H2. rewrite combine_app in H2.
  rewrite filter_app in H2. simpl in H2. rewrite filter_app.
  assumption. assumption. assumption. apply firstn_skipn. assumption.
Qed.


Theorem Complete_sequence_not_removable_element {X: Type} :
  forall (u1 u2 base : list X) a,
    ~ In a base -> ~ Shortest_complete_sequence (u1++a::u2) base.
Proof.
  intros. intro. destruct H0.
  assert (length (u1 ++ u2) < length (u1 ++ a::u2)).
  repeat rewrite length_app. apply Nat.add_lt_mono_l.
  apply Nat.lt_succ_diag_r. apply H1 in H2. apply H2.
  apply Complete_sequence_remove_element with (a := a).
  assumption. assumption.
Qed.


Theorem Complete_sequence_base_incl {X: Type} :
  forall (u base: list X), Complete_sequence u base -> incl base u.
Proof.
  intros. apply Subsequence_incl. apply H. apply Permutation_refl.
Qed.


Theorem Shortest_complete_sequence_incl_base {X: Type} :
  forall (u base: list X), Shortest_complete_sequence u base -> incl u base.
Proof.
  intros. intro. intro. apply in_split in H0. destruct H0. destruct H0.
  destruct H. rewrite H0 in H.
  apply Complete_sequence_middle_in_base with (u := x) (w := x0). assumption.
  apply H1. rewrite H0. repeat rewrite length_app.
  apply Nat.add_lt_mono_l. apply Nat.lt_succ_diag_r.
Qed.


Theorem Shortest_complete_sequence_in_base {X: Type} :
  forall (u1 u2 base : list X) a,
    Shortest_complete_sequence (u1++a::u2) base -> In a base.
Proof.
  intros. apply Shortest_complete_sequence_incl_base in H.
  apply H. apply in_elt.
Qed.


Theorem Complete_sequence_greedy {X: Type} :
  forall (u base : list X) a,
    Complete_sequence u (a::base)
    -> exists v w, u = v ++ a::w /\ Complete_sequence w base.
Proof.
  intros.

  assert (exists q, forall v, Permutation base v <-> In v q).
  apply Permutation_exists_set. destruct H0 as [q].

  pose (q' := map (cons a) q).

  assert (exists q'',
      (forall t, In t q'' -> length t = length u)
    /\ (forall t, In t q'' -> In (map snd (filter fst (combine t u))) q')
    /\ forall v, In v q' -> exists t, In t q''
                                 /\ v = map snd (filter fst (combine t u))).
  apply Set_of_subsequences_as_set_of_masks. intros. apply H.
  apply in_map_iff in H1. destruct H1. destruct H1. rewrite <- H1.
  apply perm_skip. apply H0. assumption. destruct H1. destruct H1. destruct H2.
  pose (f := fold_right (fun (b:bool) => if b then (Basics.const 0) else S) 0).
  pose (n := fold_right min (length u) (map f x)).

  assert (I: forall m, In m x -> n <= f m).
  assert (J: forall m, f m <= length m).
  intro m. induction m. easy. destruct a0. apply le_0_n.
  simpl. rewrite <- Nat.succ_le_mono. apply IHm.
  assert (L: forall x m k, m <= k -> In m x -> fold_right min k x <= m).
  intro x'; induction x'; intros. apply in_nil in H5. contradiction.
  destruct H5. rewrite H5. apply Nat.le_min_l. simpl. apply Nat.min_le_iff.
  right. apply IHx'; assumption.
  intros. apply L. rewrite <- H1 with (t := m). apply J. assumption.
  apply in_map. assumption.

  assert (I2: forall m u, length u = length m
    -> nth (f m) u a = hd a (map snd (filter fst (combine m u)))).
  intro m. induction m; intros.
  apply length_zero_iff_nil in H4. rewrite H4. easy.
  destruct u0. inversion H4. destruct a0. reflexivity.
  apply IHm. inversion H4. reflexivity.

  assert (I3: forall m (u: list X), length u = length m
    -> 0 < length (map snd (filter fst (combine m u))) -> f m < length u).
  intro m. induction m; intros. apply Nat.lt_irrefl in H5. contradiction.
  destruct u0. inversion H4. destruct a0. apply Nat.lt_0_succ.
  simpl. rewrite <- Nat.succ_lt_mono. apply IHm. inversion H4. reflexivity.
  apply H5.

  assert (I4: forall n x k, n = fold_right min k (map f x)
  -> n < k -> exists m, In m x /\ n = f m).
  intros n' x'. induction x'; intros. rewrite H4 in H5.
  apply Nat.lt_irrefl in H5. contradiction. simpl in H4.

  assert (f a0 <= fold_right min k (map f x')
         \/ fold_right min k (map f x') < f a0).
  apply Nat.le_gt_cases. destruct H6.
  assert (min (f a0) (fold_right min k (map f x')) = f a0).
  apply Nat.min_l. assumption. rewrite H7 in H4. exists a0. split.
  apply in_eq. assumption.

  assert (min (f a0) (fold_right min k (map f x'))
        = fold_right min k (map f x')). apply Nat.min_r. apply Nat.lt_le_incl.
  assumption. rewrite H7 in H4. apply IHx' in H4. destruct H4. destruct H4.
  exists x0. split. apply in_cons. assumption. assumption. assumption.

  assert (n < length u). destruct x.
  assert (Permutation base base). apply Permutation_refl.
  apply H0 in H4. assert (In (a::base) q'). apply in_map. assumption.
  apply H3 in H5. destruct H5. destruct H5. apply in_nil in H5. contradiction.
  apply Nat.le_lt_trans with (m := f l). apply I. apply in_eq.
  apply I3. rewrite H1. reflexivity. apply in_eq.
  assert (In (map snd (filter fst (combine l u))) q'). apply H2.
  apply in_eq. apply in_map_iff in H4. destruct H4. destruct H4.
  rewrite <- H4. apply Nat.lt_0_succ.

  assert (exists m, In m x /\ n = f m). apply I4 with (k := length u).
  reflexivity. assumption.

  destruct H5. destruct H5. assert (nth n u a = a). rewrite H6. rewrite I2.
  assert (In (map snd (filter fst (combine x0 u))) q'). apply H2.
  assumption. apply in_map_iff in H7. destruct H7. destruct H7. rewrite <- H7.
  easy. rewrite H1. reflexivity. assumption.

  assert (I5 := H4).
  apply nth_split with (d := a) in H4. destruct H4. destruct H4. destruct H4.
  exists x1. exists x2. split. rewrite H7 in H4. assumption.

  intro. intro. apply Subsequence_bools. apply H0 in H9.
  assert (In (a::v) q'). apply in_map. assumption. apply H3 in H10.
  destruct H10. destruct H10.

  pose (x3' := (repeat false (S (f x3))) ++ (skipn (S (f x3)) x3)).
  assert (v = map snd (filter fst (combine x3' u))).
  assert (forall m u v (a: X),
    a::v = map snd (filter fst (combine m u))
      -> v = map snd (filter fst (combine
              ((repeat false (S (f m))) ++ (skipn (S (f m)) m)) u))).
  intro m. induction m; intros. inversion H12.
  destruct u0. inversion H12. destruct a0. inversion H12. reflexivity.
  apply IHm with (a := a1). inversion H12. reflexivity.
  apply H12 with (a := a). assumption.

  assert (length x3' = length x3). unfold x3'. rewrite length_app.
  rewrite repeat_length. rewrite length_skipn. rewrite Nat.add_sub_assoc.
  rewrite Nat.add_sub_swap. rewrite Nat.sub_diag. reflexivity.
  apply Nat.le_refl. apply H1 in H10. rewrite H10. apply Nat.le_succ_l.
  apply I3. symmetry. assumption. rewrite <- H11. apply Nat.lt_0_succ.

  exists (skipn (S n) x3'). split.

  assert (length u = length (x1 ++ nth n u a :: x2)). rewrite <- H4.
  reflexivity. rewrite length_app in H14. apply H1 in H10.
  rewrite <- H10 in H14. rewrite length_skipn.
  rewrite H13. rewrite H14. rewrite H8.
  simpl. rewrite <- Nat.add_succ_comm. rewrite Nat.add_sub_swap.
  rewrite Nat.sub_diag. reflexivity. apply Nat.le_refl.

  assert (forall l, firstn (f l) l = repeat false (f l)).
  intro l. induction l. reflexivity. destruct a0. reflexivity.
  simpl. rewrite IHl. reflexivity.

  assert (I6: skipn (S n) x3'
        = (repeat false ((f x3) - n)) ++ (skipn (S (f x3)) x3)).
  unfold x3'. rewrite skipn_app. rewrite repeat_length.
  apply I in H10. apply Nat.sub_0_le in H10. rewrite Nat.sub_succ.
  rewrite H10. rewrite skipn_O. apply app_inv_tail_iff. simpl.

  rewrite skipn_repeat. reflexivity. rewrite I6.

  replace
  (filter fst (combine (repeat false (f x3 - n) ++ skipn (S (f x3)) x3) x2))
  with
    (filter fst (combine
      (repeat false (S n) ++ repeat false (f x3 - n) ++ skipn (S (f x3)) x3)
      ((x1 ++ [a]) ++ x2))).
  rewrite <- app_assoc. rewrite H7 in H4. replace (x1 ++ [a] ++ x2) with u.
  rewrite app_assoc. rewrite <- repeat_app. rewrite Nat.add_sub_assoc.
  rewrite Nat.add_sub_swap. rewrite Nat.sub_succ_l. rewrite Nat.sub_diag.
  rewrite Nat.add_1_l.
  replace (repeat false (S (f x3))) with (firstn (S (f x3)) x3').
  replace (skipn (S (f x3)) x3) with (skipn (S (f x3)) x3').
  rewrite firstn_skipn. assumption.
  unfold x3'. rewrite skipn_app. rewrite skipn_all2. rewrite skipn_skipn.
  rewrite repeat_length. rewrite Nat.sub_diag. reflexivity.
  rewrite repeat_length. apply Nat.le_refl. unfold x3'. rewrite firstn_app.
  rewrite firstn_all2. rewrite repeat_length. rewrite Nat.sub_diag.
  rewrite firstn_O. rewrite app_nil_r. reflexivity.
  rewrite repeat_length. apply Nat.le_refl. apply Nat.le_refl.
  apply Nat.le_succ_diag_r. apply I. assumption.

  rewrite combine_app. rewrite filter_app.
  assert (forall n (x: list X), filter fst (combine (repeat false n) x) = nil).
  intro n'. induction n'. destruct x4; reflexivity.
  destruct x4. reflexivity. apply IHn'. rewrite H15. reflexivity.
  rewrite length_app. rewrite H8. rewrite repeat_length. apply Nat.add_1_r.
Qed.


Theorem Shortest_complete_sequence_greedy {X: Type} :
  forall (u b: list X) a,
  Shortest_complete_sequence u (a::b)
    -> exists x y, u = x ++ a::y /\ ~ In a x /\ Complete_sequence y b.
Proof.
  intros. destruct H. assert (I := H).

  apply Complete_sequence_greedy in H.
  destruct H. destruct H. destruct H. exists x. exists x0.
  split. assumption. split. intro.
  apply in_split in H2. destruct H2. destruct H2.
  assert (Complete_sequence (x1 ++ x2 ++ a::x0) (a::b)).
  intro. intro. assert (H4 := H3).
  apply Permutation_in with (x := a) in H4. apply in_split in H4.
  destruct H4. destruct H4. assert (K := H3). apply I in H3.
  apply Subsequence_bools in H3. destruct H3. destruct H3.

  pose (x5' := firstn (length x1) x5). pose (x5'' := skipn (length x1) x5).
  assert (x5 = x5' ++ x5''). unfold x5'. unfold x5''.
  symmetry. apply firstn_skipn. destruct x5''. assert (J := H3).
  rewrite H6 in H3. rewrite H in H3. rewrite app_nil_r in H3.
  rewrite H2 in H3. rewrite <- app_assoc in H3.
  rewrite length_app in H3. unfold x5' in H3. rewrite firstn_length_le in H3.
  rewrite plus_n_O in H3 at 1. rewrite Nat.add_cancel_l in H3.
  apply Nat.neq_0_succ in H3. contradiction. rewrite J. rewrite H.
  rewrite H2. rewrite <- app_assoc. rewrite length_app. apply Nat.le_add_r.

  destruct b0.
  pose (u' := firstn (length x1) u). pose (u'' := skipn (length x1) u).
  assert (u = u' ++ u''). unfold u'. unfold u''. symmetry. apply firstn_skipn.
  rewrite H6 in H5. rewrite H7 in H5.

  assert (L0: length x1 < length u). rewrite H. rewrite H2.
  rewrite <- app_assoc.
  rewrite length_app. apply Nat.lt_add_pos_r. apply Nat.lt_0_succ.
  assert (L1: length u' = length x1). unfold u'. apply firstn_length_le.
  apply Nat.lt_le_incl. assumption.

  assert (L: u' = x1). rewrite H2 in H. rewrite H7 in H.
  rewrite <- app_assoc in H.
  apply app_eq_prefix in H. assumption. assumption.

  rewrite combine_app in H5. rewrite filter_app in H5.
  rewrite map_app in H5.

  destruct u''. rewrite H7 in L0. rewrite L in L0.
  rewrite app_nil_r in L0. apply Nat.lt_irrefl in L0. contradiction.

  assert (x6 = a). rewrite H2 in H. rewrite H7 in H.
  rewrite <- app_assoc in H. rewrite L in H.
  apply app_inv_head in H. inversion H. reflexivity.
  rewrite H8 in H5.

  pose (x7 := map snd (filter fst (combine x5' u'))).
  pose (x8 := map snd (filter fst (combine x5'' u''))).

  assert (Permutation (a::b) (a::x7++x8)).
  apply Permutation_trans with (l' := v). assumption.
  rewrite H5. symmetry. fold x7. simpl. fold x8. apply Permutation_middle.
  apply Permutation_cons_inv in H9. apply H1 in H9.

  assert (Subsequence x1 x7). unfold x7. rewrite L.
  apply Subsequence_bools. exists x5'. split. unfold x5'.
  apply firstn_length_le. rewrite H3. apply Nat.lt_le_incl. assumption.
  reflexivity.

  rewrite H5. apply Subsequence_app. assumption.
  apply Subsequence_split_3 in H9. apply Subsequence_app2_l.
  apply Subsequence_cons_eq. assumption.

  rewrite L1. unfold x5'. symmetry. apply firstn_length_le. rewrite H3.
  apply Nat.lt_le_incl. assumption.

  apply Subsequence_bools. exists (x5' ++ x5''). split.
  rewrite H6 in H3. rewrite H in H3. rewrite H2 in H3.
  rewrite <- app_assoc in H3.
  rewrite length_app in H3. rewrite length_app in H3.
  simpl in H3. rewrite Nat.add_succ_r in H3. rewrite Nat.add_succ_r in H3.
  apply Nat.succ_inj in H3.
  rewrite length_app. rewrite length_app. assumption.

  rewrite H6 in H5. rewrite H in H5. rewrite H2 in H5.
  rewrite <- app_assoc in H5.

  assert (length x1 = length x5').
  unfold x5'. symmetry. apply firstn_length_le.
  rewrite H3. rewrite H. rewrite H2. rewrite <- app_assoc.
  rewrite length_app. apply Nat.le_add_r.

  rewrite combine_app in H5. simpl in H5. rewrite H5. rewrite combine_app.
  repeat rewrite filter_app. simpl. reflexivity. assumption. assumption.
  apply in_eq. apply H0 in H3. assumption.
  rewrite H. rewrite H2. rewrite <- app_assoc.
  repeat rewrite length_app. apply Nat.add_lt_mono_l.
  repeat rewrite <- length_app. rewrite <- app_comm_cons.
  apply Nat.lt_succ_diag_r. assumption.
Qed.


Theorem Complete_sequence_first_occurrence {X: Type} :
  forall (u v: list X) a b, Complete_sequence (u++a::v) (a::b) -> ~ In a u
    -> Complete_sequence v b.
Proof.
  intros. intro. intro.
  assert (Permutation (a::b) (a::v0)). apply perm_skip. assumption.
  apply H in H2. simpl in H2. destruct H2. destruct H2. destruct H2.

  assert (length u <= length x \/ length x < length u). apply Nat.le_gt_cases.
  destruct H4. replace v with (skipn (S (length u)) (u++a::v)). rewrite H2.
  rewrite skipn_app.

  assert (Subsequence (skipn (S (length u) - length x) (a::x0)) v0).
  assert (S (length u) - length x <= 1).
  apply Nat.lt_eq_cases in H4. destruct H4. apply le_S.
  apply Nat.le_0_r. apply Nat.sub_0_le.
  apply Nat.le_succ_l. assumption. rewrite H4.
  rewrite Nat.sub_succ_l. rewrite Nat.sub_diag.
  apply Nat.eq_le_incl. reflexivity. apply Nat.le_refl.
  assert (S (length u) - length x = 0 \/ S (length u) - length x = 1).
  destruct (S (length u) - length x). left. reflexivity.
  right. destruct n. reflexivity.
  apply Nat.succ_lt_mono in H5. apply Nat.nlt_0_r in H5. contradiction.
  destruct H6; rewrite H6. apply Subsequence_cons_l. assumption. assumption.

  apply Subsequence_app2_l with (l1 := skipn (S (length u)) x) in H5.
  assumption. rewrite skipn_app. rewrite skipn_all2.
  rewrite Nat.sub_succ_l. rewrite Nat.sub_diag. reflexivity.
  apply Nat.le_refl. apply Nat.le_succ_diag_r.

  assert False. apply H0. replace u with (firstn (length u) (u++a::v)).
  rewrite H2. rewrite firstn_app. apply in_or_app. right.
  rewrite <- plus_O_n in H4 at 1. apply Nat.lt_add_lt_sub_r in H4.
  destruct (length u - length x). apply Nat.lt_irrefl in H4. contradiction.
  apply in_eq. rewrite plus_n_O at 1. rewrite firstn_app_2. apply app_nil_r.
  contradiction.
Qed.


Theorem Shortest_complete_sequence_first_occurrence {X: Type} :
  forall (u v: list X) a b, Shortest_complete_sequence (u++a::v) (a::b)
    -> Complete_sequence v b -> ~ In a u.
Proof.
  intros. intro. apply in_split in H1.
  destruct H1. destruct H1. rewrite H1 in H.
  assert (Complete_sequence ((x++x0)++a::v) (a::b)). intro. intro.
  assert (I := H2). apply H in H2. rewrite <- app_assoc in H2.
  apply Subsequence_bools in H2. destruct H2. destruct H2.

  assert (length x < length x1). rewrite H2. rewrite length_app.
  apply Nat.lt_add_pos_r. apply Nat.lt_0_succ. assert (H5 := H4).
  apply Nat.lt_le_incl in H5.

  pose (xa := firstn (length x) x1). pose (xb := skipn (length x) x1).

  assert (x1 = xa ++ xb). unfold xa. unfold xb.
  rewrite firstn_skipn. reflexivity.

  assert (length x1 = length xa + length xb). rewrite H6. apply length_app.
  assert (length xa = length x). unfold xa. apply firstn_length_le. assumption.
  destruct xb. rewrite H8 in H7. rewrite H7 in H4. rewrite <- plus_n_O in H4.
  apply Nat.lt_irrefl in H4. contradiction.

  rewrite H6 in H3. rewrite combine_app in H3. rewrite filter_app in H3.
  rewrite map_app in H3. destruct b0.

  assert (Permutation (a::b) (a::
     map snd (filter fst (combine xa x)) ++
      map snd (filter fst (combine xb (x0 ++ a :: v))))).
  apply Permutation_trans with (l' := v0). assumption. rewrite H3.
  apply Permutation_sym. apply Permutation_middle.
  apply Permutation_cons_inv in H9. apply H0 in H9.
  apply Subsequence_split_3 in H9.

  rewrite H3. apply Subsequence_app. apply Subsequence_app2_r.
  apply Subsequence_bools. exists xa. split. assumption.
  reflexivity. apply Subsequence_cons_eq. assumption.

  simpl in H3. rewrite <- map_app in H3. rewrite <- filter_app in H3.
  rewrite <- combine_app in H3. rewrite H3.
  apply Subsequence_bools. exists (xa++xb). split.
  repeat rewrite length_app. apply eq_add_S.
  rewrite <- Nat.add_succ_r. simpl in H7. rewrite <- H7. rewrite H2.
  repeat rewrite length_app. rewrite <- Nat.add_assoc.
  rewrite <- Nat.add_succ_r. rewrite Nat.add_cancel_l.
  rewrite <- Nat.add_succ_l. reflexivity.
  rewrite app_assoc. reflexivity. symmetry. assumption.
  symmetry. assumption. destruct H. apply H3 in H2. assumption.
  repeat rewrite length_app. repeat rewrite <- Nat.add_assoc.
  apply Nat.add_lt_mono_l. apply Nat.add_lt_mono_r.
  apply Nat.lt_succ_diag_r.
Qed.


Theorem Complete_sequence_suffix {X: Type} :
  forall (u v: list X) a b, Complete_sequence (u++v) (a::b)
     -> ~ In a u -> Complete_sequence v b.
Proof.
  intros. intro. intro. apply perm_skip with (x := a) in H1.
  apply H in H1. apply Subsequence_split in H1. destruct H1. destruct H1.
  destruct H1. destruct H2. destruct x.
  simpl in H1. rewrite <- H1 in H3. apply Subsequence_cons_r in H3.
  assumption. inversion H1. rewrite <- H5 in H2.
  apply Subsequence_In2 in H2. apply H0 in H2. contradiction.
Qed.


Theorem Complete_sequence_prefix {X: Type} :
  forall (u v: list X) a b, Complete_sequence (u++v) (a::b)
     -> ~ In a v -> Complete_sequence u b.
Proof.
  intros. apply Complete_sequence_rev in H.
  rewrite rev_app_distr in H. rewrite in_rev in H0. apply Complete_sequence_rev.
  apply Complete_sequence_suffix with (u := rev v) (a := a); assumption.
Qed.


Theorem Complete_sequence_middle {X: Type} :
  forall (u v w: list X) a b c,
    Complete_sequence (u++v++w) (a::b::c)
      -> ~ In a u -> ~ In b w -> Complete_sequence v c.
Proof.
  intros.
  apply Complete_sequence_prefix with (v := w) (a := b).
  apply Complete_sequence_suffix with (u := u) (a := a).
  assumption. assumption. assumption.
Qed.


Theorem Complete_sequence_middle2 {X: Type} :
  forall (u v w: list X) a b c,
    Complete_sequence (u++v++w) (a::b::c)
      -> ~ In a w -> ~ In b u -> Complete_sequence v c.
Proof.
  intros.
  apply Complete_sequence_middle with (u := u) (w := w) (a := b) (b := a).
  apply Complete_sequence_permut_base with (base := a::b::c). assumption.
  apply perm_swap. assumption. assumption.
Qed.










(* Examples *)


Example Shortest_complete_sequence_0 {X: Type} :
  forall u: list X, Shortest_complete_sequence u nil -> u = nil.
Proof.
  intros. destruct H. destruct u. reflexivity.
  assert (Complete_sequence (nil: list X) nil).
  apply Complete_sequence_nil. apply H0 in H1. contradiction.
  apply Nat.lt_0_succ.
Qed.


Example Shortest_complete_sequence_1 {X: Type} :
  forall (u: list X) a, Shortest_complete_sequence u (a::nil) -> u = a::nil.
Proof.
  intros.
  assert (Permutation [a] [a]). apply Permutation_refl.
  destruct H. destruct u.
  apply H in H0. apply Subsequence_nil_cons_r in H0. contradiction.
  destruct u. apply H in H0. simpl in H0.
  destruct H0. destruct H0. destruct H0.
  destruct x0. inversion H0. reflexivity.
  assert (length [x] = length [x]). easy. rewrite H0 in H3 at 2.
  rewrite length_app in H3. simpl in H3. rewrite Nat.add_succ_r in H3.
  inversion H3.
  assert (Complete_sequence [a] [a]).  intro. intro.
  apply Permutation_length_1_inv in H2. rewrite H2.
  apply Subsequence_id. apply H1 in H2. contradiction.
  simpl. rewrite <- Nat.succ_lt_mono. apply Nat.lt_0_succ.
Qed.
