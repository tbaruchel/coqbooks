Require Import Nat.
Require Import PeanoNat.
Require Import List.
Require Import subsequence.
Require Import Sorting.Permutation.
Require Import Lists.ListSet.
Require Import Arith.Factorial.
Import ListNotations.


Theorem Subsequence_exists_set {X: Type} :
  forall (u : list X), exists q, forall v, In v q <-> Subsequence u v.
Proof.
  assert (forall n, exists q, forall (m : list bool), length m = n <-> In m q).
  intro n. induction n. exists [nil]. intro m. split. intro.
  apply length_zero_iff_nil in H. rewrite H. apply in_eq.
  intro. destruct H. rewrite <- H. reflexivity.
  apply in_nil in H. contradiction.
  destruct IHn. exists ((map (cons true) x) ++ (map (cons false) x)).
  intro m. split. intro. destruct m. apply O_S in H0. contradiction.
  inversion H0. apply H in H2. destruct b.
  apply in_or_app. left. apply in_map. assumption.
  apply in_or_app. right. apply in_map. assumption.
  intro. destruct m.
  apply in_app_or in H0. destruct H0; apply in_map_iff in H0;
  destruct H0; destruct H0; inversion H0. simpl. apply eq_S. apply H.
  apply in_app_or in H0. destruct H0.
  apply in_map_iff in H0;
  destruct H0; destruct H0. inversion H0. rewrite <- H4. assumption.
  apply in_map_iff in H0. 
  destruct H0; destruct H0. inversion H0. rewrite <- H4. assumption.

  intro u. destruct H with (n := length u).
  exists (map (fun e => map snd (filter fst (combine e u))) x).
  intro v. split. intro. apply Subsequence_bools.
  apply in_map_iff in H1. destruct H1. destruct H1. exists x0. split.
  apply H0. assumption. rewrite H1. reflexivity.
  intro. apply Subsequence_bools in H1. destruct H1. destruct H1.
  apply H0 in H1. apply in_map_iff. exists x0. split.
  rewrite H2. reflexivity. assumption.
Qed.


Theorem Set_of_subsequences_as_set_of_masks {X: Type} :
  forall (u : list X) q,
    (forall v, In v q -> Subsequence u v) ->
    (exists q',
      (forall t, In t q' -> length t = length u)
    /\ (forall t, In t q' -> In (map snd (filter fst (combine t u))) q)
    /\ forall v, In v q -> exists t, In t q'
                                 /\ v = map snd (filter fst (combine t u))).
Proof.
  intros u q. generalize u. induction q; intros.
  exists nil. split; try split; intros; apply in_nil in H0; contradiction.

  assert (Subsequence u0 a). apply H. apply in_eq.
  apply Subsequence_bools in H0. destruct H0. destruct H0.

  assert (forall v, In v q -> Subsequence u0 v). intros. apply H.
  apply in_cons. assumption. apply IHq in H2.
  destruct H2. destruct H2. destruct H3.

  exists (x::x0).
  split; try split; intros; apply in_inv in H5; destruct H5.
  rewrite <- H5. assumption.
  apply H2. assumption.
  rewrite <- H5. rewrite <- H1. apply in_eq. apply in_cons.
  apply H3 in H5. assumption.

  exists x. split. apply in_eq. rewrite <- H5. assumption.
  apply H4 in H5. destruct H5. destruct H5.
  exists x1. split. apply in_cons. assumption. assumption.
Qed.


Theorem Permutation_exists_set {X: Type} :
  forall u : list X, exists q, forall v, Permutation u v <-> In v q.
Proof.
  intro u. induction u. exists [nil]. intro. split; intro.
  apply Permutation_nil in H. rewrite H. apply in_eq. destruct H.
  rewrite <- H. apply Permutation_refl. apply in_nil in H. contradiction.
  destruct IHu as [q].
  exists (flat_map
       (fun e => map (fun x => (firstn e x) ++ a::(skipn e x)) q)
       (seq 0 (length (a::u)))).
  intro. split; intro.
  - assert (H1 := H0).
    apply Permutation_sym in H0. apply Permutation_vs_cons_inv in H0.
    destruct H0. destruct H0. apply in_flat_map. exists (length x). split.
    apply Permutation_length in H1. rewrite H1. rewrite H0. apply in_seq.
    split. apply le_0_n. rewrite length_app. simpl.
    apply Nat.lt_add_pos_r. apply Nat.lt_0_succ.
    apply in_map_iff. exists (x++x0). split. rewrite H0.
    rewrite <- Nat.add_0_r with (n := length x) at 1. rewrite firstn_app_2.
    rewrite firstn_O. rewrite skipn_app. rewrite skipn_all.
    rewrite Nat.sub_diag. rewrite skipn_O. rewrite app_nil_r. reflexivity.
    apply H. rewrite H0 in H1.
    assert (Permutation (a::u) (a::x ++ x0)).
    apply Permutation_trans with (l' := x ++ a::x0). assumption.
    apply Permutation_sym. apply Permutation_middle.
    apply Permutation_cons_inv with (a := a). assumption.
  - apply in_flat_map in H0. destruct H0. destruct H0.
    apply in_map_iff in H1. destruct H1. destruct H1. rewrite <- H1.
    apply Permutation_trans with (l' := a::(firstn x x0) ++ (skipn x x0)).
    apply perm_skip. rewrite firstn_skipn. apply H. assumption.
    apply Permutation_middle.
Qed.


Fixpoint all_permutations {X: Type} (b: list X) : list (list X) :=
    match b with
    | nil => [nil]
    | hd::tl => flat_map (fun e => map
        (fun x => (firstn e x) ++ hd::(skipn e x)) (all_permutations tl))
        (seq 0 ((length b)))
    end.


Fixpoint subsets {X: Type} (l: list X) : list (list X) :=
    match l with
    | nil => [nil]
    | hd::tl => (subsets tl) ++ (map (fun e => hd::e) (subsets tl))
    end.


Fixpoint words_of_length {X: Type} (base: list X) (n: nat) : list (list X) :=
    match n with
    | 0 => [nil]
    | S k => flat_map (fun e => map (fun e' => cons e' e) base)
                      (words_of_length base k)
    end.

Fixpoint words_below_length {X: Type} (base: list X) (n: nat) : list (list X) :=
    match n with
    | 0 => nil
    | S k => (words_below_length base k) ++ (words_of_length base k)
    end.


Theorem all_permutations_Permutation {X: Type} :
  forall (u v: list X), In v (all_permutations u) <-> Permutation u v.
Proof.
  intro u. induction u; intro; split; intro.
  destruct H. rewrite <- H. apply Permutation_refl. apply in_nil in H.
  contradiction. apply Permutation_nil in H. rewrite H. apply in_eq.
  simpl in H. apply in_app_or in H. destruct H.
  apply in_map_iff in H. destruct H. destruct H. apply IHu in H0.
  rewrite <- H. apply perm_skip. assumption.
  apply in_flat_map in H. destruct H. destruct H.
  apply in_map_iff in H0. destruct H0. destruct H0. apply IHu in H1.
  rewrite <- H0. apply Permutation_cons_app. rewrite firstn_skipn.
  assumption.
  assert (In a v). apply Permutation_in with (l := a::u). assumption.
  apply in_eq. apply in_split in H0. destruct H0. destruct H0.
  simpl. apply in_or_app.
  destruct x. left. apply in_map_iff. exists x0. rewrite H0. split.
  reflexivity. apply IHu. rewrite H0 in H. apply Permutation_cons_inv in H.
  assumption. right. apply in_flat_map.
  exists (length (x::x1)). split. apply in_seq. split. apply le_n_S.
  apply le_0_n. rewrite H0 in H. apply Permutation_length in H.
  rewrite length_app in H. simpl in H. rewrite Nat.add_1_l. rewrite H.
  rewrite <- Nat.add_succ_l. apply Nat.lt_add_pos_r. apply Nat.lt_0_succ.
  apply in_map_iff. exists ((x::x1)++x0). split.
  rewrite firstn_app. rewrite firstn_all. rewrite skipn_app. rewrite skipn_all.
  rewrite Nat.sub_diag. rewrite firstn_O. rewrite skipn_O. rewrite H0.
  rewrite app_nil_r. reflexivity. apply IHu. rewrite H0 in H.
  apply Permutation_cons_app_inv in H. assumption.
Qed.


Theorem all_permutations_length {X: Type} :
  forall (u: list X), length (all_permutations u) = fact (length u).
Proof.
  intro u. induction u. reflexivity.
  unfold all_permutations.
  rewrite flat_map_constant_length with (c := fact (length u)).
  rewrite length_seq. easy. intros. rewrite length_map. apply IHu.
Qed.


Theorem subsets_subsequence {X: Type} :
  forall (u v: list X),
    In v (subsets u) <-> Subsequence u v.
Proof.
  intro u. induction u; intro; split; intro.
  destruct H. rewrite <- H. apply Subsequence_id.
  apply in_nil in H. contradiction.

  destruct v. apply in_eq.
  apply Subsequence_nil_cons_r in H. contradiction.

  simpl in H. apply in_app_or in H. destruct H.
  apply Subsequence_cons_l. apply IHu. assumption.
  apply in_map_iff in H. destruct H. destruct H.
  rewrite <- H. apply Subsequence_cons_eq. apply IHu. assumption.

  simpl. apply in_or_app.
  destruct v. left. apply IHu. apply Subsequence_nil_r.
  apply Subsequence_bools in H. destruct H. destruct H.
  destruct x0. inversion H. destruct b; simpl in H0.
  assert (Subsequence u v). apply Subsequence_bools. exists x0.
  split. inversion H. reflexivity. inversion H0. reflexivity.
  apply IHu in H1. right. apply in_map_iff. exists v.
  split. inversion H0. reflexivity. assumption. left.
  assert (Subsequence u (x::v)). apply Subsequence_bools. exists x0.
  split. inversion H. reflexivity. assumption. apply IHu in H1.
  assumption.
Qed.


Theorem subsets_trivial_incl {X: Type} :
  forall (u: list X) (x: X),
    incl (subsets u) (subsets (x::u)).
Proof.
  repeat intro. simpl. apply in_or_app. left. assumption.
Qed.


Theorem subsets_nil_in {X: Type} :
  forall (u: list X), In nil (subsets u).
Proof.
  induction u. apply in_eq.
  apply subsets_trivial_incl with (x := a) in IHu. assumption.
Qed.


Theorem words_of_length_length {X: Type} :
  forall (n: nat) (u w: list X),
    In u (words_of_length w n) -> length u = n.
Proof.
  intro n. induction n; intros; simpl in H. destruct H. rewrite <- H.
  easy. contradiction. apply in_flat_map in H. destruct H. destruct H.
  apply IHn in H. apply in_map_iff in H0. destruct H0. destruct H0.
  rewrite <- H0. rewrite <- H. reflexivity.
Qed.


Theorem words_of_length_incl {X: Type} :
  forall (n: nat) (u w: list X),
    In u (words_of_length w n) -> incl u w.
Proof.
  intro n. induction n; intros; simpl in H. destruct H.
  rewrite <- H. apply incl_nil_l. contradiction.
  apply in_flat_map in H. destruct H. destruct H.
  apply in_map_iff in H0. destruct H0. destruct H0.
  apply IHn in H. repeat intro. rewrite <- H0 in H2.
  destruct H2. rewrite <- H2. assumption.
  apply H in H2. assumption.
Qed.


Theorem words_below_length_length {X: Type} :
  forall (n: nat) (u w: list X),
    In u (words_below_length w n) -> length u < n.
Proof.
  intro n. induction n; intros; simpl in H. contradiction.
  apply in_app_or in H. destruct H. apply IHn in H.
  apply Nat.lt_lt_succ_r. assumption.
  apply words_of_length_length in H. rewrite H.
  apply Nat.lt_succ_diag_r.
Qed.


Theorem words_below_length_incl {X: Type} :
  forall (n: nat) (u w: list X),
    In u (words_below_length w n) -> incl u w.
Proof.
  intro n. induction n; intros; simpl in H. contradiction.
  apply in_app_or in H. destruct H. apply IHn in H. assumption.
  apply words_of_length_incl with (n := n). assumption.
Qed.


Theorem words_of_length_in {X: Type} :
  forall (n: nat) (u base: list X),
    incl u base -> length u = n -> In u (words_of_length base n).
Proof.
  intro n. induction n; intros.
  apply length_zero_iff_nil in H0. rewrite H0. simpl.
  left. reflexivity. simpl. apply in_flat_map.
  destruct u; inversion H0. apply IHn with (base := base) in H2.
  exists u. split. inversion H0. rewrite H3. assumption.
  apply in_map_iff. exists x. split. reflexivity.
  apply incl_cons_inv in H. destruct H. assumption.
  apply incl_cons_inv in H. destruct H. assumption.
Qed.


Theorem words_below_length_in {X: Type} :
  forall (n: nat) (u base: list X),
    incl u base -> length u < n -> In u (words_below_length base n).
Proof.
  intro n. induction n; intros.
  apply Nat.nlt_0_r in H0. contradiction.
  simpl. rewrite Nat.lt_succ_r in H0. apply Nat.lt_eq_cases in H0.
  apply in_or_app. destruct H0. apply IHn with (base := base) in H0.
  left. assumption. assumption. right.
  apply words_of_length_in; assumption.
Qed.


Theorem words_below_length_ordering {X: Type} :
  forall (n : nat) (l1 l2 l3: list (list X)) (base x1 x2: list X),
    l1 ++ x1::l2 ++ x2::l3 = words_below_length base n
      -> length x1 <= length x2.
Proof.
  intro n. induction n; intros; simpl in H.
  apply app_eq_nil in H. destruct H. inversion H0.
  apply app_eq_app in H. destruct H. destruct H; destruct H.
  assert (In x1 (words_of_length base n)). rewrite H0. apply in_elt.
  assert (In x2 (words_of_length base n)). rewrite H0.
  rewrite app_comm_cons. rewrite app_assoc. apply in_elt.
  apply words_of_length_length in H1. apply words_of_length_length in H2.
  rewrite H1. rewrite H2. apply le_n.
  destruct x. simpl in H0.
  assert (In x1 (words_of_length base n)). rewrite <- H0. apply in_eq.
  assert (In x2 (words_of_length base n)). rewrite <- H0.
  rewrite app_comm_cons. apply in_elt.
  apply words_of_length_length in H1. apply words_of_length_length in H2.
  rewrite H1. rewrite H2. apply le_n.
  inversion H0.
  assert (In x1 (words_below_length base n)). rewrite H. rewrite H2.
  apply in_elt.
  apply app_eq_app in H3. destruct H3. destruct H3; destruct H3.
  assert (In x2 (words_of_length base n)). rewrite H4. apply in_elt.
  apply words_below_length_length in H1. apply words_of_length_length in H5.
  rewrite H5. rewrite <- H2. apply Nat.lt_le_incl. assumption.
  destruct x0. simpl in H4.
  assert (In x2 (words_of_length base n)). rewrite <- H4. apply in_eq.
  apply words_below_length_length in H1. apply words_of_length_length in H5.
  rewrite H5. rewrite <- H2. apply Nat.lt_le_incl. assumption.
  inversion H4. rewrite H3 in H. rewrite <- H6 in H. rewrite <- H2 in H.
  symmetry in H. apply IHn in H. rewrite <- H6. rewrite <- H2.
  assumption.
Qed.


Theorem words_below_length_not_empty {X: Type} :
  forall (n : nat) (base: list X), words_below_length base (S n) <> nil.
Proof.
  intro n. induction n; intros. easy.
  intro. simpl in H.
  specialize (IHn base). apply IHn. simpl.
  apply app_eq_nil in H. destruct H. assumption.
Qed.


Fixpoint suffixes {X: Type} (u : list X) :=
  match u with
  | nil => [nil]
  | hd::tl => (suffixes tl) ++ [ hd::tl ]
  end.


Lemma suffixes_original {X: Type} :
  forall (u: list X), In u (suffixes u).
Proof.
  intro. destruct u. apply in_eq.
  simpl. apply in_or_app. right. apply in_eq.
Qed.


Lemma suffixes_subsequence {X: Type} :
  forall (u v: list X), In v (suffixes u) -> Subsequence u v.
Proof.
  intro u. induction u; intros. destruct H. rewrite <- H.
  apply Subsequence_id. apply in_nil in H. contradiction.
  simpl in H. apply in_app_or in H. destruct H. apply IHu in H.
  apply Subsequence_cons_l. assumption. destruct H. rewrite H.
  apply Subsequence_id. apply in_nil in H. contradiction.
Qed.


Lemma suffixes_last {X: Type } :
  forall (u: list X), last (suffixes u) u = u.
Proof.
  intros u. destruct u. reflexivity. simpl. rewrite last_last. reflexivity.
Qed.


Lemma suffixes_partial_length {X: Type} :
  forall (u x y: list X) (v w : list (list X)),
    suffixes u = v ++ w -> In x v -> In y w -> length x < length y.
Proof.
  intro u. induction u; intros.
  destruct v. apply in_nil in H0. contradiction.
  destruct w. apply in_nil in H1. contradiction.
  symmetry in H. apply elt_eq_unit in H. destruct H. destruct H2.
  inversion H2. simpl in H.
  replace w with (removelast w ++ [last w u]) in H.
  replace w with (removelast w ++ [last w u]) in H1.
  rewrite app_assoc in H. apply app_inj_tail in H. destruct H.
  apply in_app_or in H1. destruct H1.
  apply IHu with (x := x) (y := y) in H; assumption.
  rewrite <- H2 in H1. inversion H1. rewrite <- H3.
  assert (Subsequence u x). apply suffixes_subsequence. rewrite H.
  apply in_or_app. left. assumption.
  apply Subsequence_length in H4.
  apply Nat.le_lt_trans with (m := length u). assumption.
  apply Nat.lt_succ_diag_r. apply in_nil in H3. contradiction.
  symmetry. apply app_removelast_last.
  destruct w. apply in_nil in H1. contradiction. discriminate.
  symmetry. apply app_removelast_last.
  destruct w. apply in_nil in H1. contradiction. discriminate.
Qed.


Lemma suffixes_partial_subsequence {X: Type} :
  forall (u x y: list X) (v w : list (list X)),
    suffixes u = v ++ w
    -> In x v -> In y w -> Subsequence y x.
Proof.
  intro u. induction u; intros.
  destruct v. apply in_nil in H0. contradiction.
  destruct w. apply in_nil in H1. contradiction.
  symmetry in H. apply elt_eq_unit in H. destruct H. destruct H2.
  inversion H2. simpl in H.
  replace w with (removelast w ++ [last w u]) in H.
  replace w with (removelast w ++ [last w u]) in H1.
  rewrite app_assoc in H. apply app_inj_tail in H. destruct H.
  apply in_app_or in H1. destruct H1.
  apply IHu with (x := x) (y := y) in H; assumption.
  rewrite <- H2 in H1. inversion H1. rewrite <- H3.
  apply Subsequence_cons_l. apply suffixes_subsequence. rewrite H.
  apply in_or_app. left. assumption.
  apply in_nil in H3. contradiction.
  symmetry. apply app_removelast_last.
  destruct w. apply in_nil in H1. contradiction. discriminate.
  symmetry. apply app_removelast_last.
  destruct w. apply in_nil in H1. contradiction. discriminate.
Qed.


Lemma suffixes_nil_first {X: Type} :
  forall (u: list X), exists v, suffixes u = nil::v.
Proof.
  intro u. induction u. exists nil. reflexivity.
  destruct IHu. exists (x ++ [a::u]). simpl. rewrite H.
  reflexivity.
Qed.


Lemma suffixes_following {X: Type} :
  forall (u x y: list X) (v w : list (list X)) l,
    suffixes u = v ++ x::(l::y)::w -> x = y.
Proof.
  intro u. induction u; intros. destruct v; inversion H.
  symmetry in H2. apply app_eq_nil in H2. destruct H2. inversion H2.
  simpl in H. destruct w. replace [x;l::y] with ([x] ++ [l::y]) in H.
  rewrite app_assoc in H. apply app_inj_tail in H. destruct H.
  inversion H0. rewrite <- H3.
  assert (last (suffixes u) u = u). apply suffixes_last.
  rewrite H in H1. rewrite last_last in H1. assumption.
  easy.
  replace (l0::w) with (removelast (l0::w) ++ [last (l0::w) nil]) in H.
  rewrite app_comm_cons in H. rewrite app_comm_cons in H.
  rewrite app_assoc in H. apply app_inj_tail in H. destruct H.
  apply IHu in H. assumption.
  symmetry. apply app_removelast_last. easy.
Qed.


Lemma suffixes_middle {X: Type} :
  forall (l u: list X) (v w : list (list X)) (x: X),
    suffixes u = v ++ (x::l)::w -> In l v.
Proof.
  intros. destruct v. assert (exists v, suffixes u = nil::v).
  apply suffixes_nil_first. destruct H0. rewrite H in H0. inversion H0.
  replace (l0::v) with (removelast (l0::v) ++ [last (l0::v) nil]) in H.
  rewrite <- app_assoc in H. apply suffixes_following in H.
  replace (l0::v) with (removelast (l0::v) ++ [last (l0::v) nil]).
  rewrite <- H. apply in_or_app. right. apply in_eq.
  symmetry. apply app_removelast_last. easy.
  symmetry. apply app_removelast_last. easy.
Qed.


Set Implicit Arguments.

Section Exists_set_dec.

Variable X: Type.
Hypothesis eq_dec : forall x y: X, {x=y} + {x <> y}.




Theorem Permutation_present_exists_set :
  forall l a : list X,
    exists q, forall v, In v q <-> (Permutation a v /\ Subsequence l v).
Proof.
  intros.
  assert (exists q, forall v, Permutation a v <-> In v q).
  apply Permutation_exists_set. destruct H as [q I].
  assert (exists q, forall v, In v q <-> Subsequence l v).
  apply Subsequence_exists_set. destruct H as [q' J].
  exists (set_inter (list_eq_dec eq_dec) q q').
  intro. split; intro.
  split. apply set_inter_elim1 in H. apply I in H. assumption.
  apply set_inter_elim2 in H. apply J in H. assumption.
  destruct H. apply I in H. apply J in H0.
  apply set_inter_intro; assumption.
Qed.


Theorem Permutation_absent_exists_set :
  forall l a : list X,
    exists q, forall v, In v q <-> (Permutation a v /\ ~ Subsequence l v).
Proof.
  intros.
  assert (exists q, forall v, Permutation a v <-> In v q).
  apply Permutation_exists_set. destruct H as [q I].
  assert (exists q, forall v, In v q <-> Subsequence l v).
  apply Subsequence_exists_set. destruct H as [q' J].
  exists (set_diff (list_eq_dec eq_dec) q q').
  intro. split; intro. split. apply I.
  apply set_diff_elim1 in H. assumption.
  apply set_diff_elim2 in H. intro. apply J in H0.
  apply H in H0. assumption.
  destruct H. apply I in H.
  assert (~ In v q'). intro. apply J in H1. apply H0 in H1. assumption.
  apply set_diff_intro; assumption.
Qed.


Theorem Permutation_present_nodup_exists_set :
  forall l a : list X,
    exists q, forall v, In v q
      <-> (Permutation a v /\ Subsequence l v /\ NoDup v).
Proof.
  intros.
  assert (exists q, forall v, In v q <-> (Permutation a v /\ Subsequence l v)).
  apply Permutation_present_exists_set. destruct H as [q].

  assert (L: forall t, exists q, forall (v: list X),
    In v q <-> In v t /\ NoDup v).
  intro t. induction t. exists nil. intro. split. intro.
  apply in_nil in H0. contradiction. intro. destruct H0. assumption.

  destruct IHt as [t' T].

  assert ({NoDup a0} + {~ NoDup a0}). apply ListDec.NoDup_dec. assumption.
  destruct H0. exists (a0::t'). intro. split. intro.
  destruct H0. rewrite <- H0. split.
  apply in_eq. assumption. apply T in H0. destruct H0. split.
  apply in_cons. assumption. assumption.
  intro. destruct H0. destruct H0. rewrite H0. apply in_eq.
  apply in_cons. apply T. split; assumption.
  exists t'. intro. split. intro. apply T in H0. destruct H0.
  split. apply in_cons. assumption. assumption. intro. destruct H0.
  destruct H0. rewrite H0 in n. apply n in H1. contradiction.
  apply T. split; assumption.

  assert (exists q', forall v : list X, In v q' <-> In v q /\ NoDup v).
  apply L. destruct H0 as [q'].

  exists q'. intro. split; intro. apply H0 in H1. destruct H1.
  apply H in H1. destruct H1. split. assumption. split; assumption.
  apply H0. destruct H1. destruct H2. split. apply H.
  split; assumption. assumption.
Qed.


Theorem Subsequence_nodup_exists_set :
  forall l : list X,
    exists q, forall v, In v q <-> (NoDup v /\ Subsequence l v).
Proof.
  intros.
  assert (exists q, forall v, In v q <-> Subsequence l v).
  apply Subsequence_exists_set. destruct H as [q I].
  exists (map (nodup eq_dec) q). intros. split; intro.
  apply in_map_iff in H. destruct H. destruct H.
  apply I in H0. rewrite <- H. split. apply NoDup_nodup.
  apply Subsequence_nodup. assumption. destruct H.
  apply I in H0. apply in_map_iff. exists v. split.
  apply nodup_fixed_point. assumption. assumption.
Qed.


Theorem not_incl_exists :
  forall (q1 q2: list (list X)),
    ~ incl q1 q2 -> exists v, In v q1 /\ ~ In v q2.
Proof.
  intros.
  assert (forall a, In a (set_diff (list_eq_dec eq_dec) q1 q2)
                         <-> In a q1 /\ ~ In a q2).
  intros. apply set_diff_iff.
  assert ({set_diff (list_eq_dec eq_dec) q1 q2 = nil}
     + {set_diff (list_eq_dec eq_dec) q1 q2 <> nil}).
  apply (list_eq_dec (list_eq_dec eq_dec)). destruct H1.
  rewrite e in H0.
  assert (False). apply H. repeat intro.
  assert ({In a q2} + {~ In a q2}). apply ListDec.In_dec.
  apply list_eq_dec. apply eq_dec. destruct H2. assumption.
  assert (In a nil). apply H0. split; assumption.
  apply in_nil in H2. contradiction. contradiction.
  destruct (set_diff (list_eq_dec eq_dec) q1 q2). contradiction.
  exists l. apply H0. apply in_eq.
Qed.


Theorem Permutation_exists_set_length :
  forall u : list X, NoDup u ->
    exists q, (forall v, Permutation u v <-> In v q)
       /\ length q = fact (length u) /\ NoDup q.
Proof.
  intro u. induction u; intro K. exists [nil]. split. intro. split; intro.
  apply Permutation_nil in H. rewrite H. apply in_eq. destruct H.
  rewrite <- H. apply Permutation_refl. apply in_nil in H. contradiction.
  split. reflexivity. apply NoDup_cons_iff. split. apply in_nil.
  apply NoDup_nil.
  apply NoDup_cons_iff in K. destruct K as [K1 K2]. apply IHu in K2.
  destruct K2 as [q K2]. destruct K2 as [H K2]. destruct K2 as [K2 K3].
  exists (flat_map
       (fun e => map (fun x => (firstn e x) ++ a::(skipn e x)) q)
       (seq 0 (length (a::u)))).
    split. intro. split; intro.
  - assert (H1 := H0).
    apply Permutation_sym in H0. apply Permutation_vs_cons_inv in H0.
    destruct H0. destruct H0. apply in_flat_map. exists (length x). split.
    apply Permutation_length in H1. rewrite H1. rewrite H0. apply in_seq.
    split. apply le_0_n. rewrite length_app. simpl.
    apply Nat.lt_add_pos_r. apply Nat.lt_0_succ.
    apply in_map_iff. exists (x++x0). split. rewrite H0.
    rewrite <- Nat.add_0_r with (n := length x) at 1. rewrite firstn_app_2.
    rewrite firstn_O. rewrite skipn_app. rewrite skipn_all.
    rewrite Nat.sub_diag. rewrite skipn_O. rewrite app_nil_r. reflexivity.
    apply H. rewrite H0 in H1.
    assert (Permutation (a::u) (a::x ++ x0)).
    apply Permutation_trans with (l' := x ++ a::x0). assumption.
    apply Permutation_sym. apply Permutation_middle.
    apply Permutation_cons_inv with (a := a). assumption.
  - apply in_flat_map in H0. destruct H0. destruct H0.
    apply in_map_iff in H1. destruct H1. destruct H1. rewrite <- H1.
    apply Permutation_trans with (l' := a::(firstn x x0) ++ (skipn x x0)).
    apply perm_skip. rewrite firstn_skipn. apply H. assumption.
    apply Permutation_middle.
  - split.
    assert (forall (w: list nat) (f: nat -> list (list X)),
              (forall e, In e w -> length (f e) = length q)
                  -> length (flat_map f w) = (length w) * (length q)).
    intro w. induction w; intros. reflexivity. simpl. rewrite length_app.
    rewrite H0. rewrite IHw. reflexivity. intros. apply H0. apply in_cons.
    assumption. apply in_eq. rewrite H0. rewrite K2. rewrite length_seq.
    reflexivity. intros. apply length_map.

    generalize dependent a. generalize dependent q.
    induction u; intros. simpl. rewrite app_nil_r.
    apply NoDup_map_NoDup_ForallPairs. repeat intro.
    inversion H2. reflexivity. assumption.

    assert (L: forall (w: list nat) (f: nat -> list (list X)),
      NoDup w -> (forall x, In x w -> NoDup (f x))
       -> (forall x y z, In x w -> In y w -> In z (f x) -> In z (f y) -> x = y)
        -> NoDup (flat_map f w)).
    intro w. induction w; intros. apply NoDup_nil. simpl. apply NoDup_app.
    apply H1. apply in_eq. apply IHw. apply NoDup_cons_iff in H0.
    destruct H0. assumption.
    intros. apply H1. apply in_cons. assumption.
    intros. apply H2 with (z := z). apply in_cons.
    assumption. apply in_cons. assumption. assumption. assumption.
    intros. intro. apply in_flat_map in H4. destruct H4. destruct H4.
    apply H2 with (x := a1) in H5. rewrite H5 in H0.
    apply NoDup_cons_iff in H0. destruct H0. apply H0 in H4. contradiction.
    apply in_eq. apply in_cons. assumption. assumption.
    assert (M: forall x, In x q -> ~ In a0 x). intros.
    apply H in H0. intro. apply Permutation_in with (l' := a::u) in H1.
    apply K1 in H1. assumption. apply Permutation_sym. assumption.

    apply L. apply seq_NoDup. intros.
    apply NoDup_map_NoDup_ForallPairs.
    repeat intro.
    assert (remove eq_dec a0 (firstn x a1 ++ a0 :: skipn x a1)
       = remove eq_dec a0 (firstn x b ++ a0 :: skipn x b)). rewrite H3.
    reflexivity. repeat rewrite remove_app in H4.
    repeat rewrite remove_cons in H4. repeat rewrite <- remove_app in H4.
    repeat rewrite firstn_skipn in H4.
    rewrite notin_remove in H4. rewrite notin_remove in H4. assumption.
    apply M. assumption. apply M. assumption. assumption.

    intros. apply in_map_iff in H2. destruct H2. destruct H2.
    apply in_map_iff in H3. destruct H3. destruct H3.
    rewrite <- H2 in H3. apply app_inj_pivot in H3. destruct H3.
    assert (forall (l: list X) k, ~ In a0 l -> ~ In a0 (firstn k l)).
    intros. intro. apply H6. rewrite <- firstn_skipn with (n := k).
    apply in_or_app. left. assumption.
    destruct H3. destruct H3. apply H6 in H3. contradiction.
    apply H6 in H3. contradiction. apply M. assumption.
    destruct H3. apply H6 in H7. contradiction.
    apply M. assumption. destruct H3.

    apply in_seq in H0. destruct H0. apply in_seq in H1. destruct H1.
    apply H in H4. apply H in H5.
    apply Permutation_length in H4. apply Permutation_length in H5.
    assert (length (firstn y x1) = length (firstn x x0)).
    rewrite H3. reflexivity.
    rewrite firstn_length_le in H9.
    rewrite firstn_length_le in H9. rewrite H9. reflexivity.
    rewrite <- H4. apply Nat.lt_succ_r. assumption.
    rewrite <- H5. apply Nat.lt_succ_r. assumption.
Qed.


Theorem all_permutations_NoDup :
  forall (l: list X), NoDup l -> NoDup (all_permutations l).
Proof.
  intros.
  assert (length (all_permutations l) = fact (length l)).
  apply all_permutations_length.

  assert (exists q : list (list X),
    (forall v : list X, Permutation l v <-> In v q) /\
    length q = fact (length l) /\ NoDup q).
  apply Permutation_exists_set_length; assumption.
  destruct H1. destruct H1. destruct H2.

  apply NoDup_incl_NoDup with (l := x). assumption.
  rewrite H0. rewrite H2. apply Nat.le_refl.

  repeat intro. apply H1 in H4.
  apply all_permutations_Permutation. assumption.
Qed.



Definition subsets_nodup (l: list X) :=
  filter (fun e => if (list_eq_dec eq_dec) (nodup eq_dec e) e
                            then true else false) (subsets l).


Lemma subsets_nodup_subsets :
  forall (u v: list X), In v (subsets_nodup u) -> In v (subsets u).
Proof.
  intros. apply filter_In in H. destruct H. assumption.
Qed.


Lemma subsets_nodup_NoDup :
  forall (u v: list X), In v (subsets_nodup u) -> NoDup v.
Proof.
  intros. apply filter_In in H. destruct H.
  destruct ((list_eq_dec eq_dec) (nodup eq_dec v) v). rewrite <- e.
  apply NoDup_nodup. apply Bool.diff_false_true in H0. contradiction.
Qed.


Lemma subsets_NoDup_nodup :
  forall (u v: list X), NoDup v -> In v (subsets u) -> In v (subsets_nodup u).
Proof.
  intros. apply filter_In. split. assumption.
  apply nodup_fixed_point with (decA := eq_dec) in H. rewrite H.
  destruct (list_eq_dec eq_dec v v). reflexivity. contradiction.
Qed.


Theorem subsets_nodup_trivial_incl :
  forall (u: list X) (x: X),
    incl (subsets_nodup u) (subsets_nodup (x::u)).
Proof.
  repeat intro. assert (I := H). apply subsets_nodup_subsets in I.
  apply subsets_trivial_incl with (x := x) in I.
  apply subsets_nodup_NoDup in H.
  apply filter_In. split. assumption.
  apply nodup_fixed_point with (decA := eq_dec) in H. rewrite H.
  destruct (list_eq_dec eq_dec a a). reflexivity. contradiction.
Qed.


Theorem subsets_nodup_nil_in :
  forall (u: list X), In nil (subsets_nodup u).
Proof.
  induction u. apply in_eq.
  apply subsets_nodup_trivial_incl with (x := a) in IHu. assumption.
Qed.


Theorem Permutation_dec :
  forall (u v: list X), { Permutation u v} + {~ Permutation u v}.
Proof.
  intros. destruct (in_dec (list_eq_dec eq_dec) v (all_permutations u)).
  left. apply all_permutations_Permutation. assumption. right.
  intro. apply n. apply all_permutations_Permutation. assumption.
Qed.


Lemma suffixes_split :
  forall (u l: list X),
    exists v w, suffixes u = v ++ w
      /\ (forall x, In x v -> Subsequence l x)
      /\ (forall x, In x w -> ~ Subsequence l x).
Proof.
  intro u. induction u; intros.
  exists [ nil ]. exists nil. split. reflexivity.
  split; intros. destruct H. rewrite <- H. apply Subsequence_nil_r.
  apply in_nil in H. contradiction. apply in_nil in H. contradiction.

  destruct (Subsequence_dec eq_dec l (a::u)).
  exists (suffixes (a::u)). exists nil. split.
  rewrite app_nil_r. reflexivity.
  split; intros. apply suffixes_subsequence in H.
  apply Subsequence_trans with (l2 := a::u); assumption.
  apply in_nil in H. contradiction.

  specialize (IHu l). destruct IHu. destruct H. destruct H.
  destruct H0. exists x. exists (x0 ++ [a::u]). split.
  simpl. rewrite H. rewrite app_assoc. reflexivity.
  split; intros. apply H0. assumption.
  apply in_app_or in H2. destruct H2. apply H1. assumption.
  destruct H2. rewrite <- H2. assumption. apply in_nil in H2. contradiction.
Qed.


End Exists_set_dec.
