Require Import Nat.
Require Import PeanoNat.
Require Import List.

Import ListNotations.

Require Import Sorting.Permutation.
Require Import Arith.Factorial.

Require Import subsequence.
Require Import count.
Require Import scs.
Require Import swap.

Require Import Lists.ListSet.



Set Implicit Arguments.

Section Resilient_dec.

Variable X: Type.
Hypothesis eq_dec : forall x y: X, {x=y} + {x <> y}.





Definition Resilient (l: list X) (x: X) :=
  hd x l <> x
        /\ forall a, Count eq_dec (Swap eq_dec x (hd x l) a) x l
             = Count eq_dec a (hd x l) l.



Lemma Resilient_not_nil :
  forall (x: X), ~ Resilient nil x.
Proof.
  intros. intro. destruct H. contradiction.
Qed.










Lemma Resilient_in :
  forall (l: list X) (x: X), Resilient l x -> In x l.
Proof.
  intros. destruct H.
  assert (
    Count eq_dec (Swap eq_dec x (hd x l) nil) x l = Count eq_dec nil (hd x l) l).
    apply H0. simpl in H1.
    destruct l. contradiction.
    simpl in H1. destruct (eq_dec x0 x0).
    destruct (eq_dec x x0). rewrite e0 in H. contradiction.
  assert ({In x l}+{~ In x l}). apply ListDec.In_dec.
  assumption. destruct H2. apply in_cons. assumption.
  apply Count_zero_2 with (eq_dec := eq_dec) (a := nil) in n0.
  rewrite H1 in n0. inversion n0. contradiction.
Qed.


Lemma Resilient_not_in :
  forall (l: list X) (x: X), ~ In x l -> ~ Resilient l x.
Proof.
  intros. intro. apply H. apply Resilient_in. assumption.
Qed.







(*


Theorem Resilient_no_change :
  forall (l: list X) (x y z: X),
    Resilient (x::y::z::l) z -> z = y \/ Count_no_change eq_dec y (z::l).
Proof.
  intro l. induction l; intros. destruct (eq_dec z y). left. assumption.
  destruct H. destruct H0.





Theorem Resilient_no_change :
  forall (l: list X) (x: X), Resilient l x -> Count_no_change eq_dec x l.
Proof.
  intro l. induction l; intros. apply Resilient_not_nil in H. contradiction.
  intro. destruct H. destruct H0. simpl in H0.
  assert (Count eq_dec a0 x (a::l) = Count eq_dec a0 (hd x (a::l)) (a::l)).
  apply H1. simpl in H2. simpl. destruct (eq_dec x x). destruct (eq_dec a a).
  destruct (eq_dec x a). rewrite e1 in H0. contradiction. rewrite H2.
  destruct (remove eq_dec x a0). destruct (remove eq_dec a a0).
  reflexivity.




 *)




End Resilient_dec.
