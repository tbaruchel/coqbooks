Require Import Nat.
Require Import PeanoNat.
Require Import List.

Import ListNotations.

Require Import Sorting.Permutation.
Require Import Arith.Factorial.

Require Import subsequence.
Require Import count.
Require Import scs.
Require Import swap.
Require Import resilient.
Require Import notebook.

Require Import Lists.ListSet.



Set Implicit Arguments.

Section Scratch_dec.

Variable X: Type.
Hypothesis eq_dec : forall x y: X, {x=y} + {x <> y}.



(*

Lemma xxx :
  forall (u a l: list X) (x y: X), In u (Collect eq_dec a x l)
    -> In (Swap eq_dec x y u) (Collect eq_dec (Swap eq_dec x y a) y (y::l)).
Proof.
  intro u. induction u; intros. apply Collect_nil in H. contradiction.
  assert (I := H). apply Collect_head in I. rewrite <- I in H. rewrite <- I.

  destruct (eq_dec x y). rewrite <- e. repeat rewrite Swap_trivial_id.
  apply Collect_append. assumption.

  assert (J: {In y u} + {~ In y u}). apply ListDec.In_dec. assumption.
  destruct J.
  (*
  assert (Permutation (x::u) (Swap eq_dec x y (x::u))).
  apply Swap_base_perm. apply Collect_nodup_2 in H. assumption.
  apply in_eq. apply in_cons. assumption.
   *)

  assert (K := H). assert (N := H). apply Collect_nodup_2 in K.
  apply Collect_subsequence in H.

  apply in_split in i. destruct i. destruct H0.

  assert (M: Swap eq_dec x y (x::u) = y::x0 ++ x::x1). rewrite H0.
  rewrite app_comm_cons. rewrite Swap_app. simpl. destruct (eq_dec x x).
  destruct (eq_dec y y). destruct (eq_dec x y).
  rewrite e1 in K. rewrite H0 in K. apply NoDup_cons_iff in K.
  destruct K. assert False. apply H1. apply in_or_app. right. apply in_eq.
  contradiction. f_equal.
  rewrite H0 in K. apply NoDup_cons_iff in K. destruct K.
  assert (Swap eq_dec x y x0 = x0). apply Swap_not_in. intro.
  apply H1. apply in_or_app. left. assumption. intro. 
  apply NoDup_remove_2 in H2. apply H2. apply in_or_app. left. assumption.
  rewrite H3.
  assert (Swap eq_dec x y x1 = x1). apply Swap_not_in. intro.
  apply H1.
  apply in_or_app. right. apply in_cons. assumption. intro. 
  apply NoDup_remove_2 in H2. apply H2. apply in_or_app. right. assumption.
  rewrite H4. reflexivity. contradiction. contradiction.
  rewrite M.

  assert (O : Permutation (x :: x0 ++ y :: x1) (y :: x0 ++ x :: x1)).
  rewrite <- M. rewrite H0. apply Swap_base_perm. rewrite <- H0. assumption.
  apply in_eq. rewrite app_comm_cons. apply in_or_app. right. apply in_eq.

  apply Collect_subsequence_3.

  apply Permutation_NoDup with (l := x::x0++y::x1). assumption.
  rewrite <- H0. assumption.

  apply Collect_alphabet_incl_2 in N. rewrite H0 in N.
  repeat intro.
  destruct (eq_dec a1 x). rewrite app_comm_cons. apply in_or_app.
  right. rewrite e. apply in_eq.
  destruct (eq_dec a1 y). rewrite e. apply in_eq. assert (In a1 a0).
  rewrite <- Swap_not_swapped with (eq_dec := eq_dec) (x := x) (y := y).
  assumption. intro. rewrite H2 in n0. contradiction.
  intro. rewrite H2 in n1. contradiction.
  rewrite app_comm_cons. apply in_or_app.
  apply N in H2. rewrite app_comm_cons in H2. apply in_app_or in H2.
  destruct H2. destruct H2.
  right. rewrite H2. apply in_eq.
  left. apply in_cons. assumption. destruct H2. left. rewrite H2. apply in_eq.
  right. apply in_cons. assumption.

  repeat intro.
  destruct (eq_dec a1 x).
  assert (In y a0).
  apply Collect_alphabet_incl in N.
  assert (In y u). rewrite H0. apply in_or_app. right. apply in_eq.
  apply in_cons with (a := x) in H2. apply N in H2.
  destruct H2. apply n in H2. contradiction. assumption.
  rewrite e. apply in_split in H2. destruct H2. destruct H2. rewrite H2.
  rewrite Swap_app. apply in_or_app. right.
  simpl. destruct (eq_dec x y). apply n in e0. contradiction. left.
  destruct (eq_dec y y). reflexivity. contradiction.

  apply Swap_not_swapped.
  apply Collect_alphabet_incl in N. rewrite H0 in N.
  apply incl_cons_inv in N. destruct N.
  apply incl_app_inv in H3. destruct H3.
  apply incl_cons_inv in H4. destruct H4.
  apply in_app_or in H1. destruct H1. apply H3 in H1. destruct H1.
  rewrite H1 in n0. contradiction.
  intro. rewrite H6 in n0. contradiction.
  intro. rewrite H6 in n0. contradiction.
  rewrite H0 in K. apply NoDup_cons_iff in K. destruct K.
  apply NoDup_remove_2 in H3.
  intro. apply H3. rewrite H4.
  apply in_elt_inv in H1. destruct H1. apply n0 in H1. contradiction.
  assumption.

  rewrite H0 in N.
  apply Collect_alphabet_incl in N.
  apply in_app_or in H1. destruct H1.
  rewrite app_comm_cons in N. apply incl_app_inv in N. destruct N.
  apply incl_cons_inv in H2. destruct H2. apply H4 in H1.
  destruct H1. rewrite H1 in n0. contradiction. assumption.
  rewrite app_comm_cons in N. apply incl_app_inv in N. destruct N.
  apply incl_cons_inv in H3. destruct H3.
  destruct H1. rewrite H1 in n0. contradiction. apply H4 in H1.
  destruct H1. rewrite H1 in n0. contradiction. assumption.



  apply Subsequence_cons_eq. rewrite H0 in H.








  assert (Subsequence (y::l) (x0 ++ x::x1)).
  apply Collect_subsequence with (eq_dec := eq_dec)
                                 (a := Swap eq_dec x y a0) (x := y).
  replace (x0++x::x1) with (Swap eq_dec x y u). apply IHu.






Subsequence_cons_diff:
  forall {X : Type} (u v : list X) (a b : X),
  a <> b -> Subsequence (a :: u) (b :: v) -> Subsequence u (b :: v)



  apply N in H1. apply Permutation_in with (l := x::x0++y::x1).
  assumption. assumption.

  apply Collect_alphabet_incl in N. rewrite H0 in N.
  repeat intro.
  assert (In a1 (x::x0++y::x1)). apply in_app_or in H1. destruct H1.
  rewrite app_comm_cons. apply in_or_app. left. apply in_cons. assumption.
  destruct H1. rewrite H1. apply in_eq.
  rewrite app_comm_cons. apply in_or_app. right. apply in_cons. assumption.
  apply N in H2. destruct H2.








  apply in_or_app. left. assumption.

  replace (Swap eq_dec x y (x::u)) with (y::(Swap eq_dec x y u)).
  apply Collect_subsequence_3.



 *)



Lemma Collect_change_head :
  forall (l a u1 u2: list X) (x y: X),
    In x a -> In (x::u1 ++ y::u2) (Collect eq_dec a x l)
    -> In (y::x::u1++u2) (Collect eq_dec a y (y::l)).
Proof.
  intros.
  assert (I := H0). assert (J := H0). assert (K := H0). assert (L := H0).
  apply Collect_subsequence in I. apply Collect_nodup_2 in J.
  apply Collect_alphabet_incl in K. apply Collect_alphabet_incl_2 in L.
  apply Collect_subsequence_3.
  rewrite app_comm_cons in J. apply NoDup_remove in J. destruct J.
  rewrite <- app_comm_cons in H1. rewrite <- app_comm_cons in H2.
  apply NoDup_cons; assumption.
  repeat intro. apply L in H1.
  destruct H1. rewrite H1. apply in_cons. apply in_eq.
  apply in_app_or in H1. destruct H1. repeat apply in_cons.
  apply in_or_app. left. assumption. destruct H1. rewrite H1. apply in_eq.
  repeat apply in_cons. apply in_or_app. right. assumption.
  repeat intro. destruct H1. rewrite <- H1. assumption.
  assert (a0 <> x). apply in_app_or in H1. intro. rewrite H2 in H1.
  destruct H1; apply in_split in H1; destruct H1; destruct H1; rewrite H1 in J;
  apply NoDup_cons_iff in J; destruct J; apply H3; apply in_or_app.
  left. apply in_or_app. right. apply in_eq.
  right. rewrite app_comm_cons. apply in_or_app. right. apply in_eq.
  rewrite app_comm_cons in K. apply incl_app_inv in K. destruct K.
  apply in_app_or in H1. destruct H1.
  apply incl_cons_inv in H3. destruct H3. apply H5 in H1.
  destruct H1. rewrite H1 in H2. contradiction. assumption.
  apply incl_cons_inv in H4. destruct H4. apply H5 in H1.
  destruct H1. rewrite H1 in H2. contradiction. assumption.

  rewrite app_comm_cons in I. apply Subsequence_remove_middle in I.
  rewrite <- app_comm_cons in I. apply Subsequence_cons_eq. assumption.
Qed.






  replace (length (Collect eq_dec (remove eq_dec x a) y l))
    with (length (Collect eq_dec (y::x0++x1) y l)).



  simpl. destruct (eq_dec x x). destruct(remove eq_dec x a).
  symmetry in H0. apply app_eq_nil in H0. destruct H0. inversion H1.
  assert (Permutation (Collect eq_dec (y::x0++x1) y l)
                      (Collect eq_dec (x2::l0) y l)).
  apply Collect_alphabet_perm. rewrite H0. apply Permutation_middle.
  apply Permutation_length in H1. rewrite H1. simpl.
  rewrite length_map. rewrite length_app. apply Nat.le_add_r.


Collect_alphabet_perm:
  forall [X : Type] (eq_dec : forall x y : X, {x = y} + {x <> y})
    [a1 a2 : list X] (x : X) (l : list X),
  Permutation a1 a2 ->
  Permutation (Collect eq_dec a1 x l) (Collect eq_dec a2 x l)



Theorem xxx :
  forall (l a: list X) (x y: X),
    In x a -> In y a -> Count eq_dec a x l <= Count eq_dec a y (y::l).
Proof.
  intros. destruct (eq_dec x y). rewrite e. apply Count_monotonic.

  repeat rewrite Count_Collect_length.

  apply Nat.le_trans with (m := length 
      (map (fun w => y::x::(remove eq_dec y (tail w)))
            (Collect eq_dec a x l))). rewrite length_map.
  apply Nat.le_refl.








  assert (incl (map (fun w => y::x::(remove eq_dec y (tail w)))
            (Collect eq_dec a x l)) (Collect eq_dec a y (y::l))).
  repeat intro. apply in_map_iff in H1. destruct H1. destruct H1.
  rewrite <- H1.
  destruct x0. apply Collect_nil in H2. contradiction.
  assert (H3 := H2). apply Collect_head in H2. rewrite <- H2 in H3.
  assert (H4 := H3). apply Collect_nodup_2 in H3.
  assert (H5 := H4).
  apply Collect_alphabet_incl_2 in H4. apply H4 in H0.
  destruct H0. apply n in H0. contradiction. apply in_split in H0.
  destruct H0. destruct H0. rewrite H0. rewrite H0 in H3.
  replace (remove eq_dec y (tl (x0::x2++y::x3))) with (x2++x3).
  apply Collect_change_head. assumption.
  rewrite <- H0. assumption. simpl. rewrite remove_app.
  simpl. destruct (eq_dec y y). repeat rewrite notin_remove. reflexivity.
  rewrite app_comm_cons in H3. apply NoDup_app_remove_l in H3.
  apply NoDup_cons_iff in H3. destruct H3. assumption.
  rewrite app_comm_cons in H3. apply NoDup_remove in H3. destruct H3.
  intro. apply H6. apply in_or_app. left. apply in_cons. assumption.
  contradiction.

  replace (length (Collect eq_dec a x l))
    with (length (map (fun w : list X => y :: x :: remove eq_dec y (tl w))
       (Collect eq_dec a x l))). apply NoDup_incl_length.
  apply NoDup_map_NoDup_ForallPairs. repeat intro.





  Lemma Collect_change_head :
    forall (l a u1 u2: list X) (x y: X),
      In x a -> In (x::u1 ++ y::u2) (Collect eq_dec a x l)
      -> In (y::x::u1++u2) (Collect eq_dec a y (y::l)).


Theorem xxx :
  forall (l: list X) (x y z: X),
      x <> y -> Resilient eq_dec (x::y::z::l) z
      -> Count_no_change eq_dec y (z::l).
Proof.
  intros. destruct (eq_dec y z). rewrite e. apply Count_cons_dup.
  intro. apply Count_no_change_set. repeat intro.

  (* décider si z est dans a1 ou non *)
  assert ({In z a1} + {~ In z a1}). apply ListDec.In_dec. assumption.
  destruct H2.

  (* TODO: on peut probablement retirer ici le replace ci-dessous
           mis pour le moment par souci de clarté *)
  destruct H0. replace (hd z (x::y::z::l)) with x in H2.
  rewrite Collect_cons_not_eq.

  specialize (H2 a0) as I.
  replace (Count eq_dec (Swap eq_dec z x a0) z (x :: y :: z :: l))
    with (Count eq_dec a0 z (x::y::z::l)) in I.
  rewrite Count_cons_not_eq in I. rewrite Count_cons_not_eq in I.

  assert (J: {In x a1} + {~ In x a1}). apply ListDec.In_dec. assumption.
  destruct J. assert (J := i0).
  apply in_split in i0. destruct i0. destruct H3.

  assert (L: In x a0). apply Collect_alphabet_incl in H1.
  apply H1 in J. destruct J. rewrite H4 in H. contradiction. assumption.
  assert (M: In z a0). apply Collect_alphabet_incl in H1.
  apply H1 in i. destruct i. apply n in H4. contradiction. assumption.






Theorem Count_swap_base :
  forall (l base: list X) (x y z: X),
    NoDup base -> In x base -> In y base
    -> Count eq_dec (Swap x y base) z l = Count eq_dec base z l.






  (* on prend intentionnellement a1 plutôt que a0 *)
  (* il s'agit de prendre une base qui contient forcément a1 *)
  specialize (H2 a1).

  assert ({In x a1} + {~ In x a1}). apply ListDec.In_dec. assumption.
  destruct H3. assert (I := i0).
  apply in_split in i0. destruct i0. destruct H3.

  assert (L: In x a0). apply Collect_alphabet_incl in H1.
  apply H1 in I. destruct I. rewrite H4 in H. contradiction. assumption.

  assert (Subsequence (x::y::z::l) (x::x0 ++ x1)).
  apply Collect_subsequence with (eq_dec := eq_dec) (a := a1) (x := x).
  apply Collect_subsequence_3. apply Permutation_NoDup with (l := a1).
  rewrite H3. apply Permutation_sym. apply Permutation_middle.
  apply Collect_nodup_2 in H1. assumption. rewrite H3.
  apply incl_app. apply incl_tl. apply incl_appl. apply incl_refl.
  apply incl_cons. apply in_eq. rewrite app_comm_cons.
  apply incl_appr. apply incl_refl. rewrite H3. apply incl_app.
  apply incl_appl. apply incl_refl. apply incl_appr. apply incl_tl.
  apply incl_refl. apply Subsequence_cons_eq.
  apply Collect_subsequence in H1.
  apply Subsequence_remove_middle with (a := x). rewrite <- H3. assumption.

  assert (H5 := H4). apply Subsequence_cons_eq in H5.

  (* TODO: problème on sait que a0 contient x, or x0++x1 non *)
  assert (In (x0 ++ x1) (Collect eq_dec a0 y (z::l))). assert (K := H1).
  rewrite H3 in H1. destruct x0; apply Collect_head in H1. rewrite H1 in H.
  contradiction. rewrite <- H1. rewrite <- app_comm_cons.
  apply Collect_subsequence_3. apply Collect_nodup_2 in K.
  rewrite H3 in K. rewrite <- H1 in K. apply NoDup_remove_1 in K.
  assumption. 

  repeat intro.



  apply Collect_alphabet_incl_2 in K. rewrite H3 in K.
  rewrite <- H1 in K.

  (* FAUX : le même ne peut commencer par z et par x du fait de H0 *)
  (* à réécrire comme forall en intégrant un swap *)
  assert (incl (Collect eq_dec (Swap eq_dec z x a1) z (x::y::z::l))
               (Collect eq_dec a1 x (x::y::z::l))). repeat intro.
  assert (Permutation a1 (Swap eq_dec z x a1)). apply Collect_nodup_2 in H1.
  apply Swap_base_perm; assumption.
  apply Collect_alphabet_perm with (eq_dec := eq_dec) (x := z) (l := x::y::z::l) in H6.
  apply Permutation_in with (l' := Collect eq_dec a1 z (x::y::z::l)) in H5.




Theorem xxx :
  forall (l: list X) (x y z: X),
      x <> y -> ~ Count_no_change eq_dec y (z::l)
        -> ~ Resilient eq_dec (x::y::z::l) z.
Proof.
  intros. intro. apply H0.


Theorem xxx :
  forall (l: list X) (x y z: X),
      Resilient eq_dec (x::y::z::l) z
        -> x = y \/ Resilient eq_dec (x::y::z::l) y.
Proof.
  intro l. induction l; intros.
  destruct (eq_dec x y). left. assumption. right.
  split. assumption. intro.

  simpl. destruct (eq_dec y x). rewrite e in n. contradiction.
  destruct (eq_dec y y).
  destruct (eq_dec x x).
  rewrite Swap_remove. destruct (remove eq_dec x a). reflexivity.
  simpl.
  destruct (eq_dec
             (if eq_dec y x0 then x else if eq_dec x x0 then y else x0)
             (if eq_dec y x0 then x else if eq_dec x x0 then y else x0)).
  destruct (eq_dec y x0).
  destruct (eq_dec x z).
  rewrite Swap_trivial_swap.
  rewrite Swap_remove.
  destruct (eq_dec x0 y). rewrite e4.
  destruct (eq_dec x0 x0).
  destruct (eq_dec y y).
  destruct (remove eq_dec y l). simpl.



  rewrite <- H0. simpl.



Definition Resilient (l: list X) (x: X) :=
  hd x l <> x
        /\ forall a, Count eq_dec (Swap eq_dec x (hd x l) a) x l
             = Count eq_dec a (hd x l) l.



Theorem xxx :
  forall (l: list X) (x y z: X),
    ~ Count_no_change eq_dec y (z::l)
      -> Resilient eq_dec (x::y::z::l) z -> y = x \/ y = z.
Proof.
  intro l. induction l; intros.
  destruct (eq_dec y x). left. assumption. right.







Theorem Swappable_trivial_rev :
  forall (l: list X) (x y: X),
  Swappable eq_dec x y l -> forall u base,
    NoDup base -> In x base -> In y base -> Complete_sequence (u++l) base
      -> Swappable eq_dec x y (rev u).
Proof.
  intros. intro. intros. apply Complete_sequence_rev in H7.
  rewrite rev_app_distr in H7.


Definition Swappable (x y: X) (l: list X) :=
  forall u base, NoDup base -> In x base -> In y base
       -> Complete_sequence (u++l) base
       -> Complete_sequence (u ++ (Swap x y l)) base.




















Theorem xxx :
  forall (l: list X) (x: X),
  Resilient eq_dec l x -> Count_no_change eq_dec x l.
Proof.
  intro l. induction l; intros. apply Resilient_not_nil in H. contradiction.
  destruct H. intro. (* replace (hd x (a::l)) with a in H0 by reflexivity. *)
  assert (Count eq_dec (Swap eq_dec x a a0) x (a :: l)
    = Count eq_dec a0 a (a :: l)). apply H0.
  simpl. simpl in H1. destruct (eq_dec x a).
  rewrite e. rewrite e in H1. destruct (eq_dec a a). rewrite Swap_remove in H1.
  destruct (remove eq_dec a a0). reflexivity.
  simpl. destruct (eq_dec x0 a). destruct (eq_dec x0 x0).


  intros l x. intro. intro u. induction u; intros. reflexivity.
  simpl. destruct (eq_dec y a). destruct (eq_dec x a).

Lemma Swap_base_perm :
  forall (base : list X) (x y: X),
    NoDup base -> In x base -> In y base -> Permutation base (Swap x y base).





Theorem xxx :
  forall (l: list X) (x: X),
    Resilient eq_dec l x
    -> forall a, NoDup a -> In x a
        -> Count eq_dec a x l = Count eq_dec a (hd x l) l.
Proof.
  intros. destruct H. intros.
  assert ({ In (hd x l) a } + { ~ In (hd x l) a}).
  apply ListDec.In_dec. assumption. destruct H3. rewrite <- H2.
  assert (Permutation a (Swap eq_dec x (hd x l) a)).
  apply Swap_base_perm; assumption. apply Count_alphabet_perm. assumption.

  (* rewrite Count_defective with (x := hd x l). *)
  assert (Permutation (hd x l :: a) (Swap eq_dec x (hd x l) (hd x l :: a))).
  apply Swap_base_perm. apply NoDup_cons; assumption. apply in_cons.
  assumption. apply in_eq.
  apply Count_alphabet_perm with (eq_dec := eq_dec) (x := x) (l := l) in H3.

  replace (Swap eq_dec x (hd x l) (hd x l :: a))
    with (x :: (Swap eq_dec x (hd x l) a)) in H3.
    rewrite <- Count_defective in H3. rewrite H2 in H3.
  rewrite <- H3.





  apply Count_alphabet_perm with (eq_dec := eq_dec) (x := x) (l := l) in H3.
  assum

Count_alphabet_perm:
  forall [X : Type] (eq_dec : forall x y : X, {x = y} + {x <> y})
    [a1 a2 : list X] (x : X) (l : list X),
  Permutation a1 a2 -> Count eq_dec a1 x l = Count eq_dec a2 x l





Definition Resilient (l: list X) (x: X) :=
  hd x l <> x
        /\ forall a, Count eq_dec (Swap eq_dec x (hd x l) a) x l
             = Count eq_dec a (hd x l) l.





Theorem xxx :
  forall (l: list X) (x: X),
    Resilient eq_dec l x
    -> forall a, Count eq_dec a x l = 0 \/ (In x a /\ In (hd x l) a).
Proof.
  intros. assert ({ Count eq_dec a x l = 0 } + {~ Count eq_dec a x l = 0}).
  apply Nat.eq_dec. destruct H0. left. assumption. right. destruct H.
  rewrite Count_defective in n.
  assert (
    Count eq_dec (Swap eq_dec x (hd x l) ((hd x l)::(Swap eq_dec x (hd x l) a))) x l
      = Count eq_dec ((hd x l)::(Swap eq_dec x (hd x l) a)) (hd x l) l). apply H0.
      simpl in H1. destruct (eq_dec x (hd x l)). rewrite <- e in H.
      contradiction. destruct (eq_dec (hd x l) (hd x l)).
      rewrite Swap_trivial_swap_swap.



Theorem xxx :
  forall (l: list X) (x: X),
  Resilient eq_dec l x
  -> forall a, NoDup a -> In x a -> In (hd x l) a
      -> Count eq_dec a x l = Count eq_dec a x (x::l).
Proof.
  intros. assert (Permutation a (Swap eq_dec x (hd x l) a)).
  apply Swap_base_perm; assumption. destruct H.
  apply Count_alphabet_perm with (eq_dec := eq_dec) (x := x) (l := l) in H3.
  rewrite H3. rewrite H4.

  (* TODO but intéressant à ce stade ; y réfléchir *)




  intro l. induction l; intros. apply Resilient_not_nil in H. contradiction.
  intros. assert (Permutation a0 (Swap eq_dec x a a0)).
  apply Swap_base_perm; assumption. 

Count_alphabet_perm:
  forall (X : Type) (eq_dec : forall x y : X, {x = y} + {x <> y})
    (a1 a2 : list X) (x : X) (l : list X),
  Permutation a1 a2 -> Count X eq_dec a1 x l = Count X eq_dec a2 x l



Theorem xxx :
  forall (l: list X) (x: X),
  Resilient eq_dec l x ->
    forall u a y,
      Count eq_dec a y (u++l)
           = Count eq_dec a y ((Swap eq_dec x (hd x l) u) ++ l).
Proof.
  intros l x. intro. intro u. induction u; intros. reflexivity.
  simpl. destruct (eq_dec y a). destruct (eq_dec x a).
  destruct (eq_dec y (hd x l)). rewrite e in e1. rewrite e1 in e0.
  assert False. apply H. symmetry. rewrite e0 at 1. reflexivity. contradiction.
  rewrite <- IHu. assert (In y (u++l)). apply in_or_app. right.
  apply Resilient_in in H. rewrite e. rewrite <- e0. assumption.
  apply Subsequence_In in H0. apply Subsequence_strong in H0.
  destruct H0. destruct H0. destruct H0. destruct H1. rewrite H0.
  rewrite <- Count_prefix. simpl.
  destruct (eq_dec y y). destruct (remove eq_dec y a0).
aa









Theorem xxx :
  forall (l a : list X) (x y: X),
    Count eq_dec a x l = Count eq_dec (Swap eq_dec x y a) y l
      -> False.
Proof.
  intro l. induction l; intros.

  admit.

  simpl in H.
  destruct (eq_dec x a).
  destruct (eq_dec y a).
  rewrite Swap_trivial_swap in H. repeat rewrite Swap_remove in H.
  destruct (remove eq_dec x a0).

  admit.
  admit.
  destruct (remove eq_dec x a0).
  admit.





  (*
     quelques points d'inflexion intéressants pour k=5 :
[0, 1, 2, 3, 4, 0] [0, 4]
[0, 1, 2, 3, 4, 0, 1, 2, 3, 0] [0, 3]
[0, 1, 2, 3, 4, 0, 1, 2, 3, 0, 4, 1] [1, 4]
[0, 1, 2, 3, 4, 0, 1, 2, 3, 0, 4, 1, 2, 0] [0, 2]
[0, 1, 2, 3, 4, 0, 1, 2, 3, 0, 4, 1, 2, 0, 3, 4] [4, 3]
   *)










End Scratch_dec.
