(** * Notebook 2
 *)


Require Import Nat.
Require Import PeanoNat.
Require Import List.

Import ListNotations.

Require Import Sorting.Permutation.
Require Import Arith.Factorial.

Require Import subsequence.
Require Import count.
Require Import scs.
Require Import swap.
Require Import notebook.




Set Implicit Arguments.

Section Notebook_dec.

Variable X: Type.
Hypothesis eq_dec : forall x y: X, {x = y} + {x <> y}.


(** ** Theorems related to swappable parts of a complete sequence.

*** Definition

We call "swappable" two symbols in a sequence if any complete sequence ending with that sequence (as a suffix) remains a complete sequence after swapping both symbols in that suffix.
 *)


Definition Swappable (x y: X) (l: list X) :=
  forall u base, NoDup base -> In x base -> In y base
       -> Complete_sequence (u++l) base
       -> Complete_sequence (u ++ (Swap eq_dec x y l)) base.

(** *** Examples

An example of such a sequence would be:
<<
  [1; 3; 2; 0] ++ [1; 3; 2; 1; 0; 3; 2; 1]
>>
which can be transformed as (by swapping symbols 1 and 3):
<<
  [1; 3; 2; 0] ++ [3; 1; 2; 3; 0; 1; 2; 3]
>>
while still remaining a complete sequence.

Another example would be:
<<
   [0; 4; 2; 1; 3; 0; 2] ++ [1; 4; 0; 3; 2; 1; 0; 4; 3; 2; 1; 0]
>>
which can be transformed as (by swapping symbols 1 and 4):
<<
   [0; 4; 2; 1; 3; 0; 2] ++ [4; 1; 0; 3; 2; 4; 0; 1; 3; 2; 4; 0]
>>
while still remaining a complete sequence.

 *)



(** *** Trivial properties
Two trivial properties of a swappable sequence is that it remains swappable when swapping both symbols everywhere, and (of course) that both symbols can also be swapped in any order.
*)

Theorem Swappable_trivial_swappable_swap :
  forall (l: list X) (x y: X),
    Swappable x y l -> Swappable x y (Swap eq_dec x y l).
Proof.
  repeat intro.
  apply Complete_sequence_swap with (eq_dec := eq_dec) (x := x) (y := y) in H3.
  rewrite Swap_trivial_swap_swap. rewrite Swap_app in H3.
  rewrite Swap_trivial_swap_swap in H3.
  apply H in H3. rewrite <- Swap_app in H3.
  apply Complete_sequence_swap with (eq_dec := eq_dec) (x := x) (y := y) in H3.
  rewrite Swap_trivial_swap_swap in H3.
  apply H3; assumption. assumption. assumption. assumption. assumption.
  assumption. assumption. assumption. assumption. assumption.
Qed.


Theorem Swappable_trivial_swap :
  forall (l: list X) (x y: X),
    Swappable x y l -> Swappable y x l.
Proof.
  intros. intro. intros. apply Swappable_trivial_swappable_swap in H.
  apply Complete_sequence_swap with (eq_dec := eq_dec) (x := x) (y := y) in H3.
  rewrite Swap_app in H3. apply H in H3.
  rewrite Swap_trivial_swap_swap in H3. rewrite Swap_trivial_swap.
  apply Complete_sequence_swap with (eq_dec := eq_dec) (x := x) (y := y) in H3.
  rewrite Swap_app in H3. rewrite Swap_trivial_swap_swap in H3.
  assumption. assumption. assumption. assumption. assumption. assumption.
  assumption. assumption. assumption. assumption.
Qed.


(** *** Theorems
A fundamental theorem linking the number of specific partial permutations in a sequence and its property of being swappable.
*)

Theorem Count_eq_swappable :
  forall (l: list X) (x y : X),
    (forall (a : list X) (z : X), NoDup a ->
     Count eq_dec a z l = Count eq_dec (Swap eq_dec x y a)
       (if eq_dec x z then y else if eq_dec y z then x else z) l) ->
    Swappable x y l.
Proof.
  intros. intro. intros.
  apply Count_fact_Complete with (eq_dec := eq_dec). assumption. intros.
  rewrite Count_full_swap with (x := x) (y := y).
  rewrite Swap_app. rewrite Swap_trivial_swap_swap.
  rewrite <- Count_swap_extend with (base := base) (x := x) (y := y).
  apply Complete_Count_fact with (base := base). assumption. assumption.
  assumption. apply incl_refl. assumption. assumption. assumption.
Qed.









(*

Collect_extend:
  forall [X : Type] (eq_dec : forall x y : X, {x = y} + {x <> y})
    (a : list X) (x : X) (l u : list X),
  incl (Collect eq_dec a x l) (Collect eq_dec a x (u ++ l))

Collect_incl_subsequence:
  forall [X : Type] (eq_dec : forall x y : X, {x = y} + {x <> y})
    (a : list X) (x : X) (l1 l2 : list X),
  incl (Collect eq_dec a x l1) (Collect eq_dec a x l2) ->
  Subsequence (Collect eq_dec a x l2) (Collect eq_dec a x l1)

Collect_incl_eq:
  forall [X : Type] (eq_dec : forall x y : X, {x = y} + {x <> y})
    (a : list X) (x : X) (l1 l2 : list X),
  incl (Collect eq_dec a x l1) (Collect eq_dec a x l2) ->
  incl (Collect eq_dec a x l2) (Collect eq_dec a x l1) ->
  Collect eq_dec a x l1 = Collect eq_dec a x l2

Count_fact_collect_stable:
  forall [X : Type] (eq_dec : forall x y : X, {x = y} + {x <> y})
    (u v : list X) [a : list X] (x : X),
  In x a ->
  NoDup a ->
  Count eq_dec a x v = fact (pred (length a)) ->
  Collect eq_dec a x v = Collect eq_dec a x (u ++ v)

Count_fact_previous_fact:
  forall [X : Type] (eq_dec : forall x y : X, {x = y} + {x <> y})
    (u : list X) [base : list X] [x : X],
  NoDup (x :: base) ->
  Count eq_dec (x :: base) x u = fact (length base) ->
  forall y : X,
  In y base -> Count eq_dec base y u = fact (pred (length base))

 *)


  (*
     quelques points d'inflexion intéressants pour k=5 :
[0, 1, 2, 3, 4, 0] [0, 4]
[0, 1, 2, 3, 4, 0, 1, 2, 3, 0] [0, 3]
[0, 1, 2, 3, 4, 0, 1, 2, 3, 0, 4, 1] [1, 4]
[0, 1, 2, 3, 4, 0, 1, 2, 3, 0, 4, 1, 2, 0] [0, 2]
[0, 1, 2, 3, 4, 0, 1, 2, 3, 0, 4, 1, 2, 0, 3, 4] [4, 3]
   *)




  (* TODO:
     - somme de tous les count pour tous les subsets et les caractères
       = 2^base en cas de complete sequence
     - pour swappable : équivalence entre le forall et le exists ?
     - retenter le théorème swappable/Complete

  *)




End Notebook_dec.





Example Swappable_example_1 : Swappable Nat.eq_dec 0 2 [0;2;1;0].
Proof.
Admitted.
