(** * Notebook 4
 *)


Require Import Nat.
Require Import PeanoNat.
Require Import List.

Import ListNotations.

Require Import Sorting.Permutation.
Require Import Arith.Factorial.

Require Import subsequence.
Require Import count.
Require Import scs.




Set Implicit Arguments.

Section Notebook_dec.

Variable X: Type.
Hypothesis eq_dec : forall x y: X, {x = y} + {x <> y}.


Fixpoint shortest_merge (u: list X) :=
  fix sm_aux (v : list X) :=
    match u with
    | nil    => v
    | hd::tl => match v with
              | nil      => u
                | hd'::tl' => if (eq_dec hd hd')
                           then hd :: (shortest_merge tl tl')
                           else let m1 := hd :: (shortest_merge tl v) in
                                let m2 := hd' :: (sm_aux tl') in
                            if (ltb (length m2) (length m1)) then m2 else m1
              end
  end.


Theorem shortest_merge_nil_l :
  forall (l: list X), shortest_merge nil l = l.
Proof.
  intro. destruct l; reflexivity.
Qed.


Theorem shortest_merge_nil_r :
  forall (l: list X), shortest_merge l nil = l.
Proof.
  intro. destruct l; reflexivity.
Qed.


Theorem shortest_merge_id :
  forall (u: list X), shortest_merge u u = u.
Proof.
  intro u. induction u. rewrite shortest_merge_nil_l. reflexivity.
  simpl. destruct (eq_dec a a). rewrite IHu. reflexivity. contradiction.
Qed.


Theorem shortest_merge_length :
  forall (u v: list X),
    length (shortest_merge u v) <= length (u++v).
Proof.
  intro u. induction u; intro. rewrite shortest_merge_nil_l. apply Nat.le_refl.
  induction v. rewrite app_nil_r. apply Nat.le_refl. simpl.
  destruct (eq_dec a a0). simpl. apply le_n_S.
  apply Nat.le_trans with (m := length (u++v)). apply IHu.
  repeat rewrite length_app. simpl. rewrite Nat.add_succ_r.
  apply Nat.le_succ_diag_r.
  assert ({ltb (length (a0::(shortest_merge (a::u) v)))
               (length (a::(shortest_merge u (a0::v)))) = true}
        + {ltb (length (a0::(shortest_merge (a::u) v)))
               (length (a::(shortest_merge u (a0::v)))) <> true}).
  apply Bool.bool_dec. destruct H. simpl in e. rewrite e.
  simpl. apply le_n_S.
  replace (length (u++a0::v)) with (length ((a::u)++v)).
  apply Nat.le_trans with (m := length (shortest_merge (a::u) v)).
  simpl. apply Nat.le_refl. assumption.
  repeat rewrite length_app. simpl. rewrite Nat.add_succ_r. reflexivity.
  apply Bool.not_true_is_false in n0. assert (I := n0).
  simpl in n0. rewrite n0. apply Nat.ltb_ge in I.
  apply Nat.le_trans with (m := length (a0 :: shortest_merge (a::u) v)).
  assumption.
  replace (length (u ++ a0::v)) with (length ((a::u) ++ v)).
  simpl. apply le_n_S. simpl in IHv. assumption.
  repeat rewrite length_app. simpl. rewrite Nat.add_succ_r. reflexivity.
Qed.


Theorem shortest_merge_length_l :
  forall (u v: list X),
    length u <= length (shortest_merge u v).
Proof.
  intro u. induction u; intros. rewrite shortest_merge_nil_l.
  apply le_0_n. induction v. rewrite shortest_merge_nil_r.
  apply Nat.le_refl. simpl. destruct (eq_dec a a0). apply le_n_S.
  apply IHu.
  assert ({ltb (length (a0::(shortest_merge (a::u) v)))
               (length (a::(shortest_merge u (a0::v)))) = true}
        + {ltb (length (a0::(shortest_merge (a::u) v)))
               (length (a::(shortest_merge u (a0::v)))) <> true}).
  apply Bool.bool_dec. destruct H. simpl in e. rewrite e. apply le_n_S.
  apply Nat.le_trans with (m := length (a::u)).
  apply Nat.le_succ_diag_r. apply IHv. apply Bool.not_true_is_false in n0.
  simpl in n0. rewrite n0. simpl. rewrite <- Nat.succ_le_mono. apply IHu.
Qed.


Theorem shortest_merge_length_swap_l_r :
  forall (u v: list X),
    length (shortest_merge u v) = length (shortest_merge v u).
Proof.
  intro u. induction u; intro.
  rewrite shortest_merge_nil_l. rewrite shortest_merge_nil_r. reflexivity.
  induction v.
  rewrite shortest_merge_nil_l. rewrite shortest_merge_nil_r. reflexivity.
  simpl. destruct (eq_dec a a0); destruct (eq_dec a0 a).
  simpl. rewrite IHu. reflexivity. rewrite e in n. contradiction.
  rewrite e in n. contradiction.
  assert ({ltb (length (a0::(shortest_merge (a::u) v)))
               (length (a::(shortest_merge u (a0::v)))) = true}
        + {ltb (length (a0::(shortest_merge (a::u) v)))
               (length (a::(shortest_merge u (a0::v)))) <> true}).
  apply Bool.bool_dec. destruct H.
  assert (I := e). simpl in e. rewrite e. specialize (IHu (a0::v)).
  simpl in IHu. rewrite <- IHu. simpl in IHv. rewrite <- IHv.
  apply Nat.ltb_lt in I. apply Nat.lt_asymm in I.
  apply Nat.ltb_nlt in I. simpl in I. rewrite I.
  simpl. rewrite IHv. reflexivity.
  apply Bool.not_true_is_false in n1. assert (I := n1).
  simpl in n1. rewrite n1. apply Nat.ltb_ge in I.
  apply Nat.lt_eq_cases in I. destruct I. apply Nat.ltb_lt in H.
  specialize (IHu (a0::v)). simpl in IHu. rewrite <- IHu.
  rewrite <- IHv. simpl in H. simpl. rewrite H.
  rewrite IHu. reflexivity.
  assert (~ length (a :: shortest_merge u (a0 :: v))
     < length (a0 :: shortest_merge (a :: u) v)).
  intro. rewrite H in H0. apply Nat.lt_irrefl in H0. contradiction.
  apply Nat.ltb_nlt in H0.
  specialize (IHu (a0::v)). simpl in IHu. rewrite <- IHu.
  simpl in IHv. rewrite <- IHv. simpl in H0. rewrite H0.
  rewrite H. simpl. rewrite IHv. reflexivity.
Qed.


Theorem shortest_merge_length_r :
  forall (u v: list X),
    length v <= length (shortest_merge u v).
Proof.
  intros. rewrite shortest_merge_length_swap_l_r. apply shortest_merge_length_l.
Qed.


Theorem shortest_merge_length_max :
  forall (u v: list X),
    max (length u) (length v) <= length (shortest_merge u v).
Proof.
  intros. apply Nat.max_lub.
  apply shortest_merge_length_l. apply shortest_merge_length_r.
Qed.


Theorem shortest_merge_subsequence_l :
  forall (u v w: list X),
    Subsequence u w -> Subsequence (shortest_merge u v) w.
Proof.
  intro u. induction u; intros. destruct w.
  apply Subsequence_nil_r. apply Subsequence_nil_cons_r in H. contradiction.
  destruct w. apply Subsequence_nil_r.
  induction v. rewrite shortest_merge_nil_r. assumption.
  simpl. destruct (eq_dec a a0).
  destruct (eq_dec a x). rewrite e0 in H. apply Subsequence_cons_eq in H.
  apply IHu with (v := v) in H.
  exists nil. exists (shortest_merge u v). split. rewrite e0. reflexivity.
  assumption.
  apply Subsequence_cons_diff in H. apply IHu with (v := v) in H.
  destruct H. destruct H. destruct H. exists (a::x0). exists x1.
  rewrite H. split. reflexivity. assumption. assumption.

  assert ({ltb (length (a0::(shortest_merge (a::u) v)))
               (length (a::(shortest_merge u (a0::v)))) = true}
        + {ltb (length (a0::(shortest_merge (a::u) v)))
               (length (a::(shortest_merge u (a0::v)))) <> true}).
  apply Bool.bool_dec. destruct H0. simpl in e. rewrite e.
  destruct IHv. destruct H0. destruct H0.
  exists (a0::x0). exists x1. simpl in H0. rewrite H0.
  split. reflexivity. assumption.
  apply Bool.not_true_is_false in n0. simpl in n0. rewrite n0.
  destruct (eq_dec a x). rewrite e in H. apply Subsequence_cons_eq in H.
  apply IHu with (v := a0::v) in H. exists nil. rewrite e.
  exists (shortest_merge u (a0::v)). split. reflexivity. assumption.
  apply Subsequence_cons_diff in H. apply IHu with (v := a0::v) in H.
  destruct H. destruct H. destruct H. exists (a::x0). exists x1.
  rewrite H. split. reflexivity. assumption. assumption.
Qed.


Theorem shortest_merge_subsequence_r :
  forall (u v w: list X),
    Subsequence v w -> Subsequence (shortest_merge u v) w.
Proof.
  intros u v. generalize u. induction v; intros. destruct w.
  apply Subsequence_nil_r. apply Subsequence_nil_cons_r in H. contradiction.
  destruct w. apply Subsequence_nil_r.
  induction u0. rewrite shortest_merge_nil_l. assumption.
  simpl. destruct (eq_dec a0 a).
  destruct (eq_dec a x). rewrite e0 in H. apply Subsequence_cons_eq in H.
  apply IHv with (u := u0) in H.
  exists nil. exists (shortest_merge u0 v). split. rewrite e. rewrite e0.
  reflexivity. assumption.
  apply Subsequence_cons_diff in H. apply IHv with (u := u0) in H.
  destruct H. destruct H. destruct H. exists (a0::x0). exists x1.
  rewrite H. split. reflexivity. assumption. assumption.

  assert ({ltb (length (a::(shortest_merge (a0::u0) v)))
               (length (a0::(shortest_merge u0 (a::v)))) = true}
        + {ltb (length (a::(shortest_merge (a0::u0) v)))
               (length (a0::(shortest_merge u0 (a::v)))) <> true}).
  apply Bool.bool_dec. destruct H0. simpl in e. rewrite e.
  destruct (eq_dec a x). rewrite e0 in H. apply Subsequence_cons_eq in H.
  apply IHv with (u := a0::u0) in H. exists nil. rewrite e0.
  exists (shortest_merge (a0::u0) v). split. reflexivity. assumption.
  apply Subsequence_cons_diff in H. apply IHv with (u := a0::u0) in H.
  destruct H. destruct H. destruct H. exists (a::x0). exists x1.
  simpl in H. rewrite H. split. reflexivity. assumption. assumption.
  apply Bool.not_true_is_false in n0. simpl in n0. rewrite n0.
  destruct (eq_dec a x). rewrite e in H. apply Subsequence_cons_eq in H.
  destruct IHu0. destruct H0. destruct H0.
  exists (a0::x0). exists x1. rewrite H0. split. reflexivity. assumption.
  apply Subsequence_cons_diff in H. destruct IHu0. destruct H0. destruct H0.
  exists (a0::x0). exists x1. rewrite H0. split. reflexivity. assumption.
  assumption.
Qed.


Theorem shortest_merge_length_sub_l :
  forall (u v: list X) (n: nat),
    u ++ v <> nil -> length (shortest_merge u v) <= length (u++v) - n
    -> n <= length u.
Proof.
  intros. assert (n <= length u \/ length u < n). apply Nat.le_gt_cases.
  destruct H1. assumption. apply Nat.add_lt_mono_r with (p := length v) in H1.
  rewrite <- length_app in H1.
  assert (length (shortest_merge u v) + n <= length (u++v)). assert (H2 := H0).
  apply Nat.add_le_mono_r with (p := n) in H0. rewrite Nat.sub_add in H0.
  assumption.
  assert (n <= length (u++v) \/ length (u++v) < n). apply Nat.le_gt_cases.
  destruct H3. assumption.
  apply Nat.lt_le_incl in H3. apply Nat.sub_0_le in H3. rewrite H3 in H2.

  assert (length u <= length (shortest_merge u v)).
  apply shortest_merge_length_l.
  destruct u; destruct v. contradiction.
  rewrite shortest_merge_nil_l in H2.
  apply Nat.nle_succ_0 in H2. contradiction.
  apply Nat.nle_succ_0 in H2. contradiction.
  assert (length (x::u) <= 0).
  apply Nat.le_trans with (m := length (shortest_merge (x :: u) (x0 :: v)));
  assumption. apply Nat.nle_succ_0 in H5. contradiction.

  assert (length (shortest_merge u v) + n < n + length v).
  apply Nat.le_lt_trans with (m := length (u++v)); assumption.
  rewrite Nat.add_comm in H3. apply Nat.add_lt_mono_l in H3.
  assert (length v < length v).
  apply Nat.le_lt_trans with (m := length (shortest_merge u v)).
  apply shortest_merge_length_r. assumption. apply Nat.lt_irrefl in H4.
  contradiction.
Qed.


Theorem shortest_merge_length_sub_r :
  forall (u v: list X) (n: nat),
    u ++ v <> nil -> length (shortest_merge u v) <= length (u++v) - n
    -> n <= length v.
Proof.
  intros. 
  apply shortest_merge_length_sub_l with (v := u).
  intro. apply app_eq_nil in H1. destruct H1.
  rewrite H1 in H. rewrite H2 in H. simpl in H. contradiction.
  rewrite shortest_merge_length_swap_l_r.
  rewrite length_app. rewrite Nat.add_comm. rewrite <- length_app.
  assumption.
Qed.


Theorem shortest_merge_common_subsequence :
  forall (u v w: list X),
  Subsequence u w -> Subsequence v w
    -> length (shortest_merge u v) <= length (u++v) - length w.
Proof.
  intro u. induction u; intros. rewrite shortest_merge_nil_l.
  destruct w. rewrite Nat.sub_0_r. apply Nat.le_refl.
  apply Subsequence_nil_cons_r in H. contradiction.

  induction v. rewrite shortest_merge_nil_r.
  destruct w. rewrite Nat.sub_0_r. rewrite app_nil_r. apply Nat.le_refl.
  apply Subsequence_nil_cons_r in H0. contradiction.

  simpl. destruct (eq_dec a a0). destruct w; simpl. apply le_n_S.
  apply Nat.le_trans with (m := length (u++v)).
  apply shortest_merge_length. repeat rewrite length_app. simpl.
  rewrite Nat.add_succ_r. apply Nat.le_succ_diag_r.
  apply Subsequence_double_cons in H. apply Subsequence_double_cons in H0.
  repeat rewrite length_app. simpl. rewrite Nat.add_succ_r.
  rewrite Nat.sub_succ_l. apply le_n_S. rewrite <- length_app.
  apply IHu; assumption. apply Subsequence_length in H.
  apply Arith_base.le_plus_trans_stt. assumption.

  assert ({ltb (length (a0::(shortest_merge (a::u) v)))
               (length (a::(shortest_merge u (a0::v)))) = true}
        + {ltb (length (a0::(shortest_merge (a::u) v)))
               (length (a::(shortest_merge u (a0::v)))) <> true}).
  apply Bool.bool_dec. destruct H1. simpl in e. rewrite e.
  destruct w. simpl. apply le_n_S.
  replace (length (u++a0::v)) with (length ((a::u)++v)).
  simpl in IHv. apply IHv. easy. repeat rewrite length_app. simpl.
  apply plus_n_Sm. simpl.
  destruct (eq_dec a0 x).
  apply Subsequence_cons_diff in H. apply IHu with (v := a0::v) in H.
  apply Nat.le_trans with (m := S (length (shortest_merge u (a0::v)))).
  apply Nat.lt_le_incl. apply Nat.ltb_lt. assumption.
  apply le_n_S in H. simpl in H. rewrite Nat.sub_succ_r in H.
  rewrite Nat.succ_pred_pos in H. assumption.
  rewrite length_app. rewrite <- Nat.add_sub_assoc.
  apply Nat.add_pos_r. apply Subsequence_length in H0. simpl in H0.
  apply le_S_n in H0. apply PeanoNat.le_lt_n_Sm in H0.
  apply Nat.add_lt_mono_r with (p := length w). rewrite Nat.sub_add.
  assumption. apply Nat.lt_le_incl. assumption.
  apply Subsequence_length in H0. simpl in H0.
  apply le_S_n in H0. simpl. apply le_S. assumption. assumption.
  rewrite <- e0. assumption.
  apply Subsequence_cons_diff in H0. assert (I := H0). apply IHv in H0.
  rewrite length_app. simpl. rewrite <- Nat.add_succ_comm.
  rewrite <- Nat.add_sub_assoc. rewrite Nat.add_succ_l. apply le_n_S.
  apply Nat.le_trans with (m := length ((a::u)++v) - length (x::w)).
  assumption. rewrite length_app. simpl. rewrite Nat.add_sub_assoc. reflexivity.
  apply Subsequence_cons_r in I. apply Subsequence_length in I. assumption.
  apply Subsequence_cons_r in I. apply Subsequence_length in I. assumption.
  assumption.

  apply Bool.not_true_is_false in n0. simpl in n0. rewrite n0.
  destruct w; simpl. apply le_n_S. specialize (IHu (a0::v) nil).
  rewrite Nat.sub_0_r in IHu. apply IHu; apply Subsequence_nil_r.
  destruct (eq_dec a0 x). apply Subsequence_cons_diff in H.
  apply IHu with (v := a0::v) in H. apply le_n_S in H.
  simpl in H. rewrite <- Nat.sub_succ_l in H.
  rewrite Nat.sub_succ in H. apply H. rewrite length_app.
  rewrite Nat.add_comm. apply Arith_base.le_plus_trans_stt.
  apply Subsequence_length in H0. simpl in H0. assumption. assumption.
  rewrite <- e. assumption. apply Nat.ltb_ge in n0.
  apply Nat.le_trans with (m := S (length (shortest_merge (a::u) v))).
  simpl. assumption.
  replace (length (u++a0::v) - length w)
     with (S (length ((a::u)++v) - length (x::w))). apply le_n_S. apply IHv.
  apply Subsequence_cons_diff in H0. assumption. assumption.
  repeat rewrite length_app. simpl. rewrite Nat.add_succ_r.
  rewrite Nat.sub_succ_l. reflexivity. apply Arith_base.le_plus_trans_stt.
  apply Subsequence_double_cons in H. apply Subsequence_length in H. assumption.
Qed.


Theorem shortest_merge_common_subsequence_2 :
  forall (u v: list X) (n: nat),
    n <= length u -> n <= length v
    -> length (shortest_merge u v) <= length (u++v) - n
       -> exists w, length w = n /\ Subsequence u w /\ Subsequence v w.
Proof.
  intro u. induction u. intros. exists nil.
  rewrite shortest_merge_nil_l in H1. destruct n.
  split. reflexivity. split; apply Subsequence_nil_r.
  apply Nat.nle_succ_0 in H. contradiction.
  intro v. induction v; intros. exists nil. apply Nat.le_0_r in H0.
  split. rewrite H0. reflexivity. split; apply Subsequence_nil_r.

  destruct (eq_dec a0 a). simpl in H1. destruct (eq_dec a a0).
  destruct n. exists nil. split. reflexivity.
  split; apply Subsequence_nil_r. simpl in H1.
  replace (length (u++a0::v) - n) with (S (length (u++v) - n)) in H1.
  apply le_S_n in H1. apply IHu in H1. destruct H1. destruct H1. destruct H2.
  exists (a::x). split. simpl. rewrite H1. reflexivity. rewrite e.
  split; apply Subsequence_cons_eq; assumption.
  apply le_S_n. assumption. apply le_S_n. assumption.
  repeat rewrite length_app. simpl. rewrite Nat.add_succ_r.
  rewrite Nat.sub_succ_l. reflexivity. apply Arith_base.le_plus_trans_stt.
  apply le_S_n. assumption. rewrite e in n0. contradiction.

  simpl in H1. destruct (eq_dec a a0). rewrite e in n0. contradiction.
  assert ({ltb (length (a0::(shortest_merge (a::u) v)))
               (length (a::(shortest_merge u (a0::v)))) = true}
        + {ltb (length (a0::(shortest_merge (a::u) v)))
               (length (a::(shortest_merge u (a0::v)))) <> true}).
  apply Bool.bool_dec. destruct H2. simpl in e. rewrite e in H1.
  destruct n. exists nil. split. reflexivity.
  split; apply Subsequence_nil_r. apply Nat.lt_eq_cases in H0. destruct H0.
  apply PeanoNat.lt_n_Sm_le in H0. simpl in H1.

  replace (length (u++a0::v) - n) with (S (length ((a::u)++v) - S n)) in H1.
  apply le_S_n in H1. apply IHv in H1. destruct H1. destruct H1. destruct H2.
  exists x. split. assumption. split. assumption.
  apply Subsequence_cons_l. assumption. assumption. assumption.

  repeat rewrite length_app. simpl. rewrite Nat.add_succ_r.
  rewrite Nat.sub_succ_l. reflexivity. apply Arith_base.le_plus_trans_stt.
  apply le_S_n. assumption.

  simpl in H0. inversion H0. rewrite H3 in H1.
  replace (length (u++a0::v) - length v) with (length (a::u)) in H1.

  apply Nat.le_antisymm in H1. symmetry in H1.
  apply Subsequence_length_2 in H1. inversion H1.
  assert (length (a::u) <= length (shortest_merge (a::u) v)).
  apply shortest_merge_length_l. simpl in H2. rewrite H5 in H2.
  apply Nat.nle_succ_diag_l in H2. contradiction.

  apply Subsequence_cons_l.
  assert (Subsequence (shortest_merge (a::u) v) (a::u)).
  apply shortest_merge_subsequence_l.
  apply Subsequence_id. simpl in H2. assumption.

  simpl. apply le_n_S.
  assert (length (a::u) <= length (shortest_merge (a::u) v)).
  apply shortest_merge_length_l. simpl in H2. rewrite Nat.le_succ_l in H2.
  apply Nat.lt_le_incl in H2. assumption.

  rewrite length_app. simpl. rewrite Nat.add_succ_r. rewrite Nat.sub_succ_l.
  rewrite Nat.add_sub. reflexivity.
  rewrite Nat.add_comm. apply Arith_base.le_plus_trans_stt.
  apply Nat.le_refl.

  apply Bool.not_true_is_false in n2. simpl in n2. rewrite n2 in H1.

  destruct n. exists nil. split. reflexivity.
  split; apply Subsequence_nil_r. apply Nat.lt_eq_cases in H. destruct H.
  apply PeanoNat.lt_n_Sm_le in H. simpl in H1.

  replace (length (u++a0::v) - n) with (S (length (u++a0::v) - S n)) in H1.
  apply le_S_n in H1. apply IHu in H1. destruct H1. destruct H1. destruct H2.
  exists x. split. assumption. split.
  apply Subsequence_cons_l. assumption. assumption. assumption. assumption.

  repeat rewrite length_app. simpl. rewrite Nat.add_succ_r.
  rewrite Nat.sub_succ. rewrite Nat.sub_succ_l. reflexivity.
  rewrite Nat.add_comm. apply Arith_base.le_plus_trans_stt.
  apply le_S_n. assumption.

  simpl in H. inversion H. rewrite H3 in H1.
  replace (length (u++a0::v) - length u) with (length (a0::v)) in H1.

  apply Nat.le_antisymm in H1. symmetry in H1.
  apply Subsequence_length_2 in H1. inversion H1.

  assert (length (a0::v) <= length (shortest_merge u (a0::v))).
  apply shortest_merge_length_r. simpl in H2. rewrite H5 in H2.
  apply Nat.nle_succ_diag_l in H2. contradiction.

  apply Subsequence_cons_l. apply shortest_merge_subsequence_r.
  apply Subsequence_id.

  simpl. apply le_n_S.
  assert (length (a0::v) <= length (shortest_merge u (a0::v))).
  apply shortest_merge_length_r. simpl in H2. rewrite Nat.le_succ_l in H2.
  apply Nat.lt_le_incl in H2. assumption.

  rewrite length_app. simpl. rewrite Nat.add_sub_swap.
  rewrite Nat.sub_diag. reflexivity. apply Nat.le_refl.
Qed.


Theorem shortest_merge_common_subsequence_3 :
  forall (u v: list X) (n: nat),
    u++v <> nil -> length (shortest_merge u v) <= length (u++v) - n
       -> exists w, length w = n /\ Subsequence u w /\ Subsequence v w.
Proof.
  intros.
  assert (H1 := H0). apply shortest_merge_length_sub_l in H1.
  assert (H2 := H0). apply shortest_merge_length_sub_r in H2.
  apply shortest_merge_common_subsequence_2 in H0; assumption.
  assumption. assumption.
Qed.


Theorem shortest_merge_subsequence_l_2 :
  forall (u v: list X), Subsequence u v -> shortest_merge u v = u.
Proof.
  intros.
  assert (length (shortest_merge u v) <= length (u++v) - length v).
  apply shortest_merge_common_subsequence. assumption. apply Subsequence_id.
  rewrite length_app in H0. rewrite Nat.add_sub in H0.
  apply Nat.le_antisymm with (n := length u) in H0. symmetry in H0.
  apply Subsequence_length_2 in H0. assumption.
  apply shortest_merge_subsequence_l. apply Subsequence_id.
  apply shortest_merge_length_l.
Qed.


Theorem shortest_merge_subsequence_r_2 :
  forall (u v: list X), Subsequence v u -> shortest_merge u v = v.
Proof.
  intros.
  assert (length (shortest_merge u v) <= length (u++v) - length u).
  apply shortest_merge_common_subsequence. apply Subsequence_id. assumption.
  rewrite length_app in H0. rewrite Nat.add_sub_swap in H0.
  rewrite Nat.sub_diag in H0.
  apply Nat.le_antisymm with (n := length v) in H0. symmetry in H0.
  apply Subsequence_length_2 in H0. assumption.
  apply shortest_merge_subsequence_r. apply Subsequence_id.
  apply shortest_merge_length_r. apply Nat.le_refl.
Qed.


Theorem shortest_merge_app_l :
  forall (l u v: list X),
    shortest_merge (l ++ u) (l ++ v) = l ++ shortest_merge u v.
Proof.
  intro l. induction l; intros. reflexivity.
  simpl. destruct (eq_dec a a). rewrite IHl. reflexivity. contradiction.
Qed.


Theorem shortest_merge_app_r :
  forall (u v l: list X),
  shortest_merge (u ++ l) (v ++ l) = (shortest_merge u v) ++ l.
Proof.
  intro u. induction u. intros. rewrite shortest_merge_nil_l.
  apply Subsequence_length_2. apply shortest_merge_subsequence_r.
  apply Subsequence_id. apply Nat.le_antisymm.
  replace (length (v++l)) with (length (l++v++l) - length l).
  apply shortest_merge_common_subsequence. apply Subsequence_id.
  apply Subsequence_app2_l. apply Subsequence_id. rewrite length_app.
  rewrite Nat.add_comm. rewrite Nat.add_sub. reflexivity.
  apply shortest_merge_length_r.

  intro v. induction v; intros. rewrite shortest_merge_nil_r.
  apply Subsequence_length_2. apply shortest_merge_subsequence_l.
  apply Subsequence_id. apply Nat.le_antisymm.
  replace (length ((a::u)++l)) with (length (((a::u)++l)++l) - length l).
  apply shortest_merge_common_subsequence.
  apply Subsequence_app2_l. apply Subsequence_id. apply Subsequence_id.
  rewrite length_app. rewrite Nat.add_sub. reflexivity.
  apply shortest_merge_length_l.

  simpl. destruct (eq_dec a a0). rewrite IHu. reflexivity.

  assert ({ltb (length (a0::(shortest_merge (a::u++l) (v++l))))
    (length (a::(shortest_merge (u++l) (a0::v++l)))) = true}
  + {ltb (length (a0::(shortest_merge (a::u++l) (v++l))))
    (length (a::(shortest_merge (u++l) (a0::v++l)))) <> true}).
  apply Bool.bool_dec. destruct H. simpl in e. rewrite e.

  assert ({ltb (length (a0::(shortest_merge (a::u) v)))
    (length (a::(shortest_merge u (a0::v)))) = true}
  + {ltb (length (a0::(shortest_merge (a::u) v)))
    (length (a::(shortest_merge u (a0::v)))) <> true}).
  apply Bool.bool_dec. destruct H. simpl in e0. rewrite e0.
  specialize (IHv l). simpl in IHv. rewrite IHv. reflexivity.

  apply Bool.not_true_is_false in n0. apply Nat.ltb_ge in n0.
  apply Nat.add_le_mono_r with (p := length l) in n0.
  repeat rewrite <- length_app in n0.
  repeat rewrite <- app_comm_cons in n0.
  replace (length (a :: shortest_merge u (a0 :: v) ++ l))
    with (S (length (shortest_merge u (a0 :: v) ++ l))) in n0.
  replace (length (a0 :: shortest_merge (a :: u) v ++ l))
    with  (S (length (shortest_merge (a :: u) v ++ l))) in n0.
  rewrite <- IHu in n0. rewrite <- IHv in n0.
  apply Nat.ltb_ge in n0. simpl in n0. rewrite e in n0. inversion n0.
  reflexivity. reflexivity.

  apply Bool.not_true_is_false in n0. simpl in n0. rewrite n0.

  assert ({ltb (length (a0::(shortest_merge (a::u) v)))
    (length (a::(shortest_merge u (a0::v)))) = true}
  + {ltb (length (a0::(shortest_merge (a::u) v)))
    (length (a::(shortest_merge u (a0::v)))) <> true}).
  apply Bool.bool_dec. destruct H.
  apply Nat.ltb_lt in e.
  apply Nat.add_lt_mono_r with (p := length l) in e.
  repeat rewrite <- length_app in e.
  repeat rewrite <- app_comm_cons in e.
  replace (length (a :: shortest_merge u (a0 :: v) ++ l))
    with (S (length (shortest_merge u (a0 :: v) ++ l))) in e.
  replace (length (a0 :: shortest_merge (a :: u) v ++ l))
    with  (S (length (shortest_merge (a :: u) v ++ l))) in e.
  rewrite <- IHu in e. rewrite <- IHv in e.
  apply Nat.ltb_lt in e. simpl in e. rewrite e in n0. inversion n0.
  reflexivity. reflexivity.

  apply Bool.not_true_is_false in n1. simpl in n1. rewrite n1.
  rewrite <- app_comm_cons. rewrite <- IHu. reflexivity.
Qed.


Theorem shortest_merge_length_common :
  forall (u v: list X),
    exists w, length w = length (u++v) - length (shortest_merge u v)
      /\ Subsequence u w /\ Subsequence v w.
Proof.
  intros. pose (n := length (u++v) - length (shortest_merge u v)).
  assert (length (shortest_merge u v) <= length (u++v) - n).
  unfold n. rewrite Nat.sub_sub_distr. rewrite Nat.sub_diag.
  apply Nat.le_refl. apply shortest_merge_length. apply Nat.le_refl.
  apply shortest_merge_common_subsequence_2 in H. destruct H. destruct H.
  destruct H0. fold n. exists x. split. assumption. split; assumption.
  unfold n. apply Nat.add_le_mono_r with (p := length (shortest_merge u v)).
  rewrite Nat.sub_add. rewrite length_app. apply Nat.add_le_mono_l.
  apply shortest_merge_length_r. apply shortest_merge_length.
  unfold n. apply Nat.add_le_mono_r with (p := length (shortest_merge u v)).
  rewrite Nat.sub_add. rewrite length_app. rewrite Nat.add_comm.
  apply Nat.add_le_mono_l.
  apply shortest_merge_length_l. apply shortest_merge_length.
Qed.


Theorem shortest_merge_full_subsequence :
  forall (l u v: list X), Subsequence l u -> Subsequence l v
    -> length (shortest_merge u v) <= length l.
Proof.
  intro l. induction l; intros. destruct u. destruct v.
  apply Nat.le_refl. apply Subsequence_nil_cons_r in H0. contradiction.
  apply Subsequence_nil_cons_r in H. contradiction.

  destruct u; destruct v. apply le_0_n.
  rewrite shortest_merge_nil_l. apply Subsequence_length. assumption.
  rewrite shortest_merge_nil_r. apply Subsequence_length. assumption.
  destruct (eq_dec x a); destruct (eq_dec x0 a).
  rewrite e. rewrite e0. simpl. destruct (eq_dec a a). apply le_n_S.
  apply Subsequence_double_cons in H. apply Subsequence_double_cons in H0.
  apply IHl; assumption. contradiction.
  apply Subsequence_double_cons in H. apply Subsequence_cons_diff in H0.
  apply IHl with (u := u) in H0.
  simpl.
  destruct (eq_dec x x0). rewrite e0 in e. apply n in e. contradiction.

  assert ({ltb (length (x0::(shortest_merge (x::u) v)))
    (length (x::(shortest_merge u (x0::v)))) = true}
  + {ltb (length (x0::(shortest_merge (x::u) v)))
    (length (x::(shortest_merge u (x0::v)))) <> true}).
  apply Bool.bool_dec. destruct H1. simpl in e0. rewrite e0.
  apply Nat.le_trans with (m := S (length (shortest_merge u (x0::v)))).
  apply Nat.lt_le_incl. apply Nat.ltb_lt in e0. assumption.
  apply le_n_S. assumption.
  apply Bool.not_true_is_false in n1. simpl in n1. rewrite n1.
  apply le_n_S. assumption. assumption. intro. rewrite H1 in n. contradiction.

  simpl.

  assert ({ltb (length (x0::(shortest_merge (x::u) v)))
    (length (x::(shortest_merge u (x0::v)))) = true}
  + {ltb (length (x0::(shortest_merge (x::u) v)))
    (length (x::(shortest_merge u (x0::v)))) <> true}).
  apply Bool.bool_dec. destruct H1. simpl in e0. rewrite e0.

  destruct (eq_dec x x0). rewrite e in e1. apply n in e1. contradiction.
  apply le_n_S. specialize (IHl (x::u) v). simpl in IHl. apply IHl.
  apply Subsequence_cons_diff in H. assumption. intro. rewrite H1 in n.
  contradiction. apply Subsequence_double_cons in H0. assumption.

  destruct (eq_dec x x0). rewrite e in e0. apply n in e0. contradiction.

  apply Bool.not_true_is_false in n0. simpl in n0. rewrite n0.
  apply Nat.ltb_ge in n0.
  apply Nat.le_trans with (m := S (length (shortest_merge (x::u) v))).
  assumption. apply le_n_S. apply IHl.
  apply Subsequence_cons_diff in H. assumption. intro. rewrite H1 in n.
  contradiction. apply Subsequence_double_cons in H0. assumption.

  apply Subsequence_cons_diff in H. apply Subsequence_cons_diff in H0.
  apply IHl with (u := x::u) in H0.
  apply Nat.le_trans with (m := length l). assumption.
  apply Nat.le_succ_diag_r. assumption.
  intro. rewrite H1 in n0. contradiction.
  intro. rewrite H1 in n. contradiction.
Qed.


Theorem shortest_merge_length_rev :
  forall (u v: list X),
    length (shortest_merge (rev u) (rev v)) = length (shortest_merge u v).
Proof.
  intros. apply Nat.le_antisymm.
  replace (length (shortest_merge u v))
       with (length (rev (shortest_merge u v))).
  apply shortest_merge_full_subsequence. rewrite <- Subsequence_rev.
  apply shortest_merge_subsequence_l. apply Subsequence_id.
  rewrite <- Subsequence_rev. apply shortest_merge_subsequence_r.
  apply Subsequence_id. apply length_rev.
  replace (length (shortest_merge (rev u) (rev v)))
       with (length (rev (shortest_merge (rev u) (rev v)))).
  apply shortest_merge_full_subsequence.
  rewrite Subsequence_rev. rewrite rev_involutive.
  apply shortest_merge_subsequence_l. apply Subsequence_id.
  rewrite Subsequence_rev. rewrite rev_involutive.
  apply shortest_merge_subsequence_r. apply Subsequence_id. apply length_rev.
Qed.


Lemma shortest_merge_single_r :
  forall (u : list X) (x: X),
    shortest_merge u [x] = if (in_dec eq_dec x u) then u else u++[x].
Proof.
  intro u. induction u; intro. rewrite shortest_merge_nil_l.
  destruct (in_dec eq_dec x nil). apply in_nil in i. contradiction.
  reflexivity.
  simpl. destruct (eq_dec a x). rewrite shortest_merge_nil_r. reflexivity.
  assert ( {S (S (length u)) <? S (length (shortest_merge u [x])) = true}
         + {S (S (length u)) <? S (length (shortest_merge u [x])) <> true}).
  apply Bool.bool_dec. destruct H. rewrite e. apply Nat.ltb_lt in e.
  apply Nat.succ_lt_mono in e.
  assert (S (length u) < S (length u)).
  apply Nat.lt_le_trans with (m := length (shortest_merge u [x])).
  assumption. replace (S (length u)) with (length (u ++ [x])).
  apply shortest_merge_length. rewrite length_app. rewrite Nat.add_1_r.
  reflexivity. apply Nat.lt_irrefl in H. contradiction.
  apply Bool.not_true_is_false in n0. rewrite n0. rewrite IHu.
  destruct (in_dec eq_dec x u); reflexivity.
Qed.


Lemma shortest_merge_single_l :
  forall (u : list X) (x: X),
    shortest_merge [x] u = if (in_dec eq_dec x u) then u else [x]++u.
Proof.
  intro u. induction u; intro. rewrite shortest_merge_nil_r.
  destruct (in_dec eq_dec x nil). apply in_nil in i. contradiction.
  rewrite app_nil_r. reflexivity.
  assert (L: forall w, (fix sm_aux (v: list X) : list X := v) w = w).
  intro w. induction w; reflexivity.
  destruct (in_dec eq_dec x (a::u)). destruct i. rewrite H. simpl.
  destruct (eq_dec x x). rewrite L. reflexivity. contradiction.
  simpl. destruct (eq_dec x a). rewrite e. rewrite L. reflexivity.
  specialize (IHu x). simpl in IHu. rewrite IHu.
  destruct (in_dec eq_dec x u).
  assert ( {S (length u) <? S (S (length u)) = true}
         + {S (length u) <? S (S (length u)) <> true}).
  apply Bool.bool_dec. destruct H0. rewrite e. reflexivity.
  apply Bool.not_true_is_false in n0. apply Nat.ltb_ge in n0.
  apply Nat.nle_succ_diag_l in n0. contradiction.
  apply n0 in H. contradiction.
  simpl. destruct (eq_dec x a). rewrite e in n.
  assert False. apply n. apply in_eq. contradiction.
  specialize (IHu x). simpl in IHu. rewrite IHu.
  destruct (in_dec eq_dec x u).
  assert False. apply n. apply in_cons. apply i. contradiction.
  assert ( {S (length (x::u)) <? S (S (length u)) = true}
         + {S (length (x::u)) <? S (S (length u)) <> true}).
  apply Bool.bool_dec. destruct H. apply Nat.ltb_lt in e.
  simpl in e. apply Nat.lt_irrefl in e. contradiction.
  apply Bool.not_true_is_false in n2. rewrite n2. reflexivity.
Qed.


Theorem shortest_merge_assoc :
  forall (u v w: list X),
    shortest_merge u (shortest_merge v w)
      = shortest_merge (shortest_merge u v) w.
Proof.
  intro u. induction u. intros.
  repeat rewrite shortest_merge_nil_l. reflexivity.
  intro v. induction v. intros.
  rewrite shortest_merge_nil_l. rewrite shortest_merge_nil_r.
  reflexivity.
  intro w. induction w.
  repeat rewrite shortest_merge_nil_r. reflexivity.
  simpl.
  destruct (eq_dec a0 a1). destruct (eq_dec a a0).
  rewrite IHu.
  cbn.

  simpl. destruct (eq_dec a a1). reflexivity.
  rewrite e in e0. apply n in e0. contradiction.

  fold (shortest_merge (a::u) (shortest_merge v w)).

  assert ({ltb (length (a0::(shortest_merge (a::u) (shortest_merge v w))))
    (length (a::(shortest_merge u (a0::(shortest_merge v w))))) = true}
  + {ltb (length (a0::(shortest_merge (a::u) (shortest_merge v w))))
    (length (a::(shortest_merge u (a0::(shortest_merge v w))))) <> true}).
  apply Bool.bool_dec. destruct H. simpl in e0. rewrite e0.

  assert ({ltb (length (a0::(shortest_merge (a::u) v)))
    (length (a::(shortest_merge u (a0::v)))) = true}
  + {ltb (length (a0::(shortest_merge (a::u) v)))
    (length (a::(shortest_merge u (a0::v)))) <> true}).
  apply Bool.bool_dec. destruct H. simpl in e1. rewrite e1.

  simpl. destruct (eq_dec a0 a1).
  specialize (IHv w). simpl in IHv. rewrite IHv. reflexivity.
  apply n0 in e. contradiction.

  apply Bool.not_true_is_false in n0. simpl in n0. rewrite n0.
  rewrite <- e. simpl. destruct (eq_dec a a0). apply n in e1. contradiction.

  specialize (IHu (a0::v) (a0::w)). simpl in IHu. rewrite <- IHu.
  destruct (eq_dec a0 a0).


a0 :: (shortest_merge (a::u) (shortest_merge v w)) =
(if
  S (length (shortest_merge (a::shortest_merge u (a0::v)) w))
    <? S (length (shortest_merge (shortest_merge u (a0 :: v)) (a0 :: w)))
 then
  a0 :: (shortest_merge (a::shortest_merge u (a0::v)) w)
 else a :: shortest_merge (shortest_merge u (a0 :: v)) (a0 :: w))




a0 :: (shortest_merge (a::u) (shortest_merge v w)) =
(if
  S (length (shortest_merge (a::shortest_merge u (a0::v)) w))
 <? S (length (shortest_merge u (a0 :: shortest_merge v w)))
 then
  a0 :: (shortest_merge (a::shortest_merge u (a0::v)) w)
 else a :: shortest_merge u (a0 :: shortest_merge v w))









  simpl. destruct (eq_dec a a1).
  rewrite e1 in n. rewrite e in n. contradiction.

  specialize (IHv w). simpl in IHv. rewrite IHv. simpl.
  destruct (eq_dec a a1). rewrite e1 in n. rewrite e in n. contradiction.

  assert ({ltb (length (a1::(shortest_merge (a::(shortest_merge u (a0::v))) w)))
         (length (a::(shortest_merge (shortest_merge u (a0::v)) (a1::w))))
         = true}
      + {ltb (length (a1::(shortest_merge (a::(shortest_merge u (a0::v))) w)))
         (length (a::(shortest_merge (shortest_merge u (a0::v)) (a1::w))))
         <> true}).
  apply Bool.bool_dec. destruct H. simpl in e1. rewrite e1.



a0
:: shortest_merge
     (shortest_merge (a::u) v) w =
a1
:: shortest_merge (a::(shortest_merge u (a0::v))) w




IHw :
  shortest_merge (a :: u) (shortest_merge (a0 :: v) w) =
  shortest_merge (shortest_merge (a :: u) (a0 :: v)) w










Theorem shortest_merge_rev :
  forall (u v: list X),
    rev (shortest_merge u v) = shortest_merge (rev v) (rev u).
Proof.
  intro u. induction u. intros.
  rewrite shortest_merge_nil_l. rewrite shortest_merge_nil_r. reflexivity.
  intro v. induction v.
  rewrite shortest_merge_nil_l. rewrite shortest_merge_nil_r. reflexivity.
  simpl. destruct (eq_dec a a0). rewrite e. simpl. rewrite IHu.
  symmetry. apply shortest_merge_app_r.

  assert ({ltb (length (a0::(shortest_merge (a::u) v)))
    (length (a::(shortest_merge u (a0::v)))) = true}
  + {ltb (length (a0::(shortest_merge (a::u) v)))
    (length (a::(shortest_merge u (a0::v)))) <> true}).
  apply Bool.bool_dec. destruct H.
  simpl in e. rewrite e. simpl in IHv. simpl. rewrite IHv.






  admit.
  apply Bool.not_true_is_false in n0. simpl in n0. rewrite n0.
  simpl. rewrite IHu. simpl.






  apply Nat.ltb_lt in e.
  replace (length (a0 :: shortest_merge (a :: u) v))
    with (S (length (rev (shortest_merge (a :: u) v)))) in e.
  replace (length (a :: shortest_merge u (a0 :: v)))
    with (S (length (rev (shortest_merge u (a0 :: v))))) in e.
  apply Nat.succ_lt_mono in e. rewrite IHv in e. rewrite IHu in e.




  simpl in e. rewrite e.
  specialize (IHv l). simpl in IHv. rewrite IHv. reflexivity.



e :
  length (shortest_merge (a::u) v) < length (shortest_merge u (a0 :: v))



rev
  (if
    S (length (shortest_merge (a::u) v))
      <? S (length (shortest_merge u (a0 :: v)))
   then
    a0
    :: (fix sm_aux (v0 : list X) : list X :=
          match v0 with
          | [] => a :: u
          | hd' :: tl' =>
              if eq_dec a hd'
              then a :: shortest_merge u tl'
              else
               if S (length (sm_aux tl')) <? S (length (shortest_merge u v0))
               then hd' :: sm_aux tl'
               else a :: shortest_merge u v0
          end) v
   else a :: shortest_merge u (a0 :: v)) =
shortest_merge (rev v ++ [a0]) (rev u ++ [a])









  apply IHl.
  apply Subsequence_double_cons in H. assumption.





  apply Nat.ltb_lt in e0.


  apply Nat.le_trans with (m := S (length (shortest_merge u (x0::v)))).
  apply Nat.lt_le_incl. apply Nat.ltb_lt in e0. assumption.
  apply le_n_S. assumption.
  apply Bool.not_true_is_false in n1. simpl in n1. rewrite n1.
  apply le_n_S. assumption. assumption. intro. rewrite H1 in n. contradiction.



  specialize (IHv l). simpl in IHv. rewrite IHv. reflexivity.

  apply Bool.not_true_is_false in n0. apply Nat.ltb_ge in n0.




shortest_merge_common_subsequence:
  forall u v w : list X,
  Subsequence u w ->
  Subsequence v w ->
  length (shortest_merge u v) <= length (u ++ v) - length w



  intros. specialize (shortest_merge_length_common u v). intro.
  destruct H1. destruct H1. destruct H2.
  rewrite <- Nat.add_cancel_r with (p := length (shortest_merge u v)) in H1.
  rewrite Nat.sub_add in H1 by apply shortest_merge_length.
  apply Nat.add_le_mono_l with (p := length x). rewrite H1.





  (* prendre les masques booléens de Subsequence l u et de Subsequence l v *)
  apply Subsequence_bools in H. destruct H. destruct H.
  apply Subsequence_bools in H0. destruct H0. destruct H0.
  (* partie utile *)
  pose (w := map (fun e => orb (fst e) (snd e)) (combine x x0)).
  (* longueur de la partie commune *)
  pose (n := length (filter (fun e => andb (fst e) (snd e)) (combine x x0))).
















  intro u. induction u. intros. rewrite shortest_merge_nil_l.
  apply Subsequence_length. apply H0. apply Subsequence_id.
  intro v. induction v; intros. rewrite shortest_merge_nil_r.
  apply Subsequence_length. apply H. apply Subsequence_id.

  simpl. destruct (eq_dec a a0). destruct l. assert (Subsequence nil [a]).
  apply H. apply Subsequence_In. apply in_eq.
  apply Subsequence_nil_cons_r in H1. contradiction. simpl. apply le_n_S.
  apply IHu. intros. apply Subsequence_double_cons with (a := x) (b := a).
  apply H. apply Subsequence_cons_eq. assumption.
  intros. apply Subsequence_double_cons with (a := x) (b := a0).
  apply H0. apply Subsequence_cons_eq. assumption.

  assert ({ltb (length (a0::(shortest_merge (a::u) v)))
               (length (a::(shortest_merge u (a0::v)))) = true}
        + {ltb (length (a0::(shortest_merge (a::u) v)))
               (length (a::(shortest_merge u (a0::v)))) <> true}).
  apply Bool.bool_dec. destruct H1. simpl in e. rewrite e.

  (* length (a0 :: shortest_merge (a::u) v) <= length l *)
  destruct l. assert (Subsequence nil [a]).
  apply H. apply Subsequence_In. apply in_eq.
  apply Subsequence_nil_cons_r in H1. contradiction.

       (* length (a0 :: shortest_merge (a::u) v) <= length (x::l) *)
  simpl. apply le_n_S.






  destruct (eq_dec x a).
  assert (forall x, Subsequence u x -> Subsequence l x). intros.
  apply Subsequence_cons_eq with (a := a). rewrite e0 in H. apply H.
  apply Subsequence_cons_eq. assumption.

       (* length (shortest_merge (a::u) v) <= length l *)
  apply IHv. intros.

  (* prouver que x est différent soit de a soit de a0 *)
  destruct (eq_dec a x); destruct (eq_dec a0 x).
  rewrite e2 in n. rewrite e1 in n. contradiction.

  apply Subsequence_double_cons with (a := x) (b := x).
  apply H. rewrite e0. apply Subsequence_cons_eq.



  (* length (a :: shortest_merge u (a0::v)) <= length l *)
  apply Bool.not_true_is_false in n0. simpl in n0. rewrite n0.






  (* à ce stade, si l ne commence pas par a ou a0, il est inutilement long *)
  destruct l. assert (Subsequence nil [a]). apply H. apply Subsequence_In.
  apply in_eq. apply Subsequence_nil_cons_r in H1. contradiction.
  destruct (eq_dec x a). rewrite e in H.



















Admitted.


(* TODO:    shortest_merge u v = rev (shortest_merge (rev v) (rev u)) *)


  (* TODO:
     théorèmes du genre : pour toute séquence l, il existe
       deux séquence u et v telles que shortest_merge u v = l, etc.
   *)


(* TODO: valeur exacte de length (shortest_merge u v) *)
(* TODO: décidabilité de la longueur *)


End Notebook_dec.
