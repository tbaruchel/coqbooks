(** * Notebook 5
 *)


Require Import Nat.
Require Import PeanoNat.
Require Import List.

Import ListNotations.

Require Import Sorting.Permutation.
Require Import Arith.Factorial.

Require Import subsequence.


Set Implicit Arguments.

Section Notebook_dec.

Variable X: Type.
Hypothesis eq_dec : forall x y: X, {x = y} + {x <> y}.

(** ** Introduction

Soit <<A>> un alphabet (un ensemble de symboles), et <<A*>> l'ensemble des mots sur cet alphabet. Soient <<w₁>> et <<w₂>> deux mots de <<A*>>, on note <<w₂|w₁>> la relation indiquant que <<w₂>> est une sous-suite (suite extraite) de <<w₁>> ; on pourra ainsi noter nor|bonjour.

Il existe une famille de lois de compositions internes, notée ici pour chacune fois <<⊙>>, vérifiant les propriétés fondamentales suivantes :

   - <<w₁|(w₁⊙w₂)>>
   - <<w₂|(w₁⊙w₂)>>
   - <<∀ u ∈ A*, w₁|u ∧ w₂|u → |w₁⊙w₂| ≤ |u|>>

Autrement dit, <<⊙>> est (à chaque fois) une loi de composition interne qui produit un mot (au moins aussi long que <<w₁>> et que <<w₂>>) dont <<w₁>> et <<w₂>> sont des sous-suites, tel que <<w₁⊙w₂>> soit de longueur minimale.

Ces lois de compositions réalisent une sorte de fusion (dans le sens de la fusion de deux listes lors du tri fusion), puisqu'elles combinent, de façon plus ou moins alternée, les éléments de deux listes sans modifier l'ordre relatif des éléments de chacun des deux mots initiaux. Mais, à la différence du tri fusion, les éléments identiques sont « mutualisés », et un critère de longueur optimale régit le choix de faire passer un élément de gauche ou de droite.

Les trois propriétés énoncées ci-dessus ne garantissent pas l'unicité du résultat produit : non seulement les éléments à mutualiser peuvent être choisis différemment dans certains cas, mais le critère permettant de faire passer un élément de gauche avant un élément de droite (ou le contraire) parmi les éléments non mutualisés reste flottant.

Le calcul de <<abcd⊙dcba>> peut ainsi donner, entre autres, <<abcdcba>> ou <<dcbabcd>> qui sont de même longueur. Plusieurs contraintes supplémentaires destinées à préciser le calcul peuvent être envisagées, notamment :

   - l'associativité
   - la commutativité

(étant entendu que les propriétés ci-dessus impliquent par ailleurs d'emblée l'existence d'un élément neutre).

L'une et l'autre sont assez faciles à obtenir, notamment s'il existe un ordre lexicographique dans <<A*>> permettant de choisir un mot de préférence à un autre.

Mais la commutativité semble alourdir inutilement le code. On s'en tient donc pour la suite de ce travail à une loi de composition interne associative, qui privilégie les éléments de gauche quand cela est possible. On obtient finalement le code suivant :

 *)

Fixpoint shortest_merge (u: list X) :=
  fix sm_aux (v : list X) :=
    match u with
    | nil    => v
    | hd::tl => match v with
              | nil      => u
                | hd'::tl' => if (eq_dec hd hd')
                           then hd :: (shortest_merge tl tl')
                           else let m1 := hd :: (shortest_merge tl v) in
                                let m2 := hd' :: (sm_aux tl') in
                            if (ltb (length m2) (length m1)) then m2 else m1
              end
  end.

(**
(À partir de maintenant, la notation <<⊙>> désignera exclusivement l'application de <<A*×A*>> vers <<A*>> définie ci-dessus.)
*)


(** ** Existence d'un élément neutre

Le mot vide constitue naturellement un élément neutre :
 *)

Theorem shortest_merge_neutral_l :
  forall (l: list X), shortest_merge nil l = l.
Proof.
  intro. destruct l; reflexivity.
Qed.


Theorem shortest_merge_neutral_r :
  forall (l: list X), shortest_merge l nil = l.
Proof.
  intro. destruct l; reflexivity.
Qed.

(** ** Vérification des trois propriétés fondamentales

Les trois propriétés fondamentales énoncées initialement sont respectées par la fonction <<shortest_merge>> :
 *)


Theorem shortest_merge_subsequence_l :
  forall (u v: list X), Subsequence (shortest_merge u v) u.
Proof.
  intro u. induction u; intro. apply Subsequence_nil_r.
  induction v. rewrite shortest_merge_neutral_r. apply Subsequence_id.
  simpl. destruct (eq_dec a a0).
  exists nil. exists (shortest_merge u v). split. reflexivity. apply IHu.
  destruct IHv. do 2 destruct H. simpl in H. rewrite H.
  destruct (S (length (x ++ a :: x0))
         <? S (length (shortest_merge u (a0 :: v)))).
  exists (a0::x). exists x0. split. reflexivity. assumption.
  exists nil. exists (shortest_merge u (a0::v)). split. reflexivity. apply IHu.
Qed.


Theorem shortest_merge_subsequence_r :
  forall (u v: list X), Subsequence (shortest_merge u v) v.
Proof.
  intros. generalize u. induction v; intro. apply Subsequence_nil_r.
  induction u0. rewrite shortest_merge_neutral_l. apply Subsequence_id.
  simpl. destruct (eq_dec a0 a). rewrite e.
  exists nil. exists (shortest_merge u0 v). split. reflexivity. apply IHv.
  destruct (Compare_dec.lt_dec (S (length (shortest_merge (a0::u0) v)))
         (S (length (shortest_merge u0 (a::v))))).
  apply Nat.ltb_lt in l. simpl in l. rewrite l.
  exists nil. exists (shortest_merge (a0::u0) v). split. simpl. reflexivity.
  apply IHv.
  apply Nat.ltb_nlt in n0. simpl in n0. rewrite n0.
  destruct IHu0. do 2 destruct H. rewrite H.
  exists (a0::x). exists x0. split. reflexivity. assumption.
Qed.


Theorem shortest_merge_full_subsequence :
  forall (l u v: list X), Subsequence l u -> Subsequence l v
    -> length (shortest_merge u v) <= length l.
Proof.
  intro l. induction l; intros. destruct u. destruct v.
  apply Nat.le_refl. apply Subsequence_nil_cons_r in H0. contradiction.
  apply Subsequence_nil_cons_r in H. contradiction.

  destruct u; destruct v. apply le_0_n.
  rewrite shortest_merge_neutral_l. apply Subsequence_length. assumption.
  rewrite shortest_merge_neutral_r. apply Subsequence_length. assumption.
  destruct (eq_dec x a); destruct (eq_dec x0 a).
  rewrite e. rewrite e0. simpl. destruct (eq_dec a a). apply le_n_S.
  apply Subsequence_double_cons in H. apply Subsequence_double_cons in H0.
  apply IHl; assumption. contradiction.
  apply Subsequence_double_cons in H. apply Subsequence_cons_diff in H0.
  apply IHl with (u := u) in H0.
  simpl.
  destruct (eq_dec x x0). rewrite e0 in e. apply n in e. contradiction.

  destruct (Compare_dec.lt_dec (length (x0:: (shortest_merge (x::u) v)))
         (length (x::(shortest_merge u (x0::v))))).
  apply Nat.ltb_lt in l0. simpl in l0. rewrite l0.
  apply Nat.le_trans with (m := S (length (shortest_merge u (x0::v)))).
  apply Nat.lt_le_incl. apply Nat.ltb_lt in l0. assumption.
  apply le_n_S. assumption.
  apply Nat.ltb_nlt in n1. simpl in n1. rewrite n1.
  apply le_n_S. assumption. assumption. intro. rewrite H1 in n. contradiction.

  simpl.

  destruct (Compare_dec.lt_dec (length (x0:: (shortest_merge (x::u) v)))
         (length (x::(shortest_merge u (x0::v))))).
  apply Nat.ltb_lt in l0. simpl in l0. rewrite l0.

  destruct (eq_dec x x0). rewrite e in e0. apply n in e0. contradiction.
  apply le_n_S. specialize (IHl (x::u) v). simpl in IHl. apply IHl.
  apply Subsequence_cons_diff in H. assumption. intro. rewrite H1 in n.
  contradiction. apply Subsequence_double_cons in H0. assumption.

  destruct (eq_dec x x0). rewrite e in e0. apply n in e0. contradiction.

  apply Nat.ltb_nlt in n0. simpl in n0. rewrite n0.
  apply Nat.ltb_ge in n0.
  apply Nat.le_trans with (m := S (length (shortest_merge (x::u) v))).
  assumption. apply le_n_S. apply IHl.
  apply Subsequence_cons_diff in H. assumption. intro. rewrite H1 in n.
  contradiction. apply Subsequence_double_cons in H0. assumption.

  apply Subsequence_cons_diff in H. apply Subsequence_cons_diff in H0.
  apply IHl with (u := x::u) in H0.
  apply Nat.le_trans with (m := length l). assumption.
  apply Nat.le_succ_diag_r. assumption.
  intro. rewrite H1 in n0. contradiction.
  intro. rewrite H1 in n. contradiction.
Qed.











(* équivalent de perm4 dans le fichier permutations.lisp *)
Fixpoint all_permutations1 (b: list X) : list (list X) :=
    match b with
    | nil => [nil]
    | hd::tl => flat_map (fun e => map
        (fun x => (firstn e x) ++ hd::(skipn e x)) (all_permutations tl))
        (seq 0 ((length b)))
    end.

(* variante du précédent, même réduction EN LONGUEUR apparemment (conjecture *)
(* équivalent de perm3 dans le fichier permutations.lisp *)
Fixpoint all_permutations1_var (b: list X) : list (list X) :=
  match b with
  | nil => [nil]
  | hd::tl => flat_map (fun x => map
        (fun e => (firstn e x) ++ hd::(skipn e x)) (seq 0 ((length b))))
        (all_permutations_var tl)
  end.



(*
Require Import Program.Wf.
Require Import Coq.Wellfounded.Inverse_Image.
Require Import Coq.Arith.Wf_nat.
Inductive tuple_lt : (nat * nat) -> (nat * nat) -> Prop :=
  | fst_lt : forall a b c d, a < c -> tuple_lt (a, b) (c, d)
  | snd_lt : forall a b c d, a = c -> b < d -> tuple_lt (a, b) (c, d).

Theorem tuple_lt_wf : well_founded tuple_lt.
Proof.
  intros (a,b).
  induction a as [ a IHa ] in b |- * using (well_founded_induction lt_wf).
  induction b as [ b IHb ] using (well_founded_induction lt_wf).
  constructor.
  inversion 1; subst.
  + now apply IHa.
  + now apply IHb.
Qed.

Program Fixpoint all_permutations2bis (lst remain: list X)
  {measure (length lst, length remain) (tuple_lt)} :=
    match remain with
    | nil   => nil
    | _::x  => match lst with
               | nil => nil
               | hd::nil => [[hd]]
               | hd::tl => (map (cons hd) (all_permutations2bis tl tl))
                   ++ (all_permutations2bis (tl++[hd]) x)
               end
    end.
Next Obligation.
  apply fst_lt. apply Nat.lt_succ_diag_r.
Qed.
Next Obligation.
  apply snd_lt. rewrite length_app. rewrite Nat.add_1_r. reflexivity.
  apply Nat.lt_succ_diag_r.
Qed.
Next Obligation.
  generalize tuple_lt_wf. apply wf_inverse_image.
Qed.


Theorem xxx :
  forall (p: list X),
    NoDup p ->
      length (fold_left shortest_merge (all_permutations2bis p p) nil)
             = 2 ^ (length p) - 1.
Proof.
  intro. induction p; intros. simpl.
 *)

(* Attendu :

(all-permutations '(1 2 3 4))
((1 2 3 4) (1 2 4 3) (1 3 4 2) (1 3 2 4) (1 4 2 3) (1 4 3 2) (2 3 4 1)
 (2 3 1 4) (2 4 1 3) (2 4 3 1) (2 1 3 4) (2 1 4 3) (3 4 1 2) (3 4 2 1)
 (3 1 2 4) (3 1 4 2) (3 2 4 1) (3 2 1 4) (4 1 2 3) (4 1 3 2) (4 2 3 1)
 (4 2 1 3) (4 3 1 2) (4 3 2 1))

 *)




From Equations Require Import Equations.


Equations all_permutations2 (lst remain: list X) :
    (list (list X)) by wf (length lst, length remain) (lexprod _ _ lt lt) :=
all_permutations2 _ nil := nil;
all_permutations2 nil _ := nil;
all_permutations2 (hd1::nil) (_::x1) := [[hd1]];
all_permutations2 (hd2::tl) (_::x2) :=
               (map (cons hd2) (all_permutations2 tl tl))
                   ++ (all_permutations2 (tl++[hd2]) x2).
Next Obligation.
  rewrite length_app. rewrite Nat.add_1_r.
  apply right_lex. apply Nat.lt_succ_diag_r.
Qed.



(*

Theorem xxx :
  forall (p: list X),
    NoDup p ->
      length (fold_left shortest_merge (all_permutations2 p p) nil)
             = 2 ^ (length p) - 1.
Proof.
  intro. induction p; intros. funelim (all_permutations2 nil nil). reflexivity.
  funelim (all_permutations2 (a::p) (a::p)). reflexivity.
  rewrite Heqcall.



 *)


(* TODO:
    implémenter la bijection entre les permutations et {0, 1, 2, ...}
   puis voir si certaines suites d'entiers donnent des réductions
   intéressantes *)


(* conjectures :
   - réduction de all_permutations1 et de all_permutations1_var identiques
     (en longueur)
   - réduction de all-permutations (du fichier Lisp)
     a comme longueur 2^k-1
   - aucune réduction ne donne une longueur supérieur à 2^k-1
   - il existe une réduction qui donne la longueur optimale
   - la réduction par perm3 (du fichier lisp) est palindromique
      FAUX : semble vrai pour k <= 4 mais faux au-delà
   - la réduction de all-permutations (du fichier Lisp) ne contient
     que deux occurrences de 1, soit en position 0, soit en position 2^(k-1)
; voici les listes produites par la réduction de all-permutations
; (1 2 1)
; (1 2 3 2 1 3 2)
; (1 2 3 4 3 2 4 3 1 4 3 4 2 4 3)
; (1 2 3 4 5 4 3 5 4 2 5 4 5 3 5 4 1 5 4 5 3 5 4 5 2 5 4 5 3 5 4)
; (1 2 3 4 5 6 5 4 6 5 3 6 5 6 4 6 5 2 6 5 6 4 6 5 6 3 6 5 6 4 6 5 1 6 5
;  6 4 6 5 6 3 6 5 6 4 6 5 6 2 6 5 6 4 6 5 6 3 6 5 6 4 6 5)

*)


End Notebook_dec.
