Require Import Sorting.Permutation.

Require Import Nat.
Require Import PeanoNat.
Require Import List.


Import ListNotations.


Theorem combine_app {X: Type} {Y: Type} :
  forall (x x': list X) (y y': list Y),
    length x = length y ->
    combine (y ++ y') (x ++ x') = (combine y x) ++ (combine y' x').
Proof.
  intro y. induction y; intros.
  symmetry in H. apply length_zero_iff_nil in H. rewrite H. easy.
  destruct y0.
  apply length_zero_iff_nil in H. inversion H. simpl. rewrite IHy.
  reflexivity. inversion H. reflexivity.
Qed.


Theorem skipn_repeat {X: Type} :
  forall n k (x: X), skipn n (repeat x k) = repeat x (k - n).
Proof.
  intros. assert (n <= k \/ k < n). apply Nat.le_gt_cases. destruct H.
  replace k with (n+(k-n)) at 1. rewrite repeat_app.
  rewrite skipn_app. rewrite skipn_all2. rewrite repeat_length.
  rewrite Nat.sub_diag. reflexivity. rewrite repeat_length.
  apply Nat.le_refl. apply Arith_base.le_plus_minus_r_stt. assumption.
  apply Nat.lt_le_incl in H. rewrite skipn_all2. rewrite <- Nat.sub_0_le in H.
  rewrite H. reflexivity. rewrite repeat_length. assumption.
Qed.


Theorem app_eq_prefix {X: Type} :
  forall x1 x2 y1 y2: list X,
    length x1 = length y1 -> x1 ++ x2 = y1 ++ y2 -> x1 = y1.
Proof.
  intro x1. induction x1; intros.
  symmetry in H. apply length_zero_iff_nil in H. rewrite H. reflexivity.
  destruct y1. inversion H. inversion H0. f_equal.
  apply IHx1 with (x2 := x2) (y2 := y2). inversion H. reflexivity.
  assumption.
Qed.








Set Implicit Arguments.

Section Utilities_dec.

Variable X: Type.
Hypothesis eq_dec : forall x y: X, {x=y} + {x <> y}.

Lemma nodup_cons :
  forall (u: list X) (x: X), nodup eq_dec (u++[x])
    = (remove eq_dec x (nodup eq_dec u)) ++ [x].
Proof.
  intro u. induction u; intro. reflexivity.
  simpl. destruct (in_dec eq_dec a u).
  destruct (in_dec eq_dec a (u++[x])). apply IHu.
  assert (In a (u++[x])). apply in_or_app. left. assumption.
  apply n in H. contradiction.
  destruct (eq_dec a x).
  assert (In a (u++[x])). apply in_or_app. right. rewrite e. apply in_eq.
  destruct (in_dec eq_dec a (u++[x])). rewrite e.  rewrite remove_cons.
  apply IHu. apply n0 in H. contradiction.
  destruct (in_dec eq_dec a (u++[x])). apply in_app_or in i. destruct i.
  apply n in H. contradiction. inversion H. rewrite H0 in n0. contradiction.
  apply in_nil in H0. contradiction. rewrite IHu.
  simpl. destruct (eq_dec x a). rewrite e in n0. contradiction. reflexivity.
Qed. 


Lemma rev_remove :
  forall (l: list X) (x: X),
    rev (remove eq_dec x l) = remove eq_dec x (rev l).
Proof.
  intro l. induction l; intro. reflexivity.
  simpl. destruct (eq_dec x a). rewrite e.
  rewrite remove_app. replace (remove eq_dec a [a]) with (nil: list X).
  rewrite app_nil_r. apply IHl.
  simpl. destruct (eq_dec a a). reflexivity. contradiction.
  rewrite remove_app. replace (remove eq_dec x [a]) with [a].
  simpl. rewrite IHl. reflexivity.
  simpl. destruct (eq_dec x a). apply n in e. contradiction. reflexivity.
Qed.


Lemma nodup_remove :
  forall (l: list X) (x: X),
    nodup eq_dec (remove eq_dec x l) = remove eq_dec x (nodup eq_dec l).
Proof.
  intro l. induction l; intro. reflexivity.
  simpl. destruct (eq_dec x a). rewrite IHl.
  destruct (in_dec eq_dec a l). reflexivity.
  rewrite e. rewrite remove_cons. reflexivity.
  simpl.
  destruct (in_dec eq_dec a l).
  assert (In a (remove eq_dec x l)). apply in_in_remove.
  intro. rewrite H in n. contradiction. assumption.
  destruct (in_dec eq_dec a (remove eq_dec x l)). apply IHl.
  apply n0 in H. contradiction.
  assert (~ In a (remove eq_dec x l)). intro. apply in_remove in H. destruct H.
  apply n0 in H. contradiction.
  destruct (in_dec eq_dec a (remove eq_dec x l)). apply H in i. contradiction.
  rewrite IHl.
  simpl. destruct (eq_dec x a). apply n in e. contradiction. reflexivity.
Qed.


Theorem rev_nodup_rev_rec :
  forall (l: list X) (x: X),
    rev (nodup eq_dec (rev (x::l)))
         = x::(rev (nodup eq_dec (rev (remove eq_dec x l)))).
Proof.
  intro l. induction l; intro. reflexivity.
  simpl. destruct (eq_dec x a). rewrite <- IHl.
  replace (nodup eq_dec (rev (x::l))) with (nodup eq_dec (rev (x::a::l))).
  reflexivity. rewrite e. simpl.
  rewrite nodup_cons. rewrite nodup_cons.
  rewrite remove_app. replace (remove eq_dec a [a]) with (nil: list X).
  rewrite app_nil_r. rewrite remove_remove_eq. reflexivity.
  simpl. destruct (eq_dec a a). reflexivity. contradiction.
  rewrite nodup_cons. rewrite rev_app_distr. simpl. f_equal.
  replace (rev l ++ [a]) with (rev (a::l)).
  rewrite rev_remove.
  simpl. destruct (eq_dec x a). apply n in e. contradiction.
  rewrite rev_remove. rewrite <- rev_remove. f_equal.

  rewrite nodup_cons. rewrite remove_app. rewrite notin_remove with (l := [a]).
  rewrite nodup_cons. rewrite remove_remove_comm. f_equal. f_equal.
  symmetry. apply nodup_remove.
  intro. destruct H. rewrite H in n. contradiction. apply in_nil in H.
  assumption. reflexivity.
Qed.


Lemma remove_perm :
  forall (u v: list X) (x: X),
    Permutation u v -> Permutation (remove eq_dec x u) (remove eq_dec x v).
Proof.
  intro u. induction u; intros.
  apply Permutation_nil in H. rewrite H. apply Permutation_refl.
  assert (In a v). apply Permutation_in with (l := a::u).
  assumption. apply in_eq.
  apply in_split in H0. destruct H0. destruct H0. rewrite H0.
  simpl. destruct (eq_dec x a).
  rewrite <- e. rewrite remove_app. rewrite remove_cons.
  rewrite <- remove_app. apply IHu.
  apply Permutation_cons_inv with (a := a).
  apply Permutation_trans with (l' := v). assumption. rewrite H0.
  apply Permutation_sym. apply Permutation_middle.
  rewrite remove_app. simpl. destruct (eq_dec x a). apply n in e. contradiction.
  apply Permutation_trans with (l' := a::remove eq_dec x (x0++x1)).
  apply perm_skip. apply IHu.
  apply Permutation_cons_inv with (a := a).
  apply Permutation_trans with (l' := v). assumption. rewrite H0.
  apply Permutation_sym. apply Permutation_middle.
  rewrite remove_app. apply Permutation_middle.
Qed.


Theorem rev_nodup_rev_perm :
  forall (l: list X),
    Permutation (nodup eq_dec l) (rev (nodup eq_dec (rev l))).
Proof.
  intro l. induction l. apply Permutation_refl.

  simpl. destruct (in_dec eq_dec a l).
  replace (rev l ++ [a]) with (rev (a::l)).
  rewrite rev_nodup_rev_rec.

  rewrite <- nodup_In with (decA := eq_dec) in i.
  apply in_split in i. destruct i. destruct H. rewrite H.
  apply Permutation_trans with (l' := a::x++x0).
  apply Permutation_sym. apply Permutation_middle. apply perm_skip.
  rewrite rev_remove. rewrite nodup_remove. rewrite rev_remove.
  replace (x++x0) with (remove eq_dec a (x++a::x0)). apply remove_perm.
  rewrite <- H. assumption.
  rewrite remove_app. rewrite remove_cons. rewrite <- remove_app.
  apply notin_remove. intro. apply in_app_or in H0. destruct H0.
  apply in_split in H0. destruct H0. destruct H0. rewrite H0 in H.
  assert (NoDup (nodup eq_dec l)). apply NoDup_nodup.
  rewrite H in H1. apply NoDup_remove_2 in H1. apply H1.
  apply in_or_app. left. apply in_elt.
  apply in_split in H0. destruct H0. destruct H0. rewrite H0 in H.
  assert (NoDup (nodup eq_dec l)). apply NoDup_nodup.
  rewrite H in H1. apply NoDup_remove_2 in H1. apply H1.
  apply in_or_app. right. apply in_elt. easy.

  replace (rev l ++ [a]) with (rev (a::l)).
  rewrite rev_nodup_rev_rec. apply perm_skip.
  apply notin_remove with (eq_dec := eq_dec) in n. rewrite n.
  assumption. easy.
Qed.


Theorem rev_nodup_rev_NoDup :
  forall l, NoDup (rev (nodup eq_dec (rev l))).
Proof.
  intros. apply Permutation_NoDup with (l := nodup eq_dec l).
  apply rev_nodup_rev_perm. apply NoDup_nodup.
Qed.


Theorem nodup_missing_single_element :
  forall (v u: list X),
    NoDup u -> length u = S (length v) -> exists e, In e u /\ ~ In e v.
Proof.
  intro v. induction v; intros. destruct u. apply O_S in H0. contradiction.
  destruct u. exists x. split. apply in_eq. apply in_nil.
  simpl in H0. apply Nat.succ_inj in H0. symmetry in H0. apply O_S in H0.
  contradiction. destruct (in_dec eq_dec a u). apply in_split in i.
  destruct i. destruct H1. rewrite H1 in H0.
  rewrite length_app in H0. simpl in H0. rewrite Nat.add_succ_r in H0.
  apply Nat.succ_inj in H0. rewrite <- length_app in H0.
  assert (NoDup (x++x0)). apply NoDup_remove_1 with (a := a).
  rewrite <- H1. assumption. apply IHv in H0. destruct H0. destruct H0.
  exists x1. split. rewrite H1. apply in_app_or in H0. destruct H0;
  apply in_or_app. left. assumption. right. apply in_cons. assumption.
  apply not_in_cons. split. intro. rewrite H4 in H0.
  apply in_app_or in H0. destruct H0; apply in_split in H0;
  destruct H0; destruct H0; rewrite H0 in H1; rewrite H1 in H;
  apply NoDup_remove_2 in H; apply H; apply in_or_app; [ left | right ];
  apply in_elt. assumption. assumption.

  destruct u. apply O_S in H0. contradiction.
  destruct (in_dec eq_dec x (a::v)).
  assert (length u = S (length v)). simpl in H0. apply Nat.succ_inj in H0.
  assumption.
  apply IHv in H1. destruct H1. destruct H1.

  exists x0. split. apply in_cons. assumption.
  apply not_in_cons. split. intro. rewrite H3 in H1.
  apply in_split in H1. destruct H1. destruct H1. rewrite H1 in n.
  apply n. rewrite app_comm_cons. apply in_elt. assumption.
  apply NoDup_cons_iff in H. destruct H. assumption.

  exists x. split. apply in_eq. assumption.
Qed.


Theorem nodup_missing_elements :
  forall (u v: list X) (k: nat),
    NoDup u -> length u = k + length v
      -> exists l, length l = k /\ NoDup l
               /\ (forall e, In e l -> In e u /\ ~ In e v).
Proof.
  intro u. induction u; intros. destruct k. exists nil.
  symmetry in H0. apply length_zero_iff_nil in H0. rewrite H0.
  split. reflexivity. split. apply NoDup_nil. intros.
  split; apply in_nil in H1; contradiction.
  apply Nat.neq_0_succ in H0. contradiction.

  destruct k. exists nil. 
  split. reflexivity. split. apply NoDup_nil. intros. apply in_nil in H1.
  contradiction.

  simpl in H0. apply Nat.succ_inj in H0. assert (I := H0). apply IHu in H0.
  destruct H0. destruct H0. destruct H1.

  assert (exists e', In e' (a::u) /\ ~ In e' (x++v)).
  apply nodup_missing_single_element. assumption.
  rewrite length_app. rewrite H0. simpl. apply eq_S. assumption.

  destruct H3. destruct H3. exists (x0::x). split. simpl. apply eq_S.
  assumption. split. apply NoDup_cons. intro. apply H4.
  apply in_or_app. left. assumption. assumption.

  intros. destruct H5. rewrite H5 in H3. rewrite H5 in H4.
  split. assumption. intro. apply H4. apply in_or_app. right. assumption.
  apply H2 in H5. destruct H5. split. apply in_cons. assumption.
  assumption. apply NoDup_cons_iff in H. destruct H. assumption.
Qed.


Theorem not_NoDup_strong :
  forall (l : list X),
  ~ NoDup l ->
  exists (a : X) (l1 l2 l3 : list X), l = l1 ++ a::l2 ++ a::l3 /\ NoDup l1.
Proof.
  intro l. induction l; intro. assert False. apply H. apply NoDup_nil.
  contradiction.
  destruct (in_dec eq_dec a l). apply in_split in i.
  destruct i. destruct H0. exists a. exists nil. exists x. exists x0.
  rewrite H0. split. reflexivity. apply NoDup_nil.
  assert (~ NoDup l). intro. apply H. apply NoDup_cons; assumption.
  apply IHl in H0. destruct H0. destruct H0. destruct H0. destruct H0.
  destruct H0. exists x. exists (a::x0). exists x1. exists x2. split.
  rewrite H0. rewrite app_comm_cons. reflexivity. apply NoDup_cons.
  intro. apply n. rewrite H0. apply in_or_app. left. assumption.
  assumption.
Qed.


Theorem NoDup_remove :
  forall (l: list X) (x: X),
    NoDup l -> NoDup (remove eq_dec x l).
Proof.
  intro l. induction l; intros. apply NoDup_nil.
  simpl. apply NoDup_cons_iff in H. destruct H.
  destruct (eq_dec x a). apply IHl. assumption.
  apply NoDup_cons. intro. apply in_remove in H1. destruct H1.
  apply H in H1. assumption. apply IHl. assumption.
Qed.


(*
Fixpoint mynodup_aux (l acc: list X) :=
    match l with
    | nil => rev acc
    | hd::tl => if (ListDec.In_dec eq_dec hd acc) then mynodup_aux tl acc
                          else mynodup_aux tl (hd::acc)
    end.
Definition mynodup (l: list X) := mynodup_aux l nil.




Theorem mynodup_rec :
  forall (l w: list X) (x: X),
     mynodup_aux l (w++[x]) = x::(mynodup_aux (remove eq_dec x l) w).
Proof.
  intro l. induction l; intros. simpl. rewrite rev_app_distr. easy.
  simpl. destruct (eq_dec x a). rewrite e.
  destruct (ListDec.In_dec eq_dec a (w++[a])). rewrite IHl. reflexivity.
  assert False. apply n. apply in_or_app. right. apply in_eq.
  contradiction.
  destruct (ListDec.In_dec eq_dec a (w++[x])). rewrite IHl.
  apply in_app_or in i. destruct i.
  simpl. destruct (ListDec.In_dec eq_dec a w). reflexivity.
  apply n0 in H. contradiction. destruct H. apply n in H. contradiction.
  apply in_nil in H. contradiction.
  rewrite app_comm_cons. rewrite IHl.
  simpl. destruct (ListDec.In_dec eq_dec a w).
  assert False. apply n0. apply in_or_app. left. assumption. contradiction.
  reflexivity.
Qed.


Lemma mynodup_notin :
  forall (l: list X) (x: X), ~ In x l <-> ~ In x (mynodup l).
Proof.
  intro l. induction l; intro; split; intro.
  apply in_nil. apply in_nil.
  unfold mynodup. simpl.
  replace [a] with (nil ++ [a]). rewrite mynodup_rec.


Lemma mynodup_In :
  forall (l: list X) (x: X), In x l <-> In x (mynodup l).
Proof.
  intro l. induction l; intro; split; intro.
  apply in_nil in H. contradiction. apply in_nil in H. contradiction.
  unfold mynodup. simpl.
  replace [a] with (nil ++ [a]). rewrite mynodup_rec.
  destruct H. rewrite H.
  apply in_eq. destruct (eq_dec a x). rewrite e. apply in_eq. apply in_cons.



Lemma mynodup_nodup :
  forall (l: list X), NoDup (mynodup l).
Proof.
  intro l. induction l. apply NoDup_nil.
  unfold mynodup. simpl.
  replace [a] with (nil ++ [a]). rewrite mynodup_rec.
  apply NoDup_cons.




 *)









End Utilities_dec.
