(** * Notebook 6
 *)


Require Import Nat.
Require Import PeanoNat.
Require Import List.

Import ListNotations.

Require Import Sorting.Permutation.
Require Import Arith.Factorial.

Require Import subsequence.


Set Implicit Arguments.

Section Notebook_dec.

Variable X: Type.
Hypothesis eq_dec : forall x y: X, {x = y} + {x <> y}.


Fixpoint extractperm_aux (word: list X) (level: nat) (forbid forbid2: list X):=
  if Compare_dec.zerop level then [ nil ]
  else match word with
  | nil    => nil
  | hd::tl => ( if (orb (if (in_dec eq_dec) hd forbid then true else false)
                        (if (in_dec eq_dec) hd forbid2 then true else false))
                then nil
                else (map (cons hd) (extractperm_aux tl (pred level)
                                             (hd::forbid) nil)))
                 ++ extractperm_aux tl level forbid (hd::forbid2)
  end.

Definition extractperm (word: list X) :=
  extractperm_aux word (length (nodup eq_dec word)) nil nil.

(*
Compute extractperm Nat.eq_dec [0;1;2;3;0;1;2;3;0;1;2;3;0] 4 nil nil.
     = [[0; 1; 2; 3]; [0; 1; 3; 2]; [0; 2; 3; 1]; [
        0; 2; 1; 3]; [0; 3; 1; 2]; [0; 3; 2; 1]; [
        1; 2; 3; 0]; [1; 2; 0; 3]; [1; 3; 0; 2]; [
        1; 3; 2; 0]; [1; 0; 2; 3]; [1; 0; 3; 2]; [
        2; 3; 0; 1]; [2; 3; 1; 0]; [2; 0; 1; 3]; [
        2; 0; 3; 1]; [2; 1; 3; 0]; [2; 1; 0; 3]; [
        3; 0; 1; 2]; [3; 0; 2; 1]; [3; 1; 2; 0]; [
        3; 1; 0; 2]; [3; 2; 0; 1]]
     : list (list nat)
CL-USER> (all-permutations '(0 1 2 3))
((0 1 2 3) (0 1 3 2) (0 2 3 1) (0 2 1 3) (0 3 1 2) (0 3 2 1) (1 2 3 0)
 (1 2 0 3) (1 3 0 2) (1 3 2 0) (1 0 2 3) (1 0 3 2) (2 3 0 1) (2 3 1 0)
 (2 0 1 3) (2 0 3 1) (2 1 3 0) (2 1 0 3) (3 0 1 2) (3 0 2 1) (3 1 2 0)
 (3 1 0 2) (3 2 0 1) (3 2 1 0))
 *)


Theorem extractperm_empty_word :
  forall (u v w: list X), extractperm_aux u 0 v w = [ nil ].
Proof.
  intros. destruct u; simpl; reflexivity.
Qed.


Theorem extractperm_forbid2_set :
  forall (u v w1 w2: list X) (k: nat),
    incl w1 w2 -> incl w2 w1 -> extractperm_aux u k v w1 = extractperm_aux u k v w2.
Proof.
  intro u. induction u; intros.
  reflexivity.
  simpl. destruct (Compare_dec.zerop k). reflexivity.
  destruct (in_dec eq_dec a v). repeat rewrite Bool.orb_true_l.
  apply IHu; repeat intro; destruct H1. rewrite H1. apply in_eq.
  apply in_cons. apply H. assumption. rewrite H1. apply in_eq.
  apply in_cons. apply H0. assumption.
  repeat rewrite Bool.orb_false_l.
  destruct (in_dec eq_dec a w1); destruct (in_dec eq_dec a w2).
  apply IHu; repeat intro; destruct H1. rewrite H1. apply in_eq.
  apply in_cons. apply H. assumption. rewrite H1. apply in_eq.
  apply in_cons. apply H0. assumption.
  apply H in i. apply n0 in i. contradiction.
  apply H0 in i. apply n0 in i. contradiction.
  apply app_inv_head_iff.
  apply IHu; repeat intro; destruct H1. rewrite H1. apply in_eq.
  apply in_cons. apply H. assumption. rewrite H1. apply in_eq.
  apply in_cons. apply H0. assumption.
Qed.


Theorem extractperm_forbid_set :
  forall (u v1 v2 w: list X) (k: nat),
    incl v1 v2 -> incl v2 v1 -> extractperm_aux u k v1 w = extractperm_aux u k v2 w.
Proof.
  intro u. induction u; intros.
  reflexivity.
  simpl. destruct (Compare_dec.zerop k). reflexivity.
  destruct (in_dec eq_dec a w). repeat rewrite Bool.orb_true_r.
  apply IHu; repeat intro; [ apply H | apply H0]; assumption.
  destruct (in_dec eq_dec a v1); destruct (in_dec eq_dec a v2).
  apply IHu; repeat intro; [ apply H | apply H0]; assumption.
  apply H in i. apply n0 in i. contradiction.
  apply H0 in i. apply n0 in i. contradiction.
  rewrite IHu with (v1 := v1) (v2 := v2).
  apply app_inv_tail_iff. simpl. rewrite IHu with (v2 := a::v2). reflexivity.
  repeat intro. destruct H1. rewrite H1. apply in_eq. apply in_cons. apply H.
  assumption.
  repeat intro. destruct H1. rewrite H1. apply in_eq. apply in_cons. apply H0.
  assumption. assumption. assumption.
Qed. 


Theorem extractperm_forbid2_dup :
  forall (u v w: list X) (a: X) (k: nat),
    In a w -> extractperm_aux u k v (a::w) = extractperm_aux u k v w.
Proof.
  intros. apply extractperm_forbid2_set.
  repeat intro. destruct H0. rewrite <- H0. assumption. assumption.
  repeat intro. apply in_cons. assumption.
Qed. 


Theorem extractperm_forbid_dup :
  forall (u v w: list X) (a: X) (k: nat),
    In a v -> extractperm_aux u k (a::v) w = extractperm_aux u k v w.
Proof.
  intros. apply extractperm_forbid_set.
  repeat intro. destruct H0. rewrite <- H0. assumption. assumption.
  repeat intro. apply in_cons. assumption.
Qed. 


Theorem extractperm_forbid2 :
  forall (u v w: list X) (a: X) (k: nat),
    In a w -> extractperm_aux (a::u) k v w = extractperm_aux u k v w.
Proof.
  intro u. induction u; intros.
  simpl. destruct (Compare_dec.zerop k). reflexivity.
  destruct (in_dec eq_dec a w). rewrite Bool.orb_true_r. reflexivity.
  apply n in H. contradiction.
  simpl. destruct (Compare_dec.zerop k). reflexivity.
  destruct (in_dec eq_dec a0 w). rewrite Bool.orb_true_r.
  destruct (eq_dec a0 a). rewrite Bool.orb_true_r.
  destruct (in_dec eq_dec a w). rewrite Bool.orb_true_r. rewrite e.
  apply extractperm_forbid2_dup. apply in_eq. rewrite e in i. apply n in i.
  contradiction.
  destruct (in_dec eq_dec a v). repeat rewrite Bool.orb_true_l.
  apply extractperm_forbid2_set. repeat intro. destruct H0. rewrite H0. apply in_eq.
  destruct H0. rewrite <- H0. apply in_cons. assumption. apply in_cons. assumption.
  repeat intro. destruct H0. rewrite H0. apply in_eq.
  repeat apply in_cons. assumption. repeat rewrite Bool.orb_false_l.
  destruct (in_dec eq_dec a w).
  apply extractperm_forbid2_set. repeat intro. destruct H0. rewrite H0. apply in_eq.
  destruct H0. rewrite <- H0. apply in_cons. assumption. apply in_cons. assumption.
  repeat intro. destruct H0. rewrite H0. apply in_eq.
  repeat apply in_cons. assumption. simpl. apply app_inv_head_iff.
  apply extractperm_forbid2_set. repeat intro. destruct H0. rewrite H0. apply in_eq.
  destruct H0. rewrite <- H0. apply in_cons. assumption. apply in_cons. assumption.
  repeat intro. destruct H0. rewrite H0. apply in_eq.
  repeat apply in_cons. assumption. apply n in H. contradiction.
Qed.


Theorem extractperm_forbid2_useless_element :
  forall (u v w: list X) (a: X) (k: nat),
    In a v -> extractperm_aux u k v (a::w) = extractperm_aux u k v w.
Proof.
  intro u. induction u; intros. reflexivity.
  simpl. destruct (Compare_dec.zerop k). reflexivity.
  destruct (eq_dec a0 a). rewrite Bool.orb_true_r. rewrite <- e.
  destruct (in_dec eq_dec a0 v). rewrite Bool.orb_true_l.
  apply extractperm_forbid2_dup. apply in_eq. apply n in H. contradiction.
  destruct (in_dec eq_dec a v). repeat rewrite Bool.orb_true_l.
  replace (extractperm_aux u k v (a :: a0 :: w))
            with (extractperm_aux u k v (a0 :: a :: w)).
  apply IHu. assumption.
  apply extractperm_forbid2_set. repeat intro. destruct H0.
  apply in_cons. rewrite H0. apply in_eq. destruct H0. rewrite H0. apply in_eq.
  repeat apply in_cons. assumption.
  repeat intro. destruct H0.
  apply in_cons. rewrite H0. apply in_eq. destruct H0. rewrite H0. apply in_eq.
  repeat apply in_cons. assumption.
  repeat rewrite Bool.orb_false_l. destruct (in_dec eq_dec a w).
  replace (extractperm_aux u k v (a :: a0 :: w))
            with (extractperm_aux u k v (a0 :: a :: w)).
  apply IHu. assumption.
  apply extractperm_forbid2_set. repeat intro. destruct H0.
  apply in_cons. rewrite H0. apply in_eq. destruct H0. rewrite H0. apply in_eq.
  repeat apply in_cons. assumption.
  repeat intro. destruct H0.
  apply in_cons. rewrite H0. apply in_eq. destruct H0. rewrite H0. apply in_eq.
  repeat apply in_cons. assumption.
  apply app_inv_head_iff.
  replace (extractperm_aux u k v (a :: a0 :: w))
            with (extractperm_aux u k v (a0 :: a :: w)).
  apply IHu. assumption.
  apply extractperm_forbid2_set. repeat intro. destruct H0.
  apply in_cons. rewrite H0. apply in_eq. destruct H0. rewrite H0. apply in_eq.
  repeat apply in_cons. assumption.
  repeat intro. destruct H0.
  apply in_cons. rewrite H0. apply in_eq. destruct H0. rewrite H0. apply in_eq.
  repeat apply in_cons. assumption.
Qed.


Theorem extractperm_forbid :
  forall (u v w: list X) (a: X) (k: nat),
    In a v -> extractperm_aux (a::u) k v w = extractperm_aux u k v w.
Proof.
  intro u. induction u; intros.
  simpl. destruct (Compare_dec.zerop k). reflexivity.
  destruct (in_dec eq_dec a v). rewrite Bool.orb_true_l. reflexivity.
  apply n in H. contradiction.
  simpl. destruct (Compare_dec.zerop k). reflexivity.
  destruct (in_dec eq_dec a0 v). rewrite Bool.orb_true_l.
  destruct (eq_dec a0 a). rewrite Bool.orb_true_r.
  destruct (in_dec eq_dec a v). rewrite Bool.orb_true_l. rewrite e.
  apply extractperm_forbid2_dup. apply in_eq. rewrite e in i. apply n in i.
  contradiction.
  destruct (in_dec eq_dec a v). repeat rewrite Bool.orb_true_l.
  replace (extractperm_aux u k v (a :: a0 :: w))
            with (extractperm_aux u k v (a0 :: a :: w)).
  apply extractperm_forbid2_useless_element. assumption.
  apply extractperm_forbid2_set.
  repeat intro. destruct H0. rewrite H0. apply in_cons. apply in_eq.
  destruct H0. rewrite H0. apply in_eq. repeat apply in_cons. assumption.
  repeat intro. destruct H0. rewrite H0. apply in_cons. apply in_eq.
  destruct H0. rewrite H0. apply in_eq. repeat apply in_cons. assumption.
  repeat rewrite Bool.orb_false_l.
  destruct (in_dec eq_dec a w).
  replace (extractperm_aux u k v (a :: a0 :: w))
            with (extractperm_aux u k v (a0 :: a :: w)).
  apply extractperm_forbid2_useless_element. assumption.
  apply extractperm_forbid2_set.
  repeat intro. destruct H0. rewrite H0. apply in_cons. apply in_eq.
  destruct H0. rewrite H0. apply in_eq. repeat apply in_cons. assumption.
  repeat intro. destruct H0. rewrite H0. apply in_cons. apply in_eq.
  destruct H0. rewrite H0. apply in_eq. repeat apply in_cons. assumption.
  simpl. apply app_inv_head_iff.
  replace (extractperm_aux u k v (a :: a0 :: w))
            with (extractperm_aux u k v (a0 :: a :: w)).
  apply extractperm_forbid2_useless_element. assumption.
  apply extractperm_forbid2_set.
  repeat intro. destruct H0. rewrite H0. apply in_cons. apply in_eq.
  destruct H0. rewrite H0. apply in_eq. repeat apply in_cons. assumption.
  repeat intro. destruct H0. rewrite H0. apply in_cons. apply in_eq.
  destruct H0. rewrite H0. apply in_eq. repeat apply in_cons. assumption.
  apply n in H. contradiction.
Qed.


Theorem extractperm_forbid_remove :
  forall (u v w: list X) (a: X) (k: nat),
    In a v -> extractperm_aux u k v w = extractperm_aux (remove eq_dec a u) k v w.
Proof.
  intro u. induction u; intros. reflexivity.
  simpl. destruct (Compare_dec.zerop k). rewrite e.
  rewrite extractperm_empty_word. reflexivity.
  destruct (in_dec eq_dec a v). rewrite Bool.orb_true_l.
  rewrite extractperm_forbid2_useless_element.
  rewrite IHu with (a := a0).
  destruct (eq_dec a0 a). reflexivity. rewrite extractperm_forbid. reflexivity.
  assumption. assumption. assumption. rewrite Bool.orb_false_l.
  destruct (in_dec eq_dec a w).
  destruct (eq_dec a0 a). rewrite e in H. apply n in H. contradiction.
  rewrite extractperm_forbid2. rewrite <- IHu.
  apply extractperm_forbid2_set. repeat intro. destruct H0. rewrite <- H0.
  assumption. assumption. repeat intro. apply in_cons. assumption.
  assumption. assumption.
  destruct (eq_dec a0 a). rewrite e in H. apply n in H. contradiction.
  simpl. destruct (Compare_dec.zerop k). rewrite e in l.
  apply Nat.lt_irrefl in l. contradiction.
  destruct (in_dec eq_dec a v). apply n in i. contradiction.
  destruct (in_dec eq_dec a w). apply n0 in i. contradiction. simpl.
  repeat rewrite <- IHu. reflexivity. assumption. apply in_cons. assumption.
Qed.









Theorem extractperm_aux_remove :
  forall (u: list X) (x: X) (k: nat),
    extractperm_aux u k [ x ] nil = extractperm_aux (remove eq_dec x u) k 
Proof.





Theorem extractperm_aux_remove :
  forall (u: list X) (x: X),
    In x u -> extractperm_aux u (pred (length (nodup eq_dec u))) [ x ] nil
      = extractperm (remove eq_dec x u).
Proof.
  intro u. induction u; intros. apply in_nil in H. contradiction.
  destruct H. rewrite H. rewrite remove_cons.
  destruct (in_dec eq_dec x u). rewrite <- IHu.
  unfold nodup.
  destruct (in_dec eq_dec x u). rewrite <- IHu.



Theorem extractperm_nodup : forall (u: list X), NoDup (extractperm u).
Proof.
  intro. induction u. apply NoDup_nil.
  unfold extractperm. unfold extractperm_aux.
  destruct (Compare_dec.zerop (length (nodup eq_dec (a::u)))).
  apply NoDup_cons. apply in_nil. apply NoDup_nil.
  simpl.

 *)

End Notebook_dec.
