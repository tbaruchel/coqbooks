# Generate HTMLs from the Coq source files

source := ./
output := ./

# All Coq files in src/ are considered sources
sources := $(wildcard $(source)/*.v)
objects := $(patsubst %.v,%.vo,$(subst $(source),$(output),$(sources)))


# Remove 'scratch' files
objects := $(filter-out $(source)/scratch%,$(objects))

all: $(objects)

exists_set.vo: subsequence.vo
count.vo: exists_set.vo subsequence.vo
scs.vo: subsequence.vo exists_set.vo utilities.vo
swap.vo: subsequence.vo count.vo scs.vo
resilient.vo: subsequence.vo count.vo scs.vo swap.vo
notebook.vo: subsequence.vo count.vo scs.vo swap.vo resilient.vo utilities.vo exists_set.vo
notebook2.vo: subsequence.vo count.vo scs.vo swap.vo resilient.vo notebook.vo


# TODO: supprimer resilient




# Recipe for converting a Coq file into HTML using coqdoc
$(output)/%.vo: $(source)/%.v
	cd $(source); coqc `basename $<`

.PHONY : clean

clean:
	rm -f $(output)/*.vo $(output)/*.vok $(output)/*.vos $(output)/*.glob
